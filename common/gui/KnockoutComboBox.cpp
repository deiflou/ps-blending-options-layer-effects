/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "KnockoutComboBox.h"

KnockoutComboBox::KnockoutComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("None", static_cast<int>(LayerStyles::Knockout_None));
    addItem("Shallow", static_cast<int>(LayerStyles::Knockout_Shallow));
    addItem("Deep", static_cast<int>(LayerStyles::Knockout_Deep));

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentKnockoutChanged(static_cast<LayerStyles::Knockout>(currentData().toInt()));
        }
    );
}
KnockoutComboBox::~KnockoutComboBox()
{}

LayerStyles::Knockout KnockoutComboBox::currentKnockout() const
{
    return static_cast<LayerStyles::Knockout>(currentData().toInt());
}

void KnockoutComboBox::setCurrentKnockout(LayerStyles::Knockout newKnockout)
{
    setCurrentIndex(findData(static_cast<int>(newKnockout)));
}
