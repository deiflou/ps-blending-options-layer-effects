/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef INNERGLOWOPTIONSWIDGET_H
#define INNERGLOWOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT InnerGlowOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InnerGlowOptionsWidget(QWidget *parent = nullptr);
    ~InnerGlowOptionsWidget() override;

    LayerStyles::InnerGlow params() const;

public Q_SLOTS:
    void setGradient(const QGradientStops &gradient);

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
