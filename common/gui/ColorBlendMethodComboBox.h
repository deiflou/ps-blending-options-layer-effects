/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef COLORBLENDMETHODCOMBOBOX_H
#define COLORBLENDMETHODCOMBOBOX_H

#include <QComboBox>

#include <PixelBlending.h>

#include <Export.h>

class COMMON_EXPORT ColorBlendMethodComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit ColorBlendMethodComboBox(QWidget *parent = nullptr);
    ~ColorBlendMethodComboBox() override;

    PixelBlending::ColorBlendMethod currentColorBlendMethod() const;

public Q_SLOTS:
    void setCurrentColorBlendMethod(PixelBlending::ColorBlendMethod newColorBlendMethod);

Q_SIGNALS:
    void currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod newColorBlendMethod);
};

#endif
