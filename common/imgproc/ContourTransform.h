/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef CONTOURTRANSFORM_H
#define CONTOURTRANSFORM_H

#include <QImage>
#include <QRandomGenerator>
#include <QGradientStops>
#include <QColor>

#include <Curve.h>
#include <Export.h>

namespace ContourTransform
{

QImage COMMON_EXPORT apply(const QImage &mask,
                           bool invert,
                           qint32 range,
                           bool reverseRange,
                           const Curve &curve,
                           bool antiAliasedCurve,
                           qint32 noise,
                           QRandomGenerator &rng,
                           bool applyFade);

QImage COMMON_EXPORT applyCurve(const QImage &mask,
                                const Curve &curve,
                                bool antiAliasedCurve);

QImage COMMON_EXPORT applyRangeAndCurve(const QImage &mask,
                                        qint32 range,
                                        bool reverseRange,
                                        const Curve &curve,
                                        bool antiAliasedCurve);

QImage COMMON_EXPORT applyAndFillWithColor(const QImage &mask,
                                           bool invert,
                                           qint32 range,
                                           bool reverseRange,
                                           const Curve &curve,
                                           bool antiAliasedCurve,
                                           qint32 noise,
                                           QRandomGenerator &rng,
                                           bool applyFade,
                                           const QColor &color);

QImage COMMON_EXPORT applyAndFillWithGradient(const QImage &mask,
                                              bool invert,
                                              qint32 range,
                                              bool reverseRange,
                                              const Curve &curve,
                                              bool antiAliasedCurve,
                                              qint32 gradientJitter,
                                              qint32 noise,
                                              QRandomGenerator &rng,
                                              bool applyFade,
                                              const QGradientStops &gradient);

}

#endif
