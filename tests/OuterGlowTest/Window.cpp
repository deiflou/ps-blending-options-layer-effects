/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QPainter>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QCheckBox>
#include <QDebug>

#include <Viewport.h>
#include <OuterGlowOptionsWidget.h>
#include <BlendingOptionsWidget.h>
#include <ImgProcUtil.h>
#include <LayerStyles.h>
#include <PixelBlending.h>

#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    OuterGlowOptionsWidget *outerGlowOptionsWidget {nullptr};
    BlendingOptionsWidget *blendingOptionsWidget {nullptr};
    QCheckBox *checkBoxBackground {nullptr};
    QCheckBox *checkBoxBlurredBackdrop {nullptr};
    QCheckBox *checkBoxUseMask {nullptr};
    QImage background;
    QImage bg2;
    QImage bg2Blurred;
    QImage currentLayer;
    QImage mask;

    QImage generateImage() const
    {
        using namespace PixelBlending;
        using namespace LayerStyles;
        using namespace LayerStyles;

        const OuterGlow outerGlowOptions = outerGlowOptionsWidget->params();
        const BlendingOptions blendingOptions = blendingOptionsWidget->params();

        const bool backgroundLayerIsVisible = checkBoxBackground->isChecked();
        const bool blurredBackdrop = checkBoxBlurredBackdrop->isChecked();
        const bool useMask = checkBoxUseMask->isChecked();

        // The mask representing the shape of the current layer
        QImage currentLayerShape;
        if (blendingOptions.transparencyShapesLayer) {
            currentLayerShape = currentLayer.convertToFormat(QImage::Format_Alpha8);
        } else {
            currentLayerShape = QImage(currentLayer.size(), QImage::Format_Alpha8);
            currentLayerShape.fill(255);
        }
        if (!blendingOptions.layerMaskHidesEffects && useMask) {
            multiplyMasks(currentLayerShape, mask);
        }
        const quint8 currentLayerOpacity = static_cast<quint8>(blendingOptions.opacity * 255 / 100.0);
        const quint8 currentLayerFillOpacity = static_cast<quint8>(blendingOptions.fillOpacity * 255 / 100.0);
        // The implicit layer that contains the computed outer glow
        const QImage outerGlowImplicitLayer = generateOuterGlowImage(outerGlowOptions, currentLayerShape);
        const quint8 outerGlowOpacity = static_cast<quint8>(outerGlowOptions.opacity * 255 / 100.0);
        
        // The fully opaque, non-knockable background layer
        const QImage backgroundLayer = backgroundLayerIsVisible ? background : QImage();
        // The projection up to the current layer
        QImage projection;
        if (backgroundLayerIsVisible) {
            projection = background;
            blend(blurredBackdrop ? bg2Blurred : bg2, projection);
        } else {
            projection = blurredBackdrop ? bg2Blurred : bg2;
        }
        // The helper images that contain the composited result and the
        // knocked out region respectively
        QImage composition = projection;
        QImage knockoutComposition;
        // Generate the current layer composition        
        if (blendingOptions.knockout == Knockout_None) {
            // Use the overall projection as backdrop for the knockout
            // of the implicit effect layers
            knockoutComposition = projection;
        } else {
            // Use the background layer or a transparent image as backdrop
            // for the knockout
            if (backgroundLayerIsVisible) {
                knockoutComposition = background;
            } else {
                knockoutComposition = QImage(currentLayer.size(), QImage::Format_ARGB32);
                knockoutComposition.fill(0);
            }
        }
        blend(outerGlowImplicitLayer, composition,
              SourceOverDestination, outerGlowOptions.blendMode, true, outerGlowOpacity);
        // Paint the layer
        blend(currentLayer, knockoutComposition,
              SourceOverDestination, blendingOptions.blendMode, true,
              currentLayerFillOpacity, blendingOptions.transparencyShapesLayer,
              blendingOptions.sourceTonalRanges);
        // Mix the composition with the knockout interior.
        // This is basically the same as using the following Porter-Duff operators:
        // (composition OUT currentLayer) PLUS (layerComposition IN currentLayer)
        crossDissolve(knockoutComposition, composition, currentLayerShape);
        // Blend the composition with the overall projection
        blendWithBackdrop(composition, projection,
                          blendingOptions.layerMaskHidesEffects && useMask ? mask : QImage(),
                          blendingOptions.channels, currentLayerOpacity,
                          blendingOptions.destinationTonalRanges);

        return projection;
    }
};

Window::Window()
    : m_d(new Private)
{
    m_d->background = QImage(":/background").convertToFormat(QImage::Format_ARGB32);
    m_d->bg2 = QImage(":/bg_2").convertToFormat(QImage::Format_ARGB32);
    m_d->bg2Blurred = QImage(":/bg_2_blurred").convertToFormat(QImage::Format_ARGB32);
    m_d->currentLayer = QImage(":/current_layer").convertToFormat(QImage::Format_ARGB32);
    m_d->mask = ImgProcUtil::convertImageToMask(QImage(":/mask").convertToFormat(QImage::Format_ARGB32));

    m_d->outerGlowOptionsWidget = new OuterGlowOptionsWidget;
    m_d->outerGlowOptionsWidget->setContentsMargins(10, 10, 10, 10);
    m_d->outerGlowOptionsWidget->setGradient(
        {
            {0.000, Qt::red},
            {0.167, Qt::magenta},
            {0.333, Qt::blue},
            {0.500, Qt::cyan},
            {0.666, Qt::green},
            {0.834, Qt::yellow},
            {1.000, Qt::red}

            // {0.0, QColor(255, 192, 0, 0)},
            // {1.0, QColor(255, 192, 0, 255)}
        }
    );
    m_d->blendingOptionsWidget = new BlendingOptionsWidget;
    m_d->blendingOptionsWidget->setContentsMargins(10, 10, 10, 10);

    m_d->checkBoxBackground = new QCheckBox;
    m_d->checkBoxBackground->setChecked(true);
    m_d->checkBoxBlurredBackdrop = new QCheckBox;
    m_d->checkBoxUseMask = new QCheckBox;

    QWidget *containerOtherOptions = new QWidget;

    QFormLayout *layoutOtherOptions1 = new QFormLayout;
    layoutOtherOptions1->setContentsMargins(10, 10, 10, 10);
    layoutOtherOptions1->setSpacing(5);
    layoutOtherOptions1->addRow("Show background:", m_d->checkBoxBackground);
    layoutOtherOptions1->addRow("Blurred backdrop:", m_d->checkBoxBlurredBackdrop);
    layoutOtherOptions1->addRow("Use mask:", m_d->checkBoxUseMask);

    QVBoxLayout *layoutOtherOptions2 = new QVBoxLayout;
    layoutOtherOptions2->setContentsMargins(0, 0, 0, 0);
    layoutOtherOptions2->setSpacing(0);
    layoutOtherOptions2->addLayout(layoutOtherOptions1);
    layoutOtherOptions2->addStretch();
    containerOtherOptions->setLayout(layoutOtherOptions2);

    addControlsPanel(m_d->outerGlowOptionsWidget, "Outer Glow");
    addControlsPanel(m_d->blendingOptionsWidget, "Blending");
    addControlsPanel(containerOtherOptions, "Other");

    viewport()->setScaleCheckerBoardPattern(true);
    viewport()->setImage(m_d->generateImage());

    connect(m_d->outerGlowOptionsWidget, SIGNAL(paramsChanged()),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->blendingOptionsWidget, SIGNAL(paramsChanged()),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxBackground, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxBlurredBackdrop, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxUseMask, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );
}

Window::~Window()
{}
