/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef IMGPROCUTIL_H
#define IMGPROCUTIL_H

#include <QImage>
#include <QVector>
#include <QRect>
#include <QPoint>
#include <QGradientStops>
#include <QtConcurrent>

#include "ImgProcCommon.h"
#include <Export.h>

namespace ImgProcUtil
{

inline QVector<QRect> processingRects(const QRect &fullRect)
{
    const qint32 numberOfRects = QThread::idealThreadCount();
    const qint32 rectSize = fullRect.height() / numberOfRects;
    QVector<QRect> rects;
    for (qint32 i = fullRect.top(); i <= fullRect.bottom(); i += rectSize) {
        rects << QRect(fullRect.left(), i, fullRect.width(), rectSize).intersected(fullRect);
    }
    return rects;
}

template <typename Visitor>
inline void visitPixels(QImage &image, Visitor visitor)
{
    const qint32 bpp = image.pixelFormat().bitsPerPixel() / 8;
    QVector<QRect> rects = processingRects(image.rect());
    image.scanLine(0);
    QtConcurrent::blockingMap(
        rects,
        [bpp, &image, visitor](const QRect &r)
        {
            for (qint32 y = r.top(); y <= r.bottom(); ++y) {
                quint8 *pixel = image.scanLine(y);
                for (qint32 x = 0; x < image.width(); ++x, pixel += bpp) {
                    visitor(pixel);
                }
            }
            return r;
        }
    );
}

template <typename Visitor>
inline void visitPixels(QImage &image1, const QImage &image2, Visitor visitor)
{
    const qint32 image1Bpp = image1.pixelFormat().bitsPerPixel() / 8;
    const qint32 image2Bpp = image2.pixelFormat().bitsPerPixel() / 8;
    QVector<QRect> rects = processingRects(image1.rect());
    image1.scanLine(0);
    QtConcurrent::blockingMap(
        rects,
        [image1Bpp, image2Bpp, &image1, &image2, visitor](const QRect &r)
        {
            for (qint32 y = r.top(); y <= r.bottom(); ++y) {
                quint8 *pixel1 = image1.scanLine(y);
                const quint8 *pixel2 = image2.constScanLine(y);
                for (qint32 x = 0; x < image1.width(); ++x, pixel1 += image1Bpp, pixel2 += image2Bpp) {
                    visitor(pixel1, pixel2);
                }
            }
            return r;
        }
    );
}

template <typename Visitor>
inline void visitPixels(QImage &image1, const QImage &image2, const QImage &image3, Visitor visitor)
{
    const qint32 image1Bpp = image1.pixelFormat().bitsPerPixel() / 8;
    const qint32 image2Bpp = image2.pixelFormat().bitsPerPixel() / 8;
    const qint32 image3Bpp = image3.pixelFormat().bitsPerPixel() / 8;
    QVector<QRect> rects = processingRects(image1.rect());
    image1.scanLine(0);
    QtConcurrent::blockingMap(
        rects,
        [image1Bpp, image2Bpp, image3Bpp, &image1, &image2, &image3, visitor](const QRect &r)
        {
            for (qint32 y = r.top(); y <= r.bottom(); ++y) {
                quint8 *pixel1 = image1.scanLine(y);
                const quint8 *pixel2 = image2.constScanLine(y);
                const quint8 *pixel3 = image3.constScanLine(y);
                for (qint32 x = 0; x < image1.width(); ++x, pixel1 += image1Bpp, pixel2 += image2Bpp, pixel3 += image3Bpp) {
                    visitor(pixel1, pixel2, pixel3);
                }
            }
            return r;
        }
    );
}

template <typename Visitor>
inline void visitPixelsWithBottomRightNeighbors(QImage &image1, const QImage &image2, Visitor visitor)
{
    const qint32 image1Bpp = image1.pixelFormat().bitsPerPixel() / 8;
    const qint32 image2Bpp = image2.pixelFormat().bitsPerPixel() / 8;
    QVector<QRect> rects = processingRects(image1.rect().adjusted(0, 0, -1, -1));
    image1.scanLine(0);
    QtConcurrent::blockingMap(
        rects,
        [image1Bpp, image2Bpp, &image1, &image2, visitor](const QRect &r)
        {
            for (qint32 y = r.top(); y <= r.bottom(); ++y) {
                quint8 *pixel1 = image1.scanLine(y);
                const quint8 *pixel2 = image2.constScanLine(y);
                for (qint32 x = 0; x < image1.width() - 1; ++x, pixel1 += image1Bpp, pixel2 += image2Bpp) {
                    visitor(pixel1, pixel2, pixel2 + image2Bpp, pixel2 + image2.bytesPerLine());
                }
            }
        }
    );
    // Last row
    {
        quint8 *pixel1 = image1.scanLine(image1.height() - 1);
        const quint8 *pixel2 = image2.constScanLine(image2.height() - 1);
        for (qint32 x = 0; x < image1.width() - 1; ++x, pixel1 += image1Bpp, pixel2 += image2Bpp) {
            visitor(pixel1, pixel2, pixel2 + image2Bpp, pixel2);
        }
    }
    // Last column
    {
        quint8 *pixel1 = image1.scanLine(0) + image1Bpp * (image1.width() - 1);
        const quint8 *pixel2 = image2.constScanLine(0) + image2Bpp * (image2.width() - 1);
        for (qint32 y = 0; y < image1.height() - 1; ++y, pixel1 += image1.bytesPerLine(), pixel2 += image2.bytesPerLine()) {
            visitor(pixel1, pixel2, pixel2, pixel2 + image2.bytesPerLine());
        }
    }
    // Last pixel
    {
        quint8 *pixel1 = image1.scanLine(image1.height() - 1) + image1Bpp * (image1.width() - 1);
        const quint8 *pixel2 = image2.constScanLine(image2.height() - 1) + image2Bpp * (image2.width() - 1);
        visitor(pixel1, pixel2, pixel2, pixel2);
    }
}

template <typename Visitor>
inline void visitPixelsWithEightConnectedNeighbors(QImage &image1, const QImage &image2, Visitor visitor)
{
    const qint32 image1Bpp = image1.pixelFormat().bitsPerPixel() / 8;
    const qint32 image2Bpp = image2.pixelFormat().bitsPerPixel() / 8;
    QVector<QRect> rects = processingRects(image1.rect().adjusted(1, 1, -1, -1));
    image1.scanLine(0);
    QtConcurrent::blockingMap(
        rects,
        [image1Bpp, image2Bpp, &image1, &image2, visitor](const QRect &r)
        {
            for (qint32 y = r.top(); y <= r.bottom(); ++y) {
                quint8 *pixel1 = image1.scanLine(y) + image1Bpp;
                const quint8 *pixel2 = image2.constScanLine(y) + image2Bpp;
                for (qint32 x = 1; x < image1.width() - 1; ++x, pixel1 += image1Bpp, pixel2 += image2Bpp) {
                    visitor(pixel1, pixel2, 
                            pixel2 - image2.bytesPerLine() - image2Bpp,
                            pixel2 - image2.bytesPerLine(),
                            pixel2 - image2.bytesPerLine() + image2Bpp,
                            pixel2 - image2Bpp,
                            pixel2 + image2Bpp,
                            pixel2 + image2.bytesPerLine() - image2Bpp,
                            pixel2 + image2.bytesPerLine(),
                            pixel2 + image2.bytesPerLine() + image2Bpp);
                }
            }
        }
    );
    const quint32 nullValue = 0;
    const quint8 *nullValuePtr = reinterpret_cast<const quint8*>(&nullValue);
    // First row
    {
        quint8 *pixel1 = image1.bits() + image1Bpp;
        const quint8 *pixel2 = image2.constBits() + image2Bpp;
        for (qint32 x = 1; x < image1.width() - 1; ++x, pixel1 += image1Bpp, pixel2 += image2Bpp) {
            visitor(pixel1, pixel2, 
                    nullValuePtr, nullValuePtr, nullValuePtr,
                    pixel2 - image2Bpp,
                    pixel2 + image2Bpp,
                    pixel2 + image2.bytesPerLine() - image2Bpp,
                    pixel2 + image2.bytesPerLine(),
                    pixel2 + image2.bytesPerLine() + image2Bpp);
        }
    }
    // Last row
    {
        quint8 *pixel1 = image1.scanLine(image1.height() - 1) + image1Bpp;
        const quint8 *pixel2 = image2.constScanLine(image2.height() - 1) + image2Bpp;
        for (qint32 x = 1; x < image1.width() - 1; ++x, pixel1 += image1Bpp, pixel2 += image2Bpp) {
            visitor(pixel1, pixel2, 
                    pixel2 - image2.bytesPerLine() - image2Bpp,
                    pixel2 - image2.bytesPerLine(),
                    pixel2 - image2.bytesPerLine() + image2Bpp,
                    pixel2 - image2Bpp,
                    pixel2 + image2Bpp,
                    nullValuePtr, nullValuePtr, nullValuePtr);
        }
    }
    // First column
    {
        quint8 *pixel1 = image1.scanLine(1);
        const quint8 *pixel2 = image2.constScanLine(1);
        for (qint32 y = 1; y < image1.height() - 1; ++y, pixel1 += image1.bytesPerLine(), pixel2 += image2.bytesPerLine()) {
            visitor(pixel1, pixel2, 
                    nullValuePtr,
                    pixel2 - image2.bytesPerLine(),
                    pixel2 - image2.bytesPerLine() + image2Bpp,
                    nullValuePtr,
                    pixel2 + image2Bpp,
                    nullValuePtr,
                    pixel2 + image2.bytesPerLine(),
                    pixel2 + image2.bytesPerLine() + image2Bpp);
        }
    }
    // Last column
    {
        quint8 *pixel1 = image1.scanLine(1) + image1Bpp * (image1.width() - 1);
        const quint8 *pixel2 = image2.constScanLine(1) + image2Bpp * (image2.width() - 1);
        for (qint32 y = 1; y < image1.height() - 1; ++y, pixel1 += image1.bytesPerLine(), pixel2 += image2.bytesPerLine()) {
            visitor(pixel1, pixel2, 
                    pixel2 - image2.bytesPerLine() - image2Bpp,
                    pixel2 - image2.bytesPerLine(),
                    nullValuePtr,
                    pixel2 - image2Bpp,
                    nullValuePtr,
                    pixel2 + image2.bytesPerLine() - image2Bpp,
                    pixel2 + image2.bytesPerLine(),
                    nullValuePtr);
        }
    }
    // Top-left pixel
    {
        quint8 *pixel1 = image1.bits();
        const quint8 *pixel2 = image2.constBits();
        visitor(pixel1, pixel2, 
                nullValuePtr, nullValuePtr, nullValuePtr, nullValuePtr,
                pixel2 + image2Bpp,
                nullValuePtr,
                pixel2 + image2.bytesPerLine(),
                pixel2 + image2.bytesPerLine() + image2Bpp);
    }
    // Top-right pixel
    {
        quint8 *pixel1 = image1.scanLine(0) + image1Bpp * (image1.width() - 1);
        const quint8 *pixel2 = image2.constScanLine(0) + image2Bpp * (image2.width() - 1);
        visitor(pixel1, pixel2, 
                nullValuePtr, nullValuePtr, nullValuePtr,
                pixel2 - image2Bpp,
                nullValuePtr,
                pixel2 + image2.bytesPerLine() - image2Bpp,
                pixel2 + image2.bytesPerLine(),
                nullValuePtr);
    }
    // Bottom-left pixel
    {
        quint8 *pixel1 = image1.scanLine(image1.height() - 1);
        const quint8 *pixel2 = image2.constScanLine(image2.height() - 1);
        visitor(pixel1, pixel2, 
                nullValuePtr,
                pixel2 - image2.bytesPerLine(),
                pixel2 - image2.bytesPerLine() + image2Bpp,
                nullValuePtr,
                pixel2 + image2Bpp,
                nullValuePtr, nullValuePtr, nullValuePtr);
    }
    // Bottom-right pixel
    {
        quint8 *pixel1 = image1.scanLine(image1.height() - 1) + image1Bpp * (image1.width() - 1);
        const quint8 *pixel2 = image2.constScanLine(image2.height() - 1) + image2Bpp * (image2.width() - 1);
        visitor(pixel1, pixel2, 
                pixel2 - image2.bytesPerLine() - image2Bpp,
                pixel2 - image2.bytesPerLine(),
                nullValuePtr,
                pixel2 - image2Bpp,
                nullValuePtr, nullValuePtr, nullValuePtr, nullValuePtr);
    }
}

template <typename Visitor>
inline void visitPixelsWithCoords(QImage &image, Visitor visitor)
{
    const qint32 bpp = image.pixelFormat().bitsPerPixel() / 8;
    QVector<QRect> rects = processingRects(image.rect());
    image.scanLine(0);
    QtConcurrent::blockingMap(
        rects,
        [bpp, &image, visitor](const QRect &r)
        {
            for (qint32 y = r.top(); y <= r.bottom(); ++y) {
                quint8 *pixel = image.scanLine(y);
                for (qint32 x = 0; x < image.width(); ++x, pixel += bpp) {
                    visitor(pixel, x, y);
                }
            }
            return r;
        }
    );
}

template <typename Visitor>
inline void visitPixelsWithCoords(const QImage &image, Visitor visitor)
{
    const qint32 bpp = image.pixelFormat().bitsPerPixel() / 8;
    QVector<QRect> rects = processingRects(image.rect());
    image.scanLine(0);
    QtConcurrent::blockingMap(
        rects,
        [bpp, &image, visitor](const QRect &r)
        {
            for (qint32 y = r.top(); y <= r.bottom(); ++y) {
                const quint8 *pixel = image.constScanLine(y);
                for (qint32 x = 0; x < image.width(); ++x, pixel += bpp) {
                    visitor(pixel, x, y);
                }
            }
            return r;
        }
    );
}

template <typename Visitor>
inline void visitPixelsWithCoords(QImage &image1, const QImage &image2, Visitor visitor)
{
    const qint32 image1Bpp = image1.pixelFormat().bitsPerPixel() / 8;
    const qint32 image2Bpp = image2.pixelFormat().bitsPerPixel() / 8;
    QVector<QRect> rects = processingRects(image1.rect());
    image1.scanLine(0);
    QtConcurrent::blockingMap(
        rects,
        [image1Bpp, image2Bpp, &image1, &image2, visitor](const QRect &r)
        {
            for (qint32 y = r.top(); y <= r.bottom(); ++y) {
                quint8 *pixel1 = image1.scanLine(y);
                const quint8 *pixel2 = image2.constScanLine(y);
                for (qint32 x = 0; x < image1.width(); ++x, pixel1 += image1Bpp, pixel2 += image2Bpp) {
                    visitor(pixel1, pixel2, x, y);
                }
            }
            return r;
        }
    );
}

QImage COMMON_EXPORT shiftedImage(const QImage &image, const QPoint &offset, quint32 fillValue = 0);
QVector<ImgProcCommon::ARGB> COMMON_EXPORT generateGradientLut(const QGradientStops &gradient, qint32 size);
QImage COMMON_EXPORT scaledUpMask(const QImage &mask, bool invert = false);
QImage COMMON_EXPORT scaledDownMask(const QImage &mask, bool invert = false);
QImage COMMON_EXPORT invertMask(const QImage &mask);
QImage COMMON_EXPORT convertMaskToImage(const QImage &mask);
QImage COMMON_EXPORT convertImageToMask(const QImage &image);

qint32 COMMON_EXPORT percentageOpacityToByte(qint32 percentageOpacity);
qint32 COMMON_EXPORT byteOpacityToPercentage(qint32 byteOpacity);

}

#endif
