/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef DROPSHADOWOPTIONSWIDGET_H
#define DROPSHADOWOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT DropShadowOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DropShadowOptionsWidget(QWidget *parent = nullptr);
    ~DropShadowOptionsWidget() override;

    LayerStyles::DropShadow params() const;

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
