/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef KNOCKOUTCOMBOBOX_H
#define KNOCKOUTCOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT KnockoutComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit KnockoutComboBox(QWidget *parent = nullptr);
    ~KnockoutComboBox() override;

    LayerStyles::Knockout currentKnockout() const;

public Q_SLOTS:
    void setCurrentKnockout(LayerStyles::Knockout newKnockout);

Q_SIGNALS:
    void currentKnockoutChanged(LayerStyles::Knockout newKnockout);
};

#endif
