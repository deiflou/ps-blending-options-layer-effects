/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <cmath>

#include <QRandomGenerator>

#include <Viewport.h>
#include <BevelAndEmbossOptionsWidget.h>
#include <BevelAndEmbossTransform.h>
#include <ImgProcUtil.h>

#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    BevelAndEmbossOptionsWidget *bevelAndEmbossOptionsWidget {nullptr};

    QImage mask;

    QImage generateImage() const
    {
        LayerStyles::BevelAndEmboss options = bevelAndEmbossOptionsWidget->params();

        QImage out =
            BevelAndEmbossTransform::apply(
                mask,

                options.style == LayerStyles::BevelAndEmbossStyle_PillowEmboss
                    ? BevelAndEmbossTransform::Style_PillowEmboss
                    : BevelAndEmbossTransform::Style_Emboss,

                static_cast<BevelAndEmbossTransform::Technique>(options.technique),
                options.depth,
                static_cast<BevelAndEmbossTransform::Direction>(options.direction),

                options.style == LayerStyles::BevelAndEmbossStyle_OuterBevel ||
                options.style == LayerStyles::BevelAndEmbossStyle_InnerBevel
                    ? options.size
                    : options.size / 2,

                options.soften,
                options.lightAngle,
                options.lightAltitude,
                options.applyProfileContour ? options.profileCurve : Curve(),
                options.antiAliasedProfileCurve,

                options.applyProfileContour &&
                (options.style == LayerStyles::BevelAndEmbossStyle_OuterBevel ||
                 options.style == LayerStyles::BevelAndEmbossStyle_InnerBevel)
                ? options.profileRange : 100,

                options.style == LayerStyles::BevelAndEmbossStyle_InnerBevel,
                options.applyTexture ? options.texture : QImage(),
                options.textureScale,
                options.textureOffset,
                options.textureDepth,
                options.invertTexture,
                options.glossCurve,
                options.antiAliasedGlossCurve,
                true
            );

        // Make RGB image
        if (options.style == LayerStyles::BevelAndEmbossStyle_OuterBevel) {
            ImgProcUtil::visitPixels(
                out, mask,
                [](quint8 *pixel1, const quint8 *pixel2)
                {
                    const quint32 p1 = *reinterpret_cast<const quint32*>(pixel1);
                    const quint8 v = static_cast<quint8>((p1 >> 8) + ((p1 & 0xFF) > 0x7F));
                    ImgProcCommon::ARGB *color = reinterpret_cast<ImgProcCommon::ARGB*>(pixel1);
                    color->r = color->g = color->b = (128 * *pixel2 + v * (255 - *pixel2)) / 255;
                    color->a = 255;
                }
            );
        } else if (options.style == LayerStyles::BevelAndEmbossStyle_InnerBevel) {
            ImgProcUtil::visitPixels(
                out, mask,
                [](quint8 *pixel1, const quint8 *pixel2)
                {
                    const quint32 p1 = *reinterpret_cast<const quint32*>(pixel1);
                    const quint8 v = static_cast<quint8>((p1 >> 8) + ((p1 & 0xFF) > 0x7F));
                    ImgProcCommon::ARGB *color = reinterpret_cast<ImgProcCommon::ARGB*>(pixel1);
                    color->r = color->g = color->b = (128 * (255 - *pixel2) + v * *pixel2) / 255;
                    color->a = 255;
                }
            );
        } else {
            ImgProcUtil::visitPixels(
                out,
                [](quint8 *pixel)
                {
                    const quint32 p = *reinterpret_cast<const quint32*>(pixel);
                    const quint8 v = static_cast<quint8>((p >> 8) + ((p & 0xFF) > 0x7F));
                    ImgProcCommon::ARGB *color = reinterpret_cast<ImgProcCommon::ARGB*>(pixel);
                    color->r = color->g = color->b = v;
                    color->a = 255;
                }
            );
        }

        return out;
    }
};

Window::Window()
    : m_d(new Private)
{
    m_d->mask = QImage(":/current_layer").convertToFormat(QImage::Format_Alpha8);
    const QImage texture = QImage(":/pattern").convertToFormat(QImage::Format_ARGB32);

    m_d->bevelAndEmbossOptionsWidget = new BevelAndEmbossOptionsWidget;
    m_d->bevelAndEmbossOptionsWidget->setContentsMargins(10, 10, 10, 10);
    m_d->bevelAndEmbossOptionsWidget->setTexture(texture);

    addControlsPanel(m_d->bevelAndEmbossOptionsWidget, "Bevel and Emboss");

    viewport()->setImage(m_d->generateImage());

    connect(m_d->bevelAndEmbossOptionsWidget, SIGNAL(paramsChanged()),
            this, SLOT(requestViewportUpdate()));
    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );
}

Window::~Window()
{}
