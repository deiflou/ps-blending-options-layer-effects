/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef BEVELANDEMBOSSTECHNIQUECOMBOBOX_H
#define BEVELANDEMBOSSTECHNIQUECOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT BevelAndEmbossTechniqueComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit BevelAndEmbossTechniqueComboBox(QWidget *parent = nullptr);
    ~BevelAndEmbossTechniqueComboBox() override;

    LayerStyles::BevelAndEmbossTechnique currentTechnique() const;

public Q_SLOTS:
    void setCurrentTechnique(LayerStyles::BevelAndEmbossTechnique newTechnique);

Q_SIGNALS:
    void currentTechniqueChanged(LayerStyles::BevelAndEmbossTechnique newTechnique);
};

#endif
