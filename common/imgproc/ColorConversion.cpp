/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <cmath>

#include "ColorConversion.h"

namespace ColorConversion
{

qint32 intensity(qint32 b, qint32 g, qint32 r)
{
    return (299 * r + 587 * g + 114 * b) / 1000;
}

qint32 intensity(ImgProcCommon::ARGB color)
{
    return intensity(color.b, color.g, color.r);
}

}
