/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "StrokeFillTypeComboBox.h"

StrokeFillTypeComboBox::StrokeFillTypeComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Color", static_cast<int>(LayerStyles::StrokeFillType_Color));
    addItem("Gradient", static_cast<int>(LayerStyles::StrokeFillType_Gradient));
    addItem("Pattern", static_cast<int>(LayerStyles::StrokeFillType_Pattern));

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentStrokeFillTypeChanged(static_cast<LayerStyles::StrokeFillType>(currentData().toInt()));
        }
    );
}
StrokeFillTypeComboBox::~StrokeFillTypeComboBox()
{}

LayerStyles::StrokeFillType StrokeFillTypeComboBox::currentStrokeFillType() const
{
    return static_cast<LayerStyles::StrokeFillType>(currentData().toInt());
}

void StrokeFillTypeComboBox::setCurrentStrokeFillType(LayerStyles::StrokeFillType newStrokeFillType)
{
    setCurrentIndex(findData(static_cast<int>(newStrokeFillType)));
}
