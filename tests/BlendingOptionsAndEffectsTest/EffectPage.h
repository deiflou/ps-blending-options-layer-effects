/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef EFFECTPAGE_H
#define EFFECTPAGE_H

#include <QWidget>

class EffectPage : public QWidget
{
    Q_OBJECT

public:
    EffectPage(QWidget *effectWidget, bool showActive = true, QWidget *parent = nullptr);
    ~EffectPage() override;

    bool isActive() const;

Q_SIGNALS:
    void activeChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
