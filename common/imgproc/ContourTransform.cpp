/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "ImgProcUtil.h"
#include "ImgProcCommon.h"
#include "ContourTransform.h"

namespace ContourTransform
{

struct Identity
{
    template <typename... Args>
    Identity(Args...) {}

    template <typename... Args>
    quint32 operator()(quint32 value, Args...) const
    {
        return value;
    }
};

struct Range
{
    const qint32 range;

    Range(qint32 range)
        // 0x146 = (1/2) * 0xFF00 / 100
        : range(range == 0 ? 0x146 : range * 0xFF00 / 100)
    {}

    quint32 operator()(quint32 value) const
    {
        if (value >= range) {
            return 0xFF00;
        }
        return value * 0xFF00 / range;
    }
};

struct ReverseRange
{
    const qint32 range;
    const qint32 oneMinusRange;

    ReverseRange(qint32 range)
        // 0x146 = (1/2) * 0xFF00 / 100
        : range(range == 0 ? 0x146 : range * 0xFF00 / 100)
        , oneMinusRange(0xFF00 - this->range)
    {}

    quint32 operator()(quint32 value) const
    {
        if (value <= oneMinusRange) {
            return 0x0000;
        }
        return 0xFF00 - (0xFF00 - value) * 0xFF00 / range;
    }
};

struct Map
{
    const QVector<quint16> &contourLut;

    Map(const QVector<quint16> &contourLut) : contourLut(contourLut) {}

    quint32 sampleLut(quint32 value) const
    {
        // Code for a LUT with 1021 entries. It performs linear interpolation
        // between them based on the fractional part of the value.
        // The value is in the range [0x0000, 0xFF00], but the lut indices are
        // in the  range [0x0000, 0x03FC], so we scale it down just by bit
        // shifting and use the discarded bits (fractional part) as parameter
        // in the linear interpolation
        const quint32 scaledValue = (value >> 6);
        const quint32 scaledValueFrac = value & 0x3F;
        const quint32 mappedValue1 = contourLut[scaledValue];
        const quint32 mappedValue2 = contourLut[std::min(0x03FCU, scaledValue + 1)];
        const quint32 interpolatedValue = (mappedValue1 * (0x3F - scaledValueFrac) + mappedValue2 * scaledValueFrac) / 0x3F;
        return interpolatedValue;
        // Code for a LUT with 256 entries.
        // const quint32 scaledValue = value >> 8;
        // const quint32 scaledValueFrac = value & 0xFF;
        // const quint32 mappedValue1 = contourLut[scaledValue];
        // const quint32 mappedValue2 = contourLut[std::min(255U, scaledValue + 1)];
        // const quint32 interpolatedValue = (mappedValue1 * (255 - scaledValueFrac) + mappedValue2 * scaledValueFrac) / 255;
        // return interpolatedValue;
    }

    quint32 operator()(quint32 value) const
    {
        return sampleLut(value);
    }

    /**
     * @brief Anti-aliased version
     */
    quint32 operator()(quint32 value, quint32 valueE, quint32 valueS) const
    {
        // Here we find the minimum and maximum values between the current
        // and neighbor pixels. Then we must integrate over the region defined
        // by them in the contour lut and get the average
        const quint32 minValue = std::min(value, std::min(valueE, valueS));
        const quint32 maxValue = std::max(value, std::max(valueE, valueS));
        if (minValue == maxValue) {
            return sampleLut(value);
        }
        const quint32 delta = maxValue - minValue;
        // Version for a lut with 1021 entries (0xFF00 >> 8 << 2 + 1)
        const quint32 steps = std::max(1U, (delta >> 6) + ((delta & 0x3F) > 0x1F));
        // Version for a lut with 256 entries
        // const quint32 steps = std::max(1U, (delta >> 8) + ((delta & 0xFF) > 0x7F));
        const quint32 inc = delta / steps;
        quint32 cumulatedValue = 0;
        for (quint32 i = 0; i <= steps; ++i) {
            cumulatedValue += sampleLut(minValue + i * inc);
        }
        return cumulatedValue / (steps + 1);
    }
};

struct Noise
{
    qint32 noiseAmount;
    QRandomGenerator &rng;

    Noise(qint32 noise, QRandomGenerator &rng)
        : noiseAmount((noise * 0xFF / 100) << 8), rng(rng)
    {}

    quint32 operator()(quint32 value) const
    {
        if (value == 0) {
            return 0;
        }
        const qint32 offset = rng.bounded(2 * noiseAmount) - noiseAmount;
        const qint32 perturbedValue = std::min(0xFF00, std::max(0, static_cast<qint32>(value) + offset));
        return static_cast<quint32>(perturbedValue);
    }
};

struct Fade
{
    static constexpr qint32 fadeFactor {8};

    quint32 operator()(quint32 modifiedValue, quint32 originalValue) const
    {
        const quint32 fadedValue = std::min(modifiedValue, originalValue * fadeFactor);
        return fadedValue;
    }
};

struct Invert
{
    quint32 operator()(quint32 value) const
    {
        return 0xFF00 - value;
    }
};

template <typename InvertFn, typename RangeFn, typename MapFn, typename NoiseFn, typename FadeFn>
struct Applicator
{
    InvertFn invertFn;
    RangeFn rangeFn;
    MapFn mapFn;
    NoiseFn noiseFn;
    FadeFn fadeFn;

    Applicator(qint32 range, const QVector<quint16> &contourLut, qint32 noiseAmount, QRandomGenerator &rng)
        : rangeFn(range)
        , mapFn(contourLut)
        , noiseFn(noiseAmount, rng)
    {}

    void operator()(quint8 *pixelOut, const quint8 *pixelIn) const
    {
        quint32 *valueOut = reinterpret_cast<quint32*>(pixelOut);
        const quint32 valueIn = *reinterpret_cast<const quint32*>(pixelIn);
        *valueOut = fadeFn(noiseFn(mapFn(invertFn(rangeFn(valueIn)))), invertFn(valueIn));
    }
    
    // Anti-aliased version
    void operator()(quint8 *pixelOut, const quint8 *pixelIn, const quint8 *pixelInE, const quint8 *pixelInS) const
    {
        quint32 *valueOut = reinterpret_cast<quint32*>(pixelOut);
        const quint32 valueIn = *reinterpret_cast<const quint32*>(pixelIn);
        const quint32 valueInE = *reinterpret_cast<const quint32*>(pixelInE);
        const quint32 valueInS = *reinterpret_cast<const quint32*>(pixelInS);
        *valueOut = fadeFn(noiseFn(mapFn(invertFn(rangeFn(valueIn)), invertFn(rangeFn(valueInE)), invertFn(rangeFn(valueInS)))), invertFn(valueIn));
    }
};

template <typename InvertFn, typename RangeFn, typename MapFn, typename NoiseFn, typename FadeFn>
void apply(QImage &outMask, const QImage &inMask, qint32 range, const QVector<quint16> &contourLut, qint32 noise, QRandomGenerator &rng)
{
    Applicator<InvertFn, RangeFn, MapFn, NoiseFn, FadeFn> applicator(range, contourLut, noise, rng);
    ImgProcUtil::visitPixels(
        outMask, inMask,
        [&applicator](quint8 *pixelOut, const quint8 *pixelIn)
        {
            applicator(pixelOut, pixelIn);
        }
    );
}

template <typename InvertFn, typename RangeFn, typename MapFn, typename NoiseFn, typename FadeFn>
void applyAA(QImage &outMask, const QImage &inMask, qint32 range, const QVector<quint16> &contourLut, qint32 noise, QRandomGenerator &rng)
{
    Applicator<InvertFn, RangeFn, MapFn, NoiseFn, FadeFn> applicator(range, contourLut, noise, rng);
    ImgProcUtil::visitPixelsWithBottomRightNeighbors(
        outMask, inMask,
        [&applicator](quint8 *pixelOut, const quint8 *pixelIn, const quint8 *pixelInE, const quint8 *pixelInS)
        {
            applicator(pixelOut, pixelIn, pixelInE, pixelInS);
        }
    );
}

template <typename InvertFn, typename RangeFn, typename MapFn, typename NoiseFn, typename FadeFn>
void applyGenericSelectAntiAlias(QImage &outMask, const QImage &inMask, qint32 range,
                                 const QVector<quint16> &curveLut, bool curveIsIdentity,
                                 bool antiAliasedCurve, qint32 noise, QRandomGenerator &rng)
{
    antiAliasedCurve && !curveIsIdentity
    ? applyAA<InvertFn, RangeFn, MapFn, NoiseFn, FadeFn>
        (outMask, inMask, range, curveLut, noise, rng)
    : apply<InvertFn, RangeFn, MapFn, NoiseFn, FadeFn>
        (outMask, inMask, range, curveLut, noise, rng);
}

template <typename InvertFn, typename RangeFn, typename MapFn, typename NoiseFn>
void applyGenericSelectFade(QImage &outMask, const QImage &inMask, qint32 range,
                            const QVector<quint16> &curveLut, bool curveIsIdentity,
                            bool antiAliasedCurve, qint32 noise, QRandomGenerator &rng,
                            bool applyFade)
{
    applyFade
    ? applyGenericSelectAntiAlias<InvertFn, RangeFn, MapFn, NoiseFn, Fade>
        (outMask, inMask, range, curveLut, curveIsIdentity,
         antiAliasedCurve, noise, rng)
    : applyGenericSelectAntiAlias<InvertFn, RangeFn, MapFn, NoiseFn, Identity>
        (outMask, inMask, range, curveLut, curveIsIdentity,
         antiAliasedCurve, noise, rng);
}

template <typename InvertFn, typename RangeFn, typename MapFn>
void applyGenericSelectNoise(QImage &outMask, const QImage &inMask, qint32 range,
                             const QVector<quint16> &curveLut, bool curveIsIdentity,
                             bool antiAliasedCurve, qint32 noise, QRandomGenerator &rng,
                             bool applyFade)
{
    noise != 0
    ? applyGenericSelectFade<InvertFn, RangeFn, MapFn, Noise>
        (outMask, inMask, range, curveLut, curveIsIdentity,
         antiAliasedCurve, noise, rng, applyFade)
    : applyGenericSelectFade<InvertFn, RangeFn, MapFn, Identity>
        (outMask, inMask, range, curveLut, curveIsIdentity,
         antiAliasedCurve, noise, rng, applyFade);
}

template <typename InvertFn, typename RangeFn>
void applyGenericSelectMap(QImage &outMask, const QImage &inMask, qint32 range,
                           const QVector<quint16> &curveLut, bool curveIsIdentity,
                           bool antiAliasedCurve, qint32 noise, QRandomGenerator &rng,
                           bool applyFade)
{
    !curveIsIdentity
    ? applyGenericSelectNoise<InvertFn, RangeFn, Map>
        (outMask, inMask, range, curveLut, curveIsIdentity,
         antiAliasedCurve, noise, rng, applyFade)
    : applyGenericSelectNoise<InvertFn, RangeFn, Identity>
        (outMask, inMask, range, curveLut, curveIsIdentity,
         antiAliasedCurve, noise, rng, applyFade);
}

template <typename InvertFn>
void applyGenericSelectRange(QImage &outMask, const QImage &inMask,
                             qint32 range, bool reverseRange,
                             const QVector<quint16> &curveLut, bool curveIsIdentity,
                             bool antiAliasedCurve, qint32 noise, QRandomGenerator &rng,
                             bool applyFade)
{
    range != 100
    ? reverseRange
      ? applyGenericSelectMap<InvertFn, ReverseRange>
            (outMask, inMask, range, curveLut, curveIsIdentity,
             antiAliasedCurve, noise, rng, applyFade)
      : applyGenericSelectMap<InvertFn, Range>
            (outMask, inMask, range, curveLut, curveIsIdentity,
             antiAliasedCurve, noise, rng, applyFade)
    : applyGenericSelectMap<InvertFn, Identity>
        (outMask, inMask, range, curveLut, curveIsIdentity,
         antiAliasedCurve, noise, rng, applyFade);
}

void applyGenericSelectInvert(QImage &outMask, const QImage &inMask, bool invert,
                              qint32 range, bool reverseRange,
                              const QVector<quint16> &curveLut, bool curveIsIdentity,
                              bool antiAliasedCurve, qint32 noise, QRandomGenerator &rng,
                              bool applyFade)
{
    invert
    ? applyGenericSelectRange<Invert>
        (outMask, inMask, range, reverseRange, curveLut, curveIsIdentity,
         antiAliasedCurve, noise, rng, applyFade)
    : applyGenericSelectRange<Identity>
        (outMask, inMask, range, reverseRange, curveLut, curveIsIdentity,
         antiAliasedCurve, noise, rng, applyFade);
}

QVector<quint16> generateCurveLut(const Curve &curve)
{
    return curve.isIdentity() ? QVector<quint16>() : curve.lut<quint16>(0xFF * 4 + 1, 0xFF00);
}

void applyGeneric(QImage &outMask, const QImage &inMask, bool invert,
                  qint32 range, bool reverseRange,
                  const Curve &curve, bool antiAliasedCurve,
                  qint32 noise, QRandomGenerator &rng,
                  bool applyFade)
{
    const bool curveIsIdentity = curve.isIdentity();
    const QVector<quint16> curveLut = curveIsIdentity ? QVector<quint16>() : generateCurveLut(curve);
    applyGenericSelectInvert(outMask, inMask, invert, range, reverseRange,
                             curveLut, curveIsIdentity, antiAliasedCurve,
                             noise, rng, applyFade);
}

QImage apply(const QImage &mask, bool invert, qint32 range, bool reverseRange,
             const Curve &curve, bool antiAliasedCurve,
             qint32 noise, QRandomGenerator &rng, bool applyFade)
{
    QImage outMask(mask.size(), QImage::Format_ARGB32);
    applyGeneric(outMask, mask, invert, range, reverseRange,
                 curve, antiAliasedCurve, noise, rng, applyFade);
    return outMask;
}

QImage applyCurve(const QImage &mask,
                  const Curve &curve,
                  bool antiAliasedCurve)
{
    QRandomGenerator rng;
    return apply(mask, false, 100, false, curve, antiAliasedCurve, 0, rng, false);
}

QImage applyRangeAndCurve(const QImage &mask,
                          qint32 range,
                          bool reverseRange,
                          const Curve &curve,
                          bool antiAliasedCurve)
{
    QRandomGenerator rng;
    return apply(mask, false, range, reverseRange, curve, antiAliasedCurve, 0, rng, false);
}

QImage applyAndFillWithColor(const QImage &mask, bool invert, qint32 range, bool reverseRange,
                             const Curve &curve, bool antiAliasedCurve,
                             qint32 noise, QRandomGenerator &rng, bool applyFade,
                             const QColor &color)
{
    QImage outMask = apply(mask, invert, range, reverseRange,
                           curve, antiAliasedCurve, noise, rng, applyFade);

    QImage outImage(mask.size(), QImage::Format_ARGB32);
    ImgProcCommon::ARGB c {static_cast<quint8>(color.blue()),
                           static_cast<quint8>(color.green()),
                           static_cast<quint8>(color.red()),
                           255};

    ImgProcUtil::visitPixels(
        outImage, outMask,
        [c](quint8 *pixel1, const quint8 *pixel2)
        {
            ImgProcCommon::ARGB *finalColor = reinterpret_cast<ImgProcCommon::ARGB*>(pixel1);
            const quint32 p2 = *reinterpret_cast<const quint32*>(pixel2);
            const quint8 alpha = static_cast<quint8>((p2 >> 8) + ((p2 & 0xFF) > 0x7F));
            *finalColor = {c.b, c.g, c.r, alpha};
        }
    );

    return outImage;
}

struct GradientNoise
{
    const qint32 noiseAmount;

    GradientNoise(qint32 noiseAmount) : noiseAmount(noiseAmount) {}

    quint32 operator()(quint32 value, qint32 offset) const
    {
        const qint32 noiseOffset = noiseAmount * offset / 100;
        const qint32 noiseValue = std::min(0xFF00, std::max(0, static_cast<qint32>(value) + noiseOffset));
        return static_cast<quint32>(noiseValue);
    }
};

template <typename JitterFn, typename NoiseFn, typename FadeFn>
void applyGradient(QImage &outImage, const QImage &contourMask, const QImage &originalMask,
                   qint32 gradientJitter, const QVector<ImgProcCommon::ARGB> &gradientLut,
                   qint32 noise, QRandomGenerator &rng)
{
    JitterFn jitterFn(gradientJitter);
    NoiseFn noiseFn(noise);
    FadeFn fadeFn;
    ImgProcUtil::visitPixels(
        outImage, contourMask, originalMask,
        [&jitterFn, &noiseFn, &fadeFn, &gradientLut, &rng]
        (quint8 *pixelOut, const quint8 *contourPixel, const quint8 *originalPixel)
        {
            ImgProcCommon::ARGB *finalColor = reinterpret_cast<ImgProcCommon::ARGB*>(pixelOut);
            const quint32 contourValue = *reinterpret_cast<const quint32*>(contourPixel);
            const quint32 originalValue = *reinterpret_cast<const quint32*>(originalPixel);

            const qint32 offset = rng.bounded(2 * 0xFF00) - 0xFF00;
            const quint32 jitteredValue = jitterFn(contourValue, offset);
            const quint32 gradientLutIndex = (jitteredValue >> 6) + ((jitteredValue & 0x3F) > 0x1F);
            ImgProcCommon::ARGB gradientColor = gradientLut[gradientLutIndex];
            const quint32 noisedValue = noiseFn(gradientColor.a << 8, offset);
            const quint32 fadedValue = fadeFn(noisedValue, originalValue);
            gradientColor.a = static_cast<quint8>((fadedValue >> 8) + ((fadedValue & 0xFF) > 0x7F));

            *finalColor = gradientColor;
        }
    );
}

QImage applyAndFillWithGradient(const QImage &mask, bool invert, qint32 range, bool reverseRange,
                                const Curve &curve, bool antiAliasedCurve, qint32 gradientJitter,
                                qint32 noise, QRandomGenerator &rng, bool applyFade,
                                const QGradientStops &gradient)
{
    const QImage contourMask = apply(mask, invert, range, reverseRange,
                                     curve, antiAliasedCurve, 0, rng, false);

    QImage outImage(mask.size(), QImage::Format_ARGB32);
    QVector<ImgProcCommon::ARGB> gradientLut = ImgProcUtil::generateGradientLut(gradient, 1021);

    using namespace ImgProcCommon;

    if (gradientJitter > 0) {
        if (noise > 0) {
            if (applyFade) {
                applyGradient<GradientNoise, GradientNoise, Fade>(outImage, contourMask, mask, gradientJitter, gradientLut, noise, rng);
            } else {
                applyGradient<GradientNoise, GradientNoise, Identity>(outImage, contourMask, mask, gradientJitter, gradientLut, noise, rng);
            }
        } else {
            if (applyFade) {
                applyGradient<GradientNoise, Identity, Fade>(outImage, contourMask, mask, gradientJitter, gradientLut, noise, rng);
            } else {
                applyGradient<GradientNoise, Identity, Identity>(outImage, contourMask, mask, gradientJitter, gradientLut, noise, rng);
            }
        }
    } else {
        if (noise > 0) {
            if (applyFade) {
                applyGradient<Identity, GradientNoise, Fade>(outImage, contourMask, mask, gradientJitter, gradientLut, noise, rng);
            } else {
                applyGradient<Identity, GradientNoise, Identity>(outImage, contourMask, mask, gradientJitter, gradientLut, noise, rng);
            }
        } else {
            if (applyFade) {
                applyGradient<Identity, Identity, Fade>(outImage, contourMask, mask, gradientJitter, gradientLut, noise, rng);
            } else {
                applyGradient<Identity, Identity, Identity>(outImage, contourMask, mask, gradientJitter, gradientLut, noise, rng);
            }
        }
    }

    return outImage;
}

}
