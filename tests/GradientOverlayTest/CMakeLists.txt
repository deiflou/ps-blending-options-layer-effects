add_executable(
    gradient_overlay_test
    main.cpp
    Window.cpp
    resources.qrc
)

target_link_libraries(gradient_overlay_test PRIVATE Qt5::Widgets common)
target_include_directories(gradient_overlay_test PRIVATE "${COMMON_INCLUDE_DIRS}")

set_target_properties(
    gradient_overlay_test
    PROPERTIES
    AUTOMOC ON
    AUTORCC ON
)
