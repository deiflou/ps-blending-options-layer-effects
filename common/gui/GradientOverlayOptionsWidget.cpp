/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QHBoxLayout>
#include <QCheckBox>

#include "ColorBlendMethodComboBox.h"
#include "GradientStyleComboBox.h"
#include "GradientMethodComboBox.h"
#include "SliderSpinBox.h"

#include "GradientOverlayOptionsWidget.h"

class Q_DECL_HIDDEN GradientOverlayOptionsWidget::Private
{
public:
    ColorBlendMethodComboBox *comboBoxBlendMode {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
    GradientStyleComboBox *comboBoxGradientStyle {nullptr};
    QCheckBox *checkBoxDither {nullptr};
    QCheckBox *checkBoxReverse {nullptr};
    GradientMethodComboBox *comboBoxGradientMethod {nullptr};
    SliderSpinBox *sliderAngle {nullptr};
    SliderSpinBox *sliderScale {nullptr};
    QCheckBox *checkBoxAlignWithLayer {nullptr};
    QGradientStops gradient;
};

GradientOverlayOptionsWidget::GradientOverlayOptionsWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    LayerStyles::GradientOverlay defaultParams;

    m_d->gradient = defaultParams.gradient;

    m_d->comboBoxBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxBlendMode->setCurrentColorBlendMethod(defaultParams.blendMode);

    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(defaultParams.opacity);

    m_d->comboBoxGradientStyle = new GradientStyleComboBox;
    m_d->comboBoxGradientStyle->setCurrentGradientStyle(defaultParams.style);

    m_d->checkBoxDither = new QCheckBox;
    m_d->checkBoxDither->setChecked(defaultParams.dither);

    m_d->checkBoxReverse = new QCheckBox;
    m_d->checkBoxReverse->setChecked(defaultParams.reverse);

    m_d->comboBoxGradientMethod = new GradientMethodComboBox;
    m_d->comboBoxGradientMethod->setCurrentGradientMethod(defaultParams.method);

    m_d->sliderAngle = new SliderSpinBox;
    m_d->sliderAngle->setRange(0, 359);
    m_d->sliderAngle->setValue(defaultParams.angle);

    m_d->sliderScale = new SliderSpinBox;
    m_d->sliderScale->setRange(10, 150);
    m_d->sliderScale->setValue(defaultParams.scale);

    m_d->checkBoxAlignWithLayer = new QCheckBox;
    m_d->checkBoxAlignWithLayer->setChecked(defaultParams.alignWithLayer);

    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addRow("Blend mode:", m_d->comboBoxBlendMode);
    mainLayout->addRow("Opacity:", m_d->sliderOpacity);
    mainLayout->addRow("Style:", m_d->comboBoxGradientStyle);
    mainLayout->addRow("Dither:", m_d->checkBoxDither);
    mainLayout->addRow("Reverse:", m_d->checkBoxReverse);
    mainLayout->addRow("Method:", m_d->comboBoxGradientMethod);
    mainLayout->addRow("Angle:", m_d->sliderAngle);
    mainLayout->addRow("Scale:", m_d->sliderScale);
    mainLayout->addRow("Align with layer:", m_d->checkBoxAlignWithLayer);

    setLayout(mainLayout);

    connect(m_d->comboBoxBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxGradientStyle, SIGNAL(currentGradientStyleChanged(LayerStyles::GradientStyle)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxDither, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxReverse, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxGradientMethod, SIGNAL(currentGradientMethodChanged(LayerStyles::GradientMethod)), SIGNAL(paramsChanged()));
    connect(m_d->sliderAngle, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderScale, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxAlignWithLayer, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
}

GradientOverlayOptionsWidget::~GradientOverlayOptionsWidget()
{}

LayerStyles::GradientOverlay GradientOverlayOptionsWidget::params() const
{
    LayerStyles::GradientOverlay params;

    params.blendMode = m_d->comboBoxBlendMode->currentColorBlendMethod();
    params.gradient = m_d->gradient;
    params.opacity = m_d->sliderOpacity->value();
    params.style = m_d->comboBoxGradientStyle->currentGradientStyle();
    params.dither = m_d->checkBoxDither->isChecked();
    params.reverse = m_d->checkBoxReverse->isChecked();
    params.method = m_d->comboBoxGradientMethod->currentGradientMethod();
    params.angle = m_d->sliderAngle->value();
    params.scale = m_d->sliderScale->value();
    params.alignWithLayer = m_d->checkBoxAlignWithLayer->isChecked();
    
    return params;
}

void GradientOverlayOptionsWidget::setGradient(const QGradientStops &gradient)
{
    m_d->gradient = gradient;
    emit paramsChanged();
}
