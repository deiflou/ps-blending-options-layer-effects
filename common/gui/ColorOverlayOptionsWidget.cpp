/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QHBoxLayout>

#include "ColorBlendMethodComboBox.h"
#include "ColorButton.h"
#include "SliderSpinBox.h"

#include "ColorOverlayOptionsWidget.h"

class Q_DECL_HIDDEN ColorOverlayOptionsWidget::Private
{
public:
    ColorBlendMethodComboBox *comboBoxBlendMode {nullptr};
    ColorButton *buttonColor {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
};

ColorOverlayOptionsWidget::ColorOverlayOptionsWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    LayerStyles::ColorOverlay defaultParams;

    m_d->comboBoxBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxBlendMode->setCurrentColorBlendMethod(defaultParams.blendMode);

    m_d->buttonColor = new ColorButton;
    m_d->buttonColor->setColor(defaultParams.color);

    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(defaultParams.opacity);

    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addRow("Blend mode:", m_d->comboBoxBlendMode);
    mainLayout->addRow("Color:", m_d->buttonColor);
    mainLayout->addRow("Opacity:", m_d->sliderOpacity);

    setLayout(mainLayout);

    connect(m_d->comboBoxBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->buttonColor, SIGNAL(colorChanged(const QColor&)), SIGNAL(paramsChanged()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
}

ColorOverlayOptionsWidget::~ColorOverlayOptionsWidget()
{}

LayerStyles::ColorOverlay ColorOverlayOptionsWidget::params() const
{
    LayerStyles::ColorOverlay params;

    params.blendMode = m_d->comboBoxBlendMode->currentColorBlendMethod();
    params.color = m_d->buttonColor->color();
    params.opacity = m_d->sliderOpacity->value();
    
    return params;
}
