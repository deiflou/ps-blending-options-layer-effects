/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef ALPHABLENDMETHODCOMBOBOX_H
#define ALPHABLENDMETHODCOMBOBOX_H

#include <QComboBox>

#include <PixelBlending.h>

#include <Export.h>

class COMMON_EXPORT AlphaBlendMethodComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit AlphaBlendMethodComboBox(QWidget *parent = nullptr);
    ~AlphaBlendMethodComboBox() override;

    PixelBlending::AlphaBlendMethod currentAlphaBlendMethod() const;

public Q_SLOTS:
    void setCurrentAlphaBlendMethod(PixelBlending::AlphaBlendMethod newAlphaBlendMethod);

Q_SIGNALS:
    void currentAlphaBlendMethodChanged(PixelBlending::AlphaBlendMethod newAlphaBlendMethod);
};

#endif
