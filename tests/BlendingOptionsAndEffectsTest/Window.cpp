/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QPainter>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QCheckBox>
#include <QComboBox>
#include <QTabWidget>

#include <Viewport.h>
#include <BlendingOptionsWidget.h>
#include <DropShadowOptionsWidget.h>
#include <OuterGlowOptionsWidget.h>
#include <PatternOverlayOptionsWidget.h>
#include <GradientOverlayOptionsWidget.h>
#include <ColorOverlayOptionsWidget.h>
#include <SatinOptionsWidget.h>
#include <InnerGlowOptionsWidget.h>
#include <InnerShadowOptionsWidget.h>
#include <StrokeOptionsWidget.h>
#include <BevelAndEmbossOptionsWidget.h>
#include <ImgProcUtil.h>
#include <LayerStyles.h>
#include <Layer.h>
#include <PixelBlending.h>
#include <LayerBlending.h>

#include "EffectPage.h"
#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    BlendingOptionsWidget *blendingOptionsWidget {nullptr};
    DropShadowOptionsWidget *dropShadowOptionsWidget {nullptr};
    OuterGlowOptionsWidget *outerGlowOptionsWidget {nullptr};
    PatternOverlayOptionsWidget *patternOverlayOptionsWidget {nullptr};
    GradientOverlayOptionsWidget *gradientOverlayOptionsWidget {nullptr};
    ColorOverlayOptionsWidget *colorOverlayOptionsWidget {nullptr};
    SatinOptionsWidget *satinOptionsWidget {nullptr};
    InnerGlowOptionsWidget *innerGlowOptionsWidget {nullptr};
    InnerShadowOptionsWidget *innerShadowOptionsWidget {nullptr};
    StrokeOptionsWidget *strokeOptionsWidget {nullptr};
    BevelAndEmbossOptionsWidget *bevelAndEmbossOptionsWidget {nullptr};
    QCheckBox *checkBoxBackground {nullptr};
    QCheckBox *checkBoxBlurredBackdrop {nullptr};
    QComboBox *comboBoxCurrentLayer {nullptr};
    QCheckBox *checkBoxUseMask {nullptr};

    QImage background;
    QImage bg2;
    QImage bg2Blurred;

    Layer currentLayer;

    QImage generateImage()
    {
        const QImage backgroundImage = checkBoxBackground->isChecked() ? background : QImage();
        const QImage bottomLayerImage = checkBoxBlurredBackdrop->isChecked() ? bg2Blurred : bg2;
        const QImage backdropImage =
            [&backgroundImage, &bottomLayerImage]()
            {
                if (backgroundImage.isNull()) {
                    return bottomLayerImage;
                } else {
                    QImage r = backgroundImage;
                    PixelBlending::blend(bottomLayerImage, r);
                    return r;
                }
            }();
        return LayerBlending::blend(currentLayer, backgroundImage, backdropImage);
    }
};

Window::Window()
    : m_d(new Private)
{
    m_d->background = QImage(":/background").convertToFormat(QImage::Format_ARGB32);
    m_d->currentLayer.setImage(QImage(":/current_layer").convertToFormat(QImage::Format_ARGB32));
    m_d->bg2 = QImage(":/bg_2").convertToFormat(QImage::Format_ARGB32);
    m_d->bg2Blurred = QImage(":/bg_2_blurred").convertToFormat(QImage::Format_ARGB32);
    QImage pattern = QImage(":/pattern").convertToFormat(QImage::Format_ARGB32);
    m_d->currentLayer.setRasterMask(ImgProcUtil::convertImageToMask(QImage(":/mask").convertToFormat(QImage::Format_ARGB32)));
    m_d->currentLayer.setRasterMaskActive(false);
    
    m_d->blendingOptionsWidget = new BlendingOptionsWidget;
    EffectPage *blendingOptionsPage = new EffectPage(m_d->blendingOptionsWidget, false);

    m_d->dropShadowOptionsWidget = new DropShadowOptionsWidget;
    EffectPage *dropShadowOptionsPage = new EffectPage(m_d->dropShadowOptionsWidget);

    m_d->outerGlowOptionsWidget = new OuterGlowOptionsWidget;
    m_d->outerGlowOptionsWidget->setGradient(
        {
            {0.000, Qt::red},
            {0.167, Qt::magenta},
            {0.333, Qt::blue},
            {0.500, Qt::cyan},
            {0.666, Qt::green},
            {0.834, Qt::yellow},
            {1.000, Qt::red}

            // {0.0, QColor(255, 192, 0, 0)},
            // {1.0, QColor(255, 192, 0, 255)}
        }
    );
    EffectPage *outerGlowOptionsPage = new EffectPage(m_d->outerGlowOptionsWidget);

    m_d->patternOverlayOptionsWidget = new PatternOverlayOptionsWidget;
    m_d->patternOverlayOptionsWidget->setPattern(pattern);
    EffectPage *patternOverlayOptionsPage = new EffectPage(m_d->patternOverlayOptionsWidget);

    m_d->gradientOverlayOptionsWidget = new GradientOverlayOptionsWidget;
    m_d->gradientOverlayOptionsWidget->setGradient(
        {
            {0.000, Qt::red},
            {0.167, Qt::yellow},
            {0.333, Qt::green},
            {0.500, Qt::cyan},
            {0.666, Qt::blue},
            {0.834, Qt::magenta},
            {1.000, Qt::red}
        }
    );
    EffectPage *gradientOverlayOptionsPage = new EffectPage(m_d->gradientOverlayOptionsWidget);
    
    m_d->colorOverlayOptionsWidget = new ColorOverlayOptionsWidget;
    EffectPage *colorOverlayOptionsPage = new EffectPage(m_d->colorOverlayOptionsWidget);

    m_d->satinOptionsWidget = new SatinOptionsWidget;
    EffectPage *satinOptionsPage = new EffectPage(m_d->satinOptionsWidget);

    m_d->innerGlowOptionsWidget = new InnerGlowOptionsWidget;
    m_d->innerGlowOptionsWidget->setGradient(
        {
            {0.000, Qt::red},
            {0.167, Qt::magenta},
            {0.333, Qt::blue},
            {0.500, Qt::cyan},
            {0.666, Qt::green},
            {0.834, Qt::yellow},
            {1.000, Qt::red}

            // {0.0, QColor(255, 192, 0, 0)},
            // {1.0, QColor(255, 192, 0, 255)}
        }
    );
    EffectPage *innerGlowOptionsPage = new EffectPage(m_d->innerGlowOptionsWidget);

    m_d->innerShadowOptionsWidget = new InnerShadowOptionsWidget;
    EffectPage *innerShadowOptionsPage = new EffectPage(m_d->innerShadowOptionsWidget);

    m_d->strokeOptionsWidget = new StrokeOptionsWidget;
    m_d->strokeOptionsWidget->setGradient(
        {
            // {0.000, Qt::red},
            // {0.167, Qt::yellow},
            // {0.333, Qt::green},
            // {0.500, Qt::cyan},
            // {0.666, Qt::blue},
            // {0.834, Qt::magenta},
            // {1.000, Qt::red}

            {0.0, QColor(255, 192, 0, 255)},
            {1.0, QColor(255, 192, 0, 0)}
        }
    );
    m_d->strokeOptionsWidget->setPattern(pattern);
    EffectPage *strokeOptionsPage = new EffectPage(m_d->strokeOptionsWidget);

    m_d->bevelAndEmbossOptionsWidget = new BevelAndEmbossOptionsWidget;
    m_d->bevelAndEmbossOptionsWidget->setTexture(pattern);
    EffectPage *bevelAndEmbossOptionsPage = new EffectPage(m_d->bevelAndEmbossOptionsWidget);
    
    m_d->checkBoxBackground = new QCheckBox;
    m_d->checkBoxBackground->setChecked(true);
    m_d->checkBoxBlurredBackdrop = new QCheckBox;
    m_d->checkBoxUseMask = new QCheckBox;

    m_d->comboBoxCurrentLayer = new QComboBox;
    m_d->comboBoxCurrentLayer->addItem("Abstract brown shape", "current_layer");
    m_d->comboBoxCurrentLayer->addItem("Sharp black text", "current_layer_sharp_text");

    QWidget *containerOtherOptions = new QWidget;

    QFormLayout *layoutOtherOptions1 = new QFormLayout;
    layoutOtherOptions1->setContentsMargins(10, 10, 10, 10);
    layoutOtherOptions1->setSpacing(5);
    layoutOtherOptions1->addRow("Show background:", m_d->checkBoxBackground);
    layoutOtherOptions1->addRow("Blurred backdrop:", m_d->checkBoxBlurredBackdrop);
    layoutOtherOptions1->addRow("Current Layer:", m_d->comboBoxCurrentLayer);
    layoutOtherOptions1->addRow("Use mask:", m_d->checkBoxUseMask);

    QVBoxLayout *layoutOtherOptions2 = new QVBoxLayout;
    layoutOtherOptions2->setContentsMargins(0, 0, 0, 0);
    layoutOtherOptions2->setSpacing(0);
    layoutOtherOptions2->addLayout(layoutOtherOptions1);
    layoutOtherOptions2->addStretch();
    containerOtherOptions->setLayout(layoutOtherOptions2);

    addControlsPanel(blendingOptionsPage, "Blending");
    addControlsPanel(dropShadowOptionsPage, "Drop Shadow");
    addControlsPanel(outerGlowOptionsPage, "Outer Glow");
    addControlsPanel(patternOverlayOptionsPage, "Pattern Overlay");
    addControlsPanel(gradientOverlayOptionsPage, "Gradient Overlay");
    addControlsPanel(colorOverlayOptionsPage, "Color Overlay");
    addControlsPanel(satinOptionsPage, "Satin");
    addControlsPanel(innerGlowOptionsPage, "Inner Glow");
    addControlsPanel(innerShadowOptionsPage, "Inner Shadow");
    addControlsPanel(strokeOptionsPage, "Stroke");
    addControlsPanel(bevelAndEmbossOptionsPage, "Bevel and Emboss");
    addControlsPanel(containerOtherOptions, "Other");

    viewport()->setScaleCheckerBoardPattern(true);
    viewport()->setImage(m_d->generateImage());

    sideBar()->setDocumentMode(true);
    sideBar()->resize(400, 600);

    m_d->currentLayer.setBlendingOptions(m_d->blendingOptionsWidget->params());
    m_d->currentLayer.setDropShadowOptions(m_d->dropShadowOptionsWidget->params());
    m_d->currentLayer.setOuterGlowOptions(m_d->outerGlowOptionsWidget->params());
    m_d->currentLayer.setPatternOverlayOptions(m_d->patternOverlayOptionsWidget->params());
    m_d->currentLayer.setGradientOverlayOptions(m_d->gradientOverlayOptionsWidget->params());
    m_d->currentLayer.setColorOverlayOptions(m_d->colorOverlayOptionsWidget->params());
    m_d->currentLayer.setSatinOptions(m_d->satinOptionsWidget->params());
    m_d->currentLayer.setInnerGlowOptions(m_d->innerGlowOptionsWidget->params());
    m_d->currentLayer.setInnerShadowOptions(m_d->innerShadowOptionsWidget->params());
    m_d->currentLayer.setStrokeOptions(m_d->strokeOptionsWidget->params());
    m_d->currentLayer.setBevelAndEmbossOptions(m_d->bevelAndEmbossOptionsWidget->params());

    connect(
        m_d->blendingOptionsWidget, &BlendingOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setBlendingOptions(m_d->blendingOptionsWidget->params());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->dropShadowOptionsWidget, &DropShadowOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setDropShadowOptions(m_d->dropShadowOptionsWidget->params());
            if (m_d->currentLayer.isDropShadowActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        dropShadowOptionsPage, &EffectPage::activeChanged,
        [this, dropShadowOptionsPage]()
        {
            m_d->currentLayer.setDropShadowActive(dropShadowOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->outerGlowOptionsWidget, &OuterGlowOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setOuterGlowOptions(m_d->outerGlowOptionsWidget->params());
            if (m_d->currentLayer.isOuterGlowActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        outerGlowOptionsPage, &EffectPage::activeChanged,
        [this, outerGlowOptionsPage]()
        {
            m_d->currentLayer.setOuterGlowActive(outerGlowOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->patternOverlayOptionsWidget, &PatternOverlayOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setPatternOverlayOptions(m_d->patternOverlayOptionsWidget->params());
            if (m_d->currentLayer.isPatternOverlayActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        patternOverlayOptionsPage, &EffectPage::activeChanged,
        [this, patternOverlayOptionsPage]()
        {
            m_d->currentLayer.setPatternOverlayActive(patternOverlayOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->gradientOverlayOptionsWidget, &GradientOverlayOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setGradientOverlayOptions(m_d->gradientOverlayOptionsWidget->params());
            if (m_d->currentLayer.isGradientOverlayActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        gradientOverlayOptionsPage, &EffectPage::activeChanged,
        [this, gradientOverlayOptionsPage]()
        {
            m_d->currentLayer.setGradientOverlayActive(gradientOverlayOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->colorOverlayOptionsWidget, &ColorOverlayOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setColorOverlayOptions(m_d->colorOverlayOptionsWidget->params());
            if (m_d->currentLayer.isColorOverlayActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        colorOverlayOptionsPage, &EffectPage::activeChanged,
        [this, colorOverlayOptionsPage]()
        {
            m_d->currentLayer.setColorOverlayActive(colorOverlayOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->satinOptionsWidget, &SatinOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setSatinOptions(m_d->satinOptionsWidget->params());
            if (m_d->currentLayer.isSatinActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        satinOptionsPage, &EffectPage::activeChanged,
        [this, satinOptionsPage]()
        {
            m_d->currentLayer.setSatinActive(satinOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->innerGlowOptionsWidget, &InnerGlowOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setInnerGlowOptions(m_d->innerGlowOptionsWidget->params());
            if (m_d->currentLayer.isInnerGlowActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        innerGlowOptionsPage, &EffectPage::activeChanged,
        [this, innerGlowOptionsPage]()
        {
            m_d->currentLayer.setInnerGlowActive(innerGlowOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->innerShadowOptionsWidget, &InnerShadowOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setInnerShadowOptions(m_d->innerShadowOptionsWidget->params());
            if (m_d->currentLayer.isInnerShadowActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        innerShadowOptionsPage, &EffectPage::activeChanged,
        [this, innerShadowOptionsPage]()
        {
            m_d->currentLayer.setInnerShadowActive(innerShadowOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->strokeOptionsWidget, &StrokeOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setStrokeOptions(m_d->strokeOptionsWidget->params());
            if (m_d->currentLayer.isStrokeActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        strokeOptionsPage, &EffectPage::activeChanged,
        [this, strokeOptionsPage]()
        {
            m_d->currentLayer.setStrokeActive(strokeOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(
        m_d->bevelAndEmbossOptionsWidget, &BevelAndEmbossOptionsWidget::paramsChanged,
        [this]()
        {
            m_d->currentLayer.setBevelAndEmbossOptions(m_d->bevelAndEmbossOptionsWidget->params());
            if (m_d->currentLayer.isBevelAndEmbossActive()) {
                requestViewportUpdate();
            }
        }
    );
    connect(
        bevelAndEmbossOptionsPage, &EffectPage::activeChanged,
        [this, bevelAndEmbossOptionsPage]()
        {
            m_d->currentLayer.setBevelAndEmbossActive(bevelAndEmbossOptionsPage->isActive());
            requestViewportUpdate();
        }
    );
    connect(m_d->checkBoxBackground, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxBlurredBackdrop, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(
        m_d->comboBoxCurrentLayer, QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int index)
        {
            m_d->currentLayer.setImage(
                QImage(":/" + m_d->comboBoxCurrentLayer->currentData().toString())
                .convertToFormat(QImage::Format_ARGB32)
            );
            requestViewportUpdate();
        }
    );
    connect(
        m_d->checkBoxUseMask, &QCheckBox::toggled,
        [this](bool checked)
        {
            m_d->currentLayer.setRasterMaskActive(checked);
            requestViewportUpdate();
        }
    );
    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );
}

Window::~Window()
{}
