/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QHBoxLayout>
#include <QCheckBox>

#include "ColorBlendMethodComboBox.h"
#include "BevelAndEmbossStyleComboBox.h"
#include "BevelAndEmbossTechniqueComboBox.h"
#include "BevelAndEmbossDirectionComboBox.h"
#include "ColorButton.h"
#include "SliderSpinBox.h"
#include "CurveButton.h"

#include "BevelAndEmbossOptionsWidget.h"

class Q_DECL_HIDDEN BevelAndEmbossOptionsWidget::Private
{
public:
    BevelAndEmbossStyleComboBox *comboBoxStyle {nullptr};
    BevelAndEmbossTechniqueComboBox *comboBoxTechnique {nullptr};
    SliderSpinBox *sliderDepth {nullptr};
    BevelAndEmbossDirectionComboBox *comboBoxDirection {nullptr};
    SliderSpinBox *sliderSize {nullptr};
    SliderSpinBox *sliderSoften {nullptr};
    SliderSpinBox *sliderLightAngle {nullptr};
    SliderSpinBox *sliderLightAltitude {nullptr};
    QCheckBox *checkBoxProfile {nullptr};
    CurveButton *buttonProfileCurve {nullptr};
    QCheckBox *checkBoxProfileAntiAlias {nullptr};
    SliderSpinBox *sliderProfileRange {nullptr};
    QCheckBox *checkBoxTexture {nullptr};
    SliderSpinBox *sliderTextureScale {nullptr};
    SliderSpinBox *sliderTextureDepth {nullptr};
    QCheckBox *checkBoxInvertTexture {nullptr};
    QCheckBox *checkBoxLinkTextureWithLayer {nullptr};
    CurveButton *buttonGlossCurve {nullptr};
    QCheckBox *checkBoxGlossAntiAlias {nullptr};
    ColorBlendMethodComboBox *comboBoxLightBlendMode {nullptr};
    ColorButton *buttonLightColor {nullptr};
    SliderSpinBox *sliderLightOpacity {nullptr};
    ColorBlendMethodComboBox *comboBoxShadowBlendMode {nullptr};
    ColorButton *buttonShadowColor {nullptr};
    SliderSpinBox *sliderShadowOpacity {nullptr};
    QImage texture;
};

BevelAndEmbossOptionsWidget::BevelAndEmbossOptionsWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    LayerStyles::BevelAndEmboss defaultParams;

    m_d->comboBoxStyle = new BevelAndEmbossStyleComboBox;
    m_d->comboBoxStyle->setCurrentStyle(defaultParams.style);

    m_d->comboBoxTechnique = new BevelAndEmbossTechniqueComboBox;
    m_d->comboBoxTechnique->setCurrentTechnique(defaultParams.technique);

    m_d->sliderDepth = new SliderSpinBox;
    m_d->sliderDepth->setRange(1, 1000);
    m_d->sliderDepth->setValue(defaultParams.depth);

    m_d->comboBoxDirection = new BevelAndEmbossDirectionComboBox;
    m_d->comboBoxDirection->setCurrentDirection(defaultParams.direction);

    m_d->sliderSize = new SliderSpinBox;
    m_d->sliderSize->setRange(0, 250);
    m_d->sliderSize->setValue(defaultParams.size);

    m_d->sliderSoften = new SliderSpinBox;
    m_d->sliderSoften->setRange(0, 16);
    m_d->sliderSoften->setValue(defaultParams.soften);

    m_d->sliderLightAngle = new SliderSpinBox;
    m_d->sliderLightAngle->setRange(0, 360);
    m_d->sliderLightAngle->setValue(defaultParams.lightAngle);

    m_d->sliderLightAltitude = new SliderSpinBox;
    m_d->sliderLightAltitude->setRange(0, 90);
    m_d->sliderLightAltitude->setValue(defaultParams.lightAltitude);

    m_d->checkBoxProfile = new QCheckBox;
    m_d->checkBoxProfile->setChecked(defaultParams.applyProfileContour);

    m_d->buttonProfileCurve = new CurveButton;
    m_d->buttonProfileCurve->setCurve(defaultParams.profileCurve);

    m_d->checkBoxProfileAntiAlias = new QCheckBox("Anti-alias");
    m_d->checkBoxProfileAntiAlias->setChecked(defaultParams.antiAliasedProfileCurve);

    QWidget *containerProfileCurve = new QWidget;
    QHBoxLayout *layoutContainerProfileCurve = new QHBoxLayout;
    layoutContainerProfileCurve->setContentsMargins(0, 0, 0, 0);
    layoutContainerProfileCurve->setSpacing(5);
    layoutContainerProfileCurve->addWidget(m_d->buttonProfileCurve);
    layoutContainerProfileCurve->addWidget(m_d->checkBoxProfileAntiAlias);
    containerProfileCurve->setLayout(layoutContainerProfileCurve);

    m_d->sliderProfileRange = new SliderSpinBox;
    m_d->sliderProfileRange->setRange(0, 100);
    m_d->sliderProfileRange->setValue(defaultParams.profileRange);

    m_d->checkBoxTexture = new QCheckBox;
    m_d->checkBoxTexture->setChecked(defaultParams.applyTexture);

    m_d->texture = defaultParams.texture;

    m_d->sliderTextureScale = new SliderSpinBox;
    m_d->sliderTextureScale->setRange(1, 1000);
    m_d->sliderTextureScale->setValue(defaultParams.textureScale);

    m_d->sliderTextureDepth = new SliderSpinBox;
    m_d->sliderTextureDepth->setRange(-1000, 1000);
    m_d->sliderTextureDepth->setValue(defaultParams.textureDepth);

    m_d->checkBoxInvertTexture = new QCheckBox;
    m_d->checkBoxInvertTexture->setChecked(defaultParams.invertTexture);

    m_d->checkBoxLinkTextureWithLayer = new QCheckBox;
    m_d->checkBoxLinkTextureWithLayer->setChecked(defaultParams.linkTextureWithLayer);

    m_d->buttonGlossCurve = new CurveButton;

    m_d->checkBoxGlossAntiAlias = new QCheckBox("Anti-alias");

    QWidget *containerGlossCurve = new QWidget;
    QHBoxLayout *layoutContainerGlossCurve = new QHBoxLayout;
    layoutContainerGlossCurve->setContentsMargins(0, 0, 0, 0);
    layoutContainerGlossCurve->setSpacing(5);
    layoutContainerGlossCurve->addWidget(m_d->buttonGlossCurve);
    layoutContainerGlossCurve->addWidget(m_d->checkBoxGlossAntiAlias);
    containerGlossCurve->setLayout(layoutContainerGlossCurve);

    m_d->comboBoxLightBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxLightBlendMode->setCurrentColorBlendMethod(defaultParams.lightBlendMode);

    m_d->buttonLightColor = new ColorButton;
    m_d->buttonLightColor->setColor(defaultParams.lightColor);

    m_d->sliderLightOpacity = new SliderSpinBox;
    m_d->sliderLightOpacity->setRange(0, 100);
    m_d->sliderLightOpacity->setValue(defaultParams.lightOpacity);

    m_d->comboBoxShadowBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxShadowBlendMode->setCurrentColorBlendMethod(defaultParams.shadowBlendMode);

    m_d->buttonShadowColor = new ColorButton;
    m_d->buttonShadowColor->setColor(defaultParams.shadowColor);

    m_d->sliderShadowOpacity = new SliderSpinBox;
    m_d->sliderShadowOpacity->setRange(0, 100);
    m_d->sliderShadowOpacity->setValue(defaultParams.shadowOpacity);

    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addRow("Style:", m_d->comboBoxStyle);
    mainLayout->addRow("Technique:", m_d->comboBoxTechnique);
    mainLayout->addRow("Depth:", m_d->sliderDepth);
    mainLayout->addRow("Direction:", m_d->comboBoxDirection);
    mainLayout->addRow("Size:", m_d->sliderSize);
    mainLayout->addRow("Soften:", m_d->sliderSoften);
    mainLayout->addRow("Angle:", m_d->sliderLightAngle);
    mainLayout->addRow("Elevation:", m_d->sliderLightAltitude);
    mainLayout->addRow("Use profile contour:", m_d->checkBoxProfile);
    mainLayout->addRow("Profile curve:", containerProfileCurve);
    mainLayout->addRow("Profile range:", m_d->sliderProfileRange);
    mainLayout->addRow("Use texture:", m_d->checkBoxTexture);
    mainLayout->addRow("Texture scale:", m_d->sliderTextureScale);
    mainLayout->addRow("Texture depth:", m_d->sliderTextureDepth);
    mainLayout->addRow("Invert texture:", m_d->checkBoxInvertTexture);
    mainLayout->addRow("Link texture with layer:", m_d->checkBoxLinkTextureWithLayer);
    mainLayout->addRow("Gloss Contour:", containerGlossCurve);
    mainLayout->addRow("Light side blend mode:", m_d->comboBoxLightBlendMode);
    mainLayout->addRow("Light side color:", m_d->buttonLightColor);
    mainLayout->addRow("Light side opacity:", m_d->sliderLightOpacity);
    mainLayout->addRow("Shadow side blend mode:", m_d->comboBoxShadowBlendMode);
    mainLayout->addRow("Shadow side color:", m_d->buttonShadowColor);
    mainLayout->addRow("Shadow side opacity:", m_d->sliderShadowOpacity);

    setLayout(mainLayout);

    connect(m_d->comboBoxStyle, SIGNAL(currentStyleChanged(LayerStyles::BevelAndEmbossStyle)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxTechnique, SIGNAL(currentTechniqueChanged(LayerStyles::BevelAndEmbossTechnique)), SIGNAL(paramsChanged()));
    connect(m_d->sliderDepth, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxDirection, SIGNAL(currentDirectionChanged(LayerStyles::BevelAndEmbossDirection)), SIGNAL(paramsChanged()));
    connect(m_d->sliderSize, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderSoften, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderLightAngle, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderLightAltitude, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxProfile, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->buttonProfileCurve, SIGNAL(curveChanged(const Curve&)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxProfileAntiAlias, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->sliderProfileRange, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxTexture, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->sliderTextureScale, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderTextureDepth, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxInvertTexture, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxLinkTextureWithLayer, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->buttonGlossCurve, SIGNAL(curveChanged(const Curve&)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxGlossAntiAlias, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxLightBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->buttonLightColor, SIGNAL(colorChanged(const QColor&)), SIGNAL(paramsChanged()));
    connect(m_d->sliderLightOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxShadowBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->buttonShadowColor, SIGNAL(colorChanged(const QColor&)), SIGNAL(paramsChanged()));
    connect(m_d->sliderShadowOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
}

BevelAndEmbossOptionsWidget::~BevelAndEmbossOptionsWidget()
{}

LayerStyles::BevelAndEmboss BevelAndEmbossOptionsWidget::params() const
{
    LayerStyles::BevelAndEmboss params;

    params.style = m_d->comboBoxStyle->currentStyle();
    params.technique = m_d->comboBoxTechnique->currentTechnique();
    params.depth = m_d->sliderDepth->value();
    params.direction = m_d->comboBoxDirection->currentDirection();
    params.size = m_d->sliderSize->value();
    params.soften = m_d->sliderSoften->value();
    params.lightAngle = m_d->sliderLightAngle->value();
    params.lightAltitude = m_d->sliderLightAltitude->value();
    params.applyProfileContour = m_d->checkBoxProfile->isChecked();
    params.profileCurve = m_d->buttonProfileCurve->curve();
    params.antiAliasedProfileCurve = m_d->checkBoxProfileAntiAlias->isChecked();
    params.profileRange = m_d->sliderProfileRange->value();
    params.applyTexture = m_d->checkBoxTexture->isChecked();
    params.texture = m_d->texture;
    params.textureScale = m_d->sliderTextureScale->value();
    params.textureDepth = m_d->sliderTextureDepth->value();
    params.invertTexture = m_d->checkBoxInvertTexture->isChecked();
    params.linkTextureWithLayer = m_d->checkBoxLinkTextureWithLayer->isChecked();
    params.glossCurve = m_d->buttonGlossCurve->curve();
    params.antiAliasedGlossCurve = m_d->checkBoxGlossAntiAlias->isChecked();
    params.lightBlendMode = m_d->comboBoxLightBlendMode->currentColorBlendMethod();
    params.lightColor = m_d->buttonLightColor->color();
    params.lightOpacity = m_d->sliderLightOpacity->value();
    params.shadowBlendMode = m_d->comboBoxShadowBlendMode->currentColorBlendMethod();
    params.shadowColor = m_d->buttonShadowColor->color();
    params.shadowOpacity = m_d->sliderShadowOpacity->value();
    
    return params;
}

void BevelAndEmbossOptionsWidget::setTexture(const QImage &texture)
{
    m_d->texture = texture;
    emit paramsChanged();
}
