/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "ColorBlendMethodComboBox.h"

ColorBlendMethodComboBox::ColorBlendMethodComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Normal", static_cast<int>(PixelBlending::Normal));
    insertSeparator(count());
    addItem("Darken", static_cast<int>(PixelBlending::Darken));
    addItem("Multiply", static_cast<int>(PixelBlending::Multiply));
    addItem("Color Burn", static_cast<int>(PixelBlending::ColorBurn));
    addItem("Linear Burn", static_cast<int>(PixelBlending::LinearBurn));
    addItem("Darker Color", static_cast<int>(PixelBlending::DarkerColor));
    insertSeparator(count());
    addItem("Lighten", static_cast<int>(PixelBlending::Lighten));
    addItem("Screen", static_cast<int>(PixelBlending::Screen));
    addItem("Color Dodge", static_cast<int>(PixelBlending::ColorDodge));
    addItem("Linear Dodge", static_cast<int>(PixelBlending::LinearDodge));
    addItem("Lighter Color", static_cast<int>(PixelBlending::LighterColor));
    insertSeparator(count());
    addItem("Overlay", static_cast<int>(PixelBlending::Overlay));
    addItem("Soft Light", static_cast<int>(PixelBlending::SoftLight));
    addItem("Hard Light", static_cast<int>(PixelBlending::HardLight));
    addItem("Vivid Light", static_cast<int>(PixelBlending::VividLight));
    addItem("Linear Light", static_cast<int>(PixelBlending::LinearLight));
    addItem("Pin Light", static_cast<int>(PixelBlending::PinLight));
    addItem("Hard Mix", static_cast<int>(PixelBlending::HardMix));
    insertSeparator(count());
    addItem("Difference", static_cast<int>(PixelBlending::Difference));
    addItem("Exclusion", static_cast<int>(PixelBlending::Exclusion));
    addItem("Subtract", static_cast<int>(PixelBlending::Subtract));
    addItem("Divide", static_cast<int>(PixelBlending::Divide));
    insertSeparator(count());
    addItem("Hue", static_cast<int>(PixelBlending::Hue));
    addItem("Saturation", static_cast<int>(PixelBlending::Saturation));
    addItem("Color", static_cast<int>(PixelBlending::Color));
    addItem("Luminosity", static_cast<int>(PixelBlending::Luminosity));
    
    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentColorBlendMethodChanged(static_cast<PixelBlending::ColorBlendMethod>(currentData().toInt()));
        }
    );
}

ColorBlendMethodComboBox::~ColorBlendMethodComboBox()
{}

PixelBlending::ColorBlendMethod ColorBlendMethodComboBox::currentColorBlendMethod() const
{
    return static_cast<PixelBlending::ColorBlendMethod>(currentData().toInt());
}

void ColorBlendMethodComboBox::setCurrentColorBlendMethod(PixelBlending::ColorBlendMethod newColorBlendMethod)
{
    setCurrentIndex(findData(static_cast<int>(newColorBlendMethod)));
}
