/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QCheckBox>
#include <QVBoxLayout>
#include <QScrollArea>

#include "EffectPage.h"

class Q_DECL_HIDDEN EffectPage::Private
{
public:
    bool showActive {true};
    QCheckBox *checkBoxActive {nullptr};
    QWidget *effectWidget {nullptr};
};

EffectPage::EffectPage(QWidget *effectWidget, bool showActive, QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    m_d->showActive = showActive;

    QVBoxLayout *layoutCheckBoxActive = new QVBoxLayout;
    QFrame *separator;
    if (showActive) {
        m_d->checkBoxActive = new QCheckBox("Active");
        layoutCheckBoxActive->setContentsMargins(10, 10, 10, 5);
        layoutCheckBoxActive->setSpacing(0);
        layoutCheckBoxActive->addWidget(m_d->checkBoxActive);

        separator = new QFrame;
        separator->setFrameStyle(QFrame::HLine | QFrame::Sunken);
    }

    m_d->effectWidget = effectWidget;
    m_d->effectWidget->setContentsMargins(10, 10, 10, 10);
    QScrollArea *scrollAreaEffectWidget = new QScrollArea;
    scrollAreaEffectWidget->setWidgetResizable(true);
    scrollAreaEffectWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollAreaEffectWidget->setFrameStyle(QFrame::NoFrame | QFrame::Plain);
    scrollAreaEffectWidget->setWidget(m_d->effectWidget);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    if (showActive) {
        mainLayout->addLayout(layoutCheckBoxActive, 0);
        mainLayout->addWidget(separator);
    }
    mainLayout->addWidget(scrollAreaEffectWidget, 1);

    setLayout(mainLayout);

    if (showActive) {
        connect(m_d->checkBoxActive, SIGNAL(toggled(bool)), SIGNAL(activeChanged()));
    }
}

EffectPage::~EffectPage()
{}

bool EffectPage::isActive() const
{
    return m_d->showActive ? m_d->checkBoxActive->isChecked() : false;
}
