/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QPainter>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QCheckBox>

#include <Viewport.h>
#include <StrokeOptionsWidget.h>
#include <BlendingOptionsWidget.h>
#include <ImgProcUtil.h>
#include <LayerStyles.h>
#include <PixelBlending.h>

#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    StrokeOptionsWidget *strokeOptionsWidget {nullptr};
    BlendingOptionsWidget *blendingOptionsWidget {nullptr};
    QCheckBox *checkBoxBackground {nullptr};
    QCheckBox *checkBoxBlurredBackdrop {nullptr};
    QCheckBox *checkBoxUseMask {nullptr};
    QImage background;
    QImage bg2;
    QImage bg2Blurred;
    QImage currentLayer;
    QImage mask;

    QImage generateImage() const
    {
        using namespace PixelBlending;
        using namespace LayerStyles;
        using namespace LayerStyles;

        const Stroke strokeOptions = strokeOptionsWidget->params();
        const BlendingOptions blendingOptions = blendingOptionsWidget->params();

        const bool backgroundLayerIsVisible = checkBoxBackground->isChecked();
        const bool blurredBackdrop = checkBoxBlurredBackdrop->isChecked();
        const bool useMask = checkBoxUseMask->isChecked();

        // The mask representing the shape of the current layer
        QImage currentLayerShape;
        if (blendingOptions.transparencyShapesLayer) {
            currentLayerShape = currentLayer.convertToFormat(QImage::Format_Alpha8);
        } else {
            currentLayerShape = QImage(currentLayer.size(), QImage::Format_Alpha8);
            currentLayerShape.fill(255);
        }
        if (!blendingOptions.layerMaskHidesEffects && useMask) {
            multiplyMasks(currentLayerShape, mask);
        }
        const quint8 currentLayerOpacity = static_cast<quint8>(blendingOptions.opacity * 255 / 100.0);
        const quint8 currentLayerFillOpacity = static_cast<quint8>(blendingOptions.fillOpacity * 255 / 100.0);
        // The implicit layer that contains the computed stroke shape
        QPair<QImage, QImage> strokeOutput = generateStrokeImage(strokeOptions, currentLayerShape);
        QImage strokeImplicitLayer = strokeOutput.first;
        const QImage strokeShape = strokeOutput.second;
        const quint8 strokeOpacity =
            static_cast<quint8>(
                strokeOptions.overprint
                ? strokeOptions.opacity * blendingOptions.opacity * 255 / 10000
                : strokeOptions.opacity * 255 / 100
            );

        // The fully opaque, non-knockable background layer
        const QImage backgroundLayer = backgroundLayerIsVisible ? background : QImage();
        // The projection up to the current layer
        QImage projection;
        if (backgroundLayerIsVisible) {
            projection = background;
            blend(blurredBackdrop ? bg2Blurred : bg2, projection);
        } else {
            projection = blurredBackdrop ? bg2Blurred : bg2;
        }
        // The helper images that contain the composited result and the
        // knocked out region respectively
        QImage composition = projection;
        QImage knockoutComposition;
        // Generate the current layer composition        
        if (blendingOptions.knockout == Knockout_None) {
            // Use the overall projection as backdrop for the knockout
            knockoutComposition = projection;
        } else {
            // Use the background layer or a transparent image as backdrop
            // for the knockout
            if (backgroundLayerIsVisible) {
                knockoutComposition = background;
            } else {
                knockoutComposition = QImage(background.size(), QImage::Format_ARGB32);
                knockoutComposition.fill(0);
            }
        }
        if (strokeOptions.position == StrokePosition_Outside) {
            multiplyAlphaChannel(strokeImplicitLayer, strokeShape);
            blend(strokeImplicitLayer, composition, SourceOverDestination,
                  strokeOptions.blendMode, true, strokeOpacity);
            blend(currentLayer, knockoutComposition,
                  SourceOverDestination, blendingOptions.blendMode, true,
                  currentLayerFillOpacity, blendingOptions.transparencyShapesLayer,
                  blendingOptions.sourceTonalRanges);
            // This is basically the same as using the following Porter-Duff operators:
            // (composition OUT currentLayer) PLUS (layerComposition IN currentLayer)
            crossDissolve(knockoutComposition, composition, currentLayerShape);
        } else if (strokeOptions.position == StrokePosition_Inside) {
            if (strokeOptions.overprint) {
                multiplyAlphaChannel(strokeImplicitLayer, strokeShape);
                blend(currentLayer, knockoutComposition,
                      SourceOverDestination, blendingOptions.blendMode, true,
                      currentLayerFillOpacity, blendingOptions.transparencyShapesLayer,
                      blendingOptions.sourceTonalRanges);
                blend(strokeImplicitLayer, knockoutComposition, SourceOverDestination,
                      strokeOptions.blendMode, true, strokeOpacity);
                crossDissolve(knockoutComposition, composition, currentLayerShape);
            } else {
                blend(currentLayer, knockoutComposition,
                      SourceOverDestination, blendingOptions.blendMode, true,
                      currentLayerFillOpacity, blendingOptions.transparencyShapesLayer,
                      blendingOptions.sourceTonalRanges);
                QImage strokeComposition = projection;
                blend(strokeImplicitLayer, strokeComposition, SourceOverDestination,
                      strokeOptions.blendMode, true, strokeOpacity);
                crossDissolve(strokeComposition, knockoutComposition, strokeShape);
                crossDissolve(knockoutComposition, composition, currentLayerShape);
            }
        } else {
            if (strokeOptions.overprint) {
                multiplyAlphaChannel(strokeImplicitLayer, strokeShape);
                blend(currentLayer, knockoutComposition,
                      SourceOverDestination, blendingOptions.blendMode, true,
                      currentLayerFillOpacity, blendingOptions.transparencyShapesLayer,
                      blendingOptions.sourceTonalRanges);
                crossDissolve(knockoutComposition, composition, currentLayerShape);
                blend(strokeImplicitLayer, composition, SourceOverDestination,
                      strokeOptions.blendMode, true, strokeOpacity);
            } else {
                blend(currentLayer, knockoutComposition,
                      SourceOverDestination, blendingOptions.blendMode, true,
                      currentLayerFillOpacity, blendingOptions.transparencyShapesLayer,
                      blendingOptions.sourceTonalRanges);
                // if (blendingOptions.knockout != Knockout_None) {
                    crossDissolve(projection, knockoutComposition, currentLayerShape, true);
                // }
                QImage strokeComposition = projection;
                blend(strokeImplicitLayer, composition, SourceOverDestination,
                      strokeOptions.blendMode, true, strokeOpacity);
                crossDissolve(knockoutComposition, composition, strokeShape, true);
            }
        }
        // Blend the composition with the overall projection
        blendWithBackdrop(composition, projection,
                          blendingOptions.layerMaskHidesEffects && useMask ? mask : QImage(),
                          blendingOptions.channels, currentLayerOpacity,
                          blendingOptions.destinationTonalRanges);

        return projection;
    }
};

Window::Window()
    : m_d(new Private)
{
    m_d->background = QImage(":/background").convertToFormat(QImage::Format_ARGB32);
    m_d->bg2 = QImage(":/bg_2").convertToFormat(QImage::Format_ARGB32);
    m_d->bg2Blurred = QImage(":/bg_2_blurred").convertToFormat(QImage::Format_ARGB32);
    m_d->currentLayer = QImage(":/current_layer").convertToFormat(QImage::Format_ARGB32);
    const QImage pattern = QImage(":/pattern").convertToFormat(QImage::Format_ARGB32);
    m_d->mask = ImgProcUtil::convertImageToMask(QImage(":/mask").convertToFormat(QImage::Format_ARGB32));

    m_d->strokeOptionsWidget = new StrokeOptionsWidget;
    m_d->strokeOptionsWidget->setContentsMargins(10, 10, 10, 10);
    m_d->strokeOptionsWidget->setGradient(
        {
            // {0.000, Qt::red},
            // {0.167, Qt::yellow},
            // {0.333, Qt::green},
            // {0.500, Qt::cyan},
            // {0.666, Qt::blue},
            // {0.834, Qt::magenta},
            // {1.000, Qt::red}

            {0.0, QColor(255, 192, 0, 255)},
            {1.0, QColor(255, 192, 0, 0)}
        }
    );
    m_d->strokeOptionsWidget->setPattern(pattern);
    m_d->blendingOptionsWidget = new BlendingOptionsWidget;
    m_d->blendingOptionsWidget->setContentsMargins(10, 10, 10, 10);

    m_d->checkBoxBackground = new QCheckBox;
    m_d->checkBoxBackground->setChecked(true);
    m_d->checkBoxBlurredBackdrop = new QCheckBox;
    m_d->checkBoxUseMask = new QCheckBox;

    QWidget *containerOtherOptions = new QWidget;

    QFormLayout *layoutOtherOptions1 = new QFormLayout;
    layoutOtherOptions1->setContentsMargins(10, 10, 10, 10);
    layoutOtherOptions1->setSpacing(5);
    layoutOtherOptions1->addRow("Show background:", m_d->checkBoxBackground);
    layoutOtherOptions1->addRow("Blurred backdrop:", m_d->checkBoxBlurredBackdrop);
    layoutOtherOptions1->addRow("Use mask:", m_d->checkBoxUseMask);

    QVBoxLayout *layoutOtherOptions2 = new QVBoxLayout;
    layoutOtherOptions2->setContentsMargins(0, 0, 0, 0);
    layoutOtherOptions2->setSpacing(0);
    layoutOtherOptions2->addLayout(layoutOtherOptions1);
    layoutOtherOptions2->addStretch();
    containerOtherOptions->setLayout(layoutOtherOptions2);

    addControlsPanel(m_d->strokeOptionsWidget, "Stroke");
    addControlsPanel(m_d->blendingOptionsWidget, "Blending");
    addControlsPanel(containerOtherOptions, "Other");

    viewport()->setScaleCheckerBoardPattern(true);
    viewport()->setImage(m_d->generateImage());

    connect(m_d->strokeOptionsWidget, SIGNAL(paramsChanged()),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->blendingOptionsWidget, SIGNAL(paramsChanged()),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxBackground, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxBlurredBackdrop, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxUseMask, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );
}

Window::~Window()
{}
