/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QPainter>
#include <QPolygonF>
#include <QPainterPath>
#include <QMouseEvent>

#include "CurveWidget.h"

class Q_DECL_HIDDEN CurveWidget::Private
{
public:
    static constexpr qreal neighborKnotMargin = 0.02;

    Curve curve;
    qint32 selectedKnotIndex {0};
    bool isDragging {false};
    QPointF lastPosition;
};

CurveWidget::CurveWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{}

CurveWidget::~CurveWidget()
{}

const Curve& CurveWidget::curve() const
{
    return m_d->curve;
}

const Curve::KnotList& CurveWidget::knots() const
{
    return m_d->curve.knots();
}

const Curve::Knot& CurveWidget::knot(qint32 index) const
{
    return m_d->curve.knot(index);
}

qint32 CurveWidget::selectedKnotIndex() const
{
    return m_d->selectedKnotIndex;
}

const Curve::Knot& CurveWidget::selectedKnot() const
{
    return knot(selectedKnotIndex());
}

qint32 CurveWidget::size() const
{
    return m_d->curve.size();
}

void CurveWidget::setCurve(const Curve &newCurve)
{
    if (newCurve.size() < 2) {
        return;
    }
    m_d->curve = newCurve;
    m_d->selectedKnotIndex = 0;
    update();
    emit curveChanged(curve());
    emit selectedKnotChanged(0);
}

void CurveWidget::setCurve(Curve &&newCurve)
{
    if (newCurve.size() < 2) {
        return;
    }
    m_d->curve = std::move(newCurve);
    m_d->selectedKnotIndex = 0;
    update();
    emit curveChanged(curve());
    emit selectedKnotChanged(0);
}

void CurveWidget::setKnots(const Curve::KnotList &knotList)
{
    if (knotList.size() < 2) {
        return;
    }
    m_d->curve.setKnots(knotList);
    m_d->selectedKnotIndex = 0;
    update();
    emit curveChanged(curve());
    emit selectedKnotChanged(0);
}

void CurveWidget::setKnotAsCorner(qint32 index, bool isCorner)
{
    if (knot(index).isCorner == isCorner) {
        return;
    }
    m_d->curve.setKnotAsCorner(index, isCorner);
    emit curveChanged(curve());
    update();
}

void CurveWidget::moveKnot(qint32 index, qreal x, qreal y)
{
    if (knot(index).x == x && knot(index).y == y) {
        return;
    }
    const qreal minX = index == 0 ? 0.0 : knot(index - 1).x + m_d->neighborKnotMargin;
    const qreal maxX = index == size() - 1 ? 1.0 : knot(index + 1).x - m_d->neighborKnotMargin;
    x = qMin(maxX, qMax(minX, x));
    y = qMin(1.0, qMax(0.0, y));
    m_d->curve.setKnot(index, x, y);
    emit curveChanged(curve());
    update();
}

qint32 CurveWidget::addKnot(const Curve::Knot &knot)
{
    if (knot.x < 0.0 || knot.x > 1.0) {
        return -1;
    }
    qreal minX;
    qreal maxX;
    if (knot.x <= this->knot(0).x) {
        minX = 0.0;
        maxX = this->knot(0).x - m_d->neighborKnotMargin;
        if (maxX < minX) {
            return -1;
        }
    } else if (knot.x >= this->knot(size() - 1).x) {
        minX = this->knot(size() - 1).x + m_d->neighborKnotMargin;
        maxX = 1.0;
        if (maxX < minX) {
            return -1;
        }
    } else {
        for (qint32 i = 1; i < size(); ++i) {
            if (knot.x <= this->knot(i).x) {
                minX = this->knot(i - 1).x + m_d->neighborKnotMargin;
                maxX = this->knot(i).x - m_d->neighborKnotMargin;
                if (maxX < minX) {
                    return -1;
                }
                break;
            }
        }
    }
    const qint32 newKnotIndex = m_d->curve.addKnot({
                                    qMin(maxX, qMax(minX, knot.x)),
                                    qMin(1.0, qMax(0.0, knot.y)),
                                    knot.isCorner
                                });
    emit curveChanged(curve());
    update();
    return newKnotIndex;
}

void CurveWidget::removeKnot(qint32 index)
{
    m_d->curve.removeKnot(index);
    if (index < selectedKnotIndex()) {
        setSelectedKnotIndex(selectedKnotIndex() - 1);
    }
    emit curveChanged(curve());
    update();
}

void CurveWidget::setSelectedKnotIndex(qint32 index)
{
    if (index == m_d->selectedKnotIndex ||
        index < 0 || index >= size()) {
        return;
    }
    m_d->selectedKnotIndex = index;
    emit selectedKnotChanged(index);
    update();
}

void CurveWidget::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QPainterPath clipPath;
    clipPath.addRoundedRect(rect(), 3, 3);
    painter.setClipPath(clipPath);

    // Background
    {
        painter.fillRect(rect(), palette().color(QPalette::Dark).darker(150));
    }
    // Grid
    {
        painter.setPen(QPen(palette().color(QPalette::Mid), 1, Qt::DashLine));
        painter.drawLine(QPointF(width() * 0.25 + 0.5, 0.0), QPointF(width() * 0.25 + 0.5, height()));
        painter.drawLine(QPointF(width() * 0.75 + 0.5, 0.0), QPointF(width() * 0.75 + 0.5, height()));
        painter.drawLine(QPointF(0.0, height() * 0.25 + 0.5), QPointF(width(), height() * 0.25 + 0.5));
        painter.drawLine(QPointF(0.0, height() * 0.75 + 0.5), QPointF(width(), height() * 0.75 + 0.5));
        painter.setPen(QPen(palette().color(QPalette::Mid), 1, Qt::SolidLine));
        painter.drawLine(QPointF(width() * 0.5 + 0.5, 0.0), QPointF(width() * 0.5 + 0.5, height()));
        painter.drawLine(QPointF(0.0, height() * 0.5 + 0.5), QPointF(width(), height() * 0.5 + 0.5));
    }
    // Curve
    {
        QPolygonF path;
        const QVector<quint16> lut = m_d->curve.lut<quint16>(width(), rect().bottom() * 4, true);

        for (qint32 i = 0; i < width(); ++i) {
            path.append(QPointF(i + 0.5, rect().bottom() - (lut[i] / 4.0 - 0.5)));
        }
        path.append(QPointF(rect().right() + 10.0, path.last().y()));
        path.append(QPointF(rect().right() + 10.0, rect().bottom() + 10.0));
        path.append(QPointF(rect().left() - 10.0, rect().bottom() + 10.0));
        path.append(QPointF(rect().left() - 10.0, path.first().y()));

        QColor curveColor = palette().color(QPalette::Midlight);
        curveColor.setAlpha(64);
        painter.setBrush(curveColor);
        curveColor.setAlpha(255);
        painter.setPen(QPen(QColor(0, 0, 0, 64), 2));
        painter.drawPolygon(path.translated(1.0, 1.0));
        painter.setBrush(Qt::NoBrush);
        curveColor = palette().text().color();
        curveColor.setAlpha(64);
        painter.setPen(QPen(curveColor, 2));
        painter.drawPolyline(path.translated(0.5, 0.5));
    }
    // Knots
    {
        const qreal radiusX = 10.0 / 2.0;
        const qreal radiusY = 10.0 / 2.0;
        QColor fillColor = palette().color(QPalette::Highlight);
        qint32 opacity, fillOpacity;

        for (qint32 i = 0; i < size(); ++i)
        {
            opacity = 200;
            fillOpacity = i == selectedKnotIndex() ? 255 : 0;

            const QPointF knotPosition(
                knot(i).x * (width() - 1) + 0.5,
                rect().bottom() - knot(i).y * (height() - 1) + 0.5
            );

            fillColor.setAlpha(fillOpacity);
            painter.setBrush(fillColor);
            painter.setPen(QPen(QColor(0, 0, 0, opacity), 1));
            painter.drawEllipse(knotPosition, radiusX, radiusY);

            painter.setBrush(Qt::NoBrush);
            painter.setPen(QPen(QColor(255, 255, 255, opacity), 1));
            painter.drawEllipse(knotPosition, radiusX - 1, radiusY - 1);
        }
    }
}

void CurveWidget::mousePressEvent(QMouseEvent *e)
{
    const QPointF normalizedPosition(e->localPos().x() / rect().right(),
                                     (rect().bottom() - e->localPos().y()) / rect().bottom());
    const qint32 nearestKnotIndex = m_d->curve.nearestKnotIndex(normalizedPosition.x());
    const Curve::Knot &nearestKnot = knot(nearestKnotIndex);
    const QSizeF selectionMargin(10.0 / rect().right(), 10.0 / rect().bottom());
    const bool canSelectNearestKnotX = qAbs(normalizedPosition.x() - nearestKnot.x) <= selectionMargin.width();
    const bool canSelectNearestKnotY = qAbs(normalizedPosition.y() - nearestKnot.y) <= selectionMargin.height();
        
    if (e->button() == Qt::RightButton) {
        if (canSelectNearestKnotX && canSelectNearestKnotY && size() > 2) {
            removeKnot(nearestKnotIndex);
        }
    } else {
        if (canSelectNearestKnotX && canSelectNearestKnotY) {
            setSelectedKnotIndex(nearestKnotIndex);
            m_d->isDragging = true;
            m_d->lastPosition = normalizedPosition;
        } else {
            if (canSelectNearestKnotX) {
                return;
            }
            const qint32 newKnotIndex = addKnot({normalizedPosition.x(), normalizedPosition.y(), false});
            if (newKnotIndex != -1) {
                setSelectedKnotIndex(newKnotIndex);
                m_d->isDragging = true;
                m_d->lastPosition = normalizedPosition;
            }
        }
    }
}

void CurveWidget::mouseMoveEvent(QMouseEvent *e)
{
    if (!m_d->isDragging) {
        return;
    }
    const qreal x = e->localPos().x() / rect().right();
    const qreal y = (rect().bottom() - e->localPos().y()) / rect().bottom();
    moveKnot(selectedKnotIndex(), x, y);
}

void CurveWidget::mouseReleaseEvent(QMouseEvent *e)
{
    if (e->button() != Qt::LeftButton && e->button() != Qt::MiddleButton) {
        return;
    }
    m_d->isDragging = false;
}
