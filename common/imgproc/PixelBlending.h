/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef PIXELBLENDING_H
#define PIXELBLENDING_H

#include <QImage>

#include "TonalRange.h"
#include "ImgProcCommon.h"
#include <Export.h>

namespace PixelBlending
{

enum AlphaBlendMethod
{
    Source,
    Destination,
    SourceOverDestination,
    DestinationOverSource,
    SourceInDestination,
    DestinationInSource,
    SourceOutDestination,
    DestinationOutSource,
    SourceAtopDestination,
    DestinationAtopSource,
    SourceClearDestination,
    SourceXorDestination,
    SourcePlusDestination
};

enum ColorBlendMethod
{
    Normal,
    Darken,
    Multiply,
    ColorBurn,
    LinearBurn,
    DarkerColor,
    Lighten,
    Screen,
    ColorDodge,
    LinearDodge,
    LighterColor,
    Overlay,
    SoftLight,
    HardLight,
    VividLight,
    LinearLight,
    PinLight,
    HardMix,
    Difference,
    Exclusion,
    Subtract,
    Divide,
    Hue,
    Saturation,
    Color,
    Luminosity
};

void COMMON_EXPORT blend(const QImage &srcImage, QImage &dstImage,
                         AlphaBlendMethod alphaBlendMethod = SourceOverDestination,
                         ColorBlendMethod colorBlendMethod = Normal, bool useSpecialColorBlending = false,
                         quint8 opacity = ImgProcCommon::maxValue, bool ignoreSourceAlpha = false,
                         const PixelTonalRanges &tonalRanges = PixelTonalRanges());
void COMMON_EXPORT blendWithBackdrop(const QImage &srcImage, QImage &backdrop, const QImage &mask,
                                     qint32 activeColorComponents = (ImgProcCommon::PixelComponent_Blue |
                                                                     ImgProcCommon::PixelComponent_Green |
                                                                     ImgProcCommon::PixelComponent_Red),
                                     quint8 opacity = ImgProcCommon::maxValue,
                                     const PixelTonalRanges &tonalRanges = PixelTonalRanges());

void COMMON_EXPORT crossDissolve(const QImage &srcImage, QImage &dstImage, quint8 t);
void COMMON_EXPORT crossDissolve(const QImage &srcImage, QImage &dstImage,
                                 const QImage &mask, bool invertMask = false);
void COMMON_EXPORT crossDissolve(const QImage &srcImage, QImage &dstImage);
void COMMON_EXPORT setAlphaChannelConstant(QImage &image, quint8 value);
void COMMON_EXPORT setAlphaChannel(QImage &image, const QImage &alphaMask);
void COMMON_EXPORT multiplyAlphaChannelConstant(QImage &image, quint8 value);
void COMMON_EXPORT multiplyAlphaChannel(QImage &image, const QImage &alphaMask);
void COMMON_EXPORT multiplyMasks(QImage &mask1, const QImage &mask2);

}

#endif
