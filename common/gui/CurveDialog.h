/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef CURVEDIALOG_H
#define CURVEDIALOG_H

#include <QDialog>
#include <QScopedPointer>

#include <Curve.h>

#include <Export.h>

class COMMON_EXPORT CurveDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CurveDialog(QWidget *parent = nullptr, const Curve &initialCurve = Curve(), Qt::WindowFlags windowFlags = Qt::Dialog);
    ~CurveDialog() override;

    const Curve& curve() const;

Q_SIGNALS:
    void curveChanged(const Curve &newCurve);

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
