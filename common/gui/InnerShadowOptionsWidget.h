/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef INNERSHADOWOPTIONSWIDGET_H
#define INNERSHADOWOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT InnerShadowOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InnerShadowOptionsWidget(QWidget *parent = nullptr);
    ~InnerShadowOptionsWidget() override;

    LayerStyles::InnerShadow params() const;

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
