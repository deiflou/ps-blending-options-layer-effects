/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef OUTERGLOWOPTIONSWIDGET_H
#define OUTERGLOWOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT OuterGlowOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OuterGlowOptionsWidget(QWidget *parent = nullptr);
    ~OuterGlowOptionsWidget() override;

    LayerStyles::OuterGlow params() const;

public Q_SLOTS:
    void setGradient(const QGradientStops &gradient);

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
