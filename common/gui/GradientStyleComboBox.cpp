/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "GradientStyleComboBox.h"

GradientStyleComboBox::GradientStyleComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Linear", static_cast<int>(LayerStyles::GradientStyle_Linear));
    addItem("Radial", static_cast<int>(LayerStyles::GradientStyle_Radial));
    addItem("Angle", static_cast<int>(LayerStyles::GradientStyle_Angle));
    addItem("Reflected", static_cast<int>(LayerStyles::GradientStyle_Reflected));
    addItem("Diamond", static_cast<int>(LayerStyles::GradientStyle_Diamond));

    setCurrentIndex(2);

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentGradientStyleChanged(static_cast<LayerStyles::GradientStyle>(currentData().toInt()));
        }
    );
}

GradientStyleComboBox::~GradientStyleComboBox()
{}

LayerStyles::GradientStyle GradientStyleComboBox::currentGradientStyle() const
{
    return static_cast<LayerStyles::GradientStyle>(currentData().toInt());
}

void GradientStyleComboBox::setCurrentGradientStyle(LayerStyles::GradientStyle newGradientStyle)
{
    setCurrentIndex(findData(static_cast<int>(newGradientStyle)));
}
