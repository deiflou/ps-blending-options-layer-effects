/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QScopedPointer>

class Window : public QWidget
{
    Q_OBJECT

public:
    Window();
    ~Window() override;

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
