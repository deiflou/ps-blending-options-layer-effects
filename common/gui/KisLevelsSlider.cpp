/*
 * KDE. Krita Project.
 *
 * SPDX-FileCopyrightText: 2021 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QPainter>
#include <QMouseEvent>

#include <algorithm>
#include <cmath>

#include "KisLevelsSlider.h"

namespace KisPaintingTweaks
{

QColor blendColors(const QColor &c1, const QColor &c2, qreal r1)
{
    const qreal r2 = 1.0 - r1;

    return QColor::fromRgbF(
        c1.redF() * r1 + c2.redF() * r2,
        c1.greenF() * r1 + c2.greenF() * r2,
        c1.blueF() * r1 + c2.blueF() * r2);
}

}

KisLevelsSlider::KisLevelsSlider(QWidget *parent)
    : QWidget(parent)
    , m_constrainPositions(true)
    , m_selectedHandle(-1)
    , m_hoveredHandle(-1)
{
    setFocusPolicy(Qt::WheelFocus);
    setMouseTracking(true);
}

KisLevelsSlider::~KisLevelsSlider()
{}

qreal KisLevelsSlider::handlePosition(int handleIndex) const
{
    Q_ASSERT(handleIndex >= 0 && handleIndex < m_handles.size());

    return m_handles[handleIndex].position;
}

QColor KisLevelsSlider::handleColor(int handleIndex) const
{
    Q_ASSERT(handleIndex >= 0 && handleIndex < m_handles.size());

    return m_handles[handleIndex].color;
}

void KisLevelsSlider::setHandlePosition(int handleIndex, qreal newPosition)
{
    Q_ASSERT(handleIndex >= 0 && handleIndex < m_handles.size());

    if (newPosition == handlePosition(handleIndex)) {
        return;
    }

    if (m_constrainPositions) {
        newPosition =
            qBound(
                handleIndex == m_handles.first().index ? 0.0 : m_handles[handleIndex - 1].position + minimumSpaceBetweenHandles,
                newPosition,
                handleIndex == m_handles.last().index ? 1.0 : m_handles[handleIndex + 1].position - minimumSpaceBetweenHandles
            );
    } else {
        newPosition = qBound(0.0, newPosition, 1.0);
    }

    if (newPosition == m_handles[handleIndex].position) {
        return;
    }

    m_handles[handleIndex].position = newPosition;

    update();
    emit handlePositionChanged(handleIndex, newPosition);
}

void KisLevelsSlider::setHandleColor(int handleIndex, const QColor &newColor)
{
    Q_ASSERT(handleIndex >= 0 && handleIndex < m_handles.size());

    if (newColor == m_handles[handleIndex].color) {
        return;
    }

    m_handles[handleIndex].color = newColor;
    
    update();
    emit handleColorChanged(handleIndex, newColor);
}

QSize KisLevelsSlider::sizeHint() const
{
    return QSize(256, 20) + QSize(handleWidth, handleHeight);
}

QSize KisLevelsSlider::minimumSizeHint() const
{
    return QSize(128, 20) + QSize(handleWidth, handleHeight);
}

QRect KisLevelsSlider::gradientRect() const
{
    const int margin = handleWidth / 2;
    return rect().adjusted(margin, 0, -margin, -handleHeight);
}

QVector<KisLevelsSlider::Handle> KisLevelsSlider::sortedHandles() const
{
    QVector<Handle> sortedHandles_ = m_handles;
    std::sort(sortedHandles_.begin(), sortedHandles_.end(),
        [](const Handle &lhs, const Handle &rhs)
        {
            return qFuzzyCompare(lhs.position, rhs.position) ? lhs.index < rhs.index : lhs.position < rhs.position;
        }
    );
    return sortedHandles_;
}

int KisLevelsSlider::closestHandleToPosition(qreal position) const
{
    const QVector<Handle> sortedHandles_ = sortedHandles();
    int handleIndex = -1;

    if (position <= sortedHandles_.first().position) {
        handleIndex = sortedHandles_.first().index;
    } else if (position >= sortedHandles_.last().position) {
        handleIndex = sortedHandles_.last().index;
    } else {
        for (int i = 0; i < sortedHandles_.size() - 1; ++i) {
            if (position >= sortedHandles_[i + 1].position) {
                continue;
            }
            const qreal middlePoint = (sortedHandles_[i].position + sortedHandles_[i + 1].position) / 2.0;
            handleIndex = position <= middlePoint ? sortedHandles_[i].index : sortedHandles_[i + 1].index;
            break;
        }
    }

    return handleIndex;
}

qreal KisLevelsSlider::positionFromX(int x) const
{
    const QRect gradientRect_ = gradientRect();
    return static_cast<qreal>(x - gradientRect_.left()) / static_cast<qreal>(gradientRect_.width());
}

int KisLevelsSlider::closestHandleToX(int x) const
{
    return closestHandleToPosition(positionFromX(x));
}

int KisLevelsSlider::xFromPosition(qreal position) const
{
    const QRect gradientRect_ = gradientRect();
    return static_cast<int>(position * static_cast<qreal>(gradientRect_.width())) + gradientRect_.left();
}

void KisLevelsSlider::handleIncrementInput(int direction, Qt::KeyboardModifiers modifiers)
{
    if (direction == 0) {
        return;
    }
    if (modifiers & Qt::ControlModifier) {
        m_selectedHandle += direction < 0 ? -1 : 1;
        m_selectedHandle = qBound(0, m_selectedHandle, m_handles.size() - 1);
        update();
    } else if (m_selectedHandle >= 0 && m_selectedHandle < m_handles.size()) {
        const qreal increment = modifiers & Qt::ShiftModifier ? slowPositionIncrement : normalPositionIncrement;
        const qreal position = m_handles[m_selectedHandle].position + (direction < 0 ? -increment : increment);
        setHandlePosition(m_selectedHandle, position);
    }
}

void KisLevelsSlider::paintHandle(QPainter &painter, const QRect &rect, const Handle &handle)
{
    painter.setRenderHint(QPainter::Antialiasing, false);
    const int halfHandleWidth = handleWidth / 2.0;
    {
        const QPolygon shape =
            handle.shape == Handle::Shape_Normal
            ? QPolygon({
                  {rect.left() + halfHandleWidth, rect.top()},
                  {rect.right() + 1, rect.top() + halfHandleWidth},
                  {rect.right() + 1, rect.bottom() + 1},
                  {rect.left(), rect.bottom() + 1},
                  {rect.left(), rect.top() + halfHandleWidth}
              })
            : handle.shape == Handle::Shape_LeftHalf
              ? QPolygon({
                    {rect.left() + halfHandleWidth, rect.top()},
                    {rect.left() + halfHandleWidth + 1, rect.top()},
                    {rect.left() + halfHandleWidth + 1, rect.bottom() + 1},
                    {rect.left(), rect.bottom() + 1},
                    {rect.left(), rect.top() + halfHandleWidth}
                })
              : QPolygon({
                    {rect.left() + halfHandleWidth, rect.top()},
                    {rect.right() + 1, rect.top() + halfHandleWidth},
                    {rect.right() + 1, rect.bottom() + 1},
                    {rect.left() + halfHandleWidth, rect.bottom() + 1}
                });
        painter.setPen(Qt::NoPen);
        const bool isSelected = handle.index == m_selectedHandle;
        QColor brushColor =
            isSelected && hasFocus()
            ? KisPaintingTweaks::blendColors(handle.color, palette().highlight().color(), 0.25)
            : handle.color;
        if (!isEnabled()) {
            brushColor.setAlpha(64);
        }
        painter.setBrush(brushColor);
        painter.drawPolygon(shape);
    }
    {
        const QPolygon shape =
            handle.shape == Handle::Shape_Normal
            ? QPolygon({
                  {rect.left() + halfHandleWidth, rect.top()},
                  {rect.right(), rect.top() + halfHandleWidth},
                  {rect.right(), rect.bottom()},
                  {rect.left(), rect.bottom()},
                  {rect.left(), rect.top() + halfHandleWidth}
              })
            : handle.shape == Handle::Shape_LeftHalf
              ? QPolygon({
                    {rect.left() + halfHandleWidth, rect.top()},
                    {rect.left() + halfHandleWidth, rect.bottom()},
                    {rect.left(), rect.bottom()},
                    {rect.left(), rect.top() + halfHandleWidth}
                })
              : QPolygon({
                    {rect.left() + halfHandleWidth, rect.top()},
                    {rect.right(), rect.top() + halfHandleWidth},
                    {rect.right(), rect.bottom()},
                    {rect.left() + halfHandleWidth, rect.bottom()}
                });
        const bool isHovered = handle.index == m_hoveredHandle;
        QColor penColor =
            isHovered && isEnabled()
            ? palette().highlight().color()
            : handle.color.lightness() < 128
              ? palette().light().color()
              : palette().dark().color();
        if (!isHovered) {
            penColor.setAlpha(210);
        }
        painter.setPen(penColor);
        painter.setBrush(Qt::NoBrush);
        painter.drawPolygon(shape);
    }
}

void KisLevelsSlider::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e);

    QPainter painter(this);
    const QRect gradientRect_ = gradientRect();
    // Gradient
    painter.save();
    paintGradient(painter, gradientRect_);
    painter.restore();
    // Border
    QColor borderColor = Qt::white;
    borderColor.setAlpha(100);
    painter.setPen(borderColor);
    painter.setCompositionMode(QPainter::CompositionMode_Difference);
    painter.drawRect(gradientRect_.adjusted(0, 0, -1, -1));
    // Handles
    const QVector<Handle> sortedHandles_ = sortedHandles();
    const int halfHandleWidth = handleWidth / 2;
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.save();
    for (const Handle &handle : sortedHandles_) {
        const int handleX = static_cast<int>(qRound(handle.position * static_cast<qreal>(gradientRect_.width() - 1))) + halfHandleWidth;
        const QRect handleRect(handleX - halfHandleWidth, gradientRect_.bottom() + 1, handleWidth, handleHeight);
        paintHandle(painter, handleRect, handle);
    }
    painter.restore();
}

void KisLevelsSlider::mousePressEvent(QMouseEvent *e)
{
    if (m_handles.size() == 0) {
        return;
    }
    if (e->button() != Qt::LeftButton) {
        return;
    }

    qreal mousePosition = positionFromX(e->x());
    int handleIndex = closestHandleToPosition(mousePosition);

    if (handleIndex != -1) {
        m_selectedHandle = handleIndex;
        const int handleX = xFromPosition(m_handles[handleIndex].position);
        if (qAbs(handleX - e->x()) > handleWidth) {
            setHandlePosition(handleIndex, mousePosition);
        } else {
            update();
        }
    }
}

void KisLevelsSlider::mouseMoveEvent(QMouseEvent *e)
{
    if (m_handles.size() == 0) {
        return;
    }

    if (e->buttons() & Qt::LeftButton && m_selectedHandle != -1) {
        setHandlePosition(m_selectedHandle, positionFromX(e->x()));
    } else {
        int handleIndex = closestHandleToX(e->x());
        if (handleIndex != -1) {
            m_hoveredHandle = handleIndex;
            update();
        }
    }

}

void KisLevelsSlider::leaveEvent(QEvent *e)
{
    m_hoveredHandle = -1;
    update();
    QWidget::leaveEvent(e);
}

void KisLevelsSlider::keyPressEvent(QKeyEvent *e)
{
    if (m_handles.size() == 0) {
        return;
    }
    if (m_selectedHandle == -1) {
        return;
    }

    switch (e->key()) {
    case Qt::Key_Left:
        handleIncrementInput(-1, e->modifiers());
        return;
    case Qt::Key_Right:
        handleIncrementInput(1, e->modifiers());
        return;
    default:
        QWidget::keyPressEvent(e);
    }
}

void KisLevelsSlider::wheelEvent(QWheelEvent *e)
{
    if (e->angleDelta().y() != 0) {
        handleIncrementInput(e->angleDelta().y(), e->modifiers());
        e->accept();
    } else {
        QWidget::wheelEvent(e);
    }
}

KisTonalRangeSlider::KisTonalRangeSlider(QWidget *parent)
    : KisLevelsSlider(parent)
    , m_leftColor(Qt::black)
    , m_rightColor(Qt::white)
{
    m_handles.resize(4);
    m_handles[0].index = 0;
    m_handles[1].index = 1;
    m_handles[0].position = m_handles[1].position = 0.0;
    m_handles[0].color = m_handles[1].color = Qt::black;
    m_handles[0].shape = Handle::Shape_LeftHalf;
    m_handles[1].shape = Handle::Shape_RightHalf;
    m_handles[2].index = 2;
    m_handles[3].index = 3;
    m_handles[2].position = m_handles[3].position = 1.0;
    m_handles[2].color = m_handles[3].color = Qt::white;
    m_handles[2].shape = Handle::Shape_LeftHalf;
    m_handles[3].shape = Handle::Shape_RightHalf;
    m_selectedHandle = 0;
    m_constrainPositions = false;
    connect(this, SIGNAL(handlePositionChanged(int, qreal)), SLOT(slotHandlePositionChanged(int, qreal)));
}

KisTonalRangeSlider::~KisTonalRangeSlider()
{}

qreal KisTonalRangeSlider::blackRampStartPoint() const
{
    return m_handles.first().position;
}

qreal KisTonalRangeSlider::blackRampEndPoint() const
{
    return m_handles[1].position;
}

qreal KisTonalRangeSlider::whiteRampStartPoint() const
{
    return m_handles[2].position;
}

qreal KisTonalRangeSlider::whiteRampEndPoint() const
{
    return m_handles.last().position;
}

void KisTonalRangeSlider::setBlackRampStartPoint(qreal newBlackRampStartPoint)
{
    setHandlePosition(m_handles.first().index, newBlackRampStartPoint);
}

void KisTonalRangeSlider::setBlackRampEndPoint(qreal newBlackRampEndPoint)
{
    setHandlePosition(m_handles[1].index, newBlackRampEndPoint);
}

void KisTonalRangeSlider::setWhiteRampStartPoint(qreal newWhiteRampStartPoint)
{
    setHandlePosition(m_handles[2].index, newWhiteRampStartPoint);
}

void KisTonalRangeSlider::setWhiteRampEndPoint(qreal newWhiteRampEndPoint)
{
    setHandlePosition(m_handles.last().index, newWhiteRampEndPoint);
}

void KisTonalRangeSlider::reset(qreal newBlackRampStartPoint, qreal newBlackRampEndPoint,
                                qreal newWhiteRampStartPoint, qreal newWhiteRampEndPoint)
{
    newBlackRampStartPoint = qBound(0.0, newBlackRampStartPoint, 1.0);
    newBlackRampEndPoint = qBound(0.0, newBlackRampEndPoint, 1.0);
    newWhiteRampStartPoint = qBound(0.0, newWhiteRampStartPoint, 1.0);
    newWhiteRampEndPoint = qBound(0.0, newWhiteRampEndPoint, 1.0);

    if (newBlackRampEndPoint < newBlackRampStartPoint) {
        newBlackRampEndPoint = newBlackRampStartPoint;
    }
    if (newWhiteRampEndPoint < newWhiteRampStartPoint) {
        newWhiteRampEndPoint = newWhiteRampStartPoint;
    }

    setBlackRampStartPoint(newBlackRampStartPoint);
    setBlackRampEndPoint(newBlackRampEndPoint);
    setWhiteRampStartPoint(newWhiteRampStartPoint);
    setWhiteRampEndPoint(newWhiteRampEndPoint);
}

void KisTonalRangeSlider::paintGradient(QPainter &painter, const QRect &rect)
{
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
    if (!isEnabled()) {
        painter.setOpacity(0.5);
    }
    QImage gradientImage(rect.width(), 1, QImage::Format_ARGB32);
    const int halfGradientHeight = rect.height() / 2;

    // Top gradient
    {
        QRgb *pixel = reinterpret_cast<QRgb*>(gradientImage.bits());
        for (int x = 0; x < gradientImage.width(); ++x, ++pixel) {
            const qreal t = static_cast<qreal>(x) / static_cast<qreal>(gradientImage.width() - 1);
            *pixel = KisPaintingTweaks::blendColors(m_rightColor, m_leftColor, t).rgba();
        }
    }
    painter.drawImage(rect.adjusted(0, 0, 0, -(halfGradientHeight + (rect.height() & 1 ? 1 : 0))), gradientImage);

    // Bottom gradient
    auto getBW =
        [](qreal x, qreal deltaB, qreal deltaW, qreal startB, qreal startW) -> QPair<qreal, qreal>
        {
            const qreal dB = x - startB;
            const qreal b = deltaB == 0.0
                            ? dB < 0.0
                              ? 0.0
                              : 1.0
                            : dB / deltaB;
            const qreal dW = x - startW;
            const qreal w = deltaW == 0.0
                            ? dW > 0.0
                              ? 0.0
                              : 1.0
                            : 1.0 - dW / deltaW;
            return {b, w};
        };
    const qreal deltaB = blackRampEndPoint() - blackRampStartPoint();
    const qreal deltaW = whiteRampEndPoint() - whiteRampStartPoint();
    if (whiteRampEndPoint() < blackRampStartPoint()) {
        QRgb *pixel = reinterpret_cast<QRgb*>(gradientImage.bits());
        for (int x = 0; x < gradientImage.width(); ++x, ++pixel) {
            const qreal xf = static_cast<qreal>(x) / static_cast<qreal>(gradientImage.width() - 1);

            const qreal dB = xf - blackRampStartPoint();
            const qreal b = deltaB == 0.0 ? (dB > 0.0 ? 1.0 : 0.0) : dB / deltaB;
            const qreal dW = xf - whiteRampStartPoint();
            const qreal w = deltaW == 0.0 ? (dW < 0.0 ? 1.0 : 0.0) : 1.0 - dW / deltaW;

            const qreal t = qBound(0.0, qMax(b, w), 1.0);
            *pixel = KisPaintingTweaks::blendColors(Qt::white, Qt::black, t).rgba();
        }
    } else {
        QRgb *pixel = reinterpret_cast<QRgb*>(gradientImage.bits());
        for (int x = 0; x < gradientImage.width(); ++x, ++pixel) {
            const qreal xf = static_cast<qreal>(x) / static_cast<qreal>(gradientImage.width() - 1);

            const qreal dB = xf - blackRampStartPoint();
            const qreal b = deltaB == 0.0 ? (dB < 0.0 ? 0.0 : 1.0) : dB / deltaB;
            const qreal dW = xf - whiteRampStartPoint();
            const qreal w = deltaW == 0.0 ? (dW > 0.0 ? 0.0 : 1.0) : 1.0 - dW / deltaW;

            const qreal t = qBound(0.0, qMin(b, w), 1.0);
            *pixel = KisPaintingTweaks::blendColors(Qt::white, Qt::black, t).rgba();
        }
    }
    painter.drawImage(rect.adjusted(0, halfGradientHeight, 0, 0), gradientImage);
}

void KisTonalRangeSlider::slotHandlePositionChanged(int index, qreal position)
{
    if (index == 0) {
        if (position > blackRampEndPoint()) {
            reset(position, position,
                  whiteRampStartPoint(), whiteRampEndPoint());
        } else {
            emit blackRampStartPointChanged(position);
        }
    } else if (index == 1) {
        if (position < blackRampStartPoint()) {
            reset(position, position,
                  whiteRampStartPoint(), whiteRampEndPoint());
        } else {
            emit blackRampEndPointChanged(position);
        }
    } else if (index == 2) {
        if (position > whiteRampEndPoint()) {
            reset(blackRampStartPoint(), blackRampEndPoint(),
                  position, position);
        } else {
            emit whiteRampStartPointChanged(position);
        }
    } else {
        if (position < whiteRampStartPoint()) {
            reset(blackRampStartPoint(), blackRampEndPoint(),
                  position, position);
        } else {
            emit whiteRampEndPointChanged(position);
        }
    }
}

void KisTonalRangeSlider::setColors(const QColor &newLeftColor, const QColor &newRightColor)
{
    m_leftColor = newLeftColor;
    m_rightColor = newRightColor;
    update();
}
