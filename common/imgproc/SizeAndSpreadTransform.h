/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef SIZEANDSPREADTRANSFORM_H
#define SIZEANDSPREADTRANSFORM_H

#include <QImage>

#include <Export.h>

namespace SizeAndSpreadTransform
{

enum Method
{
    Method_Softer,
    Method_Precise,
    Method_PreciseSimple
};

QImage COMMON_EXPORT apply(const QImage &mask,
                           qint32 size,
                           qint32 spread,
                           Method method = Method_Softer,
                           bool invertMask = false);

};

#endif
