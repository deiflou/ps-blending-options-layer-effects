/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef FILTERING_H
#define FILTERING_H

#include <QImage>

#include <Export.h>

namespace Filtering
{

QImage COMMON_EXPORT boxBlurARGB(const QImage &image, qint32 radius);
QImage COMMON_EXPORT boxBlurMask(const QImage &mask, qint32 radius);
void COMMON_EXPORT boxBlur1D(const quint8 *inData, quint8 *outData,
                             qint32 entrySize, qint32 numberOfEntries, qint32 radius);
QImage COMMON_EXPORT gaussianBlurARGB(const QImage &image, qint32 radius);
QImage COMMON_EXPORT gaussianBlurMask(const QImage &mask, qint32 radius);

}

#endif
