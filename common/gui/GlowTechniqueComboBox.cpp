/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "GlowTechniqueComboBox.h"

GlowTechniqueComboBox::GlowTechniqueComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Softer", static_cast<int>(LayerStyles::GlowTechnique_Softer));
    addItem("Precise", static_cast<int>(LayerStyles::GlowTechnique_Precise));

    setCurrentIndex(2);

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentGlowTechniqueChanged(static_cast<LayerStyles::GlowTechnique>(currentData().toInt()));
        }
    );
}

GlowTechniqueComboBox::~GlowTechniqueComboBox()
{}

LayerStyles::GlowTechnique GlowTechniqueComboBox::currentGlowTechnique() const
{
    return static_cast<LayerStyles::GlowTechnique>(currentData().toInt());
}

void GlowTechniqueComboBox::setCurrentGlowTechnique(LayerStyles::GlowTechnique newGlowTechnique)
{
    setCurrentIndex(findData(static_cast<int>(newGlowTechnique)));
}
