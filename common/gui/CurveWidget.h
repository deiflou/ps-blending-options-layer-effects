/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef CURVEWIDGET_H
#define CURVEWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <Curve.h>

#include <Export.h>

class COMMON_EXPORT CurveWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CurveWidget(QWidget *parent = nullptr);
    ~CurveWidget() override;

    const Curve& curve() const;
    const Curve::KnotList& knots() const;
    const Curve::Knot& knot(qint32 index) const;
    qint32 selectedKnotIndex() const;
    const Curve::Knot& selectedKnot() const;
    qint32 size() const;

public Q_SLOTS:
    void setCurve(const Curve &newCurve);
    void setCurve(Curve &&newCurve);
    void setKnots(const Curve::KnotList &knotList);
    void setKnotAsCorner(qint32 index, bool isCorner);
    void moveKnot(qint32 index, qreal x, qreal y);
    qint32 addKnot(const Curve::Knot &knot);
    void removeKnot(qint32 index);
    void setSelectedKnotIndex(qint32 index);

Q_SIGNALS:
    void curveChanged(const Curve &newCurve);
    void selectedKnotChanged(qint32 index);

private:
    class Private;
    QScopedPointer<Private> m_d;

    void paintEvent(QPaintEvent*) override;
    void mousePressEvent(QMouseEvent *e) override;
    void mouseMoveEvent(QMouseEvent *e) override;
    void mouseReleaseEvent(QMouseEvent *e) override;
};

#endif
