/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QPainter>
#include <QScrollBar>
#include <QMouseEvent>
#include <QWheelEvent>

#include "Viewport.h"

class Q_DECL_HIDDEN Viewport::Private
{
public:
    static constexpr int numberOfScaleLevels {14};
    static constexpr int defaultScaleLevel {6};
    static constexpr qreal scaleLevels[numberOfScaleLevels] =
        {0.125, 0.250, 0.333, 0.500, 0.666, 0.750, 1.000,
         2.000, 3.000, 4.000, 5.000, 6.000, 7.000, 8.000};

    Viewport *q;
    QImage image;
    int currentScaleLevel {defaultScaleLevel};
    QPoint position {0, 0};
    QPoint lastMousePosition;
    bool listenToScrollBars {true};
    QImage checkerBoardPattern;
    bool scaleCheckerBoardPattern {false};

    Private(Viewport *q)
        : q(q)
    {}

    QWidget* viewport() const
    {
        return q->viewport();
    }

    int scaleLevelForScale(qreal scale)
    {
        if (scale <= scaleLevels[0]) {
            return 0;
        }
        if (scale >= scaleLevels[numberOfScaleLevels - 1]) {
            return numberOfScaleLevels - 1;
        }
        for (int i = 1; i < numberOfScaleLevels; ++i) {
            if (scale < scaleLevels[i]) {
                return i - 1;
            }
        }
        return defaultScaleLevel;
    }

    qreal scale() const
    {
        return scaleLevels[currentScaleLevel];
    }

    QSizeF scaledImageSize() const
    {
        return QSizeF(image.width() * scale(),
                      image.height() * scale());
    }

    QSize roundedScaledImageSize() const
    {
        const QSizeF scaledImageSize = this->scaledImageSize();
        return QSize(qRound(scaledImageSize.width()),
                     qRound(scaledImageSize.height()));
    }


    void updateScrollBars()
    {
        const QSize roundedScaledImageSize = this->roundedScaledImageSize();
        listenToScrollBars = false;
        q->horizontalScrollBar()->setPageStep(viewport()->width());
        q->horizontalScrollBar()->setRange(0, roundedScaledImageSize.width() - viewport()->width());
        q->horizontalScrollBar()->setValue(-position.x());
        q->verticalScrollBar()->setPageStep(viewport()->height());
        q->verticalScrollBar()->setRange(0, roundedScaledImageSize.height() - viewport()->height());
        q->verticalScrollBar()->setValue(-position.y());
        listenToScrollBars = true;
    }

    void adjustPosition()
    {
        const QSize roundedScaledImageSize = this->roundedScaledImageSize();
        QPoint adjustedPosition;
        if (roundedScaledImageSize.width() > viewport()->width()) {
            adjustedPosition.setX(
                qBound(-(roundedScaledImageSize.width() - viewport()->width()), position.x(), 0)
            );
        } else {
            adjustedPosition.setX((q->viewport()->width() - roundedScaledImageSize.width()) / 2);
        }
        if (roundedScaledImageSize.height() > viewport()->height()) {
            adjustedPosition.setY(
                qBound(-(roundedScaledImageSize.height() - viewport()->height()), position.y(), 0)
            );
        } else {
            adjustedPosition.setY((viewport()->height() - roundedScaledImageSize.height()) / 2);
        }
        position = adjustedPosition;
        updateScrollBars();
        viewport()->update();
    }

    void setScaleWithPivot(int newScaleLevel, const QPoint &pivotPoint)
    {
        const QPointF delta = QPointF(pivotPoint - position) / scale();
        currentScaleLevel = newScaleLevel;
        position = pivotPoint - QPoint(delta.x() * scale(), delta.y() * scale());
        adjustPosition();
    }
};

Viewport::Viewport(QWidget *parent)
    : QAbstractScrollArea(parent)
    , m_d(new Private(this))
{
    m_d->checkerBoardPattern = QImage(2, 2, QImage::Format_ARGB32);
    m_d->checkerBoardPattern.setPixel(0, 0, qRgba(192, 192, 192, 255));
    m_d->checkerBoardPattern.setPixel(1, 0, qRgba(128, 128, 128, 255));
    m_d->checkerBoardPattern.setPixel(0, 1, qRgba(128, 128, 128, 255));
    m_d->checkerBoardPattern.setPixel(1, 1, qRgba(192, 192, 192, 255));
}

Viewport::~Viewport()
{}

QImage Viewport::image() const
{
    return m_d->image;
}

qreal Viewport::scale() const
{
    return m_d->scale();
}

QPoint Viewport::position() const
{
    return m_d->position;
}

void Viewport::setImage(const QImage &newImage)
{
    m_d->image = newImage;
    m_d->adjustPosition();
    m_d->updateScrollBars();
    viewport()->update();
}

void Viewport::setScaleCheckerBoardPattern(bool scale)
{
    m_d->scaleCheckerBoardPattern = scale;
    viewport()->update();
}

void Viewport::paintEvent(QPaintEvent*)
{
    QPainter p(viewport());

    p.fillRect(rect(), Qt::gray);

    if (m_d->image.isNull())
    {
        return;
    }

    if (m_d->scaleCheckerBoardPattern) {
        QImage comp(m_d->image.size(), m_d->image.format());
        QPainter pi(&comp);
        QBrush b(m_d->checkerBoardPattern);
        QTransform t;
        t.scale(8, 8);
        b.setTransform(t);
        pi.fillRect(comp.rect(), b);
        pi.drawImage(comp.rect(), m_d->image);
        const QRect scaledImageRect(m_d->position, m_d->roundedScaledImageSize());
        p.drawImage(scaledImageRect, comp);
    } else {
        QBrush b(m_d->checkerBoardPattern);
        QTransform t;
        t.translate(m_d->position.x(), m_d->position.y());
        t.scale(8, 8);
        b.setTransform(t);
        const QRect scaledImageRect(m_d->position, m_d->roundedScaledImageSize());
        p.fillRect(scaledImageRect, b);
        p.drawImage(scaledImageRect, m_d->image);
    }
}

void Viewport::mousePressEvent(QMouseEvent *e)
{
    if (e->button() != Qt::LeftButton &&
        e->button() != Qt::MiddleButton) {
        return;
    }
    m_d->lastMousePosition = e->pos();
}

void Viewport::mouseMoveEvent(QMouseEvent *e)
{
    if (!e->buttons().testFlag(Qt::LeftButton) &&
        !e->buttons().testFlag(Qt::MiddleButton)) {
        return;
    }
    m_d->position += e->pos() - m_d->lastMousePosition;
    m_d->adjustPosition();
    m_d->lastMousePosition = e->pos();
    m_d->updateScrollBars();
    viewport()->update();
}

void Viewport::wheelEvent(QWheelEvent *e)
{
    int newScaleLevel;
    if (e->angleDelta().y() > 0) {
        newScaleLevel = qMin(m_d->currentScaleLevel + 1,
                                      m_d->numberOfScaleLevels - 1);
    } else {
        newScaleLevel = qMax(m_d->currentScaleLevel - 1, 0);
    }
    if (newScaleLevel == m_d->currentScaleLevel) {
        return;
    }
    const QPoint position(static_cast<int>(e->position().x()),
                          static_cast<int>(e->position().y()));
    m_d->setScaleWithPivot(newScaleLevel, position);
    emit scaleChanged(scale());
    m_d->updateScrollBars();
    viewport()->update();
}

void Viewport::resizeEvent(QResizeEvent*)
{
    m_d->adjustPosition();
    m_d->updateScrollBars();
    viewport()->update();
}

void Viewport::scrollContentsBy(int dx, int dy)
{
    if (!m_d->listenToScrollBars) {
        return;
    }
    m_d->position += QPoint(dx, dy);
    m_d->adjustPosition();
    viewport()->update();
}
