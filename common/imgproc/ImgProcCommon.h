/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef IMGPROCCOMMON_H
#define IMGPROCCOMMON_H

#include <QtGlobal>

namespace ImgProcCommon
{

static constexpr qint32 maxValue {255};
static constexpr qint32 halfValue {128};
static constexpr qint32 quarterValue {64};
static constexpr qint32 doubleValue {511};
static constexpr qint32 quadrupleValue {1024};

enum PixelComponent
{
    PixelComponent_None = 0,
    PixelComponent_Red = 1 << 0,
    PixelComponent_Green = 1 << 1,
    PixelComponent_Blue = 1 << 2,
    PixelComponent_Gray = 1 << 3
};

struct ARGB
{
    quint8 b, g, r, a;
};

struct HSLA
{
    quint8 h, s, l, a;
};

}

#endif
