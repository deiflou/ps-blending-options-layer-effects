/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QVBoxLayout>

#include <CurveButton.h>
#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    QPushButton *button {nullptr};
};

Window::Window()
    : m_d(new Private)
{
    m_d->button = new CurveButton;
    
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setContentsMargins(10, 10, 10, 10);
    mainLayout->addWidget(m_d->button);
    setLayout(mainLayout);

    resize(100, 100);
}

Window::~Window()
{}
