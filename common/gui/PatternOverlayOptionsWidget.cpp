/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QHBoxLayout>
#include <QCheckBox>

#include "ColorBlendMethodComboBox.h"
#include "SliderSpinBox.h"

#include "PatternOverlayOptionsWidget.h"

class Q_DECL_HIDDEN PatternOverlayOptionsWidget::Private
{
public:
    ColorBlendMethodComboBox *comboBoxBlendMode {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
    SliderSpinBox *sliderAngle {nullptr};
    SliderSpinBox *sliderScale {nullptr};
    QCheckBox *checkBoxLinkWithLayer {nullptr};
    QImage pattern;
};

PatternOverlayOptionsWidget::PatternOverlayOptionsWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    LayerStyles::PatternOverlay defaultParams;

    m_d->pattern = defaultParams.pattern;

    m_d->comboBoxBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxBlendMode->setCurrentColorBlendMethod(defaultParams.blendMode);

    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(defaultParams.opacity);

    m_d->sliderAngle = new SliderSpinBox;
    m_d->sliderAngle->setRange(0, 359);
    m_d->sliderAngle->setValue(defaultParams.angle);

    m_d->sliderScale = new SliderSpinBox;
    m_d->sliderScale->setRange(1, 1000);
    m_d->sliderScale->setValue(defaultParams.scale);

    m_d->checkBoxLinkWithLayer = new QCheckBox;
    m_d->checkBoxLinkWithLayer->setChecked(defaultParams.linkWithLayer);

    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addRow("Blend mode:", m_d->comboBoxBlendMode);
    mainLayout->addRow("Opacity:", m_d->sliderOpacity);
    mainLayout->addRow("Angle:", m_d->sliderAngle);
    mainLayout->addRow("Scale:", m_d->sliderScale);
    mainLayout->addRow("Link with layer:", m_d->checkBoxLinkWithLayer);

    setLayout(mainLayout);

    connect(m_d->comboBoxBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderAngle, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderScale, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxLinkWithLayer, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
}

PatternOverlayOptionsWidget::~PatternOverlayOptionsWidget()
{}

LayerStyles::PatternOverlay PatternOverlayOptionsWidget::params() const
{
    LayerStyles::PatternOverlay params;

    params.blendMode = m_d->comboBoxBlendMode->currentColorBlendMethod();
    params.pattern = m_d->pattern;
    params.opacity = m_d->sliderOpacity->value();
    params.angle = m_d->sliderAngle->value();
    params.scale = m_d->sliderScale->value();
    params.linkWithLayer = m_d->checkBoxLinkWithLayer->isChecked();
    
    return params;
}

void PatternOverlayOptionsWidget::setPattern(const QImage &pattern)
{
    m_d->pattern = pattern;
    emit paramsChanged();
}
