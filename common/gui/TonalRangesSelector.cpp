/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QSpinBox>
#include <QLabel>
#include <QSignalBlocker>

#include "TonalRangesSelector.h"

class Q_DECL_HIDDEN TonalRangeSliderWithSpinBoxes::Private
{
public:
    KisTonalRangeSlider *sliderTonalRange {nullptr};
    QSpinBox *spinBoxTonalRangeBlackRampStart {nullptr};
    QSpinBox *spinBoxTonalRangeBlackRampEnd {nullptr};
    QSpinBox *spinBoxTonalRangeWhiteRampStart {nullptr};
    QSpinBox *spinBoxTonalRangeWhiteRampEnd {nullptr};

    void slotSliderChanged()
    {
        QSignalBlocker sb1(spinBoxTonalRangeBlackRampStart);
        QSignalBlocker sb2(spinBoxTonalRangeBlackRampEnd);
        QSignalBlocker sb3(spinBoxTonalRangeWhiteRampStart);
        QSignalBlocker sb4(spinBoxTonalRangeWhiteRampEnd);
        spinBoxTonalRangeBlackRampStart->setValue(static_cast<int>(qRound(sliderTonalRange->blackRampStartPoint() * 255.0)));
        spinBoxTonalRangeBlackRampEnd->setValue(static_cast<int>(qRound(sliderTonalRange->blackRampEndPoint() * 255.0)));
        spinBoxTonalRangeWhiteRampStart->setValue(static_cast<int>(qRound(sliderTonalRange->whiteRampStartPoint() * 255.0)));
        spinBoxTonalRangeWhiteRampEnd->setValue(static_cast<int>(qRound(sliderTonalRange->whiteRampEndPoint() * 255.0)));
    }

    void slotSpinsChanged()
    {
        QSignalBlocker sb(sliderTonalRange);
        sliderTonalRange->reset(spinBoxTonalRangeBlackRampStart->value() / 255.0,
                                spinBoxTonalRangeBlackRampEnd->value() / 255.0,
                                spinBoxTonalRangeWhiteRampStart->value() / 255.0,
                                spinBoxTonalRangeWhiteRampEnd->value() / 255.0);
        slotSliderChanged();
    }
};

TonalRangeSliderWithSpinBoxes::TonalRangeSliderWithSpinBoxes(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    m_d->sliderTonalRange = new KisTonalRangeSlider;
    m_d->spinBoxTonalRangeBlackRampStart = new QSpinBox;
    m_d->spinBoxTonalRangeBlackRampStart->setRange(0, 255);
    m_d->spinBoxTonalRangeBlackRampStart->setValue(0);
    m_d->spinBoxTonalRangeBlackRampEnd = new QSpinBox;
    m_d->spinBoxTonalRangeBlackRampEnd->setRange(0, 255);
    m_d->spinBoxTonalRangeBlackRampEnd->setValue(0);
    m_d->spinBoxTonalRangeWhiteRampStart = new QSpinBox;
    m_d->spinBoxTonalRangeWhiteRampStart->setRange(0, 255);
    m_d->spinBoxTonalRangeWhiteRampStart->setValue(255);
    m_d->spinBoxTonalRangeWhiteRampEnd = new QSpinBox;
    m_d->spinBoxTonalRangeWhiteRampEnd->setRange(0, 255);
    m_d->spinBoxTonalRangeWhiteRampEnd->setValue(255);

    QHBoxLayout *layoutSpins = new QHBoxLayout;
    layoutSpins->setContentsMargins(0, 0, 0, 0);
    layoutSpins->setSpacing(5);
    layoutSpins->addWidget(m_d->spinBoxTonalRangeBlackRampStart);
    layoutSpins->addWidget(m_d->spinBoxTonalRangeBlackRampEnd);
    layoutSpins->addStretch(1);
    layoutSpins->addWidget(m_d->spinBoxTonalRangeWhiteRampStart);
    layoutSpins->addWidget(m_d->spinBoxTonalRangeWhiteRampEnd);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addWidget(m_d->sliderTonalRange);
    mainLayout->addLayout(layoutSpins);

    setLayout(mainLayout);
    
    connect(m_d->sliderTonalRange,
            &KisTonalRangeSlider::blackRampStartPointChanged,
            [this](qreal){ m_d->slotSliderChanged(); emit tonalRangeChanged(); });
    connect(m_d->sliderTonalRange,
            &KisTonalRangeSlider::blackRampEndPointChanged,
            [this](qreal){ m_d->slotSliderChanged(); emit tonalRangeChanged(); });
    connect(m_d->sliderTonalRange,
            &KisTonalRangeSlider::whiteRampStartPointChanged,
            [this](qreal){ m_d->slotSliderChanged(); emit tonalRangeChanged(); });
    connect(m_d->sliderTonalRange,
            &KisTonalRangeSlider::whiteRampEndPointChanged,
            [this](qreal){ m_d->slotSliderChanged(); emit tonalRangeChanged(); });
    
    connect(m_d->spinBoxTonalRangeBlackRampStart,
            &QSpinBox::editingFinished,
            [this](){ m_d->slotSpinsChanged(); emit tonalRangeChanged(); });
    connect(m_d->spinBoxTonalRangeBlackRampEnd,
            &QSpinBox::editingFinished,
            [this](){ m_d->slotSpinsChanged(); emit tonalRangeChanged(); });
    connect(m_d->spinBoxTonalRangeWhiteRampStart,
            &QSpinBox::editingFinished,
            [this](){ m_d->slotSpinsChanged(); emit tonalRangeChanged(); });
    connect(m_d->spinBoxTonalRangeWhiteRampEnd,
            &QSpinBox::editingFinished,
            [this](){ m_d->slotSpinsChanged(); emit tonalRangeChanged(); });
}

TonalRangeSliderWithSpinBoxes::~TonalRangeSliderWithSpinBoxes()
{}

KisTonalRangeSlider* TonalRangeSliderWithSpinBoxes::tonalRangeSlider() const
{
    return m_d->sliderTonalRange;
}

class Q_DECL_HIDDEN TonalRangesSelector::Private
{
public:
    PixelTonalRanges sourceTonalRanges;
    PixelTonalRanges destinationTonalRanges;

    QComboBox *comboBoxChannel {nullptr};
    TonalRangeSliderWithSpinBoxes *sliderSourceTonalRange {nullptr};
    TonalRangeSliderWithSpinBoxes *sliderDestinationTonalRange {nullptr};

    void updateSliders()
    {
        const qint32 index = comboBoxChannel->currentIndex();
        const TonalRange *currentSourceTonalRange;
        const TonalRange *currentDestinationTonalRange;
        QColor color;
        if (index == 0) {
            currentSourceTonalRange = &sourceTonalRanges.gray;
            currentDestinationTonalRange = &destinationTonalRanges.gray;
            color = Qt::white;
        } else if (index == 1) {
            currentSourceTonalRange = &sourceTonalRanges.red;
            currentDestinationTonalRange = &destinationTonalRanges.red;
            color = Qt::red;
        } else if (index == 2) {
            currentSourceTonalRange = &sourceTonalRanges.green;
            currentDestinationTonalRange = &destinationTonalRanges.green;
            color = Qt::green;
        } else {
            currentSourceTonalRange = &sourceTonalRanges.blue;
            currentDestinationTonalRange = &destinationTonalRanges.blue;
            color = Qt::blue;
        }
        sliderSourceTonalRange->tonalRangeSlider()->setColors(Qt::black, color);
        sliderDestinationTonalRange->tonalRangeSlider()->setColors(Qt::black, color);
        sliderSourceTonalRange->tonalRangeSlider()->reset(currentSourceTonalRange->blackRampStart,
                                                          currentSourceTonalRange->blackRampEnd,
                                                          currentSourceTonalRange->whiteRampStart,
                                                          currentSourceTonalRange->whiteRampEnd);
        sliderDestinationTonalRange->tonalRangeSlider()->reset(currentDestinationTonalRange->blackRampStart,
                                                               currentDestinationTonalRange->blackRampEnd,
                                                               currentDestinationTonalRange->whiteRampStart,
                                                               currentDestinationTonalRange->whiteRampEnd);
    }

    void slotSourceTonalRangeChanged()
    {
        const qint32 index = comboBoxChannel->currentIndex();
        TonalRange *currentSourceTonalRange;
        if (index == 0) {
            currentSourceTonalRange = &sourceTonalRanges.gray;
        } else if (index == 1) {
            currentSourceTonalRange = &sourceTonalRanges.red;
        } else if (index == 2) {
            currentSourceTonalRange = &sourceTonalRanges.green;
        } else {
            currentSourceTonalRange = &sourceTonalRanges.blue;
        }
        currentSourceTonalRange->blackRampStart = sliderSourceTonalRange->tonalRangeSlider()->blackRampStartPoint();
        currentSourceTonalRange->blackRampEnd = sliderSourceTonalRange->tonalRangeSlider()->blackRampEndPoint();
        currentSourceTonalRange->whiteRampStart = sliderSourceTonalRange->tonalRangeSlider()->whiteRampStartPoint();
        currentSourceTonalRange->whiteRampEnd = sliderSourceTonalRange->tonalRangeSlider()->whiteRampEndPoint();
    }

    void slotDestinationTonalRangeChanged()
    {
        const qint32 index = comboBoxChannel->currentIndex();
        TonalRange *currentDestinationTonalRange;
        if (index == 0) {
            currentDestinationTonalRange = &destinationTonalRanges.gray;
        } else if (index == 1) {
            currentDestinationTonalRange = &destinationTonalRanges.red;
        } else if (index == 2) {
            currentDestinationTonalRange = &destinationTonalRanges.green;
        } else {
            currentDestinationTonalRange = &destinationTonalRanges.blue;
        }
        currentDestinationTonalRange->blackRampStart = sliderDestinationTonalRange->tonalRangeSlider()->blackRampStartPoint();
        currentDestinationTonalRange->blackRampEnd = sliderDestinationTonalRange->tonalRangeSlider()->blackRampEndPoint();
        currentDestinationTonalRange->whiteRampStart = sliderDestinationTonalRange->tonalRangeSlider()->whiteRampStartPoint();
        currentDestinationTonalRange->whiteRampEnd = sliderDestinationTonalRange->tonalRangeSlider()->whiteRampEndPoint();
    }
};

TonalRangesSelector::TonalRangesSelector(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    m_d->comboBoxChannel = new QComboBox;
    m_d->comboBoxChannel->addItem("Gray");
    m_d->comboBoxChannel->addItem("Red");
    m_d->comboBoxChannel->addItem("Green");
    m_d->comboBoxChannel->addItem("Blue");

    m_d->sliderSourceTonalRange = new TonalRangeSliderWithSpinBoxes;

    m_d->sliderDestinationTonalRange = new TonalRangeSliderWithSpinBoxes;

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addWidget(m_d->comboBoxChannel);
    mainLayout->addWidget(new QLabel("Current layer:"));
    mainLayout->addWidget(m_d->sliderSourceTonalRange);
    mainLayout->addWidget(new QLabel("Underlying layer:"));
    mainLayout->addWidget(m_d->sliderDestinationTonalRange);

    setLayout(mainLayout);

    connect(m_d->comboBoxChannel,
            QOverload<int>::of(&QComboBox::currentIndexChanged),
            [this](int index){ m_d->updateSliders(); });
    connect(m_d->sliderSourceTonalRange,
            &TonalRangeSliderWithSpinBoxes::tonalRangeChanged,
            [this]()
            {
                m_d->slotSourceTonalRangeChanged();
                emit tonalRangesChanged(m_d->sourceTonalRanges, m_d->destinationTonalRanges);
            });
    connect(m_d->sliderDestinationTonalRange,
            &TonalRangeSliderWithSpinBoxes::tonalRangeChanged,
            [this]()
            {
                m_d->slotDestinationTonalRangeChanged();
                emit tonalRangesChanged(m_d->sourceTonalRanges, m_d->destinationTonalRanges);
            });
}

TonalRangesSelector::~TonalRangesSelector()
{}

PixelTonalRanges TonalRangesSelector::sourceTonalRanges() const
{
    return m_d->sourceTonalRanges;
}

PixelTonalRanges TonalRangesSelector::destinationTonalRanges() const
{
    return m_d->destinationTonalRanges;
}

void TonalRangesSelector::setTonalRanges(const PixelTonalRanges &newSourceTonalRanges,
                                         const PixelTonalRanges &newDestinationTonalRanges)
{
    m_d->sourceTonalRanges = newSourceTonalRanges;
    m_d->sourceTonalRanges = newDestinationTonalRanges;
    m_d->updateSliders();
}

void TonalRangesSelector::setSourceTonalRanges(const PixelTonalRanges &newSourceTonalRanges)
{
    m_d->sourceTonalRanges = newSourceTonalRanges;
    m_d->updateSliders();
}

void TonalRangesSelector::setDestinationTonalRanges(const PixelTonalRanges &newDestinationTonalRanges)
{
    m_d->sourceTonalRanges = newDestinationTonalRanges;
    m_d->updateSliders();
}
