/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "GradientMethodComboBox.h"

GradientMethodComboBox::GradientMethodComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Perceptual", static_cast<int>(LayerStyles::GradientMethod_Perceptual));
    addItem("Linear", static_cast<int>(LayerStyles::GradientMethod_Linear));
    addItem("Classic", static_cast<int>(LayerStyles::GradientMethod_Classic));

    setCurrentIndex(2);

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentGradientMethodChanged(static_cast<LayerStyles::GradientMethod>(currentData().toInt()));
        }
    );
}

GradientMethodComboBox::~GradientMethodComboBox()
{}

LayerStyles::GradientMethod GradientMethodComboBox::currentGradientMethod() const
{
    return static_cast<LayerStyles::GradientMethod>(currentData().toInt());
}

void GradientMethodComboBox::setCurrentGradientMethod(LayerStyles::GradientMethod newGradientMethod)
{
    setCurrentIndex(findData(static_cast<int>(newGradientMethod)));
}
