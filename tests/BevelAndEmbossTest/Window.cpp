/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QPainter>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QCheckBox>

#include <Viewport.h>
#include <BevelAndEmbossOptionsWidget.h>
#include <BlendingOptionsWidget.h>
#include <ImgProcUtil.h>
#include <LayerStyles.h>
#include <PixelBlending.h>

#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    BevelAndEmbossOptionsWidget *bevelAndEmbossOptionsWidget {nullptr};
    BlendingOptionsWidget *blendingOptionsWidget {nullptr};
    QCheckBox *checkBoxBackground {nullptr};
    QCheckBox *checkBoxBlurredBackdrop {nullptr};
    QCheckBox *checkBoxUseMask {nullptr};
    QImage background;
    QImage bg2;
    QImage bg2Blurred;
    QImage currentLayer;
    QImage mask;

    QImage generateImage() const
    {
        using namespace PixelBlending;
        using namespace LayerStyles;
        using namespace LayerStyles;

        const BevelAndEmboss bevelAndEmbossOptions = bevelAndEmbossOptionsWidget->params();
        const BlendingOptions blendingOptions = blendingOptionsWidget->params();

        const bool backgroundLayerIsVisible = checkBoxBackground->isChecked();
        const bool blurredBackdrop = checkBoxBlurredBackdrop->isChecked();
        const bool useMask = checkBoxUseMask->isChecked();

        // The mask representing the shape of the current layer
        QImage currentLayerShape;
        if (blendingOptions.transparencyShapesLayer) {
            currentLayerShape = currentLayer.convertToFormat(QImage::Format_Alpha8);
        } else {
            currentLayerShape = QImage(currentLayer.size(), QImage::Format_Alpha8);
            currentLayerShape.fill(255);
        }
        if (!blendingOptions.layerMaskHidesEffects && useMask) {
            multiplyMasks(currentLayerShape, mask);
        }
        const quint8 currentLayerOpacity = static_cast<quint8>(blendingOptions.opacity * 255 / 100.0);
        const quint8 currentLayerFillOpacity = static_cast<quint8>(blendingOptions.fillOpacity * 255 / 100.0);
        // The implicit layers that contain the computed bevel and emboss effect
        QPair<QImage, QImage> bevelAndEmbossOutput =
            generateBevelAndEmbossImage(bevelAndEmbossOptions, currentLayerShape);
        const QImage bevelAndEmbossLightImplicitLayer = bevelAndEmbossOutput.first;
        const QImage bevelAndEmbossShadowImplicitLayer = bevelAndEmbossOutput.second;
        const quint8 bevelAndEmbossLightOpacity =
            static_cast<quint8>(bevelAndEmbossOptions.lightOpacity * 255 / 100.0);
        const quint8 bevelAndEmbossShadowOpacity =
            static_cast<quint8>(bevelAndEmbossOptions.shadowOpacity * 255 / 100.0);

        // The fully opaque, non-knockable background layer
        const QImage backgroundLayer = backgroundLayerIsVisible ? background : QImage();
        // The projection up to the current layer
        QImage projection;
        if (backgroundLayerIsVisible) {
            projection = background;
            blend(blurredBackdrop ? bg2Blurred : bg2, projection);
        } else {
            projection = blurredBackdrop ? bg2Blurred : bg2;
        }
        // The helper images that contain the composited result and the
        // knocked out region respectively
        QImage composition = projection;
        QImage knockoutComposition;
        // Generate the current layer composition
        if (blendingOptions.knockout == Knockout_None) {
            // Use the overall projection as backdrop for the knockout
            // of the implicit effect layers
            knockoutComposition = projection;
        } else {
            // Use the background layer or a transparent image as backdrop
            // for the knockout
            if (backgroundLayerIsVisible) {
                knockoutComposition = background;
            } else {
                knockoutComposition = QImage(currentLayer.size(), QImage::Format_ARGB32);
                knockoutComposition.fill(0);
            }
        }
        if (bevelAndEmbossOptions.style == BevelAndEmbossStyle_OuterBevel) {
            blend(bevelAndEmbossLightImplicitLayer, composition, SourceOverDestination,
                  bevelAndEmbossOptions.lightBlendMode, true, bevelAndEmbossLightOpacity);
            blend(bevelAndEmbossShadowImplicitLayer, composition, SourceOverDestination,
                  bevelAndEmbossOptions.shadowBlendMode, true, bevelAndEmbossShadowOpacity);
            blend(currentLayer, knockoutComposition,
                  SourceOverDestination, blendingOptions.blendMode, true,
                  currentLayerFillOpacity, blendingOptions.transparencyShapesLayer,
                  blendingOptions.sourceTonalRanges);
            // This is basically the same as using the following Porter-Duff operators:
            // (composition OUT currentLayer) PLUS (layerComposition IN currentLayer)
            PixelBlending::crossDissolve(knockoutComposition, composition, currentLayerShape);

        } else if (bevelAndEmbossOptions.style == BevelAndEmbossStyle_InnerBevel) {
            blend(currentLayer, knockoutComposition,
                  SourceOverDestination, blendingOptions.blendMode, true,
                  currentLayerFillOpacity, blendingOptions.transparencyShapesLayer,
                  blendingOptions.sourceTonalRanges);
            blend(bevelAndEmbossLightImplicitLayer, knockoutComposition, SourceOverDestination,
                  bevelAndEmbossOptions.lightBlendMode, true, bevelAndEmbossLightOpacity);
            blend(bevelAndEmbossShadowImplicitLayer, knockoutComposition, SourceOverDestination,
                  bevelAndEmbossOptions.shadowBlendMode, true, bevelAndEmbossShadowOpacity);
            PixelBlending::crossDissolve(knockoutComposition, composition, currentLayerShape);

        } else {
            blend(currentLayer, knockoutComposition,
                  SourceOverDestination, blendingOptions.blendMode, true,
                  currentLayerFillOpacity, blendingOptions.transparencyShapesLayer,
                  blendingOptions.sourceTonalRanges);
            PixelBlending::crossDissolve(knockoutComposition, composition, currentLayerShape);
            blend(bevelAndEmbossLightImplicitLayer, composition, SourceOverDestination,
                  bevelAndEmbossOptions.lightBlendMode, true, bevelAndEmbossLightOpacity);
            blend(bevelAndEmbossShadowImplicitLayer, composition, SourceOverDestination,
                  bevelAndEmbossOptions.shadowBlendMode, true, bevelAndEmbossShadowOpacity);
        }
        // Blend the composition with the overall projection
        blendWithBackdrop(composition, projection,
                          blendingOptions.layerMaskHidesEffects && useMask ? mask : QImage(),
                          blendingOptions.channels, currentLayerOpacity,
                          blendingOptions.destinationTonalRanges);

        return projection;
    }
};

Window::Window()
    : m_d(new Private)
{
    m_d->background = QImage(":/background").convertToFormat(QImage::Format_ARGB32);
    m_d->bg2 = QImage(":/bg_2").convertToFormat(QImage::Format_ARGB32);
    m_d->bg2Blurred = QImage(":/bg_2_blurred").convertToFormat(QImage::Format_ARGB32);
    m_d->currentLayer = QImage(":/current_layer").convertToFormat(QImage::Format_ARGB32);
    const QImage texture = QImage(":/pattern").convertToFormat(QImage::Format_ARGB32);
    m_d->mask = ImgProcUtil::convertImageToMask(QImage(":/mask").convertToFormat(QImage::Format_ARGB32));

    m_d->bevelAndEmbossOptionsWidget = new BevelAndEmbossOptionsWidget;
    m_d->bevelAndEmbossOptionsWidget->setContentsMargins(10, 10, 10, 10);
    m_d->bevelAndEmbossOptionsWidget->setTexture(texture);
    m_d->blendingOptionsWidget = new BlendingOptionsWidget;
    m_d->blendingOptionsWidget->setContentsMargins(10, 10, 10, 10);

    m_d->checkBoxBackground = new QCheckBox;
    m_d->checkBoxBackground->setChecked(true);
    m_d->checkBoxBlurredBackdrop = new QCheckBox;
    m_d->checkBoxUseMask = new QCheckBox;

    QWidget *containerOtherOptions = new QWidget;

    QFormLayout *layoutOtherOptions1 = new QFormLayout;
    layoutOtherOptions1->setContentsMargins(10, 10, 10, 10);
    layoutOtherOptions1->setSpacing(5);
    layoutOtherOptions1->addRow("Show background:", m_d->checkBoxBackground);
    layoutOtherOptions1->addRow("Blurred backdrop:", m_d->checkBoxBlurredBackdrop);
    layoutOtherOptions1->addRow("Use mask:", m_d->checkBoxUseMask);

    QVBoxLayout *layoutOtherOptions2 = new QVBoxLayout;
    layoutOtherOptions2->setContentsMargins(0, 0, 0, 0);
    layoutOtherOptions2->setSpacing(0);
    layoutOtherOptions2->addLayout(layoutOtherOptions1);
    layoutOtherOptions2->addStretch();
    containerOtherOptions->setLayout(layoutOtherOptions2);

    addControlsPanel(m_d->bevelAndEmbossOptionsWidget, "Bevel and Emboss");
    addControlsPanel(m_d->blendingOptionsWidget, "Blending");
    addControlsPanel(containerOtherOptions, "Other");

    viewport()->setScaleCheckerBoardPattern(true);
    viewport()->setImage(m_d->generateImage());

    connect(m_d->bevelAndEmbossOptionsWidget, SIGNAL(paramsChanged()),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->blendingOptionsWidget, SIGNAL(paramsChanged()),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxBackground, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxBlurredBackdrop, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxUseMask, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );
}

Window::~Window()
{}
