/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef COLORCONVERSION_H
#define COLORCONVERSION_H

#include <Export.h>
#include "ImgProcCommon.h"

namespace ColorConversion
{

qint32 COMMON_EXPORT intensity(qint32 b, qint32 g, qint32 r);
qint32 COMMON_EXPORT intensity(ImgProcCommon::ARGB color);

}

#endif
