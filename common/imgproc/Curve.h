/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef CURVE_H
#define CURVE_H

#include <QVector>
#include <QScopedPointer>

#include <Export.h>

class COMMON_EXPORT Curve
{
public:
    struct Knot
    {
        qreal x, y;
        bool isCorner {false};
    };
    using KnotList = QVector<Knot>;

    Curve();
    Curve(const Curve &other);
    Curve(Curve &&other);
    Curve(const KnotList &knotList);
    ~Curve();

    Curve& operator=(const Curve &other);
    Curve& operator=(Curve &&other) noexcept;

    bool operator==(const Curve &other) const;
    bool operator!=(const Curve &other) const;

    const KnotList& knots() const;
    const Knot& knot(qint32 index) const;
    qint32 size() const;
    qint32 nearestKnotIndex(qreal x) const;
    bool isIdentity() const;
    void setKnots(const KnotList &knotList);
    qint32 setKnotX(qint32 index, qreal x);
    void setKnotY(qint32 index, qreal y);
    void setKnotAsCorner(qint32 index, bool isCorner);
    qint32 setKnot(qint32 index, qreal x, qreal y);
    qint32 setKnot(qint32 index, qreal x, qreal y, bool isCorner);
    qint32 addKnot(const Knot &knot, bool replace = false);
    void removeKnot(qint32 index);
    void clear();

    qreal f(qreal x) const;
    qreal operator()(qreal x) const;
    template<typename PixelType>
    const QVector<PixelType> lut(qint32 size = 256, qint32 scale = 255, bool clamp = true) const;

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
