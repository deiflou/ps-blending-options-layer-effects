/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef SLIDERSPINBOX_H
#define SLIDERSPINBOX_H

#include <QWidget>
#include <QScopedPointer>

#include <Export.h>

class COMMON_EXPORT SliderSpinBox : public QWidget
{
    Q_OBJECT

public:
    explicit SliderSpinBox(QWidget *parent = nullptr);
    ~SliderSpinBox() override;

    int value() const;

public Q_SLOTS:
    void setValue(int newValue);
    void setRange(int minimum, int maximum);

Q_SIGNALS:
    void valueChanged(int newValue);

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
