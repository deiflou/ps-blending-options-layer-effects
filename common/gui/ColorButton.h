/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef COLORBUTTON_H
#define COLORBUTTON_H

#include <QPushButton>
#include <QScopedPointer>
#include <QColor>

#include <Export.h>

class COMMON_EXPORT ColorButton : public QPushButton
{
    Q_OBJECT

public:
    explicit ColorButton(QWidget *parent = nullptr);
    ~ColorButton() override;

    const QColor& color() const;

public Q_SLOTS:
    void setColor(const QColor &newColor);

Q_SIGNALS:
    void colorChanged(const QColor &newColor);

protected:
    void paintEvent(QPaintEvent *e) override;

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
