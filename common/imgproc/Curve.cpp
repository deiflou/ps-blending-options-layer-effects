/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <Eigen/Sparse>
#include <vector>
#include <algorithm>

#include "Curve.h"

class Q_DECL_HIDDEN Curve::Private
{
public:
    struct Coefficients
    {
        qreal a, b, c, d;
    };

    KnotList knots;
    mutable QVector<Coefficients> coefficients;
    mutable bool isDirty {true};

    void setDirty()
    {
        coefficients.clear();
        isDirty = true;
    }

    void computeCoefficients()
    {
        coefficients.clear();

        if (knots.size() < 2) {
            return;
        }
        
        if (knots.size() == 2) {
            // Linear function, optimization
            Coefficients segmentCoefficients;
            segmentCoefficients.a = segmentCoefficients.b = 0.0;
            segmentCoefficients.c = (knots[1].y - knots[0].y) / (knots[1].x - knots[0].x);
            segmentCoefficients.d = knots[0].y - segmentCoefficients.c * knots[0].x;
            coefficients.append(segmentCoefficients);
            return;
        }

        using Triplet = Eigen::Triplet<qreal>;
        using Matrix = Eigen::SparseMatrix<qreal>;
        using Vector = Eigen::VectorXd;

        const qint32 numberOfEquations = knots.size() - 1;
        const qint32 numberOfRows = numberOfEquations * 4;
        const qint32 numberOfColumns = numberOfRows;
        std::vector<Triplet> triplets;
        Matrix A(numberOfRows, numberOfColumns);
        Vector b(numberOfRows);

        // Fill the triplet list
        triplets.reserve(numberOfRows * 4);
        qint32 row = 0;
        // Fill rows with position equations
        qreal knotX = knots.first().x;
        qreal knotY = knots.first().y;
        qreal knotXSquared = knotX * knotX;
        qreal knotXCubed = knotXSquared * knotX;
        for (qint32 i = 0; i < numberOfEquations; ++i) {
            const qint32 baseColumn = i * 4;
            triplets.push_back(Triplet(row, baseColumn + 0, knotXCubed));
            triplets.push_back(Triplet(row, baseColumn + 1, knotXSquared));
            triplets.push_back(Triplet(row, baseColumn + 2, knotX));
            triplets.push_back(Triplet(row, baseColumn + 3, 1.0));
            b(row) = knotY;
            ++row;
            knotX = knots[i + 1].x;
            knotY = knots[i + 1].y;
            knotXSquared = knotX * knotX;
            knotXCubed = knotXSquared * knotX;
            triplets.push_back(Triplet(row, baseColumn + 0, knotXCubed));
            triplets.push_back(Triplet(row, baseColumn + 1, knotXSquared));
            triplets.push_back(Triplet(row, baseColumn + 2, knotX));
            triplets.push_back(Triplet(row, baseColumn + 3, 1.0));
            b(row) = knotY;
            ++row;
        }
        // Fill rows with derivative equations
        // Extreme knots second derivatives
        knotX = knots.first().x;
        triplets.push_back(Triplet(row, 0, 6.0 * knotX));
        triplets.push_back(Triplet(row, 1, 2.0));
        b(row) = 0.0;
        ++row;
        knotX = knots.last().x;
        triplets.push_back(Triplet(row, numberOfColumns - 4, 6.0 * knotX));
        triplets.push_back(Triplet(row, numberOfColumns - 3, 2.0));
        b(row) = 0.0;
        ++row;
        // Interior knots derivatives
        for (qint32 i = 1; i < knots.size() - 1; ++i) {
            knotX = knots[i].x;
            const qint32 baseColumn = i * 4;
            if (knots[i].isCorner) {
                triplets.push_back(Triplet(row, baseColumn - 4, 6.0 * knotX));
                triplets.push_back(Triplet(row, baseColumn - 3, 2.0));
                b(row) = 0.0;
                ++row;
                triplets.push_back(Triplet(row, baseColumn + 0, 6.0 * knotX));
                triplets.push_back(Triplet(row, baseColumn + 1, 2.0));
                b(row) = 0.0;
                ++row;
            } else {
                knotXSquared = knotX * knotX;
                triplets.push_back(Triplet(row, baseColumn - 4, 3.0 * knotXSquared));
                triplets.push_back(Triplet(row, baseColumn - 3, 2.0 * knotX));
                triplets.push_back(Triplet(row, baseColumn - 2, 1.0));
                triplets.push_back(Triplet(row, baseColumn + 0, -3.0 * knotXSquared));
                triplets.push_back(Triplet(row, baseColumn + 1, -2.0 * knotX));
                triplets.push_back(Triplet(row, baseColumn + 2, -1.0));
                b(row) = 0.0;
                ++row;
                triplets.push_back(Triplet(row, baseColumn - 4, 6.0 * knotX));
                triplets.push_back(Triplet(row, baseColumn - 3, 2.0));
                triplets.push_back(Triplet(row, baseColumn + 0, -6.0 * knotX));
                triplets.push_back(Triplet(row, baseColumn + 1, -2.0));
                b(row) = 0.0;
                ++row;
            }
        }
        // Solve
        A.setFromTriplets(triplets.begin(), triplets.end());
        Eigen::SparseLU<Matrix> solver(A);
        Vector x = solver.solve(b);
        // Fill coefficients
        for (qint32 i = 0; i < numberOfEquations; ++i) {
            row = i * 4;
            coefficients.append({x(row), x(row + 1), x(row + 2), x(row + 3)});
        }
    }
};

Curve::Curve()
    : m_d(new Private)
{
    addKnot({0.0, 0.0, false});
    addKnot({1.0, 1.0, false});
}

Curve::Curve(const KnotList &knotList)
    : m_d(new Private)
{
    setKnots(knotList);
}

Curve::Curve(const Curve &other)
    : m_d(new Private)
{
    m_d->knots = other.m_d->knots;
    m_d->coefficients = other.m_d->coefficients;
    m_d->isDirty = other.m_d->isDirty;
}

Curve::Curve(Curve &&other)
    : m_d(other.m_d.take())
{}

Curve::~Curve()
{}

Curve& Curve::operator=(const Curve &other)
{
    if (&other == this) {
        return *this;
    }
    setKnots(other.knots());
    return *this;
}

Curve& Curve::operator=(Curve &&other) noexcept
{
    if (&other == this) {
        return *this;
    }
    m_d.reset(other.m_d.take());
    return *this;
}

bool Curve::operator==(const Curve &other) const
{
    if (size() != other.size()) {
        return false;
    }
    for (qint32 i = 0; i < size(); ++i) {
        if (m_d->knots[i].x != other.m_d->knots[i].x ||
            m_d->knots[i].y != other.m_d->knots[i].y ||
            m_d->knots[i].isCorner != other.m_d->knots[i].isCorner) {
            return false;
        }
    }
    return true;
}

bool Curve::operator!=(const Curve &other) const
{
    return (*this == other);
}

const Curve::KnotList& Curve::knots() const
{
    return m_d->knots;
}

const Curve::Knot& Curve::knot(qint32 index) const
{
    return m_d->knots[index];
}

qint32 Curve::size() const
{
    return knots().size();
}

qint32 Curve::nearestKnotIndex(qreal x) const
{
    qint32 nearestIndex = 0;
    qreal minimumDistance = 1000.0;
    for (qint32 i = 0; i < size(); ++i) {
        const qreal distance = qAbs(knot(i).x - x);
        if (distance < minimumDistance) {
            nearestIndex = i;
            minimumDistance = distance;
        }
    }
    return nearestIndex;
}

bool Curve::isIdentity() const
{
    return size() == 2 &&
           knot(0).x == 0.0 && knot(0).y == 0.0 &&
           knot(1).x == 1.0 && knot(1).y == 1.0;
}

void Curve::setKnots(const KnotList &knotList)
{
    m_d->knots.clear();
    m_d->setDirty();
    if (knotList.isEmpty()) {
        return;
    }
    KnotList sortedKnots = knotList;
    std::sort(sortedKnots.begin(), sortedKnots.end(), [](const Knot &a, const Knot &b){ return a.x < b.x; });
    m_d->knots.append(sortedKnots[0]);
    for (qint32 i = 1; i < sortedKnots.size(); ++i) {
        if (sortedKnots[i].x == knots().last().x) {
            // Overwrite the last point if the current knot is at the same position
            m_d->knots.last() = sortedKnots[i];
        } else {
            // Add new knot
            m_d->knots.append(sortedKnots[i]);
        }
    }
}

qint32 Curve::setKnotX(qint32 index, qreal x)
{
    return setKnot(index, x, knot(index).y, knot(index).isCorner);
}

void Curve::setKnotY(qint32 index, qreal y)
{
    if (knot(index).y == y) {
        return;
    }
    m_d->knots[index].y = y;
    m_d->setDirty();
}

void Curve::setKnotAsCorner(qint32 index, bool isCorner)
{
    if (knot(index).isCorner == isCorner) {
        return;
    }
    m_d->knots[index].isCorner = isCorner;
    m_d->setDirty();
}

qint32 Curve::setKnot(qint32 index, qreal x, qreal y)
{
    return setKnot(index, x, y, knot(index).isCorner);
}

qint32 Curve::setKnot(qint32 index, qreal x, qreal y, bool isCorner)
{
    if (knot(index).x == x && knot(index).y == y && knot(index).isCorner == isCorner) {
        return index;
    }
    if (size() == 1 ||
        (index == 0 && x < knot(1).x) ||
        (index == size() - 1 && x > knot(index - 1).x) ||
        (index > 0 && index < size() - 1 && x > knot(index - 1).x && x < knot(index + 1).x)) {
        m_d->knots[index].x = x;
        m_d->knots[index].y = y;
        m_d->knots[index].isCorner = isCorner;
        m_d->setDirty();
        return index;
    }
    m_d->knots.remove(index);
    return addKnot({x, y, isCorner}, false);
}

qint32 Curve::addKnot(const Knot &knot, bool replace)
{
    for (qint32 i = 0; i < size(); ++i) {
        if (knot.x < this->knot(i).x) {
            m_d->knots.insert(i, knot);
            m_d->setDirty();
            return i;
        } else if (knot.x == this->knot(i).x) {
            if (replace) {
                m_d->knots[i] = knot;
            } else {
                m_d->knots.insert(i, knot);
            }
            m_d->setDirty();
            return i;
        }
    }
    m_d->knots.insert(size(), knot);
    m_d->setDirty();
    return size() - 1;
}

void Curve::removeKnot(qint32 index)
{
    m_d->knots.remove(index);
    m_d->setDirty();
}

void Curve::clear()
{
    m_d->knots.clear();
    m_d->setDirty();
}

qreal Curve::f(qreal x) const
{
    if (size() == 1) {
        // Constant function
        return m_d->knots[0].y;

    } else if (size() > 1) {
        if (x <= knots().first().x) {
            return knots().first().y;
        }
        if (x >= knots().last().x) {
            return knots().last().y;
        }
        if (m_d->isDirty) {
            m_d->computeCoefficients();
            m_d->isDirty = false;
        }

        quint32 segmentIndex;
        for (qint32 i = 1; i < size(); ++i) {
            if (x < knot(i).x) {
                segmentIndex = i - 1;
                break;
            }
        }

        const qreal xSquared = x * x;
        const qreal xCubed = xSquared * x;
        const Private::Coefficients& coefficients = m_d->coefficients[segmentIndex];
        return coefficients.a * xCubed + coefficients.b * xSquared +
               coefficients.c * x + coefficients.d;
    }

    // Return the identity function if there are no knots
    return x;
}

qreal Curve::operator()(qreal x) const
{
    return f(x);
}

template<typename PixelType>
const QVector<PixelType> Curve::lut(qint32 size, qint32 scale, bool clamp) const
{
    QVector<PixelType> curveLut(size);

    const qreal maxX = static_cast<qreal>(size - 1);
    const qreal scaleF = static_cast<qreal>(scale);

    if (this->size() == 0 || isIdentity()) {
        // Identity function
        for (qint32 i = 0; i < size; ++i) {
            curveLut[i] = static_cast<PixelType>(qRound(static_cast<qreal>(i) * scaleF / maxX));
        }

    } else if (this->size() == 1) {
        // Constant function
        const qreal y = clamp ? qMin(1.0, qMax(0.0, knot(0).y)) : knot(0).y;
        const PixelType scaledY = static_cast<PixelType>(qRound(y * scaleF));
        for (qint32 i = 0; i < size; ++i) {
            curveLut[i] = scaledY;
        }

    } else {
        for (qint32 i = 0; i < size; ++i) {
            const qreal x = static_cast<qreal>(i) / maxX;
            const qreal y = clamp ? qMin(1.0, qMax(0.0, f(x))) : f(x);
            const PixelType scaledY = static_cast<PixelType>(qRound(y * scaleF));
            curveLut[i] = scaledY;
        }
        for (const Knot &knot : knots()) {
            const qint32 knotX = static_cast<qint32>(knot.x * maxX);
            if (knotX < 0 || knotX > size - 1) {
                continue;
            }
            const qint32 scaledY = static_cast<qint32>(knot.y * scaleF);
            const qint32 knotY = clamp ? qMin(scale, qMax(0, scaledY)) : scaledY;
            curveLut[knotX] = knotY;
        }
    }

    return curveLut;
}

template const QVector<quint8> Curve::lut<quint8>(qint32 size, qint32 scale, bool clamp) const;
template const QVector<quint16> Curve::lut<quint16>(qint32 size, qint32 scale, bool clamp) const;
template const QVector<quint32> Curve::lut<quint32>(qint32 size, qint32 scale, bool clamp) const;
template const QVector<qint8> Curve::lut<qint8>(qint32 size, qint32 scale, bool clamp) const;
template const QVector<qint16> Curve::lut<qint16>(qint32 size, qint32 scale, bool clamp) const;
template const QVector<qint32> Curve::lut<qint32>(qint32 size, qint32 scale, bool clamp) const;
