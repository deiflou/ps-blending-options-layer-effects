/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef CURVEBUTTON_H
#define CURVEBUTTON_H

#include <QPushButton>
#include <QScopedPointer>

#include <Curve.h>

#include <Export.h>

class COMMON_EXPORT CurveButton : public QPushButton
{
    Q_OBJECT

public:
    explicit CurveButton(QWidget *parent = nullptr);
    ~CurveButton() override;

    const Curve& curve() const;

public Q_SLOTS:
    void setCurve(const Curve &newCurve);

Q_SIGNALS:
    void curveChanged(const Curve &newCurve);

protected:
    void paintEvent(QPaintEvent *e) override;

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
