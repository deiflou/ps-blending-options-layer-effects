/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef STROKEPOSITIONCOMBOBOX_H
#define STROKEPOSITIONCOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT StrokePositionComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit StrokePositionComboBox(QWidget *parent = nullptr);
    ~StrokePositionComboBox() override;

    LayerStyles::StrokePosition currentStrokePosition() const;

public Q_SLOTS:
    void setCurrentStrokePosition(LayerStyles::StrokePosition newStrokePosition);

Q_SIGNALS:
    void currentStrokePositionChanged(LayerStyles::StrokePosition newStrokePosition);
};

#endif
