/*
 * KDE. Krita Project.
 *
 * SPDX-FileCopyrightText: 2021 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef KIS_LEVELS_SLIDER_H
#define KIS_LEVELS_SLIDER_H

#include <QWidget>
#include <QVector>

#include <Export.h>

/**
 * @brief A base class for levels slider like widgets: a slider with a gradient
 * and multiple handles
 */
class COMMON_EXPORT KisLevelsSlider : public QWidget
{
    Q_OBJECT

public:
    KisLevelsSlider(QWidget *parent);
    ~KisLevelsSlider();

    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

    /**
     * @brief Gets the normalized position of a given handle
     */
    qreal handlePosition(int handleIndex) const;
    /**
     * @brief Gets the color associated with a given handle
     */
    QColor handleColor(int handleIndex) const;
    /**
     * @brief Gets the rect where the gradient will be painted
     */
    virtual QRect gradientRect() const;

public Q_SLOTS:
    /**
     * @brief Sets the normalized position of the given handle
     */
    virtual void setHandlePosition(int handleIndex, qreal newPosition);
    /**
     * @brief Sets the color associated with the given handle
     */
    virtual void setHandleColor(int handleIndex, const QColor &newColor);

Q_SIGNALS:
    /**
     * @brief Signal emited when the position of a handle changes
     */
    void handlePositionChanged(int handleIndex, qreal position);
    /**
     * @brief Signal emited when the color associated with a handle changes
     */
    void handleColorChanged(int handleIndex, const QColor &color);

protected:
    struct Handle
    {
        enum Shape
        {
            Shape_Normal,
            Shape_LeftHalf,
            Shape_RightHalf
        };

        int index;
        qreal position;
        QColor color;
        Shape shape {Shape_Normal};
    };

    static constexpr int handleWidth{11};
    static constexpr int handleHeight{11};
    static constexpr qreal minimumSpaceBetweenHandles{0.001};
    static constexpr qreal normalPositionIncrement{0.01};
    static constexpr qreal slowPositionIncrement{0.001};

    /**
     * @brief The collection of handles
     */
    QVector<Handle> m_handles;
    /**
     * @brief This variable indicates if the handles can have unordered
     * positions. If it is set to true then the user won't be able to move a
     * handle pass another one. If it is set to false then the ser will be able
     * to move the handles freely
     */
    int m_constrainPositions;

    int m_selectedHandle;
    int m_hoveredHandle;
    
    /**
     * @brief Regardless the index of a handle, they can be unordered in terms
     * of the position. This returns a sorted vector with the handles that have
     * a smaller position first. If two handles have the same position then the
     * index is used for sorting
     */
    QVector<Handle> sortedHandles() const;
    /**
     * @brief Given a normalized position, this function returns the closest
     * handle to that position
     */
    int closestHandleToPosition(qreal position) const;
    /**
     * @brief Given a widget-relative x position in pixels, this function
     * returns the normalized position relative to the gradient rect
     */
    qreal positionFromX(int x) const;
    /**
     * @brief Given a widget-relative x position, this function returns the
     * closest handle to that position
     */
    int closestHandleToX(int x) const;
    /**
     * @brief Given a gradient rect relative position, this function returns the
     * x position in pixels relative to the widget
     */
    int xFromPosition(qreal position) const;
    /**
     * @brief Derived classes must override this function to draw the gradient
     * inside the given rect. A border is automatically drawn after
     */
    virtual void paintGradient(QPainter &painter, const QRect &rect) = 0;
    /**
     * @brief Override this function to paint custom handles
     */
    virtual void paintHandle(QPainter &painter, const QRect &rect, const Handle &handle);

    void handleIncrementInput(int direction, Qt::KeyboardModifiers modifiers);

    void paintEvent(QPaintEvent *e) override;
    void mousePressEvent(QMouseEvent *e) override;
    void mouseMoveEvent(QMouseEvent *e) override;
    void leaveEvent(QEvent *e) override;
    void keyPressEvent(QKeyEvent *e) override;
    void wheelEvent(QWheelEvent *e) override;
};

class COMMON_EXPORT KisTonalRangeSlider : public KisLevelsSlider
{
    Q_OBJECT

public:
    KisTonalRangeSlider(QWidget *parent = nullptr);
    ~KisTonalRangeSlider();

    qreal blackRampStartPoint() const;
    qreal blackRampEndPoint() const;
    qreal whiteRampStartPoint() const;
    qreal whiteRampEndPoint() const;

public Q_SLOTS:
    virtual void setBlackRampStartPoint(qreal newBlackRampStartPoint);
    virtual void setBlackRampEndPoint(qreal newBlackRampEndPoint);
    virtual void setWhiteRampStartPoint(qreal newWhiteRampStartPoint);
    virtual void setWhiteRampEndPoint(qreal newWhiteRampEndPoint);
    
    virtual void reset(qreal newBlackRampStartPoint, qreal newBlackRampEndPoint,
                       qreal newWhiteRampStartPoint, qreal newWhiteRampEndPoint);
    void setColors(const QColor &newLeftColor, const QColor &newRightColor);

Q_SIGNALS:
    void blackRampStartPointChanged(qreal newBlackRampStartPoint);
    void blackRampEndPointChanged(qreal newBlackRampEndPoint);
    void whiteRampStartPointChanged(qreal newWhiteRampStartPoint);
    void whiteRampEndPointChanged(qreal newWhiteRampEndPoint);

protected:
    void paintGradient(QPainter &painter, const QRect &rect) override;

private Q_SLOTS:
    void slotHandlePositionChanged(int index, qreal position);

private:
    QColor m_leftColor;
    QColor m_rightColor;
};

#endif
