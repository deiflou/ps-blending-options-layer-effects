/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QHBoxLayout>
#include <QCheckBox>

#include "ColorBlendMethodComboBox.h"
#include "ColorButton.h"
#include "SliderSpinBox.h"
#include "CurveButton.h"

#include "SatinOptionsWidget.h"

class Q_DECL_HIDDEN SatinOptionsWidget::Private
{
public:
    ColorBlendMethodComboBox *comboBoxBlendMode {nullptr};
    ColorButton *buttonColor {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
    SliderSpinBox *sliderAngle {nullptr};
    SliderSpinBox *sliderDistance {nullptr};
    SliderSpinBox *sliderSize {nullptr};
    CurveButton *buttonCurve {nullptr};
    QCheckBox *checkBoxAntiAlias {nullptr};
    QCheckBox *checkBoxInvert {nullptr};
};

SatinOptionsWidget::SatinOptionsWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    LayerStyles::Satin defaultParams;

    m_d->comboBoxBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxBlendMode->setCurrentColorBlendMethod(defaultParams.blendMode);

    m_d->buttonColor = new ColorButton;
    m_d->buttonColor->setColor(defaultParams.color);

    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(defaultParams.opacity);

    m_d->sliderAngle = new SliderSpinBox;
    m_d->sliderAngle->setRange(0, 359);
    m_d->sliderAngle->setValue(defaultParams.angle);

    m_d->sliderDistance = new SliderSpinBox;
    m_d->sliderDistance->setRange(1, 250);
    m_d->sliderDistance->setValue(defaultParams.distance);

    m_d->sliderSize = new SliderSpinBox;
    m_d->sliderSize->setRange(0, 250);
    m_d->sliderSize->setValue(defaultParams.size);

    m_d->buttonCurve = new CurveButton;
    m_d->buttonCurve->setCurve(defaultParams.curve);

    m_d->checkBoxAntiAlias = new QCheckBox("Anti-alias");
    m_d->checkBoxAntiAlias->setChecked(defaultParams.antiAliasedCurve);

    m_d->checkBoxInvert = new QCheckBox("Invert");
    m_d->checkBoxInvert->setChecked(defaultParams.invertContour);

    QWidget *containerContour = new QWidget;
    QVBoxLayout *layoutContainerContour1 = new QVBoxLayout;
    layoutContainerContour1->setContentsMargins(0, 0, 0, 0);
    layoutContainerContour1->setSpacing(5);
    layoutContainerContour1->addWidget(m_d->checkBoxAntiAlias);
    layoutContainerContour1->addWidget(m_d->checkBoxInvert);
    QHBoxLayout *layoutContainerContour2 = new QHBoxLayout;
    layoutContainerContour2->setContentsMargins(0, 0, 0, 0);
    layoutContainerContour2->setSpacing(5);
    layoutContainerContour2->addWidget(m_d->buttonCurve);
    layoutContainerContour2->addLayout(layoutContainerContour1);
    containerContour->setLayout(layoutContainerContour2);

    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addRow("Blend mode:", m_d->comboBoxBlendMode);
    mainLayout->addRow("Color:", m_d->buttonColor);
    mainLayout->addRow("Opacity:", m_d->sliderOpacity);
    mainLayout->addRow("Angle:", m_d->sliderAngle);
    mainLayout->addRow("Distance:", m_d->sliderDistance);
    mainLayout->addRow("Size:", m_d->sliderSize);
    mainLayout->addRow("Contour:", containerContour);

    setLayout(mainLayout);

    connect(m_d->comboBoxBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->buttonColor, SIGNAL(colorChanged(const QColor&)), SIGNAL(paramsChanged()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderAngle, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderDistance, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderSize, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->buttonCurve, SIGNAL(curveChanged(const Curve&)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxAntiAlias, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxInvert, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
}

SatinOptionsWidget::~SatinOptionsWidget()
{}

LayerStyles::Satin SatinOptionsWidget::params() const
{
    LayerStyles::Satin params;

    params.blendMode = m_d->comboBoxBlendMode->currentColorBlendMethod();
    params.color = m_d->buttonColor->color();
    params.opacity = m_d->sliderOpacity->value();
    params.angle = m_d->sliderAngle->value();
    params.distance = m_d->sliderDistance->value();
    params.size = m_d->sliderSize->value();
    params.curve = m_d->buttonCurve->curve();
    params.antiAliasedCurve = m_d->checkBoxAntiAlias->isChecked();
    params.invertContour = m_d->checkBoxInvert->isChecked();
    
    return params;
}
