/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef LAYERSTYLES_H
#define LAYERSTYLES_H

#include <QImage>
#include <QPoint>
#include <QColor>
#include <QFlags>
#include <QGradient>

#include "imgproc/ImgProcCommon.h"
#include "imgproc/PixelBlending.h"
#include "imgproc/Curve.h"
#include "imgproc/TonalRange.h"

#include <Export.h>

namespace LayerStyles
{

using BlendMode = PixelBlending::ColorBlendMethod;

enum Channel
{
    Channel_Red = ImgProcCommon::PixelComponent_Red,
    Channel_Green = ImgProcCommon::PixelComponent_Green,
    Channel_Blue = ImgProcCommon::PixelComponent_Blue
};

enum Knockout
{
    Knockout_None,
    Knockout_Shallow,
    Knockout_Deep
};

enum GradientStyle
{
    GradientStyle_Linear,
    GradientStyle_Radial,
    GradientStyle_Angle,
    GradientStyle_Reflected,
    GradientStyle_Diamond,
};

enum GradientMethod
{
    GradientMethod_Perceptual,
    GradientMethod_Linear,
    GradientMethod_Classic
};

enum GlowTechnique
{
    GlowTechnique_Softer,
    GlowTechnique_Precise
};

enum GlowSource
{
    GlowSource_Center,
    GlowSource_Edge
};

enum StrokePosition
{
    StrokePosition_Outside,
    StrokePosition_Inside,
    StrokePosition_Center
};

enum StrokeFillType
{
    StrokeFillType_Color,
    StrokeFillType_Gradient,
    StrokeFillType_Pattern
};

enum BevelAndEmbossStyle
{
    BevelAndEmbossStyle_OuterBevel,
    BevelAndEmbossStyle_InnerBevel,
    BevelAndEmbossStyle_Emboss,
    BevelAndEmbossStyle_PillowEmboss,
    BevelAndEmbossStyle_StrokeEmboss
};

enum BevelAndEmbossTechnique
{
    BevelAndEmbossTechnique_Smooth,
    BevelAndEmbossTechnique_ChiselHard,
    BevelAndEmbossTechnique_ChiselSoft
};

enum BevelAndEmbossDirection
{
    BevelAndEmbossDirection_Up,
    BevelAndEmbossDirection_Down
};

struct BlendingOptions
{
    BlendMode blendMode {BlendMode::Normal};
    qint32 opacity {100};
    qint32 fillOpacity {100};
    qint32 channels {Channel_Red | Channel_Green | Channel_Blue};
    Knockout knockout {Knockout_None};
    bool blendInteriorEffectsAsGroup {false};
    bool blendClippedLayersAsGroup {true};
    bool transparencyShapesLayer {true};
    bool layerMaskHidesEffects {false};
    bool vectorMaskHidesEffects {false};
    bool layerKnocksOutDropShadow {true};
    PixelTonalRanges sourceTonalRanges;
    PixelTonalRanges destinationTonalRanges;
};

struct DropShadow
{
    BlendMode blendMode {BlendMode::Multiply};
    QColor color {Qt::black};
    qint32 opacity {75};
    qint32 angle {135};
    qint32 distance {10};
    qint32 spread {0};
    qint32 size {10};
    Curve curve;
    bool antiAliasedCurve {false};
    qint32 noise {0};
};

struct OuterGlow
{
    BlendMode blendMode {BlendMode::Screen};
    QColor color {Qt::white};
    QGradientStops gradient {{0.0, Qt::white}, {1.0, QColor(255, 255, 255, 0)}};
    GradientMethod gradientMethod {GradientMethod_Perceptual};
    qint32 gradientJitter {0};
    bool useGradient {false};
    qint32 opacity {75};
    GlowTechnique technique {GlowTechnique_Softer};
    qint32 spread {0};
    qint32 size {10};
    Curve curve;
    bool antiAliasedCurve {false};
    qint32 range {50};
    qint32 noise {0};
};

struct PatternOverlay
{
    BlendMode blendMode {BlendMode::Multiply};
    QImage pattern;
    qint32 opacity {100};
    qint32 angle {0};
    qint32 scale {100};
    QPoint offset {0, 0};
    bool linkWithLayer {true};
};

struct GradientOverlay
{
    BlendMode blendMode {BlendMode::Multiply};
    QGradientStops gradient {{0.0, Qt::black}, {1.0, Qt::white}};
    qint32 opacity {100};
    GradientStyle style {GradientStyle_Linear};
    bool dither {false};
    bool reverse {false};
    GradientMethod method {GradientMethod_Perceptual};
    qint32 angle {0};
    qint32 scale {100};
    QPoint offset {0, 0};
    bool alignWithLayer {true};
};

struct ColorOverlay
{
    BlendMode blendMode {BlendMode::Multiply};
    QColor color {Qt::black};
    qint32 opacity {100};
};

struct Satin
{
    BlendMode blendMode {BlendMode::Multiply};
    QColor color {Qt::black};
    qint32 opacity {75};
    qint32 angle {135};
    qint32 distance {10};
    qint32 size {10};
    Curve curve;
    bool antiAliasedCurve {false};
    bool invertContour {true};
};

struct InnerGlow
{
    BlendMode blendMode {BlendMode::Screen};
    QColor color {Qt::white};
    QGradientStops gradient {{0.0, Qt::white}, {1.0, QColor(255, 255, 255, 0)}};
    GradientMethod gradientMethod {GradientMethod_Perceptual};
    qint32 gradientJitter {0};
    bool useGradient {false};
    qint32 opacity {75};
    GlowTechnique technique {GlowTechnique_Softer};
    GlowSource source {GlowSource_Edge};
    qint32 spread {0};
    qint32 size {10};
    Curve curve;
    bool antiAliasedCurve {false};
    qint32 range {50};
    qint32 noise {0};
};

struct InnerShadow
{
    BlendMode blendMode {BlendMode::Multiply};
    QColor color {Qt::black};
    qint32 opacity {75};
    qint32 angle {135};
    qint32 distance {10};
    qint32 spread {0};
    qint32 size {10};
    Curve curve;
    bool antiAliasedCurve {false};
    qint32 noise {0};
};

struct Stroke
{
    qint32 size {1};
    StrokePosition position {StrokePosition_Outside};
    BlendMode blendMode {BlendMode::Normal};
    qint32 opacity {100};
    bool overprint {false};
    StrokeFillType fillType {StrokeFillType_Color};
    QColor color {Qt::black};
    QGradientStops gradient {{0.0, Qt::black}, {1.0, Qt::white}};
    GradientStyle gradientStyle {GradientStyle_Linear};
    bool ditherGradient {false};
    bool reverseGradient {false};
    GradientMethod gradientMethod {GradientMethod_Perceptual};
    qint32 gradientAngle {0};
    qint32 gradientScale {100};
    QPoint gradientOffset {0, 0};
    bool alignGradientWithLayer {true};
    QImage pattern;
    qint32 patternAngle {0};
    qint32 patternScale {100};
    QPoint patternOffset {0, 0};
    bool linkPatternWithLayer {true};
};

struct BevelAndEmboss
{
    BevelAndEmbossStyle style {BevelAndEmbossStyle_OuterBevel};
    BevelAndEmbossTechnique technique {BevelAndEmbossTechnique_Smooth};
    qint32 depth {100};
    BevelAndEmbossDirection direction {BevelAndEmbossDirection_Up};
    qint32 size {50};
    qint32 soften {0};
    qint32 lightAngle {135};
    qint32 lightAltitude {45};
    bool applyProfileContour {false};
    Curve profileCurve;
    bool antiAliasedProfileCurve {false};
    qint32 profileRange {100};
    bool applyTexture {false};
    QImage texture;
    qint32 textureScale {100};
    QPoint textureOffset {0, 0};
    qint32 textureDepth {100};
    bool invertTexture {false};
    bool linkTextureWithLayer {true};
    Curve glossCurve;
    bool antiAliasedGlossCurve {false};
    BlendMode lightBlendMode {BlendMode::Screen};
    QColor lightColor {Qt::white};
    qint32 lightOpacity {75};
    BlendMode shadowBlendMode {BlendMode::Multiply};
    QColor shadowColor {Qt::black};
    qint32 shadowOpacity {75};
};

QImage COMMON_EXPORT generateDropShadowImage(const DropShadow &options, const QImage &layerMask);
QImage COMMON_EXPORT generateOuterGlowImage(const OuterGlow &options, const QImage &layerMask);
QImage COMMON_EXPORT generatePatternOverlayImage(const PatternOverlay &options, const QImage &layerMask);
QImage COMMON_EXPORT generateGradientOverlayImage(const GradientOverlay &options, const QImage &layerMask);
QImage COMMON_EXPORT generateColorOverlayImage(const ColorOverlay &options, const QImage &layerMask);
QImage COMMON_EXPORT generateSatinImage(const Satin &options, const QImage &layerMask);
QImage COMMON_EXPORT generateInnerGlowImage(const InnerGlow &options, const QImage &layerMask);
QImage COMMON_EXPORT generateInnerShadowImage(const InnerShadow &options, const QImage &layerMask);
QPair<QImage, QImage> COMMON_EXPORT generateStrokeImage(const Stroke &options, const QImage &layerMask);
QPair<QImage, QImage> COMMON_EXPORT generateBevelAndEmbossImage(const BevelAndEmboss &options,
                                                                const QImage &layerMask,
                                                                StrokePosition strokePosition = StrokePosition_Outside);

}

#endif
