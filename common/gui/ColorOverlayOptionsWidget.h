/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef COLOROVERLAYOPTIONSWIDGET_H
#define COLOROVERLAYOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT ColorOverlayOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ColorOverlayOptionsWidget(QWidget *parent = nullptr);
    ~ColorOverlayOptionsWidget() override;

    LayerStyles::ColorOverlay params() const;

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
