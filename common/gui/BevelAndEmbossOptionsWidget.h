/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef BEVELANDEMBOSSOPTIONSWIDGET_H
#define BEVELANDEMBOSSOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT BevelAndEmbossOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit BevelAndEmbossOptionsWidget(QWidget *parent = nullptr);
    ~BevelAndEmbossOptionsWidget() override;

    LayerStyles::BevelAndEmboss params() const;

public Q_SLOTS:
    void setTexture(const QImage &texture);

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
