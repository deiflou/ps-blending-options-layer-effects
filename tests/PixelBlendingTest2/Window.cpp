/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QVBoxLayout>
#include <QComboBox>
#include <QCheckBox>

#include <SliderSpinBox.h>
#include <Viewport.h>
#include <AlphaBlendMethodComboBox.h>
#include <ColorBlendMethodComboBox.h>
#include <ColorButton.h>
#include <TonalRangesSelector.h>
#include <PixelBlending.h>
#include <ImgProcUtil.h>
#include <ImgProcCommon.h>

#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    AlphaBlendMethodComboBox *comboBoxAlphaBlendMethod {nullptr};
    ColorBlendMethodComboBox *comboBoxColorBlendMethod {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
    QCheckBox *checkBoxRed {nullptr};
    QCheckBox *checkBoxGreen {nullptr};
    QCheckBox *checkBoxBlue {nullptr};
    QCheckBox *checkBoxUseSpecialBlendModes {nullptr};
    QCheckBox *checkBoxIgnoreSourceAlpha {nullptr};
    QCheckBox *checkBoxBlendWithBackdrop {nullptr};
    TonalRangesSelector *widgetTonalRangesSelector {nullptr};
    QImage dstImage;
    QImage srcImage;

    QImage generateImage() const
    {
        const PixelBlending::AlphaBlendMethod alphaBlendMethod = comboBoxAlphaBlendMethod->currentAlphaBlendMethod();
        const PixelBlending::ColorBlendMethod colorBlendMethod = comboBoxColorBlendMethod->currentColorBlendMethod();
        const quint8 opacity = static_cast<quint8>(sliderOpacity->value() * 255 / 100);
        QImage out(dstImage);

        if (checkBoxBlendWithBackdrop->isChecked()) {
            const qint32 activeColorComponents = (checkBoxBlue->isChecked() ? ImgProcCommon::PixelComponent_Blue : 0) |
                                                 (checkBoxGreen->isChecked() ? ImgProcCommon::PixelComponent_Green : 0) |
                                                 (checkBoxRed->isChecked() ? ImgProcCommon::PixelComponent_Red : 0);
            PixelBlending::blendWithBackdrop(srcImage, out, QImage(), activeColorComponents, opacity,
                                             widgetTonalRangesSelector->destinationTonalRanges());

        } else {
            PixelBlending::blend(srcImage, out, alphaBlendMethod, colorBlendMethod,
                                 checkBoxUseSpecialBlendModes->isChecked(),
                                 opacity, checkBoxIgnoreSourceAlpha->isChecked(),
                                 widgetTonalRangesSelector->sourceTonalRanges());
        }
                             
        return out;
    }
};

Window::Window()
    : m_d(new Private)
{
    m_d->dstImage = QImage(":/bg_2_2").convertToFormat(QImage::Format_ARGB32);
    m_d->srcImage = QImage(":/bg_3").convertToFormat(QImage::Format_ARGB32);

    m_d->comboBoxAlphaBlendMethod = new AlphaBlendMethodComboBox;
    m_d->comboBoxColorBlendMethod = new ColorBlendMethodComboBox;

    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(100);

    m_d->checkBoxRed = new QCheckBox("Red");
    m_d->checkBoxRed->setChecked(true);
    m_d->checkBoxGreen = new QCheckBox("Green");
    m_d->checkBoxGreen->setChecked(true);
    m_d->checkBoxBlue = new QCheckBox("Blue");
    m_d->checkBoxBlue->setChecked(true);
    QWidget *containerChannels = new QWidget;
    QHBoxLayout *layoutChannels = new QHBoxLayout;
    layoutChannels->setContentsMargins(0, 0, 0, 0);
    layoutChannels->setSpacing(5);
    layoutChannels->addWidget(m_d->checkBoxRed);
    layoutChannels->addWidget(m_d->checkBoxGreen);
    layoutChannels->addWidget(m_d->checkBoxBlue);
    layoutChannels->addStretch();
    containerChannels->setLayout(layoutChannels);

    m_d->checkBoxUseSpecialBlendModes = new QCheckBox("Use special blend modes");
    m_d->checkBoxIgnoreSourceAlpha = new QCheckBox("Ignore source alpha");
    m_d->checkBoxBlendWithBackdrop = new QCheckBox("Blend with backdrop");
    QWidget *containerSpecialOptions = new QWidget;
    QVBoxLayout *layoutSpecialOptions = new QVBoxLayout;
    layoutSpecialOptions->setContentsMargins(0, 0, 0, 0);
    layoutSpecialOptions->setSpacing(5);
    layoutSpecialOptions->addWidget(m_d->checkBoxUseSpecialBlendModes);
    layoutSpecialOptions->addWidget(m_d->checkBoxIgnoreSourceAlpha);
    layoutSpecialOptions->addWidget(m_d->checkBoxBlendWithBackdrop);
    layoutSpecialOptions->addStretch();
    containerSpecialOptions->setLayout(layoutSpecialOptions);

    m_d->widgetTonalRangesSelector = new TonalRangesSelector;

    QWidget *containerBlendingOptions = new QWidget;

    QFormLayout *layoutBlendingOptions1 = new QFormLayout;
    layoutBlendingOptions1->setContentsMargins(10, 10, 10, 10);
    layoutBlendingOptions1->setSpacing(5);
    layoutBlendingOptions1->addRow("Alpha operator:", m_d->comboBoxAlphaBlendMethod);
    layoutBlendingOptions1->addRow("Color operator:", m_d->comboBoxColorBlendMethod);
    layoutBlendingOptions1->addRow("Opacity:", m_d->sliderOpacity);
    layoutBlendingOptions1->addRow("Channels:", containerChannels);
    layoutBlendingOptions1->addRow("Special options:", containerSpecialOptions);
    layoutBlendingOptions1->addRow("Blend if:", m_d->widgetTonalRangesSelector);

    QVBoxLayout *layoutBlendingOptions2 = new QVBoxLayout;
    layoutBlendingOptions2->setContentsMargins(0, 0, 0, 0);
    layoutBlendingOptions2->setSpacing(0);
    layoutBlendingOptions2->addLayout(layoutBlendingOptions1);
    layoutBlendingOptions2->addStretch();
    containerBlendingOptions->setLayout(layoutBlendingOptions2);

    addControlsPanel(containerBlendingOptions, "Options");

    viewport()->setImage(m_d->generateImage());

    connect(m_d->comboBoxAlphaBlendMethod, SIGNAL(currentAlphaBlendMethodChanged(PixelBlending::AlphaBlendMethod)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->comboBoxColorBlendMethod, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxRed, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxGreen, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxBlue, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxUseSpecialBlendModes, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxIgnoreSourceAlpha, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxBlendWithBackdrop, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->widgetTonalRangesSelector, SIGNAL(tonalRangesChanged(PixelTonalRanges, PixelTonalRanges)),
            this, SLOT(requestViewportUpdate()));
    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );
}

Window::~Window()
{}
