/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef GLOWTECHNIQUECOMBOBOX_H
#define GLOWTECHNIQUECOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>
#include <Export.h>

class COMMON_EXPORT GlowTechniqueComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit GlowTechniqueComboBox(QWidget *parent = nullptr);
    ~GlowTechniqueComboBox() override;

    LayerStyles::GlowTechnique currentGlowTechnique() const;

public Q_SLOTS:
    void setCurrentGlowTechnique(LayerStyles::GlowTechnique newGlowTechnique);

Q_SIGNALS:
    void currentGlowTechniqueChanged(LayerStyles::GlowTechnique newGlowTechnique);
};

#endif
