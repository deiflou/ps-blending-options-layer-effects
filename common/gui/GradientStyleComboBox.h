/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef GRADIENTSTYLECOMBOBOX_H
#define GRADIENTSTYLECOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>
#include <Export.h>

class COMMON_EXPORT GradientStyleComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit GradientStyleComboBox(QWidget *parent = nullptr);
    ~GradientStyleComboBox() override;

    LayerStyles::GradientStyle currentGradientStyle() const;

public Q_SLOTS:
    void setCurrentGradientStyle(LayerStyles::GradientStyle newGradientStyle);

Q_SIGNALS:
    void currentGradientStyleChanged(LayerStyles::GradientStyle newGradientStyle);
};

#endif
