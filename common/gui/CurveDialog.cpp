/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QSpinBox>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "CurveWidget.h"
#include "CurveDialog.h"

class Q_DECL_HIDDEN CurveDialog::Private
{
public:
    CurveWidget *widgetCurve {nullptr};
    QSpinBox *spinBoxInput {nullptr};
    QSpinBox *spinBoxOutput {nullptr};
    QCheckBox *checkBoxCorner {nullptr};
    QDialogButtonBox *buttonBox {nullptr};
};

CurveDialog::CurveDialog(QWidget *parent, const Curve &initialCurve, Qt::WindowFlags windowFlags)
    : QDialog(parent, windowFlags)
    , m_d(new Private)
{
    m_d->widgetCurve = new CurveWidget;
    m_d->widgetCurve->setFixedSize(256, 256);
    m_d->widgetCurve->setCurve(initialCurve);

    m_d->spinBoxInput = new QSpinBox;
    m_d->spinBoxInput->setRange(0, 100);
    m_d->spinBoxInput->setPrefix("Input: ");
    m_d->spinBoxInput->setSuffix("%");

    m_d->spinBoxOutput = new QSpinBox;
    m_d->spinBoxOutput->setRange(0, 100);
    m_d->spinBoxOutput->setPrefix("Output: ");
    m_d->spinBoxOutput->setSuffix("%");

    m_d->checkBoxCorner = new QCheckBox("Corner");

    m_d->buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    QVBoxLayout *curveLayout = new QVBoxLayout;
    curveLayout->setContentsMargins(2, 2, 2, 0);
    curveLayout->addWidget(m_d->widgetCurve);

    QVBoxLayout *spinBoxesLayout = new QVBoxLayout;
    spinBoxesLayout->setContentsMargins(0, 0, 0, 0);
    spinBoxesLayout->setSpacing(5);
    spinBoxesLayout->addWidget(m_d->spinBoxInput);
    spinBoxesLayout->addWidget(m_d->spinBoxOutput);

    QHBoxLayout *controlsLayout = new QHBoxLayout;
    controlsLayout->setContentsMargins(10, 0, 10, 0);
    controlsLayout->setSpacing(5);
    controlsLayout->addLayout(spinBoxesLayout);
    controlsLayout->addStretch();
    controlsLayout->addWidget(m_d->checkBoxCorner);

    QVBoxLayout *buttonBoxLayout = new QVBoxLayout;
    buttonBoxLayout->setContentsMargins(10, 0, 10, 10);
    buttonBoxLayout->addWidget(m_d->buttonBox);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(10);
    mainLayout->addLayout(curveLayout);
    mainLayout->addLayout(controlsLayout);
    mainLayout->addLayout(buttonBoxLayout);

    setLayout(mainLayout);
    setFixedSize(0, 0);

    auto curveChangedSlot = 
        [this]()
        {
            const qint32 index = m_d->widgetCurve->selectedKnotIndex();
            m_d->spinBoxInput->setValue(static_cast<qint32>(qRound(curve().knot(index).x * 100.0)));
            m_d->spinBoxOutput->setValue(static_cast<qint32>(qRound(curve().knot(index).y * 100.0)));
            m_d->checkBoxCorner->setChecked(curve().knot(index).isCorner);
        };

    curveChangedSlot();

    connect(m_d->widgetCurve, &CurveWidget::curveChanged, this, &CurveDialog::curveChanged);
    connect(m_d->widgetCurve, &CurveWidget::curveChanged, curveChangedSlot);
    connect(m_d->widgetCurve, &CurveWidget::selectedKnotChanged, curveChangedSlot);
    connect(m_d->spinBoxInput, QOverload<qint32>::of(&QSpinBox::valueChanged),
        [this](qint32 value)
        {
            m_d->widgetCurve->moveKnot(
                m_d->widgetCurve->selectedKnotIndex(),
                static_cast<qreal>(value) / 100.0,
                m_d->widgetCurve->selectedKnot().y
            );
        }
    );
    connect(m_d->spinBoxOutput, QOverload<qint32>::of(&QSpinBox::valueChanged),
        [this](qint32 value)
        {
            m_d->widgetCurve->moveKnot(
                m_d->widgetCurve->selectedKnotIndex(),
                m_d->widgetCurve->selectedKnot().x,
                static_cast<qreal>(value) / 100.0
            );
        }
    );
    connect(m_d->checkBoxCorner, &QCheckBox::toggled,
        [this](bool checked)
        {
            m_d->widgetCurve->setKnotAsCorner(
                m_d->widgetCurve->selectedKnotIndex(),
                checked
            );
        }
    );
    connect(m_d->buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(m_d->buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

CurveDialog::~CurveDialog()
{}

const Curve& CurveDialog::curve() const
{
    return m_d->widgetCurve->curve();
}
