/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef BEVELANDEMBOSSSTYLECOMBOBOX_H
#define BEVELANDEMBOSSSTYLECOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT BevelAndEmbossStyleComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit BevelAndEmbossStyleComboBox(QWidget *parent = nullptr);
    ~BevelAndEmbossStyleComboBox() override;

    LayerStyles::BevelAndEmbossStyle currentStyle() const;

public Q_SLOTS:
    void setCurrentStyle(LayerStyles::BevelAndEmbossStyle newStyle);

Q_SIGNALS:
    void currentStyleChanged(LayerStyles::BevelAndEmbossStyle newStyle);
};

#endif
