/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <QAbstractScrollArea>
#include <QImage>
#include <QPointF>
#include <QScopedPointer>

#include <Export.h>

class COMMON_EXPORT Viewport : public QAbstractScrollArea
{
    Q_OBJECT

public:
    explicit Viewport(QWidget *parent = nullptr);
    ~Viewport() override;

    QImage image() const;
    qreal scale() const;
    QPoint position() const;

    void setScaleCheckerBoardPattern(bool scale);
    
public Q_SLOTS:
    void setImage(const QImage &newImage);

Q_SIGNALS:
    void scaleChanged(qreal newScale);

protected:
    void paintEvent(QPaintEvent*) override;
    void mousePressEvent(QMouseEvent *e) override;
    void mouseMoveEvent(QMouseEvent *e) override;
    void wheelEvent(QWheelEvent *e) override;
    void resizeEvent(QResizeEvent*) override;
    void scrollContentsBy(int dx, int dy) override;
    
private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
