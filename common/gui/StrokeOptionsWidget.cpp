/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QVBoxLayout>
#include <QStackedWidget>
#include <QCheckBox>

#include "SliderSpinBox.h"
#include "ColorBlendMethodComboBox.h"
#include "StrokePositionComboBox.h"
#include "StrokeFillTypeComboBox.h"
#include "ColorButton.h"
#include "GradientStyleComboBox.h"
#include "GradientMethodComboBox.h"

#include "StrokeOptionsWidget.h"

class Q_DECL_HIDDEN StrokeOptionsWidget::Private
{
public:
    SliderSpinBox *sliderSize {nullptr};
    StrokePositionComboBox *comboBoxPosition {nullptr};
    ColorBlendMethodComboBox *comboBoxBlendMode {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
    QCheckBox *checkBoxOverprint {nullptr};
    StrokeFillTypeComboBox *comboBoxFillType {nullptr};
    ColorButton *buttonColor {nullptr};
    GradientStyleComboBox *comboBoxGradientStyle {nullptr};
    QCheckBox *checkBoxGradientDither {nullptr};
    QCheckBox *checkBoxGradientReverse {nullptr};
    GradientMethodComboBox *comboBoxGradientMethod {nullptr};
    SliderSpinBox *sliderGradientAngle {nullptr};
    SliderSpinBox *sliderGradientScale {nullptr};
    QCheckBox *checkBoxGradientAlignWithLayer {nullptr};
    SliderSpinBox *sliderPatternAngle {nullptr};
    SliderSpinBox *sliderPatternScale {nullptr};
    QCheckBox *checkBoxLinkPatternWithLayer {nullptr};
    QGradientStops gradient;
    QImage pattern;
};

StrokeOptionsWidget::StrokeOptionsWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    LayerStyles::Stroke defaultParams;

    m_d->sliderSize = new SliderSpinBox;
    m_d->sliderSize->setRange(1, 250);
    m_d->sliderSize->setValue(defaultParams.size);

    m_d->comboBoxPosition = new StrokePositionComboBox;
    m_d->comboBoxPosition->setCurrentStrokePosition(defaultParams.position);

    m_d->comboBoxBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxBlendMode->setCurrentColorBlendMethod(defaultParams.blendMode);

    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(defaultParams.opacity);

    m_d->checkBoxOverprint = new QCheckBox("Overprint");
    m_d->checkBoxOverprint->setChecked(defaultParams.overprint);

    m_d->comboBoxFillType = new StrokeFillTypeComboBox;
    m_d->comboBoxFillType->setCurrentStrokeFillType(defaultParams.fillType);

    m_d->buttonColor = new ColorButton;
    m_d->buttonColor->setColor(defaultParams.color);

    m_d->comboBoxGradientStyle = new GradientStyleComboBox;
    m_d->comboBoxGradientStyle->setCurrentGradientStyle(defaultParams.gradientStyle);

    m_d->checkBoxGradientDither = new QCheckBox;
    m_d->checkBoxGradientDither->setChecked(defaultParams.ditherGradient);

    m_d->checkBoxGradientReverse = new QCheckBox;
    m_d->checkBoxGradientReverse->setChecked(defaultParams.reverseGradient);

    m_d->comboBoxGradientMethod = new GradientMethodComboBox;
    m_d->comboBoxGradientMethod->setCurrentGradientMethod(defaultParams.gradientMethod);

    m_d->sliderGradientAngle = new SliderSpinBox;
    m_d->sliderGradientAngle->setRange(0, 359);
    m_d->sliderGradientAngle->setValue(defaultParams.gradientAngle);

    m_d->sliderGradientScale = new SliderSpinBox;
    m_d->sliderGradientScale->setRange(10, 150);
    m_d->sliderGradientScale->setValue(defaultParams.gradientScale);

    m_d->checkBoxGradientAlignWithLayer = new QCheckBox;
    m_d->checkBoxGradientAlignWithLayer->setChecked(defaultParams.alignGradientWithLayer);
    
    m_d->gradient = defaultParams.gradient;

    m_d->sliderPatternAngle = new SliderSpinBox;
    m_d->sliderPatternAngle->setRange(0, 359);
    m_d->sliderPatternAngle->setValue(defaultParams.patternAngle);

    m_d->sliderPatternScale = new SliderSpinBox;
    m_d->sliderPatternScale->setRange(1, 1000);
    m_d->sliderPatternScale->setValue(defaultParams.patternScale);

    m_d->checkBoxLinkPatternWithLayer = new QCheckBox;
    m_d->checkBoxLinkPatternWithLayer->setChecked(defaultParams.linkPatternWithLayer);

    QVBoxLayout *colorLayout = new QVBoxLayout;
    colorLayout->setContentsMargins(0, 0, 0, 0);
    colorLayout->setSpacing(5);
    colorLayout->addWidget(m_d->buttonColor);
    colorLayout->addStretch();

    QFormLayout *gradientLayout = new QFormLayout;
    gradientLayout->setContentsMargins(0, 0, 0, 0);
    gradientLayout->setSpacing(5);
    gradientLayout->addRow("Style:", m_d->comboBoxGradientStyle);
    gradientLayout->addRow("Dither:", m_d->checkBoxGradientDither);
    gradientLayout->addRow("Reverse:", m_d->checkBoxGradientReverse);
    gradientLayout->addRow("Method:", m_d->comboBoxGradientMethod);
    gradientLayout->addRow("Angle:", m_d->sliderGradientAngle);
    gradientLayout->addRow("Scale:", m_d->sliderGradientScale);
    gradientLayout->addRow("Align with layer:", m_d->checkBoxGradientAlignWithLayer);

    QFormLayout *patternLayout = new QFormLayout;
    patternLayout->setContentsMargins(0, 0, 0, 0);
    patternLayout->setSpacing(5);
    patternLayout->addRow("Angle:", m_d->sliderPatternAngle);
    patternLayout->addRow("Scale:", m_d->sliderPatternScale);
    patternLayout->addRow("Link with layer:", m_d->checkBoxLinkPatternWithLayer);

    QStackedWidget *fillTypeContainer = new QStackedWidget;
    QWidget *colorContainer = new QWidget;
    QWidget *gradientContainer = new QWidget;
    QWidget *patternContainer = new QWidget;
    colorContainer->setLayout(colorLayout);
    gradientContainer->setLayout(gradientLayout);
    patternContainer->setLayout(patternLayout);
    fillTypeContainer->addWidget(colorContainer);
    fillTypeContainer->addWidget(gradientContainer);
    fillTypeContainer->addWidget(patternContainer);

    QVBoxLayout *fillTypeLayout = new QVBoxLayout;
    fillTypeLayout->setContentsMargins(0, 0, 0, 0);
    fillTypeLayout->setSpacing(5);
    fillTypeLayout->addWidget(m_d->comboBoxFillType);
    fillTypeLayout->addWidget(fillTypeContainer);

    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addRow("Size:", m_d->sliderSize);
    mainLayout->addRow("Position:", m_d->comboBoxPosition);
    mainLayout->addRow("Blend mode:", m_d->comboBoxBlendMode);
    mainLayout->addRow("Opacity:", m_d->sliderOpacity);
    mainLayout->addRow("Overprint:", m_d->checkBoxOverprint);
    mainLayout->addRow("Fill type:", fillTypeLayout);

    setLayout(mainLayout);

    connect(m_d->sliderSize, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxPosition, SIGNAL(currentStrokePositionChanged(LayerStyles::StrokePosition)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxOverprint, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxFillType, SIGNAL(currentStrokeFillTypeChanged(LayerStyles::StrokeFillType)), SIGNAL(paramsChanged()));
    connect(m_d->buttonColor, SIGNAL(colorChanged(const QColor&)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxGradientStyle, SIGNAL(currentGradientStyleChanged(LayerStyles::GradientStyle)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxGradientDither, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxGradientReverse, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxGradientMethod, SIGNAL(currentGradientMethodChanged(LayerStyles::GradientMethod)), SIGNAL(paramsChanged()));
    connect(m_d->sliderGradientAngle, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderGradientScale, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxGradientAlignWithLayer, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->sliderPatternAngle, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderPatternScale, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxLinkPatternWithLayer, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));

    connect(m_d->comboBoxFillType, SIGNAL(activated(int)), fillTypeContainer, SLOT(setCurrentIndex(int)));
}

StrokeOptionsWidget::~StrokeOptionsWidget()
{}

LayerStyles::Stroke StrokeOptionsWidget::params() const
{
    LayerStyles::Stroke params;

    params.size = m_d->sliderSize->value();
    params.position = m_d->comboBoxPosition->currentStrokePosition();
    params.blendMode = m_d->comboBoxBlendMode->currentColorBlendMethod();
    params.opacity = m_d->sliderOpacity->value();
    params.overprint = m_d->checkBoxOverprint->isChecked();
    params.fillType = m_d->comboBoxFillType->currentStrokeFillType();
    params.color = m_d->buttonColor->color();
    params.gradient = m_d->gradient;
    params.gradientStyle = m_d->comboBoxGradientStyle->currentGradientStyle();
    params.ditherGradient = m_d->checkBoxGradientDither->isChecked();
    params.reverseGradient = m_d->checkBoxGradientReverse->isChecked();
    params.gradientMethod = m_d->comboBoxGradientMethod->currentGradientMethod();
    params.gradientAngle = m_d->sliderGradientAngle->value();
    params.gradientScale = m_d->sliderGradientScale->value();
    params.alignGradientWithLayer = m_d->checkBoxGradientAlignWithLayer->isChecked();
    params.pattern = m_d->pattern;
    params.patternAngle = m_d->sliderPatternAngle->value();
    params.patternScale = m_d->sliderPatternScale->value();
    params.linkPatternWithLayer = m_d->checkBoxLinkPatternWithLayer->isChecked();
    
    return params;
}

void StrokeOptionsWidget::setGradient(const QGradientStops &gradient)
{
    m_d->gradient = gradient;
    emit paramsChanged();
}

void StrokeOptionsWidget::setPattern(const QImage &pattern)
{
    m_d->pattern = pattern;
    emit paramsChanged();
}
