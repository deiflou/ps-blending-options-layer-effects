/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "Layer.h"

class Layer::Private
{
public:
    bool blendingOptionsAreDirty {true};
    QImage layerImage;
    mutable QImage layerShape;
    QImage rasterMask;
    bool rasterMaskIsActive {true};
    LayerStyles::BlendingOptions blendingOptions;

    bool dropShadowIsActive {false};
    mutable bool dropShadowIsDirty {true};
    LayerStyles::DropShadow dropShadowOptions;
    mutable QImage dropShadowImplicitLayer;

    bool outerGlowIsActive {false};
    mutable bool outerGlowIsDirty {true};
    LayerStyles::OuterGlow outerGlowOptions;
    mutable QImage outerGlowImplicitLayer;

    bool patternOverlayIsActive {false};
    mutable bool patternOverlayIsDirty {true};
    LayerStyles::PatternOverlay patternOverlayOptions;
    mutable QImage patternOverlayImplicitLayer;

    bool gradientOverlayIsActive {false};
    mutable bool gradientOverlayIsDirty {true};
    LayerStyles::GradientOverlay gradientOverlayOptions;
    mutable QImage gradientOverlayImplicitLayer;

    bool colorOverlayIsActive {false};
    mutable bool colorOverlayIsDirty {true};
    LayerStyles::ColorOverlay colorOverlayOptions;
    mutable QImage colorOverlayImplicitLayer;

    bool satinIsActive {false};
    mutable bool satinIsDirty {true};
    LayerStyles::Satin satinOptions;
    mutable QImage satinImplicitLayer;

    bool innerGlowIsActive {false};
    mutable bool innerGlowIsDirty {true};
    LayerStyles::InnerGlow innerGlowOptions;
    mutable QImage innerGlowImplicitLayer;

    bool innerShadowIsActive {false};
    mutable bool innerShadowIsDirty {true};
    LayerStyles::InnerShadow innerShadowOptions;
    mutable QImage innerShadowImplicitLayer;

    bool strokeIsActive {false};
    mutable bool strokeIsDirty {true};
    LayerStyles::Stroke strokeOptions;
    mutable QImage strokeImplicitLayer;
    mutable QImage strokeFill;
    mutable QImage strokeShape;

    bool bevelAndEmbossIsActive {false};
    mutable bool bevelAndEmbossIsDirty {true};
    LayerStyles::BevelAndEmboss bevelAndEmbossOptions;
    mutable QImage bevelAndEmbossLightImplicitLayer;
    mutable QImage bevelAndEmbossShadowImplicitLayer;

    void generateLayerShape() const
    {
        if (layerImage.isNull()) {
            layerShape = QImage();
        } else {
            if (blendingOptions.transparencyShapesLayer) {
                layerShape = layerImage.convertToFormat(QImage::Format_Alpha8);
            } else {
                layerShape = QImage(layerImage.size(), QImage::Format_Alpha8);
                layerShape.fill(255);
            }
            if (!blendingOptions.layerMaskHidesEffects && rasterMaskIsActive && !rasterMask.isNull()) {
                PixelBlending::multiplyMasks(layerShape, rasterMask);
            }
        }
    }

    void generateStrokeImages() const
    {
        QPair<QImage, QImage> strokeOutput =
            LayerStyles::generateStrokeImage(strokeOptions, layerShape);
        strokeFill = strokeOutput.first;
        strokeShape = strokeOutput.second;
        strokeImplicitLayer = strokeFill;
        PixelBlending::multiplyAlphaChannel(strokeImplicitLayer, strokeShape);
    }

    void generateBevelAndEmbossImages() const
    {
        QPair<QImage, QImage> bevelAndEmbossOutput =
            LayerStyles::generateBevelAndEmbossImage(
                bevelAndEmbossOptions,
                layerShape,
                strokeOptions.position
            );
        bevelAndEmbossLightImplicitLayer = bevelAndEmbossOutput.first;
        bevelAndEmbossShadowImplicitLayer = bevelAndEmbossOutput.second;
    }

    void setAllEffectsDirty()
    {
        dropShadowIsDirty = outerGlowIsDirty = patternOverlayIsDirty =
        gradientOverlayIsDirty = colorOverlayIsDirty = satinIsDirty =
        innerGlowIsDirty = innerShadowIsDirty = strokeIsDirty =
        bevelAndEmbossIsDirty = true;
    }
};

Layer::Layer()
    : m_d(new Private)
{}

Layer::~Layer()
{}

QImage Layer::image() const
{
    return m_d->layerImage;
}

QImage Layer::shape() const
{
    return m_d->layerShape;
}

QImage Layer::rasterMask() const
{
    return m_d->rasterMask;
}

bool Layer::rasterMaskIsActive() const
{
    return m_d->rasterMaskIsActive;
}

const LayerStyles::BlendingOptions& Layer::blendingOptions() const
{
    return m_d->blendingOptions;
}

const LayerStyles::DropShadow& Layer::dropShadowOptions() const
{
    return m_d->dropShadowOptions;
}

bool Layer::isDropShadowActive() const
{
    return m_d->dropShadowIsActive;
}

QImage Layer::dropShadowImage() const
{
    if (m_d->layerShape.isNull() || !m_d->dropShadowIsActive) {
        return QImage();
    }
    if (m_d->dropShadowIsDirty) {
        m_d->dropShadowImplicitLayer = LayerStyles::generateDropShadowImage(m_d->dropShadowOptions, m_d->layerShape);
        m_d->dropShadowIsDirty = false;
    }
    return m_d->dropShadowImplicitLayer;
}

const LayerStyles::OuterGlow& Layer::outerGlowOptions() const
{
    return m_d->outerGlowOptions;
}

bool Layer::isOuterGlowActive() const
{
    return m_d->outerGlowIsActive;
}

QImage Layer::outerGlowImage() const
{
    if (m_d->layerShape.isNull() || !m_d->outerGlowIsActive) {
        return QImage();
    }
    if (m_d->outerGlowIsDirty) {
        m_d->outerGlowImplicitLayer = LayerStyles::generateOuterGlowImage(m_d->outerGlowOptions, m_d->layerShape);
        m_d->outerGlowIsDirty = false;
    }
    return m_d->outerGlowImplicitLayer;
}

const LayerStyles::PatternOverlay& Layer::patternOverlayOptions() const
{
    return m_d->patternOverlayOptions;
}

bool Layer::isPatternOverlayActive() const
{
    return m_d->patternOverlayIsActive;
}

QImage Layer::patternOverlayImage() const
{
    if (m_d->layerShape.isNull() || !m_d->patternOverlayIsActive) {
        return QImage();
    }
    if (m_d->patternOverlayIsDirty) {
        m_d->patternOverlayImplicitLayer =
            LayerStyles::generatePatternOverlayImage(m_d->patternOverlayOptions, m_d->layerShape);
        m_d->patternOverlayIsDirty = false;
    }
    return m_d->patternOverlayImplicitLayer;
}

const LayerStyles::GradientOverlay& Layer::gradientOverlayOptions() const
{
    return m_d->gradientOverlayOptions;
}

bool Layer::isGradientOverlayActive() const
{
    return m_d->gradientOverlayIsActive;
}

QImage Layer::gradientOverlayImage() const
{
    if (m_d->layerShape.isNull() || !m_d->gradientOverlayIsActive) {
        return QImage();
    }
    if (m_d->gradientOverlayIsDirty) {
        m_d->gradientOverlayImplicitLayer =
            LayerStyles::generateGradientOverlayImage(m_d->gradientOverlayOptions, m_d->layerShape);
        m_d->gradientOverlayIsDirty = false;
    }
    return m_d->gradientOverlayImplicitLayer;
}

const LayerStyles::ColorOverlay& Layer::colorOverlayOptions() const
{
    return m_d->colorOverlayOptions;
}

bool Layer::isColorOverlayActive() const
{
    return m_d->colorOverlayIsActive;
}

QImage Layer::colorOverlayImage() const
{
    if (m_d->layerShape.isNull() || !m_d->colorOverlayIsActive) {
        return QImage();
    }
    if (m_d->colorOverlayIsDirty) {
        m_d->colorOverlayImplicitLayer = LayerStyles::generateColorOverlayImage(m_d->colorOverlayOptions, m_d->layerShape);
        m_d->colorOverlayIsDirty = false;
    }
    return m_d->colorOverlayImplicitLayer;
}

const LayerStyles::Satin& Layer::satinOptions() const
{
    return m_d->satinOptions;
}

bool Layer::isSatinActive() const
{
    return m_d->satinIsActive;
}

QImage Layer::satinImage() const
{
    if (m_d->layerShape.isNull() || !m_d->satinIsActive) {
        return QImage();
    }
    if (m_d->satinIsDirty) {
        m_d->satinImplicitLayer = LayerStyles::generateSatinImage(m_d->satinOptions, m_d->layerShape);
        m_d->satinIsDirty = false;
    }
    return m_d->satinImplicitLayer;
}

const LayerStyles::InnerGlow& Layer::innerGlowOptions() const
{
    return m_d->innerGlowOptions;
}

bool Layer::isInnerGlowActive() const
{
    return m_d->innerGlowIsActive;
}

QImage Layer::innerGlowImage() const
{
    if (m_d->layerShape.isNull() || !m_d->innerGlowIsActive) {
        return QImage();
    }
    if (m_d->innerGlowIsDirty) {
        m_d->innerGlowImplicitLayer = LayerStyles::generateInnerGlowImage(m_d->innerGlowOptions, m_d->layerShape);
        m_d->innerGlowIsDirty = false;
    }
    return m_d->innerGlowImplicitLayer;
}

const LayerStyles::InnerShadow& Layer::innerShadowOptions() const
{
    return m_d->innerShadowOptions;
}

bool Layer::isInnerShadowActive() const
{
    return m_d->innerShadowIsActive;
}

QImage Layer::innerShadowImage() const
{
    if (m_d->layerShape.isNull() || !m_d->innerShadowIsActive) {
        return QImage();
    }
    if (m_d->innerShadowIsDirty) {
        m_d->innerShadowImplicitLayer = LayerStyles::generateInnerShadowImage(m_d->innerShadowOptions, m_d->layerShape);
        m_d->innerShadowIsDirty = false;
    }
    return m_d->innerShadowImplicitLayer;
}

const LayerStyles::Stroke& Layer::strokeOptions() const
{
    return m_d->strokeOptions;
}

bool Layer::isStrokeActive() const
{
    return m_d->strokeIsActive;
}

QImage Layer::strokeImage() const
{
    if (m_d->layerShape.isNull() || !m_d->strokeIsActive) {
        return QImage();
    }
    if (m_d->strokeIsDirty) {
        m_d->generateStrokeImages();
        m_d->strokeIsDirty = false;
    }
    return m_d->strokeImplicitLayer;
}

QImage Layer::strokeShape() const
{
    if (m_d->layerShape.isNull() || !m_d->strokeIsActive) {
        return QImage();
    }
    if (m_d->strokeIsDirty) {
        m_d->generateStrokeImages();
        m_d->strokeIsDirty = false;
    }
    return m_d->strokeShape;
}

QImage Layer::strokeFill() const
{
    if (m_d->layerShape.isNull() || !m_d->strokeIsActive) {
        return QImage();
    }
    if (m_d->strokeIsDirty) {
        m_d->generateStrokeImages();
        m_d->strokeIsDirty = false;
    }
    return m_d->strokeFill;
}

const LayerStyles::BevelAndEmboss& Layer::bevelAndEmbossOptions() const
{
    return m_d->bevelAndEmbossOptions;
}

bool Layer::isBevelAndEmbossActive() const
{
    return m_d->bevelAndEmbossIsActive;
}

QImage Layer::bevelAndEmbossLightImage() const
{
    if (m_d->layerShape.isNull() || !m_d->bevelAndEmbossIsActive) {
        return QImage();
    }
    if (m_d->bevelAndEmbossIsDirty) {
        m_d->generateBevelAndEmbossImages();
        m_d->bevelAndEmbossIsDirty = false;
    }
    return m_d->bevelAndEmbossLightImplicitLayer;
}

QImage Layer::bevelAndEmbossShadowImage() const
{
    if (m_d->layerShape.isNull() || !m_d->bevelAndEmbossIsActive) {
        return QImage();
    }
    if (m_d->bevelAndEmbossIsDirty) {
        m_d->generateBevelAndEmbossImages();
        m_d->bevelAndEmbossIsDirty = false;
    }
    return m_d->bevelAndEmbossShadowImplicitLayer;
}

void Layer::setImage(const QImage &newImage)
{
    m_d->layerImage = newImage;
    m_d->generateLayerShape();
    m_d->setAllEffectsDirty();
}

void Layer::setRasterMask(const QImage &newRasterMask)
{
    m_d->rasterMask = newRasterMask.isNull()
                      ? QImage()
                      : newRasterMask.convertToFormat(QImage::Format_Alpha8);
    m_d->generateLayerShape();
    m_d->setAllEffectsDirty();
}

void Layer::setRasterMaskActive(bool active)
{
    if (active == m_d->rasterMaskIsActive) {
        return;
    }
    m_d->rasterMaskIsActive = active;
    m_d->generateLayerShape();
    m_d->setAllEffectsDirty();
}

void Layer::setBlendingOptions(const LayerStyles::BlendingOptions &newBlendingOptions)
{
    const bool mustRegenerateShape = 
        newBlendingOptions.transparencyShapesLayer != m_d->blendingOptions.transparencyShapesLayer ||
        (!m_d->rasterMask.isNull() && newBlendingOptions.layerMaskHidesEffects != m_d->blendingOptions.layerMaskHidesEffects);
    m_d->blendingOptions = newBlendingOptions;
    if (mustRegenerateShape) {
        m_d->generateLayerShape();
        m_d->setAllEffectsDirty();
    }
}

void Layer::setDropShadowOptions(const LayerStyles::DropShadow &newDropShadowOptions)
{
    m_d->dropShadowOptions = newDropShadowOptions;
    m_d->dropShadowIsDirty = true;
}

void Layer::setDropShadowActive(bool active)
{
    m_d->dropShadowIsActive = active;
}

void Layer::setOuterGlowOptions(const LayerStyles::OuterGlow &newOuterGlowOptions)
{
    m_d->outerGlowOptions = newOuterGlowOptions;
    m_d->outerGlowIsDirty = true;
}

void Layer::setOuterGlowActive(bool active)
{
    m_d->outerGlowIsActive = active;
}

void Layer::setPatternOverlayOptions(const LayerStyles::PatternOverlay &newPatternOverlayOptions)
{
    m_d->patternOverlayOptions = newPatternOverlayOptions;
    m_d->patternOverlayIsDirty = true;
}

void Layer::setPatternOverlayActive(bool active)
{
    m_d->patternOverlayIsActive = active;
}

void Layer::setGradientOverlayOptions(const LayerStyles::GradientOverlay &newGradientOverlayOptions)
{
    m_d->gradientOverlayOptions = newGradientOverlayOptions;
    m_d->gradientOverlayIsDirty = true;
}

void Layer::setGradientOverlayActive(bool active)
{
    m_d->gradientOverlayIsActive = active;
}

void Layer::setColorOverlayOptions(const LayerStyles::ColorOverlay &newColorOverlayOptions)
{
    m_d->colorOverlayOptions = newColorOverlayOptions;
    m_d->colorOverlayIsDirty = true;
}

void Layer::setColorOverlayActive(bool active)
{
    m_d->colorOverlayIsActive = active;
}

void Layer::setSatinOptions(const LayerStyles::Satin &newSatinOptions)
{
    m_d->satinOptions = newSatinOptions;
    m_d->satinIsDirty = true;
}

void Layer::setSatinActive(bool active)
{
    m_d->satinIsActive = active;
}

void Layer::setInnerGlowOptions(const LayerStyles::InnerGlow &newInnerGlowOptions)
{
    m_d->innerGlowOptions = newInnerGlowOptions;
    m_d->innerGlowIsDirty = true;
}

void Layer::setInnerGlowActive(bool active)
{
    m_d->innerGlowIsActive = active;
}

void Layer::setInnerShadowOptions(const LayerStyles::InnerShadow &newInnerShadowOptions)
{
    m_d->innerShadowOptions = newInnerShadowOptions;
    m_d->innerShadowIsDirty = true;
}

void Layer::setInnerShadowActive(bool active)
{
    m_d->innerShadowIsActive = active;
}

void Layer::setStrokeOptions(const LayerStyles::Stroke &newStrokeOptions)
{
    m_d->strokeOptions = newStrokeOptions;
    m_d->strokeIsDirty = true;
    if (m_d->bevelAndEmbossOptions.style == LayerStyles::BevelAndEmbossStyle_StrokeEmboss) {
        m_d->bevelAndEmbossIsDirty = true;
    }
}

void Layer::setStrokeActive(bool active)
{
    m_d->strokeIsActive = active;
}

void Layer::setBevelAndEmbossOptions(const LayerStyles::BevelAndEmboss &newBevelAndEmbossOptions)
{
    m_d->bevelAndEmbossOptions = newBevelAndEmbossOptions;
    m_d->bevelAndEmbossIsDirty = true;
}

void Layer::setBevelAndEmbossActive(bool active)
{
    m_d->bevelAndEmbossIsActive = active;
}
