/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef GENERICWINDOW_H
#define GENERICWINDOW_H

#include <QWidget>
#include <QImage>
#include <QPointF>
#include <QScopedPointer>

#include <Export.h>

class Viewport;
class QTabWidget;

class COMMON_EXPORT GenericWindow : public QWidget
{
    Q_OBJECT

public:
    GenericWindow();
    ~GenericWindow() override;

    void addControlsPanel(QWidget *widget, const QString &name);
    Viewport* viewport() const;
    QTabWidget* sideBar() const;

public Q_SLOTS:
    void requestViewportUpdate();
    void setCurrentControlsPanel(qint32 index);

Q_SIGNALS:
    void viewportReadyForUpdate();
    
private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
