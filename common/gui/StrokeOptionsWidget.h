/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef STROKEOPTIONSWIDGET_H
#define STROKEOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT StrokeOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StrokeOptionsWidget(QWidget *parent = nullptr);
    ~StrokeOptionsWidget() override;

    LayerStyles::Stroke params() const;

public Q_SLOTS:
    void setGradient(const QGradientStops &gradient);
    void setPattern(const QImage &pattern);

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
