/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef STROKEFILLTYPECOMBOBOX_H
#define STROKEFILLTYPECOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT StrokeFillTypeComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit StrokeFillTypeComboBox(QWidget *parent = nullptr);
    ~StrokeFillTypeComboBox() override;

    LayerStyles::StrokeFillType currentStrokeFillType() const;

public Q_SLOTS:
    void setCurrentStrokeFillType(LayerStyles::StrokeFillType newStrokeFillType);

Q_SIGNALS:
    void currentStrokeFillTypeChanged(LayerStyles::StrokeFillType newStrokeFillType);
};

#endif
