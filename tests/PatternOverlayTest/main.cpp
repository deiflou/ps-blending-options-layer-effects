/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <Application.h>

#include "Window.h"

int main(int argc, char ** argv)
{
    Application app(argc, argv);

    Window wnd;
    wnd.show();

    return app.exec();
}
