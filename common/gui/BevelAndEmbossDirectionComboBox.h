/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef BEVELANDEMBOSSDIRECTIONCOMBOBOX_H
#define BEVELANDEMBOSSDIRECTIONCOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT BevelAndEmbossDirectionComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit BevelAndEmbossDirectionComboBox(QWidget *parent = nullptr);
    ~BevelAndEmbossDirectionComboBox() override;

    LayerStyles::BevelAndEmbossDirection currentDirection() const;

public Q_SLOTS:
    void setCurrentDirection(LayerStyles::BevelAndEmbossDirection newDirection);

Q_SIGNALS:
    void currentDirectionChanged(LayerStyles::BevelAndEmbossDirection newDirection);
};

#endif
