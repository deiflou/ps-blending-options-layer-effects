/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFrame>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTabWidget>
#include <QTimer>
#include <QApplication>

#include "Viewport.h"
#include "GenericWindow.h"

class Q_DECL_HIDDEN GenericWindow::Private
{
public:
    static constexpr int updateInterval {10};

    QTimer timerUpdate;
    Viewport *viewport {nullptr};
    QTabWidget *sideBarTabWidget {nullptr};
};

GenericWindow::GenericWindow()
    : m_d(new Private)
{
    m_d->timerUpdate.setSingleShot(true);
    m_d->timerUpdate.setInterval(m_d->updateInterval);

    m_d->viewport = new Viewport;

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(m_d->viewport);
    setLayout(mainLayout);

    m_d->sideBarTabWidget = new QTabWidget(this);
    m_d->sideBarTabWidget->setWindowFlags(Qt::Tool |
                                          Qt::CustomizeWindowHint |
                                          Qt::WindowTitleHint);
    m_d->sideBarTabWidget->show();

    auto scaleLabelUpdateFn = 
        [this](qreal newScale)
        {
            setWindowTitle(qApp->applicationName() + " - " + QString::number(newScale * 100.0) + "%");
        };

    connect(viewport(), &Viewport::scaleChanged, scaleLabelUpdateFn);
    connect(&m_d->timerUpdate, SIGNAL(timeout()), this, SIGNAL(viewportReadyForUpdate()));

    scaleLabelUpdateFn(viewport()->scale());
    resize(800, 600);
}

GenericWindow::~GenericWindow()
{}

void GenericWindow::addControlsPanel(QWidget *widget, const QString &name)
{
    if (!widget) {
        return;
    }
    m_d->sideBarTabWidget->addTab(widget, name);
    m_d->sideBarTabWidget->resize(m_d->sideBarTabWidget->minimumSizeHint());
}

Viewport* GenericWindow::viewport() const
{
    return m_d->viewport;
}

QTabWidget* GenericWindow::sideBar() const
{
    return m_d->sideBarTabWidget;
}

void GenericWindow::requestViewportUpdate()
{
    if (!m_d->timerUpdate.isActive()) {
        m_d->timerUpdate.start();
    }
}

void GenericWindow::setCurrentControlsPanel(qint32 index)
{
    m_d->sideBarTabWidget->setCurrentIndex(index);
}
