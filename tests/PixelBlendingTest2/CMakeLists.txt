add_executable(
    pixel_blending_test_2
    main.cpp
    Window.cpp
    resources.qrc
)

target_link_libraries(pixel_blending_test_2 PRIVATE Qt5::Widgets common)
target_include_directories(pixel_blending_test_2 PRIVATE "${COMMON_INCLUDE_DIRS}")

set_target_properties(
    pixel_blending_test_2
    PROPERTIES
    AUTOMOC ON
    AUTORCC ON
)
