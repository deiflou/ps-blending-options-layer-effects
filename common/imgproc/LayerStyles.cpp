/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <cmath>

#include <QPair>
#include <QTransform>
#include <QPainter>
#include <QRandomGenerator>

#include "ImgProcUtil.h"
#include "ImgProcCommon.h"
#include "PixelBlending.h"
#include "Curve.h"
#include "DistanceTransform.h"
#include "SizeAndSpreadTransform.h"
#include "ContourTransform.h"
#include "BevelAndEmbossTransform.h"

#include "LayerStyles.h"

namespace LayerStyles
{

QImage generateDropShadowImage(const DropShadow &options, const QImage &layerMask)
{
    QRandomGenerator rng;
    const QImage dropShadowImage = 
        ContourTransform::applyAndFillWithColor(
            SizeAndSpreadTransform::apply(layerMask, options.size, options.spread),
            false, 100, false, options.curve, options.antiAliasedCurve,
            options.noise, rng,
            options.curve.f(0.0) != 0.0 || options.noise > 0,
            options.color
        );

    const qreal angle = static_cast<qreal>(options.angle) * M_PI / 180.0;
    const qreal distance = static_cast<qreal>(options.distance);
    const QPoint offset(
        static_cast<int>(std::round(-std::cos(angle) * distance)),
        static_cast<int>(std::round(std::sin(angle) * distance))
    );
    return ImgProcUtil::shiftedImage(dropShadowImage, offset);
}

QImage generateOuterGlowImage(const OuterGlow &options, const QImage &layerMask)
{
    QRandomGenerator rng;

    if (options.useGradient) {
        return ContourTransform::applyAndFillWithGradient(
                   SizeAndSpreadTransform::apply(layerMask, options.size, options.spread, 
                                                 options.technique == GlowTechnique_Precise
                                                 ? SizeAndSpreadTransform::Method_Precise
                                                 : SizeAndSpreadTransform::Method_Softer),
                   false, options.range, false, options.curve, options.antiAliasedCurve,
                   options.gradientJitter, options.noise, rng, true, options.gradient
               );
    } else {
        const bool applyFade = options.curve.f(0.0) > 0.0 || options.noise > 0;
        return ContourTransform::applyAndFillWithColor(
                   SizeAndSpreadTransform::apply(layerMask, options.size, options.spread, 
                                                 options.technique == GlowTechnique_Precise
                                                 ? SizeAndSpreadTransform::Method_Precise
                                                 : SizeAndSpreadTransform::Method_Softer),
                   false, options.range, false, options.curve, options.antiAliasedCurve,
                   options.noise, rng, applyFade, options.color
               );
    }
}

QImage generatePatternOverlayImage(const PatternOverlay &options, const QImage &layerMask)
{
    QImage patternOverlayImage(layerMask.size(), QImage::Format_ARGB32);

    const qreal scale = static_cast<qreal>(options.scale) / 100.0;
    QTransform transform;
    transform.scale(scale, scale);
    transform.rotate(static_cast<qreal>(-options.angle));
    transform.translate(options.offset.x(), options.offset.y());
    const QTransform inverseTransform = transform.inverted();
    const QImage pattern = options.pattern;

    ImgProcUtil::visitPixelsWithCoords(
        patternOverlayImage,
        [&inverseTransform, &pattern](quint8 *pixel, qint32 x, qint32 y)
        {
            const QPointF mappedPos = inverseTransform.map(QPointF(x, y));
            const qreal inPatternX = mappedPos.x() - std::floor(mappedPos.x() / (pattern.width() - 1)) * (pattern.width() - 1);
            const qreal inPatternY = mappedPos.y() - std::floor(mappedPos.y() / (pattern.height() - 1)) * (pattern.height() - 1);
            quint32 *finalColor = reinterpret_cast<quint32*>(pixel);
            *finalColor = pattern.pixel(static_cast<qint32>(std::round(inPatternX)),
                                        static_cast<qint32>(std::round(inPatternY)));
        }
    );

    return patternOverlayImage;
}

QImage generateGradientOverlayImage(const GradientOverlay &options, const QImage &layerMask)
{
    QImage gradientOverlayImage(layerMask.size(), QImage::Format_ARGB32);

    QGradientStops gradient;
    
    if (options.reverse) {
        for (qint32 i = options.gradient.size() - 1; i >= 0; --i) {
            gradient.append({1.0 - options.gradient[i].first, options.gradient[i].second});
        }
    } else {
        gradient = options.gradient;
    }

    QRect gradientRect;
    if (options.alignWithLayer) {
        qint32 minX = layerMask.width();
        qint32 minY = layerMask.height();
        qint32 maxX = -1;
        qint32 maxY = -1;
        ImgProcUtil::visitPixelsWithCoords(
            layerMask,
            [&minX, &minY, &maxX, &maxY](const quint8 *pixel, qint32 x, qint32 y)
            {
                if (*pixel > 0) {
                    if (minX > x) {
                        minX = x;
                    } else if (minY > y) {
                        minY = y;
                    } else if (maxX < x) {
                        maxX = x;
                    } else if (maxY < y) {
                        maxY = y;
                    }
                }
            }
        );
        gradientRect = QRect(QPoint(minX, minY), QPoint(maxX, maxY));
    } else {
        gradientRect = layerMask.rect();
    }

    const QPointF center = QPointF(gradientRect.topLeft() + gradientRect.bottomRight()) / 2.0;
    QPainter p(&gradientOverlayImage);

    if (options.style == GradientStyle_Angle) {
        QConicalGradient g(center, options.angle);
        g.setStops(gradient);
        p.fillRect(gradientOverlayImage.rect(), g);
    } else {
        // Find point of intersection between the rect and the ray with center
        // at the center of the rect and angle equal to options.angle. That
        // intersection point will mart the length of the gradient
        const qreal angle = options.angle * M_PI / 180.0;
        const qreal scale = options.scale / 100.0;
        const qreal cosAngle = std::cos(angle);
        const qreal sinAngle = -std::sin(angle);
        const QPointF relativeCenter = center - gradientRect.topLeft();
        const qreal dydx = sinAngle / cosAngle;
        qreal x, y;
        // Intersect with top border
        y = relativeCenter.y();
        x = y / dydx;
        if (sinAngle < 0.0 || x < -relativeCenter.x() || x > relativeCenter.x()) {
            // Intersect with bottom border
            y = -relativeCenter.y();
            x = y / dydx;
            if (sinAngle > 0.0 || x < -relativeCenter.x() || x > relativeCenter.x()) {
                // Intersect with left border
                x = -relativeCenter.x();
                y = x * dydx;
                if (cosAngle > 0.0 || y < -relativeCenter.y() || y > relativeCenter.y()) {
                    // Intersect with right border
                    x = relativeCenter.x();
                    y = x * dydx;
                }
            }
        }

        if (options.style == GradientStyle_Radial) {
            QRadialGradient g(center, scale * std::sqrt(x * x + y * y));
            g.setStops(gradient);
            p.fillRect(gradientOverlayImage.rect(), g);
        } else {
            QLinearGradient g(center - QPointF(x, y) * scale, center + QPointF(x, y) * scale);
            g.setStops(gradient);
            p.fillRect(gradientOverlayImage.rect(), g);
        }
    }

    return gradientOverlayImage;
}

QImage generateColorOverlayImage(const ColorOverlay &options, const QImage &layerMask)
{
    QImage colorOverlayImage(layerMask.size(), QImage::Format_ARGB32);
    colorOverlayImage.fill(options.color.rgb());
    return colorOverlayImage;
}

QImage generateSatinImage(const Satin &options, const QImage &layerMask)
{
    const QImage satinMask =
        ContourTransform::applyCurve(
            SizeAndSpreadTransform::apply(layerMask, options.size, 0),
            options.curve, options.antiAliasedCurve
        );
    const qreal angle = static_cast<qreal>(options.angle) * M_PI / 180.0;
    const qreal distance = static_cast<qreal>(options.distance);
    const QPoint offset(
        static_cast<int>(std::round(-std::cos(angle) * distance)),
        static_cast<int>(std::round(std::sin(angle) * distance))
    );
    const QImage shiftedMask1 = ImgProcUtil::shiftedImage(satinMask, offset);
    const QImage shiftedMask2 = ImgProcUtil::shiftedImage(satinMask, -offset);
    ImgProcCommon::ARGB color {static_cast<quint8>(options.color.blue()),
                               static_cast<quint8>(options.color.green()),
                               static_cast<quint8>(options.color.red()),
                               255};
    QImage satinImage(layerMask.size(), QImage::Format_ARGB32);

    if (options.invertContour) {
        ImgProcUtil::visitPixels(
            satinImage, shiftedMask1, shiftedMask2,
            [color](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                ImgProcCommon::ARGB *finalColor = reinterpret_cast<ImgProcCommon::ARGB*>(pixel1);
                const quint32 p2 = *reinterpret_cast<const quint32*>(pixel2);
                const quint32 p3 = *reinterpret_cast<const quint32*>(pixel3);
                const qint32 v1 = static_cast<qint32>((p2 >> 8) + ((p2 & 0xFF) > 0x7F));
                const qint32 v2 = static_cast<qint32>((p3 >> 8) + ((p3 & 0xFF) > 0x7F));
                const quint8 alpha = static_cast<quint8>(255 - std::abs(v1 - v2));
                *finalColor = {color.b, color.g, color.r, alpha};
            }
        );
    } else {
        ImgProcUtil::visitPixels(
            satinImage, shiftedMask1, shiftedMask2,
            [color](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                ImgProcCommon::ARGB *finalColor = reinterpret_cast<ImgProcCommon::ARGB*>(pixel1);
                const quint32 p2 = *reinterpret_cast<const quint32*>(pixel2);
                const quint32 p3 = *reinterpret_cast<const quint32*>(pixel3);
                const qint32 v1 = static_cast<qint32>((p2 >> 8) + ((p2 & 0xFF) > 0x7F));
                const qint32 v2 = static_cast<qint32>((p3 >> 8) + ((p3 & 0xFF) > 0x7F));
                const quint8 alpha = static_cast<quint8>(std::abs(v1 - v2));
                *finalColor = {color.b, color.g, color.r, alpha};
            }
        );
    }

    return satinImage;
}

QImage generateInnerGlowImage(const InnerGlow &options, const QImage &layerMask)
{
    QRandomGenerator rng;

    if (options.useGradient) {
        return ContourTransform::applyAndFillWithGradient(
                   SizeAndSpreadTransform::apply(layerMask, options.size, options.spread,
                                                 options.technique == GlowTechnique_Precise
                                                 ? SizeAndSpreadTransform::Method_Precise
                                                 : SizeAndSpreadTransform::Method_Softer, true),
                   options.source == GlowSource_Center,
                   options.range, false, options.curve, options.antiAliasedCurve,
                   options.gradientJitter, options.noise, rng, false,
                   options.gradient
               );
    } else {
        return ContourTransform::applyAndFillWithColor(
                   SizeAndSpreadTransform::apply(layerMask, options.size, options.spread,
                                                 options.technique == GlowTechnique_Precise
                                                 ? SizeAndSpreadTransform::Method_Precise
                                                 : SizeAndSpreadTransform::Method_Softer, true),
                   options.source == GlowSource_Center,
                   options.range, false, options.curve, options.antiAliasedCurve,
                   options.noise, rng, false, options.color
               );
    }
}

QImage generateInnerShadowImage(const InnerShadow &options, const QImage &layerMask)
{
    QRandomGenerator rng;
    const QImage innerShadowImage =
        ContourTransform::applyAndFillWithColor(
            SizeAndSpreadTransform::apply(layerMask, options.size, options.spread,
                                          SizeAndSpreadTransform::Method_Softer, true),
            false, 100, false, options.curve, options.antiAliasedCurve,
            options.noise, rng, false, options.color
        );
    const qreal angle = static_cast<qreal>(options.angle) * M_PI / 180.0;
    const qreal distance = static_cast<qreal>(options.distance);
    const QPoint offset(
        static_cast<int>(std::round(-std::cos(angle) * distance)),
        static_cast<int>(std::round(std::sin(angle) * distance))
    );
    return ImgProcUtil::shiftedImage(innerShadowImage, offset);
}

QPair<QImage, QImage> generateStrokeImage(const Stroke &options, const QImage &layerMask)
{
    QImage strokeMask;

    if (options.position == StrokePosition_Outside) {
        strokeMask = ImgProcUtil::scaledDownMask(SizeAndSpreadTransform::apply(layerMask, options.size, 100));
    } else if (options.position == StrokePosition_Inside) {
        strokeMask = ImgProcUtil::scaledDownMask(SizeAndSpreadTransform::apply(layerMask, options.size, 100,
                                                                               SizeAndSpreadTransform::Method_Softer,
                                                                               true));
    } else {
        strokeMask = QImage(layerMask.size(), QImage::Format_Alpha8);
        if (options.size % 2 == 0) {
            const qint32 halfSize = options.size / 2;
            const QImage outsideMask = SizeAndSpreadTransform::apply(layerMask, halfSize, 100);
            const QImage insideMask = SizeAndSpreadTransform::apply(layerMask, halfSize, 100,
                                                                    SizeAndSpreadTransform::Method_Softer,
                                                                    true);
            ImgProcUtil::visitPixels(
                strokeMask, outsideMask, insideMask,
                [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
                {
                    const quint32 outsideValue = *reinterpret_cast<const quint32*>(pixel2);
                    const quint32 insideValue = 0xFF00 - *reinterpret_cast<const quint32*>(pixel3);
                    const quint32 strokeValue = outsideValue - insideValue;
                    *pixel1 = static_cast<quint8>((strokeValue >> 8) + ((strokeValue & 0xFF) > 0x7F));
                }
            );
        } else {
            const qint32 maxDistance = (options.size / 2) << 8;
            const qint32 maxDistancePlusOne = maxDistance + 0x100;
            const QImage outsideDT = DistanceTransform::chamfer5x5_Euclidean(layerMask);
            const QImage insideDT = DistanceTransform::chamfer5x5_Euclidean(layerMask, DistanceTransform::InvertMaskInitFn());
            ImgProcUtil::visitPixels(
                strokeMask, outsideDT, insideDT,
                [maxDistance, maxDistancePlusOne](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
                {
                    const quint32 outsidePixel = *reinterpret_cast<const quint32*>(pixel2);
                    const quint32 insidePixel = *reinterpret_cast<const quint32*>(pixel3);
                    const quint32 outsideDistance = outsidePixel > 0x7F ? outsidePixel - 0x7F : 0;
                    const quint32 insideDistance = insidePixel > 0x7F ? insidePixel - 0x7F : 0;
                    const quint32 outsideValue = outsideDistance >= maxDistancePlusOne
                                                 ? 0
                                                 : outsideDistance > maxDistance
                                                   ? 0xFF - (outsideDistance & 0xFF)
                                                   : 0xFF;
                    const quint32 insideValue = insideDistance >= maxDistancePlusOne
                                                ? 0xFF
                                                : insideDistance > maxDistance
                                                  ? insideDistance & 0xFF
                                                  : 0;
                    const quint32 strokeValue = outsideValue - insideValue;
                    *pixel1 = static_cast<quint8>(strokeValue);
                }
            );
        }
    }
    
    QImage strokeImage;
    if (options.fillType == StrokeFillType_Color) {
        ColorOverlay colorOptions;
        colorOptions.color = options.color;
        strokeImage = generateColorOverlayImage(colorOptions, options.position == StrokePosition_Inside
                                                              ? layerMask : strokeMask);
    } else if (options.fillType == StrokeFillType_Gradient) {
        GradientOverlay gradientOptions;
        gradientOptions.gradient = options.gradient;
        gradientOptions.style = options.gradientStyle;
        gradientOptions.dither = options.ditherGradient;
        gradientOptions.reverse = options.reverseGradient;
        gradientOptions.method = options.gradientMethod;
        gradientOptions.angle = options.gradientAngle;
        gradientOptions.scale = options.gradientScale;
        gradientOptions.offset = options.gradientOffset;
        gradientOptions.alignWithLayer = options.alignGradientWithLayer;
        strokeImage = generateGradientOverlayImage(gradientOptions, options.position == StrokePosition_Inside
                                                                    ? layerMask : strokeMask);
    } else {
        PatternOverlay patternOptions;
        patternOptions.pattern = options.pattern;
        patternOptions.angle = options.patternAngle;
        patternOptions.scale = options.patternScale;
        patternOptions.offset = options.patternOffset;
        patternOptions.linkWithLayer = options.linkPatternWithLayer;
        strokeImage = generatePatternOverlayImage(patternOptions, options.position == StrokePosition_Inside
                                                                  ? layerMask : strokeMask);
    }

    return {strokeImage, strokeMask};
}

QPair<QImage, QImage> generateBevelAndEmbossImage(const BevelAndEmboss &options,
                                                  const QImage &layerMask,
                                                  StrokePosition strokePosition)
{
    const QImage lightMap = 
        BevelAndEmbossTransform::apply(
            layerMask,

            options.style == BevelAndEmbossStyle_PillowEmboss
            ? BevelAndEmbossTransform::Style_PillowEmboss
            : BevelAndEmbossTransform::Style_Emboss,

            static_cast<BevelAndEmbossTransform::Technique>(options.technique),
            options.depth,
            static_cast<BevelAndEmbossTransform::Direction>(options.direction),

            options.style == BevelAndEmbossStyle_OuterBevel ||
            options.style == BevelAndEmbossStyle_InnerBevel ||
            (options.style == BevelAndEmbossStyle_StrokeEmboss &&
             strokePosition != StrokePosition_Center)
            ? options.size
            : options.size / 2,

            options.soften,
            options.lightAngle,
            options.lightAltitude,

            options.applyProfileContour
            ? options.profileCurve
            : Curve(),

            options.antiAliasedProfileCurve,

            options.applyProfileContour &&
            (options.style == BevelAndEmbossStyle_OuterBevel ||
             options.style == BevelAndEmbossStyle_InnerBevel ||
             (options.style == BevelAndEmbossStyle_StrokeEmboss &&
              strokePosition != StrokePosition_Center))
            ? options.profileRange
            : 100,

            options.style == BevelAndEmbossStyle_InnerBevel ||
            (options.style == BevelAndEmbossStyle_StrokeEmboss &&
             strokePosition == StrokePosition_Inside),

            options.applyTexture
            ? options.texture
            : QImage(),

            options.textureScale,
            options.textureOffset,
            options.textureDepth,
            options.invertTexture,
            options.glossCurve,
            options.antiAliasedGlossCurve,
            options.style != BevelAndEmbossStyle_StrokeEmboss
        );

    QImage lightImage(layerMask.size(), QImage::Format_ARGB32);
    ImgProcCommon::ARGB lightColor {static_cast<quint8>(options.lightColor.blue()),
                                    static_cast<quint8>(options.lightColor.green()),
                                    static_cast<quint8>(options.lightColor.red()),
                                    255};
    ImgProcUtil::visitPixels(
        lightImage, lightMap,
        [lightColor](quint8 *pixel1, const quint8 *pixel2)
        {
            ImgProcCommon::ARGB *finalColor = reinterpret_cast<ImgProcCommon::ARGB*>(pixel1);
            const quint32 p2 = *reinterpret_cast<const quint32*>(pixel2);
            const quint32 scaledAlpha = p2 < 0x7F80
                                        ? 0x0000
                                        : 2 * (p2 - 0x7F80);
            const quint8 alpha = static_cast<quint8>((scaledAlpha >> 8) + ((scaledAlpha & 0xFF) > 0x7F));
            *finalColor = {lightColor.b, lightColor.g, lightColor.r, alpha};
        }
    );

    QImage shadowImage(layerMask.size(), QImage::Format_ARGB32);
    ImgProcCommon::ARGB shadowColor {static_cast<quint8>(options.shadowColor.blue()),
                                     static_cast<quint8>(options.shadowColor.green()),
                                     static_cast<quint8>(options.shadowColor.red()),
                                     255};
    ImgProcUtil::visitPixels(
        shadowImage, lightMap,
        [shadowColor](quint8 *pixel1, const quint8 *pixel2)
        {
            ImgProcCommon::ARGB *finalColor = reinterpret_cast<ImgProcCommon::ARGB*>(pixel1);
            const quint32 p2 = *reinterpret_cast<const quint32*>(pixel2);
            const quint32 scaledAlpha = p2 < 0x7F80
                                        ? 0xFF00 - 2 * p2
                                        : 0x00;
            const quint8 alpha = static_cast<quint8>((scaledAlpha >> 8) + ((scaledAlpha & 0xFF) > 0x7F));
            *finalColor = {shadowColor.b, shadowColor.g, shadowColor.r, alpha};
        }
    );

    return {lightImage, shadowImage};
}

}
