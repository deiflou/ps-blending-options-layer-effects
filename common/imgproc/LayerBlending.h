/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef LAYERBLENDING_H
#define LAYERBLENDING_H

#include <QImage>

#include "Layer.h"

namespace LayerBlending
{

QImage COMMON_EXPORT blend(const Layer &layer, const QImage &background, const QImage &backdrop);

}

#endif
