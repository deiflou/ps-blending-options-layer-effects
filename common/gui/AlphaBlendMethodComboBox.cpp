/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "AlphaBlendMethodComboBox.h"

AlphaBlendMethodComboBox::AlphaBlendMethodComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Source", static_cast<int>(PixelBlending::Source));
    addItem("Destination", static_cast<int>(PixelBlending::Destination));
    addItem("Source Over Destination", static_cast<int>(PixelBlending::SourceOverDestination));
    addItem("Destination Over Source", static_cast<int>(PixelBlending::DestinationOverSource));
    addItem("Source In Destination", static_cast<int>(PixelBlending::SourceInDestination));
    addItem("Destination In Source", static_cast<int>(PixelBlending::DestinationInSource));
    addItem("Source Out Destination", static_cast<int>(PixelBlending::SourceOutDestination));
    addItem("Destination Out Source", static_cast<int>(PixelBlending::DestinationOutSource));
    addItem("Source Atop Destination", static_cast<int>(PixelBlending::SourceAtopDestination));
    addItem("Destination Atop Source", static_cast<int>(PixelBlending::DestinationAtopSource));
    addItem("Source Clear Destination", static_cast<int>(PixelBlending::SourceClearDestination));
    addItem("Source Xor Destination", static_cast<int>(PixelBlending::SourceXorDestination));
    addItem("Source Plus Destination", static_cast<int>(PixelBlending::SourcePlusDestination));

    setCurrentIndex(2);

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentAlphaBlendMethodChanged(static_cast<PixelBlending::AlphaBlendMethod>(currentData().toInt()));
        }
    );
}

AlphaBlendMethodComboBox::~AlphaBlendMethodComboBox()
{}

PixelBlending::AlphaBlendMethod AlphaBlendMethodComboBox::currentAlphaBlendMethod() const
{
    return static_cast<PixelBlending::AlphaBlendMethod>(currentData().toInt());
}

void AlphaBlendMethodComboBox::setCurrentAlphaBlendMethod(PixelBlending::AlphaBlendMethod newAlphaBlendMethod)
{
    setCurrentIndex(findData(static_cast<int>(newAlphaBlendMethod)));
}
