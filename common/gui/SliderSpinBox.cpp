/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QSlider>
#include <QSpinBox>
#include <QHBoxLayout>

#include "SliderSpinBox.h"

class Q_DECL_HIDDEN SliderSpinBox::Private
{
public:
    QSlider *slider {nullptr};
    QSpinBox *spinBox {nullptr};
};

SliderSpinBox::SliderSpinBox(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    m_d->slider = new QSlider(Qt::Horizontal);
    m_d->slider->setRange(0, 100);
    m_d->slider->setMinimumWidth(100);

    m_d->spinBox = new QSpinBox;
    m_d->spinBox->setRange(0, 100);

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->setSpacing(5);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(m_d->slider);
    mainLayout->addWidget(m_d->spinBox);

    connect(m_d->slider, SIGNAL(valueChanged(int)), m_d->spinBox, SLOT(setValue(int)));
    connect(m_d->slider, SIGNAL(valueChanged(int)), SIGNAL(valueChanged(int)));
    connect(m_d->spinBox, SIGNAL(valueChanged(int)), m_d->slider, SLOT(setValue(int)));
    connect(m_d->spinBox, SIGNAL(valueChanged(int)), SIGNAL(valueChanged(int)));

    setLayout(mainLayout);
}

SliderSpinBox::~SliderSpinBox()
{}

int SliderSpinBox::value() const
{
    return m_d->slider->value();
}

void SliderSpinBox::setValue(int newValue)
{
    m_d->slider->setValue(newValue);
}

void SliderSpinBox::setRange(int minimum, int maximum)
{
    m_d->slider->setRange(minimum, maximum);
    m_d->spinBox->setRange(minimum, maximum);
}
