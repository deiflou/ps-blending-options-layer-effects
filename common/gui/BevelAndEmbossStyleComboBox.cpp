/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "BevelAndEmbossStyleComboBox.h"

BevelAndEmbossStyleComboBox::BevelAndEmbossStyleComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Outer Bevel", static_cast<int>(LayerStyles::BevelAndEmbossStyle_OuterBevel));
    addItem("Inner Bevel", static_cast<int>(LayerStyles::BevelAndEmbossStyle_InnerBevel));
    addItem("Emboss", static_cast<int>(LayerStyles::BevelAndEmbossStyle_Emboss));
    addItem("Pillow Emboss", static_cast<int>(LayerStyles::BevelAndEmbossStyle_PillowEmboss));
    addItem("Stroke Emboss", static_cast<int>(LayerStyles::BevelAndEmbossStyle_StrokeEmboss));

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentStyleChanged(static_cast<LayerStyles::BevelAndEmbossStyle>(currentData().toInt()));
        }
    );
}
BevelAndEmbossStyleComboBox::~BevelAndEmbossStyleComboBox()
{}

LayerStyles::BevelAndEmbossStyle BevelAndEmbossStyleComboBox::currentStyle() const
{
    return static_cast<LayerStyles::BevelAndEmbossStyle>(currentData().toInt());
}

void BevelAndEmbossStyleComboBox::setCurrentStyle(LayerStyles::BevelAndEmbossStyle newStyle)
{
    setCurrentIndex(findData(static_cast<int>(newStyle)));
}
