/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef APP_H
#define APP_H

#include <QApplication>

#include <Export.h>

class COMMON_EXPORT Application : public QApplication
{
public:
    Application(int &argc, char **argv);
    ~Application() override;
};

#endif
