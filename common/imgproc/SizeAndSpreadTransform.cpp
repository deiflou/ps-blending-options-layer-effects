/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "ImgProcUtil.h"
#include "Filtering.h"
#include "DistanceTransform.h"
#include "SizeAndSpreadTransform.h"

namespace SizeAndSpreadTransform
{

QImage& inPlaceDilatedMaskFromDT(QImage &dt, qint32 dilationAmount)
{
    const quint32 scaledDilationAmount = dilationAmount << 8;
    const quint32 scaledDilationAmountPlusOne = scaledDilationAmount + 256;
    ImgProcUtil::visitPixels(
        dt,
        [scaledDilationAmount, scaledDilationAmountPlusOne](quint8 *pixel)
        {
            quint32 *p = reinterpret_cast<quint32*>(pixel);
            *p = *p >= scaledDilationAmountPlusOne
                 ? 0
                 : *p > scaledDilationAmount
                   ? (0xFF - (*p & 0xFF)) << 8
                   : 0xFF00;
        }
    );
    return dt;
}

QImage apply(const QImage &mask,
             qint32 size,
             qint32 spread,
             Method method,
             bool invertMask)
{
    if (size == 0) {
        return ImgProcUtil::scaledUpMask(mask, invertMask);
    }

    const qreal normalizedSpread = static_cast<qreal>(spread) / 100.0;
    const qint32 dilationAmount = static_cast<qint32>(std::round(static_cast<qreal>(size) * normalizedSpread));
    const qint32 blurAmount = size - dilationAmount;
    
    if (method == Method_Softer) {
        QImage outMask;
        if (dilationAmount > 0) {
            outMask = invertMask
                      ? DistanceTransform::chamfer5x5_Euclidean(mask, DistanceTransform::InvertMaskInitFn())
                      : DistanceTransform::chamfer5x5_Euclidean(mask, DistanceTransform::DefaultInitFn());
            inPlaceDilatedMaskFromDT(outMask, dilationAmount);
        } else {
            outMask = ImgProcUtil::scaledUpMask(mask, invertMask);
        }
        if (blurAmount > 0) {
            outMask = Filtering::gaussianBlurMask(outMask, blurAmount);
        }
        return outMask;

    } else if (method == Method_Precise) {
        QImage outMask1 = invertMask
                          ? DistanceTransform::chamfer5x5_Euclidean(mask, DistanceTransform::InvertMaskInitFn())
                          : DistanceTransform::chamfer5x5_Euclidean(mask, DistanceTransform::DefaultInitFn());

        if (blurAmount == 0) {
            return inPlaceDilatedMaskFromDT(outMask1, size);
        }

        const qint32 scaledDilationAmount = dilationAmount << 8;
        const qint32 scaledBlurAmountPlusOne = (blurAmount + 1) << 8;
        const qint32 scaledTotalBlurAmount = scaledBlurAmountPlusOne * 2;

        outMask1 =
            DistanceTransform::chamfer5x5_Euclidean(
                outMask1,
                [scaledDilationAmount, scaledBlurAmountPlusOne]
                (const quint8 *pixel, quint32 infinity) -> quint32
                {
                    const qint32 p = *reinterpret_cast<const qint32*>(pixel);
                    const quint32 mappedValue = 
                        static_cast<quint32>(std::max(0, scaledBlurAmountPlusOne - (p - scaledDilationAmount)));
                    return mappedValue < scaledBlurAmountPlusOne
                           ? mappedValue | DistanceTransform::stencilMask
                           : infinity;
                }
            );

        ImgProcUtil::visitPixels(
            outMask1,
            [scaledTotalBlurAmount]
            (quint8 *pixel) -> void
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                const qint32 v = static_cast<qint32>(DistanceTransform::distance(*p) * 0xFF00LL / scaledTotalBlurAmount);
                *p = std::min(0xFF00, v);
            }
        );

        return outMask1;

    } else {
        QImage outMask1 = invertMask
                          ? DistanceTransform::chamfer5x5_Euclidean(mask, DistanceTransform::InvertMaskInitFn())
                          : DistanceTransform::chamfer5x5_Euclidean(mask, DistanceTransform::DefaultInitFn());

        if (blurAmount == 0) {
            return inPlaceDilatedMaskFromDT(outMask1, size);
        }

        const qint32 scaledDilationAmount = dilationAmount << 8;
        const qint32 scaledBlurAmountPlusOne = (blurAmount + 1) << 8;
        const qint32 scaledTotalBlurAmount = scaledBlurAmountPlusOne * 2;
        const qint32 scaledBlurStart = scaledDilationAmount - scaledBlurAmountPlusOne;

        ImgProcUtil::visitPixels(
            outMask1,
            [scaledBlurStart, scaledTotalBlurAmount]
            (quint8 *pixel) -> void
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                const qint32 v = static_cast<qint32>((*p - scaledBlurStart) * 0xFF00LL / scaledTotalBlurAmount);
                *p = 0xFF00 - std::min(0xFF00, std::max(0, v));
            }
        );

        return outMask1;
    }
}

}
