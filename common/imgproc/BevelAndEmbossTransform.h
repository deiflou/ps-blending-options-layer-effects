/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef BEVELEMBOSSTRANSFORM_H
#define BEVELEMBOSSTRANSFORM_H

#include <QImage>

#include "Curve.h"
#include <Export.h>

namespace BevelAndEmbossTransform
{

enum Style
{
    Style_Emboss,
    Style_PillowEmboss,
};

enum Technique
{
    Technique_Smooth,
    Technique_ChiselHard,
    Technique_ChiselSoft
};

enum Direction
{
    Direction_Up,
    Direction_Down
};

QImage COMMON_EXPORT generateHeightMap(const QImage &mask,
                                       Technique technique,
                                       qint32 size);
QImage COMMON_EXPORT applyProfile(const QImage &heightMap,
                                  qint32 profileRange,
                                  bool reverseProfileRange,
                                  const Curve &profileCurve,
                                  bool useProfileCurveAntiAliasing);
QImage COMMON_EXPORT generateLightMap(const QImage &heightMap,
                                      qint32 depth,
                                      Direction direction,
                                      qint32 size,
                                      qint32 lightAngle,
                                      qint32 lightAltitude);
void COMMON_EXPORT applyMidGrayRemapping(QImage &lightMap,
                                         qint32 lightAltitude);
void COMMON_EXPORT applyMidGrayRemappingAndFade(const QImage &heightMap,
                                                QImage &lightMap,
                                                qint32 lightAltitude);
QImage COMMON_EXPORT generateTextureHeightMap(const QImage &mask,
                                              const QImage &texture,
                                              qint32 textureScale,
                                              const QPoint &textureOffset);
void COMMON_EXPORT applyTextureHeightMap(QImage &heightMap,
                                         const QImage &textureHeightMap,
                                         qint32 textureDepth,
                                         bool invertTexture);

QImage COMMON_EXPORT apply(const QImage &mask,
                           Style style,
                           Technique technique,
                           qint32 depth,
                           Direction direction,
                           qint32 size,
                           qint32 soften,
                           qint32 lightAngle,
                           qint32 lightAltitude,
                           const Curve &profileCurve,
                           bool useProfileCurveAntiAliasing,
                           qint32 profileRange,
                           bool reverseProfileRange,
                           const QImage &texture,
                           qint32 textureScale,
                           const QPoint &textureOffset,
                           qint32 textureDepth,
                           bool invertTexture,
                           const Curve &glossCurve,
                           bool useGlossCurveAntiAliasing,
                           bool applyFade);

};

#endif
