/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "BevelAndEmbossDirectionComboBox.h"

BevelAndEmbossDirectionComboBox::BevelAndEmbossDirectionComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Up", static_cast<int>(LayerStyles::BevelAndEmbossDirection_Up));
    addItem("Down", static_cast<int>(LayerStyles::BevelAndEmbossDirection_Down));

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentDirectionChanged(static_cast<LayerStyles::BevelAndEmbossDirection>(currentData().toInt()));
        }
    );
}
BevelAndEmbossDirectionComboBox::~BevelAndEmbossDirectionComboBox()
{}

LayerStyles::BevelAndEmbossDirection BevelAndEmbossDirectionComboBox::currentDirection() const
{
    return static_cast<LayerStyles::BevelAndEmbossDirection>(currentData().toInt());
}

void BevelAndEmbossDirectionComboBox::setCurrentDirection(LayerStyles::BevelAndEmbossDirection newDirection)
{
    setCurrentIndex(findData(static_cast<int>(newDirection)));
}
