/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef TONALRANGESSELECTOR_H
#define TONALRANGESSELECTOR_H

#include <QWidget>

#include <KisLevelsSlider.h>
#include <TonalRange.h>
#include <Export.h>

class COMMON_EXPORT TonalRangeSliderWithSpinBoxes : public QWidget
{
    Q_OBJECT

public:
    explicit TonalRangeSliderWithSpinBoxes(QWidget *parent = nullptr);
    ~TonalRangeSliderWithSpinBoxes() override;

    KisTonalRangeSlider* tonalRangeSlider() const;

Q_SIGNALS:
    void tonalRangeChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

class COMMON_EXPORT TonalRangesSelector : public QWidget
{
    Q_OBJECT

public:
    explicit TonalRangesSelector(QWidget *parent = nullptr);
    ~TonalRangesSelector() override;

    PixelTonalRanges sourceTonalRanges() const;
    PixelTonalRanges destinationTonalRanges() const;

public Q_SLOTS:
    void setTonalRanges(const PixelTonalRanges &newSourceTonalRanges,
                        const PixelTonalRanges &newDestinationTonalRanges);
    void setSourceTonalRanges(const PixelTonalRanges &newSourceTonalRanges);
    void setDestinationTonalRanges(const PixelTonalRanges &newDestinationTonalRanges);

Q_SIGNALS:
    void tonalRangesChanged(const PixelTonalRanges &newSourceTonalRanges,
                            const PixelTonalRanges &newDestinationTonalRanges);

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
