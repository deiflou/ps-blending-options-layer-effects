/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <cmath>

#include <QPainter>
#include <QLinearGradient>

#include <ColorConversion.h>
#include <ImgProcCommon.h>

#include "ImgProcUtil.h"

namespace ImgProcUtil
{

QImage shiftedImage(const QImage &image, const QPoint &offset, quint32 fillValue)
{
    QImage shiftedImage(image.size(), image.format());
    shiftedImage.fill(image.pixel(0, 0));

    if (offset.x() < -image.width() || offset.x() >= image.width() ||
        offset.y() < -image.height() || offset.y() >= image.height()) {
        return shiftedImage;
    }

    const QRect imageRect = image.rect();
    const QRect visibleImageRect = imageRect.translated(-offset).intersected(imageRect);
    const QPoint topLeftCorner(std::max(0, offset.x()), std::max(0, offset.y()));

    const qint32 imageBpp = image.pixelFormat().bitsPerPixel() / 8;

    for (qint32 y = 0; y < visibleImageRect.height(); ++y) {
        const quint8 *src = image.constScanLine(y + visibleImageRect.top()) + visibleImageRect.left() * imageBpp;
        quint8 *dst = shiftedImage.scanLine(y + topLeftCorner.y()) + topLeftCorner.x() * imageBpp;
        memcpy(dst, src, visibleImageRect.width() * imageBpp);
    }

    return shiftedImage;
}

QVector<ImgProcCommon::ARGB> generateGradientLut(const QGradientStops &gradient, qint32 size)
{
    QVector<ImgProcCommon::ARGB> lut(size, {0, 0, 0, 0});
    QImage gradientLut(reinterpret_cast<quint8*>(lut.data()), size, 1, QImage::Format_ARGB32);
    QPainter p(&gradientLut);
    QLinearGradient g(0, 0, size, 0);
    g.setStops(gradient);
    p.fillRect(0, 0, size, 1, g);
    return lut;
}

QImage scaledUpMask(const QImage &mask, bool invert)
{
    QImage outMask(mask.size(), QImage::Format_ARGB32);
    if (invert) {
        ImgProcUtil::visitPixels(
            outMask, mask,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                quint32 *p1 = reinterpret_cast<quint32*>(pixel1);
                *p1 = (255 - *pixel2) << 8;
            }
        );
    } else {
        ImgProcUtil::visitPixels(
            outMask, mask,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                quint32 *p1 = reinterpret_cast<quint32*>(pixel1);
                *p1 = *pixel2 << 8;
            }
        );
    }
    return outMask;
}

QImage scaledDownMask(const QImage &mask, bool invert)
{
    QImage outMask(mask.size(), QImage::Format_Alpha8);
    if (invert) {
        ImgProcUtil::visitPixels(
            outMask, mask,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                const quint32 *p2 = reinterpret_cast<const quint32*>(pixel2);
                const quint32 invertedValue = 0xFF00 - *p2;
                *pixel1 = (invertedValue >> 8) + ((invertedValue & 0xFF) > 0x7F);
            }
        );
    } else {
        ImgProcUtil::visitPixels(
            outMask, mask,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                const quint32 *p2 = reinterpret_cast<const quint32*>(pixel2);
                *pixel1 = (*p2 >> 8) + ((*p2 & 0xFF) > 0x7F);
            }
        );
    }
    return outMask;
}

QImage invertMask(const QImage &mask)
{
    if (mask.format() == QImage::Format_Alpha8) {
        QImage invertedMask(mask.size(), QImage::Format_Alpha8);
        ImgProcUtil::visitPixels(
            invertedMask, mask,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = 0xFF - *pixel2;
            }
        );
        return invertedMask;
    } else if (mask.format() == QImage::Format_ARGB32) {
        QImage invertedMask(mask.size(), QImage::Format_ARGB32);
        ImgProcUtil::visitPixels(
            invertedMask, mask,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                quint32 *p1 = reinterpret_cast<quint32*>(pixel1);
                const quint32 *p2 = reinterpret_cast<const quint32*>(pixel2);
                *p1 = 0xFF00 - *p2;
            }
        );
        return invertedMask;
    }
    return mask;
}

QImage convertMaskToImage(const QImage &mask)
{
    QImage rgbaImage(mask.size(), QImage::Format_ARGB32);
    ImgProcUtil::visitPixels(
        rgbaImage, mask,
        [](quint8 *pixel1, const quint8 *pixel2)
        {
            ImgProcCommon::ARGB *p1 = reinterpret_cast<ImgProcCommon::ARGB*>(pixel1);
            *p1 = {*pixel2, *pixel2, *pixel2, 255};
        }
    );
    return rgbaImage;
}

QImage convertImageToMask(const QImage &image)
{
    QImage mask(image.size(), QImage::Format_Alpha8);
    ImgProcUtil::visitPixels(
        mask, image,
        [](quint8 *pixel1, const quint8 *pixel2)
        {
            const ImgProcCommon::ARGB p2 = *reinterpret_cast<const ImgProcCommon::ARGB*>(pixel2);
            *pixel1 = static_cast<quint8>(ColorConversion::intensity(p2) * p2.a / ImgProcCommon::maxValue);
        }
    );
    return mask;
}

qint32 percentageOpacityToByte(qint32 percentageOpacity)
{
    return percentageOpacity * 255 / 100;
}

qint32 byteOpacityToPercentage(qint32 byteOpacity)
{
    return byteOpacity * 100 / 255;
}

}
