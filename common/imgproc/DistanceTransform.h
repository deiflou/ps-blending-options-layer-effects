/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef DISTANCETRANSFORM_H
#define DISTANCETRANSFORM_H

#include <QImage>

#include <ImgProcUtil.h>
#include <Export.h>

namespace DistanceTransform
{

static constexpr qint32 chamfer3x3DistanceA_ChessBoard {256}; // 1 * 256
static constexpr qint32 chamfer3x3DistanceB_ChessBoard {256}; // 1 * 256

static constexpr qint32 chamfer3x3DistanceA_CityBlock {256}; // 1 * 256
static constexpr qint32 chamfer3x3DistanceB_CityBlock {512}; // 2 * 256

static constexpr qint32 chamfer3x3DistanceA_Euclidean {256}; // 1 * 256
static constexpr qint32 chamfer3x3DistanceB_Euclidean {362}; // sqrt(2) * 256

static constexpr qint32 chamfer3x3DistanceA_Borgefors {256}; // 3/3 * 256
static constexpr qint32 chamfer3x3DistanceB_Borgefors {341}; // 4/3 * 256

static constexpr qint32 chamfer3x3DistanceA_ButtMaragosMAE {246}; // 0.9619 * 256
static constexpr qint32 chamfer3x3DistanceB_ButtMaragosMAE {348}; // 1.3604 * 256

static constexpr qint32 chamfer3x3DistanceA_ButtMaragosMSE {243}; // 0.9489 * 256
static constexpr qint32 chamfer3x3DistanceB_ButtMaragosMSE {344}; // 1.3419 * 256

static constexpr qint32 chamfer5x5DistanceA_Euclidean {256}; // 1 * 256
static constexpr qint32 chamfer5x5DistanceB_Euclidean {362}; // sqrt(2) * 256
static constexpr qint32 chamfer5x5DistanceC_Euclidean {572}; // sqrt(5) * 256

static constexpr qint32 chamfer5x5DistanceA_Borgefors {256}; // 5/5 * 256
static constexpr qint32 chamfer5x5DistanceB_Borgefors {358}; // 7/5 * 256
static constexpr qint32 chamfer5x5DistanceC_Borgefors {563}; // 11/5 * 256

static constexpr qint32 chamfer5x5DistanceA_ButtMaragosMAE {253}; // 0.9866 * 256
static constexpr qint32 chamfer5x5DistanceB_ButtMaragosMAE {362}; // sqrt(2) * 256
static constexpr qint32 chamfer5x5DistanceC_ButtMaragosMAE {565}; // 2.2062 * 256

static constexpr qint32 chamfer5x5DistanceA_ButtMaragosMSE {251}; // 0.9802 * 256
static constexpr qint32 chamfer5x5DistanceB_ButtMaragosMSE {360}; // 1.4060 * 256
static constexpr qint32 chamfer5x5DistanceC_ButtMaragosMSE {564}; // 2.2046 * 256

static constexpr quint32 stencilMask {0x80000000};
static constexpr quint32 distanceMask {0x7FFFFFFF};

inline bool isStenciled(quint32 value)
{
    return value & stencilMask;
}

inline quint32 distance(quint32 value)
{
    return value & distanceMask;
}

struct DefaultInitFn
{
    quint32 operator()(const quint8 *pixel, quint32 infinity) const
    {
        return *pixel == 0 ? infinity : 255 - *pixel;
    }
};

struct InvertMaskInitFn
{
    quint32 operator()(const quint8 *pixel, quint32 infinity) const
    {
        return *pixel == 255 ? infinity : *pixel;
    }
};

template <typename InitFn = DefaultInitFn>
inline void initChamferDistanceMap(QImage &distanceMap, const QImage &mask, InitFn initFn = InitFn())
{
    const quint32 infinity = (mask.width() + mask.height()) << 8;
    ImgProcUtil::visitPixels(
        distanceMap, mask,
        [infinity, initFn](quint8 *pixel1, const quint8 *pixel2)
        {
            quint32 *distancePixel = reinterpret_cast<quint32 *>(pixel1);
            *distancePixel = initFn(pixel2, infinity);
        }
    );
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer3x3(const QImage &mask, qint32 a, qint32 b, InitFn initFn = InitFn())
{
    QImage distanceMap(mask.size(), QImage::Format_ARGB32);

    const qint32 scanLineSize = distanceMap.bytesPerLine();

    // Initialize
    initChamferDistanceMap(distanceMap, mask, initFn);
    // Forward pass
    for (qint32 y = 1; y < distanceMap.height() - 1; ++y) {
        quint8 *distancePixel = distanceMap.scanLine(y) + 4;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
        for (qint32 x = 1; x < distanceMap.width() - 1; ++x, ++thisPixel, ++topPixel) {
            if (isStenciled(*thisPixel)) {
                continue;
            }
            const quint32 da1 = distance(*(thisPixel - 1)) + a;
            const quint32 db1 = distance(*(topPixel + 1)) + b;
            const quint32 da2 = distance(*(topPixel)) + a;
            const quint32 db2 = distance(*(topPixel - 1)) + b;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(db1, std::min(da2, db2))));
        }
    }
    // Backward pass
    for (qint32 y = distanceMap.height() - 2; y > 0; --y) {
        quint8 *distancePixel = distanceMap.scanLine(y) + scanLineSize - 8;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
        for (qint32 x = distanceMap.width() - 3; x > 1; --x, --thisPixel, --bottomPixel) {
            if (isStenciled(*thisPixel)) {
                continue;
            }
            const quint32 da1 = distance(*(thisPixel + 1)) + a;
            const quint32 db1 = distance(*(bottomPixel - 1)) + b;
            const quint32 da2 = distance(*(bottomPixel)) + a;
            const quint32 db2 = distance(*(bottomPixel + 1)) + b;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(db1, std::min(da2, db2))));
        }
    }

    return distanceMap;
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer3x3_ChessBoard(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer3x3(mask,
                      chamfer3x3DistanceA_ChessBoard,
                      chamfer3x3DistanceB_ChessBoard,
                      initFn);
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer3x3_CityBlock(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer3x3(mask,
                      chamfer3x3DistanceA_CityBlock,
                      chamfer3x3DistanceB_CityBlock,
                      initFn);
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer3x3_Euclidean(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer3x3(mask,
                      chamfer3x3DistanceA_Euclidean,
                      chamfer3x3DistanceB_Euclidean,
                      initFn);
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer3x3_Borgefors(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer3x3(mask,
                      chamfer3x3DistanceA_Borgefors,
                      chamfer3x3DistanceB_Borgefors,
                      initFn);
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer3x3_ButtMaragosMAE(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer3x3(mask,
                      chamfer3x3DistanceA_ButtMaragosMAE,
                      chamfer3x3DistanceB_ButtMaragosMAE,
                      initFn);
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer3x3_ButtMaragosMSE(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer3x3(mask,
                      chamfer3x3DistanceA_ButtMaragosMSE,
                      chamfer3x3DistanceB_ButtMaragosMSE,
                      initFn);
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer5x5(const QImage &mask, qint32 a, qint32 b, qint32 c, InitFn initFn = InitFn())
{
    QImage distanceMap(mask.size(), QImage::Format_ARGB32);

    const qint32 scanLineSize = distanceMap.bytesPerLine();

    // Initialize
    initChamferDistanceMap(distanceMap, mask, initFn);
    // Forward pass
    // First row
    {
        quint8 *distancePixel = distanceMap.scanLine(0) + 4;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        for (qint32 x = 1; x < distanceMap.width(); ++x, ++thisPixel) {
            if (isStenciled(*thisPixel)) {
                continue;
            }
            const quint32 da1 = distance(*(thisPixel - 1)) + a;
            *thisPixel = std::min(*thisPixel, da1);
        }
    }
    // Second row, first pixel
    {
        quint8 *distancePixel = distanceMap.scanLine(1);
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
        if (!isStenciled(*thisPixel)) {
            const quint32 dc1 = distance(*(topPixel + 2)) + c;
            const quint32 db1 = distance(*(topPixel + 1)) + b;
            const quint32 da2 = distance(*(topPixel)) + a;
            *thisPixel = std::min(*thisPixel, std::min(dc1, std::min(db1, da2)));
        }
    }
    // Second row, second pixel
    {
        quint8 *distancePixel = distanceMap.scanLine(1) + 4;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
        if (!isStenciled(*thisPixel)) {
            const quint32 da1 = distance(*(thisPixel - 1)) + a;
            const quint32 dc1 = distance(*(topPixel + 2)) + c;
            const quint32 db1 = distance(*(topPixel + 1)) + b;
            const quint32 da2 = distance(*(topPixel)) + a;
            const quint32 db2 = distance(*(topPixel - 1)) + b;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(dc1, std::min(db1,
                            std::min(da2, db2)))));
        }
    }
    // Second row, middle pixels
    {
        quint8 *distancePixel = distanceMap.scanLine(1) + 8;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
        for (qint32 x = 2; x < distanceMap.width() - 2; ++x, ++thisPixel, ++topPixel) {
            if (isStenciled(*thisPixel)) {
                continue;
            }
            const quint32 da1 = distance(*(thisPixel - 1)) + a;
            const quint32 dc1 = distance(*(topPixel + 2)) + c;
            const quint32 db1 = distance(*(topPixel + 1)) + b;
            const quint32 da2 = distance(*(topPixel)) + a;
            const quint32 db2 = distance(*(topPixel - 1)) + b;
            const quint32 dc2 = distance(*(topPixel - 2)) + c;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(dc1, std::min(db1,
                            std::min(da2, std::min(db2, dc2))))));
        }
    }
    // Second row, second to last pixel
    {
        quint8 *distancePixel = distanceMap.scanLine(1) + distanceMap.width() * 4 - 8;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
        if (!isStenciled(*thisPixel)) {
            const quint32 da1 = distance(*(thisPixel - 1)) + a;
            const quint32 db1 = distance(*(topPixel + 1)) + b;
            const quint32 da2 = distance(*(topPixel)) + a;
            const quint32 db2 = distance(*(topPixel - 1)) + b;
            const quint32 dc2 = distance(*(topPixel - 2)) + c;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(db1,
                            std::min(da2, std::min(db2, dc2)))));
        }
    }
    // Second row, last pixel
    {
        quint8 *distancePixel = distanceMap.scanLine(1) + distanceMap.width() * 4 - 4;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
        if (!isStenciled(*thisPixel)) {
            const quint32 da1 = distance(*(thisPixel - 1)) + a;
            const quint32 da2 = distance(*(topPixel)) + a;
            const quint32 db2 = distance(*(topPixel - 1)) + b;
            const quint32 dc2 = distance(*(topPixel - 2)) + c;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(da2, std::min(db2, dc2))));
        }
    }
    // Rest of rows
    for (qint32 y = 2; y < distanceMap.height(); ++y) {
        // First pixel
        {
            quint8 *distancePixel = distanceMap.scanLine(y);
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
            quint32 *topTopPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize * 2);
            if (!isStenciled(*thisPixel)) {
                const quint32 dc1 = distance(*(topPixel + 2)) + c;
                const quint32 db1 = distance(*(topPixel + 1)) + b;
                const quint32 da2 = distance(*(topPixel)) + a;
                const quint32 dc3 = distance(*(topTopPixel + 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(dc1, std::min(db1, std::min(da2, dc3))));
            }
        }
        // Second pixel
        {
            quint8 *distancePixel = distanceMap.scanLine(y) + 4;
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
            quint32 *topTopPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize * 2);
            if (!isStenciled(*thisPixel)) {
                const quint32 da1 = distance(*(thisPixel - 1)) + a;
                const quint32 dc1 = distance(*(topPixel + 2)) + c;
                const quint32 db1 = distance(*(topPixel + 1)) + b;
                const quint32 da2 = distance(*(topPixel)) + a;
                const quint32 db2 = distance(*(topPixel - 1)) + b;
                const quint32 dc3 = distance(*(topTopPixel + 1)) + c;
                const quint32 dc4 = distance(*(topTopPixel - 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(da1, std::min(dc1, std::min(db1,
                                std::min(da2, std::min(db2, std::min(dc3, dc4)))))));
            }
        }
        // Middle pixels
        {
            quint8 *distancePixel = distanceMap.scanLine(y) + 8;
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
            quint32 *topTopPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize * 2);
            for (qint32 x = 2; x < distanceMap.width() - 2; ++x, ++thisPixel, ++topPixel, ++topTopPixel) {
                if (isStenciled(*thisPixel)) {
                    continue;
                }
                const quint32 da1 = distance(*(thisPixel - 1)) + a;
                const quint32 dc1 = distance(*(topPixel + 2)) + c;
                const quint32 db1 = distance(*(topPixel + 1)) + b;
                const quint32 da2 = distance(*(topPixel)) + a;
                const quint32 db2 = distance(*(topPixel - 1)) + b;
                const quint32 dc2 = distance(*(topPixel - 2)) + c;
                const quint32 dc3 = distance(*(topTopPixel + 1)) + c;
                const quint32 dc4 = distance(*(topTopPixel - 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(da1, std::min(dc1, std::min(db1,
                                std::min(da2, std::min(db2, std::min(dc2, std::min(dc3, dc4))))))));
            }
        }
        // Second to last pixel
        {
            quint8 *distancePixel = distanceMap.scanLine(y) + distanceMap.width() * 4 - 8;
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
            quint32 *topTopPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize * 2);
            if (!isStenciled(*thisPixel)) {
                const quint32 da1 = distance(*(thisPixel - 1)) + a;
                const quint32 db1 = distance(*(topPixel + 1)) + b;
                const quint32 da2 = distance(*(topPixel)) + a;
                const quint32 db2 = distance(*(topPixel - 1)) + b;
                const quint32 dc2 = distance(*(topPixel - 2)) + c;
                const quint32 dc3 = distance(*(topTopPixel + 1)) + c;
                const quint32 dc4 = distance(*(topTopPixel - 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(da1, std::min(db1,
                                std::min(da2, std::min(db2, std::min(dc2, std::min(dc3, dc4)))))));
            }
        }
        // Last pixel
        {
            quint8 *distancePixel = distanceMap.scanLine(y) + distanceMap.width() * 4 - 4;
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *topPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize);
            quint32 *topTopPixel = reinterpret_cast<quint32 *>(distancePixel - scanLineSize * 2);
            if (!isStenciled(*thisPixel)) {
                const quint32 da1 = distance(*(thisPixel - 1)) + a;
                const quint32 da2 = distance(*(topPixel)) + a;
                const quint32 db2 = distance(*(topPixel - 1)) + b;
                const quint32 dc2 = distance(*(topPixel - 2)) + c;
                const quint32 dc4 = distance(*(topTopPixel - 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(da1, std::min(da2, std::min(db2,
                                std::min(dc2, dc4)))));
            }
        }
    }
    // Backward pass
    // First row
    {
        quint8 *distancePixel = distanceMap.scanLine(distanceMap.height() - 1) + distanceMap.width() * 4 - 8;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        for (qint32 x = distanceMap.width() - 2; x >= 0; --x, --thisPixel) {
            if (isStenciled(*thisPixel)) {
                continue;
            }
            const quint32 da1 = distance(*(thisPixel + 1)) + a;
            *thisPixel = std::min(*thisPixel, da1);
        }
    }
    // Second row, first pixel
    {
        quint8 *distancePixel = distanceMap.scanLine(distanceMap.height() - 2) + distanceMap.width() * 4 - 4;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
        if (!isStenciled(*thisPixel)) {
            const quint32 dc1 = distance(*(bottomPixel - 2)) + c;
            const quint32 db1 = distance(*(bottomPixel - 1)) + b;
            const quint32 da2 = distance(*(bottomPixel)) + a;
            *thisPixel = std::min(*thisPixel, std::min(dc1, std::min(db1, da2)));
        }
    }
    // Second row, second pixel
    {
        quint8 *distancePixel = distanceMap.scanLine(distanceMap.height() - 2) + distanceMap.width() * 4 - 8;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
        if (!isStenciled(*thisPixel)) {
            const quint32 da1 = distance(*(thisPixel + 1)) + a;
            const quint32 dc1 = distance(*(bottomPixel - 2)) + c;
            const quint32 db1 = distance(*(bottomPixel - 1)) + b;
            const quint32 da2 = distance(*(bottomPixel)) + a;
            const quint32 db2 = distance(*(bottomPixel + 1)) + b;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(dc1, std::min(db1,
                            std::min(da2, db2)))));
        }
    }
    // Second row, middle pixels
    {
        quint8 *distancePixel = distanceMap.scanLine(distanceMap.height() - 2) + distanceMap.width() * 4 - 12;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
        for (qint32 x = distanceMap.width() - 3; x > 1; --x, --thisPixel, --bottomPixel) {
            if (isStenciled(*thisPixel)) {
                continue;
            }
            const quint32 da1 = distance(*(thisPixel + 1)) + a;
            const quint32 dc1 = distance(*(bottomPixel - 2)) + c;
            const quint32 db1 = distance(*(bottomPixel - 1)) + b;
            const quint32 da2 = distance(*(bottomPixel)) + a;
            const quint32 db2 = distance(*(bottomPixel + 1)) + b;
            const quint32 dc2 = distance(*(bottomPixel + 2)) + c;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(dc1, std::min(db1,
                            std::min(da2, std::min(db2, dc2))))));
        }
    }
    // Second row, second to last pixel
    {
        quint8 *distancePixel = distanceMap.scanLine(distanceMap.height() - 2) + 4;
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
        if (!isStenciled(*thisPixel)) {
            const quint32 da1 = distance(*(thisPixel + 1)) + a;
            const quint32 db1 = distance(*(bottomPixel - 1)) + b;
            const quint32 da2 = distance(*(bottomPixel)) + a;
            const quint32 db2 = distance(*(bottomPixel + 1)) + b;
            const quint32 dc2 = distance(*(bottomPixel + 2)) + c;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(db1,
                            std::min(da2, std::min(db2, dc2)))));
        }
    }
    // Second row, last pixel
    {
        quint8 *distancePixel = distanceMap.scanLine(distanceMap.height() - 2);
        quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
        quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
        if (!isStenciled(*thisPixel)) {
            const quint32 da1 = distance(*(thisPixel + 1)) + a;
            const quint32 da2 = distance(*(bottomPixel)) + a;
            const quint32 db2 = distance(*(bottomPixel + 1)) + b;
            const quint32 dc2 = distance(*(bottomPixel + 2)) + c;
            *thisPixel = std::min(*thisPixel, std::min(da1, std::min(da2, std::min(db2, dc2))));
        }
    }
    // Rest of Rows
    for (qint32 y = distanceMap.height() - 3; y >= 0; --y) {
        // First pixel
        {
            quint8 *distancePixel = distanceMap.scanLine(y) + distanceMap.width() * 4 - 4;
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
            quint32 *bottomBottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize * 2);
            if (!isStenciled(*thisPixel)) {
                const quint32 dc1 = distance(*(bottomPixel - 2)) + c;
                const quint32 db1 = distance(*(bottomPixel - 1)) + b;
                const quint32 da2 = distance(*(bottomPixel)) + a;
                const quint32 dc3 = distance(*(bottomBottomPixel - 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(dc1, std::min(db1, std::min(da2, dc3))));
            }
        }
        // Second pixel
        {
            quint8 *distancePixel = distanceMap.scanLine(y) + distanceMap.width() * 4 - 8;
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
            quint32 *bottomBottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize * 2);
            if (!isStenciled(*thisPixel)) {
                const quint32 da1 = distance(*(thisPixel + 1)) + a;
                const quint32 dc1 = distance(*(bottomPixel - 2)) + c;
                const quint32 db1 = distance(*(bottomPixel - 1)) + b;
                const quint32 da2 = distance(*(bottomPixel)) + a;
                const quint32 db2 = distance(*(bottomPixel + 1)) + b;
                const quint32 dc3 = distance(*(bottomBottomPixel - 1)) + c;
                const quint32 dc4 = distance(*(bottomBottomPixel + 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(da1, std::min(dc1, std::min(db1,
                                std::min(da2, std::min(db2, std::min(dc3, dc4)))))));
            }
        }
        // Middle pixels
        {
            quint8 *distancePixel = distanceMap.scanLine(y) + distanceMap.width() * 4 - 12;
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
            quint32 *bottomBottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize * 2);
            for (qint32 x = distanceMap.width() - 3; x > 1; --x, --thisPixel, --bottomPixel, --bottomBottomPixel) {
                if (isStenciled(*thisPixel)) {
                    continue;
                }
                const quint32 da1 = distance(*(thisPixel + 1)) + a;
                const quint32 dc1 = distance(*(bottomPixel - 2)) + c;
                const quint32 db1 = distance(*(bottomPixel - 1)) + b;
                const quint32 da2 = distance(*(bottomPixel)) + a;
                const quint32 db2 = distance(*(bottomPixel + 1)) + b;
                const quint32 dc2 = distance(*(bottomPixel + 2)) + c;
                const quint32 dc3 = distance(*(bottomBottomPixel - 1)) + c;
                const quint32 dc4 = distance(*(bottomBottomPixel + 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(da1, std::min(dc1, std::min(db1,
                                std::min(da2, std::min(db2, std::min(dc2, std::min(dc3, dc4))))))));
            }
        }
        // Second to last pixel
        {
            quint8 *distancePixel = distanceMap.scanLine(y) + 4;
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
            quint32 *bottomBottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize * 2);
            if (!isStenciled(*thisPixel)) {
                const quint32 da1 = distance(*(thisPixel + 1)) + a;
                const quint32 db1 = distance(*(bottomPixel - 1)) + b;
                const quint32 da2 = distance(*(bottomPixel)) + a;
                const quint32 db2 = distance(*(bottomPixel + 1)) + b;
                const quint32 dc2 = distance(*(bottomPixel + 2)) + c;
                const quint32 dc3 = distance(*(bottomBottomPixel - 1)) + c;
                const quint32 dc4 = distance(*(bottomBottomPixel + 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(da1, std::min(db1,
                                std::min(da2, std::min(db2, std::min(dc2, std::min(dc3, dc4)))))));
            }
        }
        // Last pixel
        {
            quint8 *distancePixel = distanceMap.scanLine(y);
            quint32 *thisPixel = reinterpret_cast<quint32 *>(distancePixel);
            quint32 *bottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize);
            quint32 *bottomBottomPixel = reinterpret_cast<quint32 *>(distancePixel + scanLineSize * 2);
            if (!isStenciled(*thisPixel)) {
                const quint32 da1 = distance(*(thisPixel + 1)) + a;
                const quint32 da2 = distance(*(bottomPixel)) + a;
                const quint32 db2 = distance(*(bottomPixel + 1)) + b;
                const quint32 dc2 = distance(*(bottomPixel + 2)) + c;
                const quint32 dc4 = distance(*(bottomBottomPixel + 1)) + c;
                *thisPixel = std::min(*thisPixel, std::min(da1, std::min(da2, std::min(db2,
                                std::min(dc2, dc4)))));
            }
        }
    }

    return distanceMap;
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer5x5_Euclidean(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer5x5(mask,
                      chamfer5x5DistanceA_Euclidean,
                      chamfer5x5DistanceB_Euclidean,
                      chamfer5x5DistanceC_Euclidean,
                      initFn);
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer5x5_Borgefors(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer5x5(mask,
                      chamfer5x5DistanceA_Borgefors,
                      chamfer5x5DistanceB_Borgefors,
                      chamfer5x5DistanceC_Borgefors,
                      initFn);
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer5x5_ButtMaragosMAE(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer5x5(mask,
                      chamfer5x5DistanceA_ButtMaragosMAE,
                      chamfer5x5DistanceB_ButtMaragosMAE,
                      chamfer5x5DistanceC_ButtMaragosMAE,
                      initFn);
}

template <typename InitFn = DefaultInitFn>
inline QImage chamfer5x5_ButtMaragosMSE(const QImage &mask, InitFn initFn = InitFn())
{
    return chamfer5x5(mask,
                      chamfer5x5DistanceA_ButtMaragosMSE,
                      chamfer5x5DistanceB_ButtMaragosMSE,
                      chamfer5x5DistanceC_ButtMaragosMSE,
                      initFn);
}

}

#endif
