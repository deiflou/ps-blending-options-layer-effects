/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QColorDialog>
#include <QPaintEvent>
#include <QPainter>
#include <QStyle>

#include "ColorButton.h"

class Q_DECL_HIDDEN ColorButton::Private
{
public:
    QColor color {Qt::black};
};

ColorButton::ColorButton(QWidget *parent)
    : QPushButton(parent)
    , m_d(new Private)
{
    setMinimumHeight(32);
    connect(
        this,&QPushButton::clicked, [this]()
        {
            const QColor newColor = QColorDialog::getColor(m_d->color, this);
            if (newColor.isValid()) {
                setColor(newColor);
            }
        }
    );
}

ColorButton::~ColorButton()
{}

const QColor& ColorButton::color() const
{
    return m_d->color;
}

void ColorButton::setColor(const QColor &newColor)
{
    if (newColor == m_d->color) {
        return;
    }
    m_d->color = newColor;
    emit colorChanged(m_d->color);
    update();
}

void ColorButton::paintEvent(QPaintEvent *e)
{
    QPushButton::paintEvent(e);

    QPainter p(this);

    const int buttonMargin = style()->pixelMetric(QStyle::PM_ButtonMargin);
    QRect colorRect = rect().adjusted(buttonMargin, buttonMargin, -buttonMargin, -buttonMargin);
    if (isDown()) {
        const int buttonHorizontalShift = style()->pixelMetric(QStyle::PM_ButtonShiftHorizontal);
        const int buttonVerticalShift = style()->pixelMetric(QStyle::PM_ButtonShiftVertical);
        colorRect.translate(buttonHorizontalShift, buttonVerticalShift);
    }
    p.fillRect(colorRect, m_d->color);
    p.setBrush(Qt::NoBrush);
    p.setPen(QColor(0, 0, 0, 128));
    p.drawRect(colorRect.adjusted(0, 0, -1, -1));
}
