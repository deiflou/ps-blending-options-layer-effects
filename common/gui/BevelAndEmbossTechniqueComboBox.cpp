/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "BevelAndEmbossTechniqueComboBox.h"

BevelAndEmbossTechniqueComboBox::BevelAndEmbossTechniqueComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Smooth", static_cast<int>(LayerStyles::BevelAndEmbossTechnique_Smooth));
    addItem("Chisel Hard", static_cast<int>(LayerStyles::BevelAndEmbossTechnique_ChiselHard));
    addItem("Chisel Soft", static_cast<int>(LayerStyles::BevelAndEmbossTechnique_ChiselSoft));

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentTechniqueChanged(static_cast<LayerStyles::BevelAndEmbossTechnique>(currentData().toInt()));
        }
    );
}
BevelAndEmbossTechniqueComboBox::~BevelAndEmbossTechniqueComboBox()
{}

LayerStyles::BevelAndEmbossTechnique BevelAndEmbossTechniqueComboBox::currentTechnique() const
{
    return static_cast<LayerStyles::BevelAndEmbossTechnique>(currentData().toInt());
}

void BevelAndEmbossTechniqueComboBox::setCurrentTechnique(LayerStyles::BevelAndEmbossTechnique newTechnique)
{
    setCurrentIndex(findData(static_cast<int>(newTechnique)));
}
