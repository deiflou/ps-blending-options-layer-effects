/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <cmath>

#include <QFormLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QPainter>

#include <Viewport.h>
#include <SliderSpinBox.h>
#include <ImgProcUtil.h>
#include <ImgProcCommon.h>
#include <TonalRangesSelector.h>
#include <PixelBlending.h>
#include <DistanceTransform.h>
#include <SizeAndSpreadTransform.h>
#include <ContourTransform.h>
#include <LayerStyles.h>
#include <Layer.h>
#include <LayerBlending.h>
#include <BevelAndEmbossTransform.h>
#include <Filtering.h>

#include "Window.h"

using namespace ImgProcUtil;
using namespace ImgProcCommon;
using namespace PixelBlending;
using namespace LayerStyles;
using namespace LayerBlending;

class Q_DECL_HIDDEN Window::Private
{
public:


    Window *window {nullptr};
    Private(Window *parent)
        : window(parent)
    {}

    qint32 currentImageIndex {0};
    mutable QRandomGenerator rng;

    TonalRangeSliderWithSpinBoxes *tonalRangeSelector1;
    TonalRangeSliderWithSpinBoxes *tonalRangeSelector2;
    SliderSpinBox *slider1 {nullptr};

    QImage tex01, tex02, tex03, pattern;

    QImage generateCheckerboardPattern(qint32 width, qint32 height) const
    {
        QImage checkerBoardPattern(2, 2, QImage::Format_ARGB32);
        checkerBoardPattern.setPixel(0, 0, qRgba(192, 192, 192, 255));
        checkerBoardPattern.setPixel(1, 0, qRgba(128, 128, 128, 255));
        checkerBoardPattern.setPixel(0, 1, qRgba(128, 128, 128, 255));
        checkerBoardPattern.setPixel(1, 1, qRgba(192, 192, 192, 255));
        QImage pattern(width, height, QImage::Format_ARGB32);
        QPainter p(&pattern);
        QBrush b(checkerBoardPattern);
        QTransform t;
        t.scale(8, 8);
        b.setTransform(t);
        p.fillRect(pattern.rect(), b);
        return pattern;
    }

    QImage generateEllipseMask(qint32 width, qint32 height, qreal scaleX, qreal scaleY, qreal fadeStart, qreal fadeEnd) const
    {
        QImage mask(width, height, QImage::Format_Alpha8);
        const qreal centerX = width / 2.0;
        const qreal centerY = height / 2.0;
        const qreal maxRadius = std::min(centerX, centerY);

        visitPixelsWithCoords(mask,
            [centerX, centerY, maxRadius, scaleX, scaleY, fadeStart, fadeEnd]
            (quint8 *pixel, qint32 x, qint32 y)
            {
                const qreal xf = (static_cast<qreal>(x) - centerX) / scaleX;
                const qreal yf = (static_cast<qreal>(y) - centerY) / scaleY;
                const qreal r = std::sqrt(xf * xf + yf * yf);
                const qreal a = r > maxRadius * fadeEnd
                                ? 0.0
                                : r > maxRadius * fadeStart
                                  ? 255.0 - (r - (maxRadius * fadeStart)) * 255.0 / (maxRadius * (fadeEnd - fadeStart))
                                  : 255.0;
                *pixel = static_cast<quint8>(std::round(a));
            }
        );

        return mask;
    }

    QImage generateBackgroundImage(qint32 width, qint32 height) const
    {
        QImage background(width, height, QImage::Format_ARGB32);
        QPainter p(&background);
        QLinearGradient grd({0.0, 0.0}, {static_cast<qreal>(width), 0.0});
        grd.setStops({{0.0, QColor(255, 20, 70)}, {1.0, QColor(255, 170, 150)}});
        p.fillRect(background.rect(), grd);
        return background;
    }

    QImage generateLayer1Image(qint32 width, qint32 height) const
    {
        QImage fill = tex01.scaled(width, height);
        QImage shape = generateEllipseMask(width, height, 1.0, 0.2, 0.9, 1.0);
        setAlphaChannel(fill, shape);
        return fill;
    }

    QImage generateCurrentLayerImage(qint32 width, qint32 height) const
    {
        QImage fill = tex02.scaled(width, height);
        QImage shape = generateEllipseMask(width, height, 0.7, 0.7, 0.99, 1.0);
        setAlphaChannel(fill, shape);
        return fill;
    }

    QImage generateImage1() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.33, 0.85);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.33, 0.85);
        QImage mask3(150, 150, QImage::Format_Alpha8);
        QImage mask4(150, 150, QImage::Format_Alpha8);
        QImage mask5(150, 150, QImage::Format_Alpha8);
        QImage mask6(150, 150, QImage::Format_Alpha8);

        visitPixels(mask3, mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                *pixel1 = *pixel2 * *pixel3 / maxValue;
            }
        );
        visitPixels(mask4, mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                *pixel1 = maxValue - (maxValue - *pixel2) * (maxValue - *pixel3) / maxValue;
            }
        );
        visitPixels(mask5, mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                *pixel1 = std::min(*pixel2, *pixel3);
            }
        );
        visitPixels(mask6, mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                *pixel1 = std::max(*pixel2, *pixel3);
            }
        );

        QImage out(150 * 3 + 10 * 4, 150 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(10, 10*2 + 150, convertMaskToImage(mask2));
        p.drawImage(10*2 + 150, 10, convertMaskToImage(mask3));
        p.drawImage(10*2 + 150, 10*2+150, convertMaskToImage(mask4));
        p.drawImage(10*3 + 150*2, 10, convertMaskToImage(mask5));
        p.drawImage(10*3 + 150*2, 10*2+150, convertMaskToImage(mask6));

        out.save("mixing_masks.png");

        return out;
    }

    QImage generateImage2() const
    {
        TonalRangeLUT lut({tonalRangeSelector1->tonalRangeSlider()->blackRampStartPoint(),
                           tonalRangeSelector1->tonalRangeSlider()->blackRampEndPoint(),
                           tonalRangeSelector1->tonalRangeSlider()->whiteRampStartPoint(),
                           tonalRangeSelector1->tonalRangeSlider()->whiteRampEndPoint()});

        QImage graph(152, 200, QImage::Format_ARGB32);
        graph.fill(Qt::white);
        QPainter p(&graph);
        p.setRenderHint(QPainter::Antialiasing);
        p.setPen(Qt::gray);
        p.drawRect(QRectF(1.5, 1.5, 149.0, 149.0));
        QPalette pal1 = tonalRangeSelector1->tonalRangeSlider()->palette();
        QPalette pal2 = tonalRangeSelector1->tonalRangeSlider()->palette();
        pal2.setColor(QPalette::Window, Qt::white);
        tonalRangeSelector1->tonalRangeSlider()->setPalette(pal2);
        p.drawPixmap(1, 160, tonalRangeSelector1->tonalRangeSlider()->grab());
        tonalRangeSelector1->tonalRangeSlider()->setPalette(pal1);
        QPolygonF poly;
        for (int x = 0; x < 256; ++x) {
            poly.append(QPointF(x * 150.0 / 255.0, 150.0 - lut(x) * 150.0 / 255.0) + QPointF(1.0, 1.0));
        }
        p.setPen(QPen(QColor(0, 128, 0), 2));
        p.drawPolyline(poly);

        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, .10, 1.31);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, .10, 1.31);
        QImage mask3(150, 150, QImage::Format_Alpha8);
        visitPixels(mask3, mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                *pixel1 = std::max(*pixel2, *pixel3);
            }
        );
        QImage mask4(mask1.size(), QImage::Format_Alpha8);
        visitPixels(mask4, mask3,
            [lut](quint8 *pixel1, const quint8 *pixel2) {
                *pixel1 = lut(*pixel2);
            }
        );

        QImage out(152 + 150 + 150 + 10 * 4, 210, QImage::Format_ARGB32);
        QPainter p2(&out);
        out.fill(Qt::white);
        p2.drawImage(10, 10, graph);
        p2.drawImage(10 * 2 + 152, 10, convertMaskToImage(mask3));
        p2.drawImage(10 * 3 + 152 + 150, 10, convertMaskToImage(mask4));
        // p2.drawText(10 * 2 + 152, 175, "An image");
        // p2.drawText(10 * 3 + 152 + 150, 175, "An mask generated from the");
        // p2.drawText(10 * 3 + 152 + 150, 175 + 12, "selected tonal range");

        return out;
    }

    QImage generateImage3() const
    {
        TonalRangeLUT lut({tonalRangeSelector2->tonalRangeSlider()->blackRampStartPoint(),
                           tonalRangeSelector2->tonalRangeSlider()->blackRampEndPoint(),
                           tonalRangeSelector2->tonalRangeSlider()->whiteRampStartPoint(),
                           tonalRangeSelector2->tonalRangeSlider()->whiteRampEndPoint()});

        QImage graph(152, 200, QImage::Format_ARGB32);
        graph.fill(Qt::white);
        QPainter p(&graph);
        p.setRenderHint(QPainter::Antialiasing);
        p.setPen(Qt::gray);
        p.drawRect(QRectF(1.5, 1.5, 149.0, 149.0));
        QPalette pal1 = tonalRangeSelector2->tonalRangeSlider()->palette();
        QPalette pal2 = tonalRangeSelector2->tonalRangeSlider()->palette();
        pal2.setColor(QPalette::Window, Qt::white);
        tonalRangeSelector2->tonalRangeSlider()->setPalette(pal2);
        p.drawPixmap(1, 160, tonalRangeSelector2->tonalRangeSlider()->grab());
        tonalRangeSelector2->tonalRangeSlider()->setPalette(pal1);
        QPolygonF poly;
        for (int x = 0; x < 256; ++x) {
            poly.append(QPointF(x * 150.0 / 255.0, 150.0 - lut(x) * 150.0 / 255.0) + QPointF(1.0, 1.0));
        }
        p.setPen(QPen(QColor(0, 128, 0), 2));
        p.drawPolyline(poly);

        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, .10, 1.31);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, .10, 1.31);
        QImage mask3(150, 150, QImage::Format_Alpha8);
        visitPixels(mask3, mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                *pixel1 = std::max(*pixel2, *pixel3);
            }
        );
        QImage mask4 = generateEllipseMask(150, 150, 1.0, 1.0, .9, .92);
        QImage mask5(mask1.size(), QImage::Format_Alpha8);
        visitPixels(mask5, mask3, mask4,
            [lut](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3) {
                *pixel1 = maxValue - (maxValue - lut(*pixel2)) * *pixel3 / maxValue;
            }
        );
        QImage image(150, 150, QImage::Format_ARGB32);
        visitPixels(image, mask3, mask4,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3) {
                ARGB *p1 = reinterpret_cast<ARGB*>(pixel1);
                *p1 = {*pixel2, *pixel2, *pixel2, *pixel3};
            }
        );

        QImage out(152 + 150 + 150 + 10 * 4, 210, QImage::Format_ARGB32);
        QPainter p2(&out);
        out.fill(Qt::white);
        p2.drawImage(10, 10, graph);
        p2.drawImage(10 * 2 + 152, 10, generateCheckerboardPattern(150, 150));
        p2.drawImage(10 * 2 + 152, 10, image);
        p2.drawImage(10 * 3 + 152 + 150, 10, convertMaskToImage(mask5));
        p2.setPen(Qt::gray);
        p2.drawRect(QRectF(10 * 3 + 152 + 150 + 0.5, 10.5, 149.0, 149.0));
        p2.setPen(Qt::black);
        // p2.drawText(10 * 2 + 152, 175, "An image");
        // p2.drawText(10 * 3 + 152 + 150, 175, "An mask generated from the");
        // p2.drawText(10 * 3 + 152 + 150, 175 + 12, "selected tonal range using");
        // p2.drawText(10 * 3 + 152 + 150, 175 + 24, "transparencyFromBackdropPixelTones()");

        out.save("tonal_range_backdrop.png");
        return out;
    }

    QImage generateImage4() const
    {
        QImage image1 = generateCheckerboardPattern(150, 150);
        QImage image2 = tex02;
        QImage mask = generateEllipseMask(150, 150, 1.0, 1.0, 0.7, 0.9);
        multiplyAlphaChannel(image2, mask);
        blend(image2, image1);
        QImage image3 = tex01;
        blend(image2, image3, SourceOverDestination, HardMix, false, 128);
        QImage image4 = tex01;
        blend(image2, image4, SourceOverDestination, HardMix, true, 128);
        QImage out(150 * 4 + 10 * 5, 150 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);

        QPainter p(&out);
        p.drawImage(10, 10, tex01);
        p.drawImage(150 + 10 * 2, 10, image1);
        p.drawImage(150 * 2 + 10 * 3, 10, image3);
        p.drawImage(150 * 3 + 10 * 4, 10, image4);

        out.save("special_color_blend.png");
        return out;
    }

    QImage generateImage5() const
    {
        QImage image1 = generateCheckerboardPattern(150, 150);
        QImage image2 = tex02;
        multiplyAlphaChannelConstant(image2, 128);
        blend(image2, image1);
        QImage mask = generateEllipseMask(150, 150, 1.0, 1.0, 0.7, 0.9);
        QImage image3 = tex01;
        crossDissolve(image1, image3, mask);
        QImage out(150 * 4 + 10 * 5, 150 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);

        QPainter p(&out);
        p.drawImage(10, 10, tex01);
        p.drawImage(150 + 10 * 2, 10, image1);
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask));
        p.drawImage(150 * 3 + 10 * 4, 10, image3);
        p.end();

        out.save("cross_dissolve.png");

        image1 = generateCheckerboardPattern(150, 150);
        image2 = tex02;
        multiplyAlphaChannel(image2, generateEllipseMask(150, 150, 2.0, 0.5, 0.4, 1.0));
        blend(image2, image1);
        image3 = tex01;
        crossDissolve(image1, image3, mask);
        out.fill(Qt::white);

        QPainter p2(&out);
        p2.drawImage(10, 10, tex01);
        p2.drawImage(150 + 10 * 2, 10, image1);
        p2.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask));
        p2.drawImage(150 * 3 + 10 * 4, 10, image3);

        out.save("cross_dissolve_2.png");

        return out;
    }

    QImage generateImage6() const
    {
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.66, 0.85);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.66, 0.85);
        QImage image1 = tex02;
        QImage image2 = tex01;
        multiplyAlphaChannel(image1, mask1);
        multiplyAlphaChannel(image2, mask2);
        QImage image3 = checkerboardPattern;
        QImage image4 = checkerboardPattern;
        blend(image1, image3);
        blend(image2, image4);
        QImage image5 = image1;
        QImage image6 = image1;
        blendWithBackdrop(image2, image5, QImage(), PixelComponent_Red | PixelComponent_Blue);
        blendWithBackdrop(image2, image6, QImage(), PixelComponent_Red);
        QImage image7 = checkerboardPattern;
        QImage image8 = checkerboardPattern;
        blend(image5, image7);
        blend(image6, image8);
        QImage out(150 * 4 + 10 * 5, 150 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);

        QPainter p(&out);
        p.drawImage(10, 10, image3);
        p.drawImage(150 + 10 * 2, 10, image4);
        p.drawImage(150 * 2 + 10 * 3, 10, image7);
        p.drawImage(150 * 3 + 10 * 4, 10, image8);

        out.save("exclude_components.png");
        return out;
    }

    QImage generateImage7() const
    {
        QImage mask1(150, 150, QImage::Format_Alpha8);
        mask1.fill(0);
        *(mask1.scanLine(75) + 75) = 255;
        QImage dt1 = DistanceTransform::chamfer5x5_Euclidean(mask1);
        QImage mask2(150, 150, QImage::Format_Alpha8);
        qint32 maxDistance = *reinterpret_cast<qint32*>(dt1.bits());
        visitPixels(mask2, dt1,
            [maxDistance](quint8 *pixel1, const quint8 *pixel2)
            {
                const qint32 d = *reinterpret_cast<const qint32*>(pixel2);
                *pixel1 = static_cast<quint8>(qMin(255, d * 255 / maxDistance));
            }
        );
        QImage mask3(150, 150, QImage::Format_Alpha8);
        visitPixels(mask3, dt1,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                const qint32 d = *reinterpret_cast<const qint32*>(pixel2);
                *pixel1 = d > (70 << 8) ? 0 : 255;
            }
        );
        QImage mask4(150, 150, QImage::Format_Alpha8);
        visitPixelsWithCoords(mask4,
            [](quint8 *pixel, qint32 x, qint32 y)
            {
                const qint32 dx = x - 75;
                const qint32 dy = y - 75;
                const qreal d = std::sqrt(dx * dx + dy * dy);
                *pixel = static_cast<quint8>(qRound(qMin(255.0, d * 255 / (75.0 * M_SQRT2))));
            }
        );
        QImage mask5(150, 150, QImage::Format_Alpha8);
        visitPixelsWithCoords(mask5,
            [](quint8 *pixel, qint32 x, qint32 y)
            {
                const qint32 dx = x - 75;
                const qint32 dy = y - 75;
                const qreal d = std::sqrt(dx * dx + dy * dy);
                *pixel = d > 70.0 ? 0 : 255;
            }
        );
        QImage mask6 = mask2;
        visitPixels(mask6, mask4,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = static_cast<quint8>(qMin(255, qAbs(static_cast<qint32>(*pixel1) - *pixel2) * 30));
            }
        );

        QImage out(150 * 3 + 10 * 4, 150 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(10 * 2 + 150, 10, convertMaskToImage(mask2));
        p.drawImage(10 * 3 + 150 * 2, 10, convertMaskToImage(mask3));
        p.drawImage(10, 10 * 2 + 150, convertMaskToImage(mask6));
        p.drawImage(10 * 2 + 150, 10 * 2 + 150, convertMaskToImage(mask4));
        p.drawImage(10 * 3 + 150 * 2, 10 * 2 + 150, convertMaskToImage(mask5));

        out.save("distance_transform_1.png");
        return out;
    }

    QImage generateImage8() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 0.6, 0.1, 0.9, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.1, 0.6, 0.9, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QImage dt1 = DistanceTransform::chamfer5x5_Euclidean(mask1);
        QImage mask3(150, 150, QImage::Format_Alpha8);
        qint32 maxDistance = *reinterpret_cast<qint32*>(dt1.bits());
        visitPixels(mask3, dt1,
            [maxDistance](quint8 *pixel1, const quint8 *pixel2)
            {
                const qint32 d = *reinterpret_cast<const qint32*>(pixel2);
                *pixel1 = static_cast<quint8>(qMin(255, d * 255 / maxDistance));
            }
        );
        QImage mask4(150, 150, QImage::Format_Alpha8);
        visitPixels(mask4, dt1,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                const qint32 d = *reinterpret_cast<const qint32*>(pixel2);
                *pixel1 = d > (20 << 8) ? 0 : 255;
            }
        );
        QImage mask5(150, 150, QImage::Format_Alpha8);
        visitPixels(mask5, dt1,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                const qint32 d = *reinterpret_cast<const qint32*>(pixel2);
                *pixel1 = d >= (21 << 8) ? 0 : d > (20 << 8) ? 255 - (d & 0xFF) : 255;
            }
        );
        
        QImage out(150 * 4 + 10 * 5, 150 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 + 10 * 2, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask4));
        p.drawImage(150 * 3 + 10 * 4, 10, convertMaskToImage(mask5));

        out.save("distance_transform_3.png");
        return out;
    }

    QImage generateImage9() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 0.6, 0.1, 0.9, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.1, 0.6, 0.9, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QImage mask3 = scaledDownMask(SizeAndSpreadTransform::apply(mask1, 30, 100));
        QImage mask4 = scaledDownMask(SizeAndSpreadTransform::apply(mask1, 50, 60));
        
        QImage out(150 * 3 + 10 * 4, 150 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 + 10 * 2, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask4));

        out.save("size_and_spread_1.png");
        return out;
    }

    QImage generateImage10() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 0.6, 0.1, 0.9, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.1, 0.6, 0.9, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QImage mask3 = scaledDownMask(SizeAndSpreadTransform::apply(mask1, 30, 100));
        QImage mask4 = scaledDownMask(SizeAndSpreadTransform::apply(mask1, 50, 60, SizeAndSpreadTransform::Method_PreciseSimple));
        
        QImage out(150 * 3 + 10 * 4, 150 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 + 10 * 2, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask4));

        out.save("size_and_spread_2.png");
        return out;
    }

    QImage generateImage11() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 0.6, 0.1, 0.9, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.1, 0.6, 0.9, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QImage mask3 = scaledDownMask(SizeAndSpreadTransform::apply(mask1, 30, 100));
        QImage mask4 = scaledDownMask(SizeAndSpreadTransform::apply(mask1, 50, 60, SizeAndSpreadTransform::Method_Precise));
        
        QImage out(150 * 3 + 10 * 4, 150 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 + 10 * 2, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask4));

        out.save("size_and_spread_3.png");
        return out;
    }

    QImage generateImage12() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 0.6, 0.1, 0.9, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.1, 0.6, 0.9, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QImage dt1 = DistanceTransform::chamfer5x5_Euclidean(mask1);
        QImage mask3(150, 150, QImage::Format_Alpha8);
        qint32 maxDistance = *reinterpret_cast<qint32*>(dt1.bits());
        visitPixels(mask3, dt1,
            [maxDistance](quint8 *pixel1, const quint8 *pixel2)
            {
                const qint32 d = *reinterpret_cast<const qint32*>(pixel2);
                *pixel1 = static_cast<quint8>(qMin(255, d * 255 / maxDistance));
            }
        );
        QImage mask4(150, 150, QImage::Format_Alpha8);
        visitPixels(dt1,
            [](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                *p = std::max(0, *p - (30 << 8));
            }
        );
        visitPixels(mask4, dt1,
            [maxDistance](quint8 *pixel1, const quint8 *pixel2)
            {
                const qint32 d = *reinterpret_cast<const qint32*>(pixel2);
                *pixel1 = static_cast<quint8>(qMin(255, d * 255 / maxDistance));
            }
        );
        QImage mask5(150, 150, QImage::Format_Alpha8);
        visitPixels(dt1,
            [](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                *p = std::max(0, ((20 + 1) << 8) - *p);
            }
        );
        visitPixels(mask5, dt1,
            [maxDistance](quint8 *pixel1, const quint8 *pixel2)
            {
                const qint32 d = *reinterpret_cast<const qint32*>(pixel2);
                *pixel1 = static_cast<quint8>(qMin(255, d * 255 / maxDistance));
            }
        );
        QImage dt2 = DistanceTransform::chamfer5x5_Euclidean(dt1,
                        [](const quint8 *pixel, quint32 infinity) -> quint32
                        {
                            const quint32 *p = reinterpret_cast<const quint32*>(pixel);
                            return *p < ((20 + 1) << 8)
                                   ? *p | DistanceTransform::stencilMask
                                   : infinity;
                        }
                      );
        QImage mask6(150, 150, QImage::Format_Alpha8);
        visitPixels(mask6, dt2,
            [maxDistance](quint8 *pixel1, const quint8 *pixel2)
            {
                const quint32 d = DistanceTransform::distance(*reinterpret_cast<const quint32*>(pixel2));
                *pixel1 = static_cast<quint8>(qMin(255U, d * 255 / maxDistance));
            }
        );
        QImage mask7(150, 150, QImage::Format_Alpha8);
        visitPixels(mask7, dt2,
            [maxDistance](quint8 *pixel1, const quint8 *pixel2)
            {
                const quint32 d = DistanceTransform::distance(*reinterpret_cast<const quint32*>(pixel2));
                *pixel1 = static_cast<quint8>(qMin(255U, d * 255 / ((20 + 1) * 2 << 8)));
            }
        );

        QImage out(150 * 3 + 10 * 4, 150 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 + 10 * 2, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask4));
        p.drawImage(10, 150 + 10 * 2, convertMaskToImage(mask5));
        p.drawImage(150 + 10 * 2, 150 + 10 * 2, convertMaskToImage(mask6));
        p.drawImage(150 * 2 + 10 * 3, 150 + 10 * 2, convertMaskToImage(mask7));

        out.save("size_and_spread_4.png");
        return out;
    }

    QImage generateImage13() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 0.9, 0.2, 0.5, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.2, 0.9, 0.5, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        const qint32 range = slider1->value();
        QImage graph1(152, 152, QImage::Format_ARGB32);
        {
            graph1.fill(Qt::white);
            QPainter p(&graph1);
            p.setRenderHint(QPainter::Antialiasing);
            p.setPen(Qt::gray);
            p.drawRect(QRectF(1.5, 1.5, 149.0, 149.0));
            QPolygonF poly;
            poly.append(QPointF(0.0, 150.0) + QPointF(1.0, 1.0));
            poly.append(QPointF(range * 150.0 / 100, 0.0) + QPointF(1.0, 1.0));
            poly.append(QPointF(150.0, 0.0) + QPointF(1.0, 1.0));
            p.setPen(QPen(QColor(0, 128, 0), 2));
            p.drawPolyline(poly);
        }
        QImage graph2(152, 152, QImage::Format_ARGB32);
        {
            graph2.fill(Qt::white);
            QPainter p(&graph2);
            p.setRenderHint(QPainter::Antialiasing);
            p.setPen(Qt::gray);
            p.drawRect(QRectF(1.5, 1.5, 149.0, 149.0));
            QPolygonF poly;
            poly.append(QPointF(0.0, 150.0) + QPointF(1.0, 1.0));
            poly.append(QPointF((100 - range) * 150.0 / 100, 150.0) + QPointF(1.0, 1.0));
            poly.append(QPointF(150.0, 0.0) + QPointF(1.0, 1.0));
            p.setPen(QPen(QColor(0, 128, 0), 2));
            p.drawPolyline(poly);
        }
        QImage mask3 = scaledDownMask(ContourTransform::applyRangeAndCurve(scaledUpMask(mask1), range, false, Curve(), false));
        QImage mask4 = scaledDownMask(ContourTransform::applyRangeAndCurve(scaledUpMask(mask1), range, true, Curve(), false));

        QImage out(150 * 5 + 10 * 6, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2, 10, graph1);
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 3 + 10 * 4, 10, graph2);
        p.drawImage(150 * 4 + 10 * 5, 10, convertMaskToImage(mask4));

        out.save("range.png");
        return out;
    }

    QImage generateImage14() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 0.9, 0.2, 0.5, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.2, 0.9, 0.5, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QImage mask3 = scaledDownMask(ContourTransform::apply(scaledUpMask(mask1), false, 100, false, Curve(), false, 25, rng, false));
        QImage mask4 = scaledDownMask(ContourTransform::apply(scaledUpMask(mask1), false, 100, false, Curve(), false, 50, rng, false));
        QImage mask5 = scaledDownMask(ContourTransform::apply(scaledUpMask(mask1), false, 100, false, Curve(), false, 100, rng, false));

        QImage out(150 * 4 + 10 * 5, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask4));
        p.drawImage(150 * 3 + 10 * 4, 10, convertMaskToImage(mask5));

        out.save("noise.png");
        return out;
    }

    QImage generateImage15() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 1.0, 0.0, 1.0);
        Curve curve1({{0.0, 0.0, false}, {0.25, 0.6, false}, {0.5, 0.25, false}, {0.75, 0.8, false}, {1.0, 1.0, false}});
        Curve curve2 = curve1;
        curve2.setKnotAsCorner(2, true);
        QImage graph1(152, 152, QImage::Format_ARGB32);
        {
            graph1.fill(Qt::white);
            QPainter p(&graph1);
            p.setRenderHint(QPainter::Antialiasing);
            p.setPen(Qt::gray);
            p.drawRect(QRectF(1.5, 1.5, 149.0, 149.0));
            QPolygonF poly;
            for (int i = 0; i < 150; ++i) {
                const qreal x = static_cast<qreal>(i) / 149.0;
                poly.append(QPointF(x * 150.0, 150.0 - curve1(x) * 150.0) + QPointF(1.0, 1.0));
            }
            p.setPen(QPen(QColor(0, 128, 0), 2));
            p.drawPolyline(poly);
            p.setPen(Qt::NoPen);
            p.setBrush(Qt::red);
            for (int i = 0; i < curve1.size(); ++i) {
                const qreal x = curve1.knot(i).x * 150.0 + 1.0;
                const qreal y = (1.0 - curve1.knot(i).y) * 150.0 + 1.0;
                p.drawEllipse(QPointF(x, y), 2, 2);
            }
        }
        QImage graph2(152, 152, QImage::Format_ARGB32);
        {
            graph2.fill(Qt::white);
            QPainter p(&graph2);
            p.setRenderHint(QPainter::Antialiasing);
            p.setPen(Qt::gray);
            p.drawRect(QRectF(1.5, 1.5, 149.0, 149.0));
            QPolygonF poly;
            for (int i = 0; i < 150; ++i) {
                const qreal x = static_cast<qreal>(i) / 149.0;
                poly.append(QPointF(x * 150.0, 150.0 - curve2(x) * 150.0) + QPointF(1.0, 1.0));
            }
            p.setPen(QPen(QColor(0, 128, 0), 2));
            p.drawPolyline(poly);
            p.setPen(Qt::NoPen);
            p.setBrush(Qt::red);
            for (int i = 0; i < curve2.size(); ++i) {
                const qreal x = curve2.knot(i).x * 150.0 + 1.0;
                const qreal y = (1.0 - curve2.knot(i).y) * 150.0 + 1.0;
                p.drawEllipse(QPointF(x, y), 2, 2);
            }
        }
        QImage mask3 = scaledDownMask(ContourTransform::applyCurve(scaledUpMask(mask1), curve1, false));
        QImage mask4 = scaledDownMask(ContourTransform::applyCurve(scaledUpMask(mask1), curve2, false));

        QImage out(150 * 5 + 10 * 6, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2 - 1, 10 - 1, graph1);
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 3 + 10 * 4 - 1, 10 - 1, graph2);
        p.drawImage(150 * 4 + 10 * 5, 10, convertMaskToImage(mask4));

        out.save("curve_1.png");
        return out;
    }

    QImage generateImage16() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 1.0, 0.0, 1.0);
        Curve curve1({{0.000, 0.00, false},
                      {0.333, 0.60, false},
                      {0.334, 0.10, false},
                      {0.350, 0.10, false},
                      {0.351, 0.60, false},
                      {0.372, 0.60, false},
                      {0.373, 0.10, false},
                      {0.666, 0.10, false},
                      {0.667, 0.60, false},
                      {1.000, 1.00, false}});
        QImage graph1(152, 152, QImage::Format_ARGB32);
        {
            graph1.fill(Qt::white);
            QPainter p(&graph1);
            p.setRenderHint(QPainter::Antialiasing);
            p.setPen(Qt::gray);
            p.drawRect(QRectF(1.5, 1.5, 149.0, 149.0));
            QPolygonF poly;
            for (int i = 0; i < 150; ++i) {
                const qreal x = static_cast<qreal>(i) / 149.0;
                poly.append(QPointF(x * 150.0, 150.0 - std::min(1.0, std::max(0.0, curve1(x))) * 150.0) + QPointF(1.0, 1.0));
            }
            p.setPen(QPen(QColor(0, 128, 0), 2));
            p.drawPolyline(poly);
            p.setPen(Qt::NoPen);
            p.setBrush(Qt::red);
            for (int i = 0; i < curve1.size(); ++i) {
                const qreal x = curve1.knot(i).x * 150.0 + 1.0;
                const qreal y = (1.0 - curve1.knot(i).y) * 150.0 + 1.0;
                p.drawEllipse(QPointF(x, y), 2, 2);
            }
        }
        QImage mask3 = scaledDownMask(ContourTransform::applyCurve(scaledUpMask(mask1), curve1, false));
        QImage mask4 = scaledDownMask(ContourTransform::applyCurve(scaledUpMask(mask1), curve1, true));

        QImage out(150 * 4 + 10 * 5, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2 - 1, 10 - 1, graph1);
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 3 + 10 * 4, 10, convertMaskToImage(mask4));

        out.save("curve_2.png");
        return out;
    }

    QImage generateImage17() const
    {
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.0, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.0, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QGradientStops gradientStops {
            {0.000, Qt::red},
            {0.167, Qt::magenta},
            {0.333, Qt::blue},
            {0.500, QColor(0, 255, 255, 0)},
            {0.666, Qt::green},
            {0.834, Qt::yellow},
            {1.000, Qt::red}
        };
        QImage image1 = checkerboardPattern;
        {
            QPainter p(&image1);
            QLinearGradient g(0, 0, 150, 0);
            g.setStops(gradientStops);
            p.fillRect(0, 0, 150, 150, g);
        }
        QImage image2 = checkerboardPattern;
        QImage mappedImage = ContourTransform::applyAndFillWithGradient(scaledUpMask(mask1), false, 100, false, Curve(), false, 0, 0, rng, false, gradientStops);
        blend(mappedImage, image2);

        QImage out(150 * 3 + 10 * 4, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2, 10, image1);
        p.drawImage(150 * 2 + 10 * 3, 10, image2);

        out.save("gradient_map_1.png");
        return out;
    }

    QImage generateImage18() const
    {
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.0, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.0, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QGradientStops gradientStops {
            {0.000, Qt::red},
            {0.167, Qt::magenta},
            {0.333, Qt::blue},
            {0.500, QColor(0, 255, 255, 0)},
            {0.666, Qt::green},
            {0.834, Qt::yellow},
            {1.000, Qt::red}
        };
        QImage image1 = checkerboardPattern;
        {
            QPainter p(&image1);
            QLinearGradient g(0, 0, 150, 0);
            g.setStops(gradientStops);
            p.fillRect(0, 0, 150, 150, g);
        }
        QImage image2 = checkerboardPattern;
        QImage mappedImage = ContourTransform::applyAndFillWithGradient(scaledUpMask(mask1), false, 100, false, Curve(), false, 10, 0, rng, false, gradientStops);
        blend(mappedImage, image2);

        QImage out(150 * 3 + 10 * 4, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2, 10, image1);
        p.drawImage(150 * 2 + 10 * 3, 10, image2);

        out.save("gradient_map_2.png");
        return out;
    }

    QImage generateImage19() const
    {
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.0, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.0, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QImage mask3 = mask1;
        visitPixels(mask3,
            [](quint8 *pixel)
            {
                *pixel = std::min(255, *pixel * 8);
            }
        );

        QImage out(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2, 10, convertMaskToImage(mask3));

        out.save("fade_1.png");
        return out;
    }

    QImage generateImage20() const
    {
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.0, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.0, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QImage mask3 = mask1;
        visitPixels(mask3,
            [](quint8 *pixel)
            {
                *pixel = std::min(255, *pixel * 8);
            }
        );
        QGradientStops gradientStops {
            {0.000, Qt::red},
            {0.167, Qt::magenta},
            {0.333, Qt::blue},
            {0.500, QColor(0, 255, 255, 0)},
            {0.666, Qt::green},
            {0.834, Qt::yellow},
            {1.000, Qt::red}
        };
        QImage image1 = checkerboardPattern;
        {
            QPainter p(&image1);
            QLinearGradient g(0, 0, 150, 0);
            g.setStops(gradientStops);
            p.fillRect(0, 0, 150, 150, g);
        }
        QImage image2 = checkerboardPattern;
        QImage image3 = checkerboardPattern;
        QImage mappedImage1 = ContourTransform::applyAndFillWithGradient(scaledUpMask(mask1), false, 100, false, Curve(), false, 0, 0, rng, false, gradientStops);
        QImage mappedImage2 = ContourTransform::applyAndFillWithGradient(scaledUpMask(mask1), false, 100, false, Curve(), false, 0, 0, rng, true, gradientStops);
        blend(mappedImage1, image2);
        blend(mappedImage2, image3);

        QImage out(150 * 3 + 10 * 4, 150 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2, 10, image1);
        p.drawImage(150 * 2 + 10 * 3, 10, image2);
        p.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, convertMaskToImage(mask3));
        p.drawImage(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, image3);

        out.save("fade_2.png");
        return out;
    }

    QImage generateImage21() const
    {
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.0, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.0, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QImage mask3 = mask1;
        visitPixels(mask3,
            [](quint8 *pixel)
            {
                *pixel = std::min(255, *pixel * 8);
            }
        );
        Curve curve1({{0.0, 0.5, false}, {0.25, 0.6, false}, {0.5, 0.25, false}, {0.75, 0.8, false}, {1.0, 1.0, false}});
        QImage graph1(152, 152, QImage::Format_ARGB32);
        {
            graph1.fill(Qt::white);
            QPainter p(&graph1);
            p.setRenderHint(QPainter::Antialiasing);
            p.setPen(Qt::gray);
            p.drawRect(QRectF(1.5, 1.5, 149.0, 149.0));
            QPolygonF poly;
            for (int i = 0; i < 150; ++i) {
                const qreal x = static_cast<qreal>(i) / 149.0;
                poly.append(QPointF(x * 150.0, 150.0 - std::min(1.0, std::max(0.0, curve1(x))) * 150.0) + QPointF(1.0, 1.0));
            }
            p.setPen(QPen(QColor(0, 128, 0), 2));
            p.drawPolyline(poly);
            p.setPen(Qt::NoPen);
            p.setBrush(Qt::red);
            for (int i = 0; i < curve1.size(); ++i) {
                const qreal x = curve1.knot(i).x * 150.0 + 1.0;
                const qreal y = (1.0 - curve1.knot(i).y) * 150.0 + 1.0;
                p.drawEllipse(QPointF(x, y), 2, 2);
            }
        }
        QImage mask4 = scaledDownMask(ContourTransform::applyCurve(scaledUpMask(mask1), curve1, false));
        QImage mask5 = scaledDownMask(ContourTransform::apply(scaledUpMask(mask1), false, 100, false, curve1, false, 0, rng, true));

        QImage out(150 * 3 + 10 * 4, 150 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2 - 1, 10 - 1, graph1);
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask4));
        p.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, convertMaskToImage(mask3));
        p.drawImage(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, convertMaskToImage(mask5));

        out.save("fade_3.png");
        return out;
    }

    QImage generateImage22() const
    {
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.0, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.0, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        Curve curve1({{0.0, 0.0, false}, {0.25, 0.6, false}, {0.5, 0.25, false}, {0.75, 0.8, false}, {1.0, 1.0, false}});
        QImage mask3 = scaledDownMask(ContourTransform::apply(scaledUpMask(mask1), false, 50, false, curve1, false, 10, rng, true));
        QImage mask4 = scaledDownMask(ContourTransform::apply(scaledUpMask(mask1), true, 50, true, curve1, false, 10, rng, true));
        QImage mask5 = scaledDownMask(ContourTransform::apply(scaledUpMask(mask1), false, 30, false, curve1, true, 50, rng, true));
        QImage mask6 = scaledDownMask(ContourTransform::apply(scaledUpMask(mask1), true, 20, false, curve1, false, 0, rng, true));

        QImage out(150 * 5 + 10 * 6, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2, 10, convertMaskToImage(mask3));
        p.drawImage(150 * 2 + 10 * 3, 10, convertMaskToImage(mask4));
        p.drawImage(150 * 3 + 10 * 4, 10, convertMaskToImage(mask5));
        p.drawImage(150 * 4 + 10 * 5, 10, convertMaskToImage(mask6));

        out.save("contour_transform_1.png");
        return out;
    }

    QImage generateImage23() const
    {
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.0, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.0, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        Curve curve1({{0.0, 0.0, false}, {0.25, 0.6, false}, {0.5, 0.25, false}, {0.75, 0.8, false}, {1.0, 1.0, false}});
        QImage image = ContourTransform::applyAndFillWithColor(scaledUpMask(mask1), false, 50, false, curve1, false, 10, rng, true, QColor(255, 128, 128));
        QImage image2 = checkerboardPattern;
        blend(image, image2);

        QImage out(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2, 10, image2);

        out.save("contour_transform_2.png");
        return out;
    }

    QImage generateImage24() const
    {
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.0, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.0, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );
        QGradientStops gradientStops {
            {0.000, Qt::red},
            {0.167, Qt::magenta},
            {0.333, Qt::blue},
            {0.500, QColor(0, 255, 255, 0)},
            {0.666, Qt::green},
            {0.834, Qt::yellow},
            {1.000, Qt::red}
        };
        Curve curve1({{0.0, 0.0, false}, {0.25, 0.6, false}, {0.5, 0.25, false}, {0.75, 0.8, false}, {1.0, 1.0, false}});
        QImage image = ContourTransform::applyAndFillWithGradient(scaledUpMask(mask1), false, 50, false, curve1, false, 3, 10, rng, true, gradientStops);
        QImage image2 = checkerboardPattern;
        blend(image, image2);

        QImage out(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(10, 10, convertMaskToImage(mask1));
        p.drawImage(150 * 1 + 10 * 2, 10, image2);

        out.save("contour_transform_3.png");
        return out;
    }

    QImage generateImage25() const
    {
        QImage background(150, 150, QImage::Format_ARGB32);
        background.fill(Qt::red);
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage image1(150, 150, QImage::Format_ARGB32);
        image1.fill(Qt::cyan);
        QImage image2(150, 150, QImage::Format_ARGB32);
        image2.fill(Qt::green);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.9, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.9, 1.0);
        multiplyAlphaChannel(image1, mask1);
        multiplyAlphaChannel(image2, mask2);
        QImage backdrop = background;
        blend(image1, backdrop);
        QImage image3 = backdrop;
        visitPixels(image3, image2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                ARGB *p1 = reinterpret_cast<ARGB*>(pixel1);
                const ARGB *p2 = reinterpret_cast<const ARGB*>(pixel2);
                p1->a = std::max(0, p1->a - p2->a);
            }
        );
        QImage image4 = background;
        blend(image3, image4);
        QImage image5 = image4;
        blend(image2, image5);
        QImage image6 = image4;
        blend(image2, image6, SourceOverDestination, Normal, false, 64);

        QImage out(150 * 4 + 10 * 5, 150 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p2(&out);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 0 + 10 * 1, background);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 0 + 10 * 1, image1);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 0 + 10 * 1, image2);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 0 + 10 * 1, backdrop);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, image3);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 1 + 10 * 2, image4);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, image5);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 1 + 10 * 2, image6);

        out.save("knockout_1.png");

        return out;
    }

    QImage generateImage26() const
    {
        QImage background(150, 150, QImage::Format_ARGB32);
        background.fill(0);
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage image1(150, 150, QImage::Format_ARGB32);
        image1.fill(Qt::cyan);
        QImage image2(150, 150, QImage::Format_ARGB32);
        image2.fill(Qt::green);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.9, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.9, 1.0);
        multiplyAlphaChannel(image1, mask1);
        multiplyAlphaChannel(image2, mask2);
        QImage backdrop = background;
        blend(image1, backdrop);
        QImage image3 = backdrop;
        visitPixels(image3, image2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                ARGB *p1 = reinterpret_cast<ARGB*>(pixel1);
                const ARGB *p2 = reinterpret_cast<const ARGB*>(pixel2);
                p1->a = std::max(0, p1->a - p2->a);
            }
        );
        QImage image4 = background;
        blend(image3, image4);
        QImage image5 = image4;
        blend(image2, image5);
        QImage image6 = image4;
        blend(image2, image6, SourceOverDestination, Normal, false, 64);

        QImage out(150 * 4 + 10 * 5, 150 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p2(&out);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 0 + 10 * 1, background);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 0 + 10 * 1, image1);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 0 + 10 * 1, image2);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 0 + 10 * 1, backdrop);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, image3);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 1 + 10 * 2, image4);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, image5);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 1 + 10 * 2, image6);

        out.save("knockout_2.png");

        return out;
    }

    QImage generateImage27() const
    {
        QImage background(150, 150, QImage::Format_ARGB32);
        background.fill(Qt::red);
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage image1(150, 150, QImage::Format_ARGB32);
        image1.fill(Qt::cyan);
        QImage image2(150, 150, QImage::Format_ARGB32);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.9, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.9, 1.0);
        multiplyAlphaChannel(image1, mask1);
        visitPixels(image2, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                ARGB *p1 = reinterpret_cast<ARGB*>(pixel1);
                if (*pixel2 > 0) {
                    *p1 = {0, 255, 0, *pixel2};
                } else {
                    p1->a = 0;
                }
            }
        );
        QImage backdrop = background;
        blend(image1, backdrop);
        QImage knockoutComposition = background;
        blend(image2, knockoutComposition, SourceOverDestination, Normal, false, 255, true);
        QImage composition = knockoutComposition;
        crossDissolve(backdrop, composition, mask2, true);
        QImage knockoutComposition2 = background;
        blend(image2, knockoutComposition2, SourceOverDestination, Normal, false, 64, true);
        QImage composition2 = knockoutComposition2;
        crossDissolve(backdrop, composition2, mask2, true);

        QImage out(150 * 4 + 10 * 5, 150 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p2(&out);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 0 + 10 * 1, background);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 0 + 10 * 1, image1);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 0 + 10 * 1, image2);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 0 + 10 * 1, backdrop);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, knockoutComposition);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 1 + 10 * 2, composition);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, knockoutComposition2);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 1 + 10 * 2, composition2);

        out.save("knockout_3.png");

        return out;
    }

    QImage generateImage28() const
    {
        QImage background(150, 150, QImage::Format_ARGB32);
        background.fill(0);
        QImage checkerboardPattern = generateCheckerboardPattern(150, 150);
        QImage image1(150, 150, QImage::Format_ARGB32);
        image1.fill(Qt::cyan);
        QImage image2(150, 150, QImage::Format_ARGB32);
        QImage mask1 = generateEllipseMask(150, 150, 1.0, 0.5, 0.9, 1.0);
        QImage mask2 = generateEllipseMask(150, 150, 0.5, 1.0, 0.9, 1.0);
        multiplyAlphaChannel(image1, mask1);
        visitPixels(image2, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                ARGB *p1 = reinterpret_cast<ARGB*>(pixel1);
                if (*pixel2 > 0) {
                    *p1 = {0, 255, 0, *pixel2};
                } else {
                    p1->a = 0;
                }
            }
        );
        QImage backdrop = background;
        blend(image1, backdrop);
        QImage knockoutComposition = background;
        blend(image2, knockoutComposition, SourceOverDestination, Normal, false, 255, true);
        QImage composition = knockoutComposition;
        crossDissolve(backdrop, composition, mask2, true);
        QImage knockoutComposition2 = background;
        blend(image2, knockoutComposition2, SourceOverDestination, Normal, false, 64, true);
        QImage composition2 = knockoutComposition2;
        crossDissolve(backdrop, composition2, mask2, true);

        QImage out(150 * 4 + 10 * 5, 150 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p2(&out);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 0 + 10 * 1, background);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 0 + 10 * 1, image1);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 0 + 10 * 1, image2);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 0 + 10 * 1, checkerboardPattern);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 0 + 10 * 1, backdrop);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 0 + 10 * 1, 150 * 1 + 10 * 2, knockoutComposition);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 1 + 10 * 2, 150 * 1 + 10 * 2, composition);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 2 + 10 * 3, 150 * 1 + 10 * 2, knockoutComposition2);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 1 + 10 * 2, checkerboardPattern);
        p2.drawImage(150 * 3 + 10 * 4, 150 * 1 + 10 * 2, composition2);

        out.save("knockout_4.png");

        return out;
    }

    QImage generateImage29() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        DropShadow effectOptions;
        effectOptions.opacity = 80;
        effectOptions.distance = 20;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setDropShadowOptions(effectOptions);
        layer.setDropShadowActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);
    
        QImage img2 = checkerboardPattern;
        blend(layer.dropShadowImage(), img2);

        QImage img3 = blend(layer, background, backdrop);

        blendingOptions.fillOpacity = 10;
        layer.setBlendingOptions(blendingOptions);
        QImage img4 = blend(layer, background, backdrop);

        blendingOptions.layerKnocksOutDropShadow = false;
        layer.setBlendingOptions(blendingOptions);
        QImage img5 = blend(layer, background, backdrop);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img1);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img5);
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, img4);

        out.save("drop_shadow.png");

        return out;
    }

    QImage generateImage30() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        OuterGlow effectOptions;
        effectOptions.opacity = 80;
        effectOptions.size = 30;
        effectOptions.gradient =
            {
                {0.000, Qt::red},
                {0.167, Qt::yellow},
                {0.333, Qt::green},
                {0.500, Qt::cyan},
                {0.666, Qt::blue},
                {0.834, Qt::magenta},
                {1.000, Qt::red}
            };
        effectOptions.useGradient = true;
        effectOptions.blendMode = Normal;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setOuterGlowOptions(effectOptions);
        layer.setOuterGlowActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);
    
        QImage img2 = checkerboardPattern;
        blend(layer.outerGlowImage(), img2);

        QImage img3 = blend(layer, background, backdrop);

        blendingOptions.fillOpacity = 50;
        layer.setBlendingOptions(blendingOptions);
        QImage img4 = blend(layer, background, backdrop);

        effectOptions.technique = GlowTechnique_Precise;
        layer.setOuterGlowOptions(effectOptions);
        QImage img5 = blend(layer, background, backdrop);

        effectOptions.useGradient = false;
        layer.setOuterGlowOptions(effectOptions);
        QImage img6 = blend(layer, background, backdrop);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img1);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img4);
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, img5);
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, img6);

        out.save("outer_glow.png");

        return out;
    }

    QImage generateImage31() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        PatternOverlay effectOptions;
        effectOptions.blendMode = Normal;
        effectOptions.pattern = pattern;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setPatternOverlayOptions(effectOptions);
        layer.setPatternOverlayActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);
    
        QImage img2 = checkerboardPattern;
        blend(layer.patternOverlayImage(), img2);

        QImage img3 = blend(layer, background, backdrop);

        blendingOptions.fillOpacity = 30;
        layer.setBlendingOptions(blendingOptions);
        QImage img4 = blend(layer, background, backdrop);

        effectOptions.scale = 50;
        layer.setPatternOverlayOptions(effectOptions);
        QImage img5 = blend(layer, background, backdrop);

        blendingOptions.blendInteriorEffectsAsGroup = true;
        layer.setBlendingOptions(blendingOptions);
        QImage img6 = blend(layer, background, backdrop);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img1);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img4);
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, img5);
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, img6);

        out.save("pattern_overlay.png");

        return out;
    }

    QImage generateImage32() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        GradientOverlay effectOptions;
        effectOptions.gradient =
            {
                {0.000, Qt::red},
                {0.167, Qt::yellow},
                {0.333, Qt::green},
                {0.500, QColor(0, 255, 255, 0)},
                {0.666, Qt::blue},
                {0.834, Qt::magenta},
                {1.000, Qt::red}
            };
        effectOptions.blendMode = Normal;
        effectOptions.alignWithLayer = true;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setGradientOverlayOptions(effectOptions);
        layer.setGradientOverlayActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);
    
        QImage img2 = checkerboardPattern;
        blend(layer.gradientOverlayImage(), img2);

        QImage img3 = blend(layer, background, backdrop);

        blendingOptions.fillOpacity = 30;
        layer.setBlendingOptions(blendingOptions);
        QImage img4 = blend(layer, background, backdrop);

        effectOptions.scale = 50;
        layer.setGradientOverlayOptions(effectOptions);
        QImage img5 = blend(layer, background, backdrop);

        blendingOptions.blendInteriorEffectsAsGroup = true;
        layer.setBlendingOptions(blendingOptions);
        QImage img6 = blend(layer, background, backdrop);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img1);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img4);
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, img5);
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, img6);

        out.save("gradient_overlay.png");

        return out;
    }

    QImage generateImage33() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        ColorOverlay effectOptions;
        effectOptions.color = QColor(255, 192, 0);
        effectOptions.blendMode = Normal;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setColorOverlayOptions(effectOptions);
        layer.setColorOverlayActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);
    
        QImage img2 = checkerboardPattern;
        blend(layer.colorOverlayImage(), img2);

        QImage img3 = blend(layer, background, backdrop);

        blendingOptions.blendInteriorEffectsAsGroup = true;
        blendingOptions.fillOpacity = 30;
        layer.setBlendingOptions(blendingOptions);
        QImage img4 = blend(layer, background, backdrop);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img1);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img4);

        out.save("color_overlay.png");

        return out;
    }

    QImage generateImage34() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        Satin effectOptions;
        effectOptions.blendMode = Normal;
        effectOptions.opacity = 80;
        effectOptions.distance = 50;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setSatinOptions(effectOptions);
        layer.setSatinActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);
    
        QImage img2 = checkerboardPattern;
        blend(layer.satinImage(), img2);

        QImage img3 = blend(layer, background, backdrop);

        blendingOptions.fillOpacity = 30;
        layer.setBlendingOptions(blendingOptions);
        QImage img4 = blend(layer, background, backdrop);

        blendingOptions.blendInteriorEffectsAsGroup = true;
        layer.setBlendingOptions(blendingOptions);
        QImage img5 = blend(layer, background, backdrop);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img1);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img4);
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, img5);

        out.save("satin.png");

        return out;
    }

    QImage generateImage35() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        InnerGlow effectOptions;
        effectOptions.opacity = 80;
        effectOptions.size = 30;
        effectOptions.gradient =
            {
                {0.000, Qt::red},
                {0.167, Qt::yellow},
                {0.333, Qt::green},
                {0.500, Qt::cyan},
                {0.666, Qt::blue},
                {0.834, Qt::magenta},
                {1.000, Qt::red}
            };
        effectOptions.useGradient = true;
        effectOptions.blendMode = Normal;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setInnerGlowOptions(effectOptions);
        layer.setInnerGlowActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);
    
        QImage img2 = checkerboardPattern;
        blend(layer.innerGlowImage(), img2);

        QImage img3 = blend(layer, background, backdrop);

        blendingOptions.fillOpacity = 30;
        layer.setBlendingOptions(blendingOptions);
        QImage img4 = blend(layer, background, backdrop);

        effectOptions.technique = GlowTechnique_Precise;
        layer.setInnerGlowOptions(effectOptions);
        QImage img5 = blend(layer, background, backdrop);

        effectOptions.useGradient = false;
        layer.setInnerGlowOptions(effectOptions);
        QImage img6 = blend(layer, background, backdrop);

        effectOptions.source = GlowSource_Center;
        layer.setInnerGlowOptions(effectOptions);
        QImage img7 = blend(layer, background, backdrop);

        blendingOptions.blendInteriorEffectsAsGroup = true;
        layer.setBlendingOptions(blendingOptions);
        QImage img8 = blend(layer, background, backdrop);

        QImage out(250 * 3 + 10 * 4, 250 * 3 + 10 * 4, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img1);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img4);
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, img5);
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, img6);
        p.drawImage(250 * 0 + 10 * 1, 250 * 2 + 10 * 3, img7);
        p.drawImage(250 * 1 + 10 * 2, 250 * 2 + 10 * 3, img8);

        out.save("inner_glow.png");

        return out;
    }

    QImage generateImage36() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        InnerShadow effectOptions;
        effectOptions.opacity = 80;
        effectOptions.distance = 20;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setInnerShadowOptions(effectOptions);
        layer.setInnerShadowActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);
    
        QImage img2 = checkerboardPattern;
        blend(layer.innerShadowImage(), img2);

        QImage img3 = blend(layer, background, backdrop);

        blendingOptions.fillOpacity = 10;
        layer.setBlendingOptions(blendingOptions);
        QImage img4 = blend(layer, background, backdrop);


        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img1);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img4);

        out.save("inner_shadow.png");

        return out;
    }

    QImage generateImage37() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        Stroke effectOptions;
        effectOptions.opacity = 50;
        effectOptions.size = 30;
        effectOptions.position = StrokePosition_Outside;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setStrokeOptions(effectOptions);
        layer.setStrokeActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);
    
        QImage img2 = checkerboardPattern;
        blend(layer.strokeImage(), img2);

        QImage img3 = blend(layer, background, backdrop);

        effectOptions.position = StrokePosition_Inside;
        layer.setStrokeOptions(effectOptions);
        QImage img4 = checkerboardPattern;
        blend(layer.strokeImage(), img4);

        QImage img5 = blend(layer, background, backdrop);

        effectOptions.overprint = true;
        layer.setStrokeOptions(effectOptions);
        QImage img6 = blend(layer, background, backdrop);

        effectOptions.position = StrokePosition_Center;
        effectOptions.overprint = false;
        layer.setStrokeOptions(effectOptions);
        QImage img7 = checkerboardPattern;
        blend(layer.strokeImage(), img7);

        QImage img8 = blend(layer, background, backdrop);

        effectOptions.overprint = true;
        layer.setStrokeOptions(effectOptions);
        QImage img9 = blend(layer, background, backdrop);

        QImage out(250 * 3 + 10 * 4, 250 * 3 + 10 * 4, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img1);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img4);
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, img5);
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, img6);
        p.drawImage(250 * 0 + 10 * 1, 250 * 2 + 10 * 3, img7);
        p.drawImage(250 * 1 + 10 * 2, 250 * 2 + 10 * 3, img8);
        p.drawImage(250 * 2 + 10 * 3, 250 * 2 + 10 * 3, img9);

        out.save("stroke.png");

        return out;
    }

    QImage generateImage38() const
    {
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage smoothHeightMap = generateHeightMap(mask1, Technique_Smooth, 20);
        QImage chiselHardHeightMap = generateHeightMap(mask1, Technique_ChiselHard, 20);
        QImage chiselSoftHeightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);

        QImage out(250 * 2 + 10 * 3, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(mask1));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(smoothHeightMap)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(chiselHardHeightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(chiselSoftHeightMap)));

        out.save("bevel_emboss_1.png");

        return out;
    }

    QImage generateImage39() const
    {
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage heightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);
        Curve curve({{0.0, 0.0}, {0.25, 0.95}, {0.75, 0.05}, {1.0, 1.0}});
        QImage mappedHeightMap1 = applyProfile(heightMap, 100, false, curve, false);
        QImage mappedHeightMap2 = applyProfile(heightMap, 50, false, Curve(), false);
        QImage mappedHeightMap3 = applyProfile(heightMap, 50, false, curve, false);
        curve.setKnotX(0, 0.01);
        QImage mappedHeightMap4 = applyProfile(heightMap, 50, true, curve, false);
        curve.setKnotX(0, 0.0);
        QImage mappedHeightMap5 = applyProfile(heightMap, 50, true, curve, false);
        qint32 min = 0xFF00, max = 0;
        visitPixels(mappedHeightMap5,
            [&min, &max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                if (*p > max) {
                    max = *p;
                }
                if (*p < min) {
                    min = *p;
                }
            }
        );
        visitPixels(mappedHeightMap5,
            [min, max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                const quint64 v1 = (*p - min);
                const quint64 v2 = (max - min);
                *p = v1 * 0xFF00UL / v2;
            }
        );
        

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(heightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(mappedHeightMap1)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(mappedHeightMap2)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedHeightMap3)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedHeightMap4)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedHeightMap5)));

        out.save("bevel_emboss_2.png");

        return out;
    }

    QImage generateImage40() const
    {
        QImage texture = generateCheckerboardPattern(128, 128);
        blend(pattern, texture);
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage heightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);
        QImage texturedHeightMap1 = heightMap;
        QImage texturedHeightMap2 = heightMap;
        QImage textureHeightMap = generateTextureHeightMap(mask1, pattern, 50, {0, 0});
        applyTextureHeightMap(texturedHeightMap1, textureHeightMap, 20, false);
        applyTextureHeightMap(texturedHeightMap2, textureHeightMap, -10, false);
        
        qint32 min = 0xFF00, max = 0;

        visitPixels(texturedHeightMap1,
            [&min, &max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                if (*p > max) {
                    max = *p;
                }
                if (*p < min) {
                    min = *p;
                }
            }
        );
        visitPixels(texturedHeightMap1,
            [min, max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                const quint64 v1 = (*p - min);
                const quint64 v2 = (max - min);
                *p = v1 * 0xFF00UL / v2;
            }
        );

        min = 0xFF00;
        max = 0;

        visitPixels(texturedHeightMap2,
            [&min, &max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                if (*p > max) {
                    max = *p;
                }
                if (*p < min) {
                    min = *p;
                }
            }
        );
        visitPixels(texturedHeightMap2,
            [min, max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                const quint64 v1 = (*p - min);
                const quint64 v2 = (max - min);
                *p = v1 * 0xFF00UL / v2;
            }
        );
        

        QImage out(250 * 2 + 10 * 3, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(heightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, texture);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(texturedHeightMap1)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(texturedHeightMap2)));

        out.save("bevel_emboss_3.png");

        return out;
    }

    QImage generateImage41() const
    {
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage smoothHeightMap = generateHeightMap(mask1, Technique_Smooth, 20);
        QImage chiselHardHeightMap = generateHeightMap(mask1, Technique_ChiselHard, 20);
        QImage chiselSoftHeightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);
        QImage smoothLightMap = generateLightMap(smoothHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselHardLightMap = generateLightMap(chiselHardHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselSoftLightMap = generateLightMap(chiselSoftHeightMap, 100, Direction_Up, 20, 135, 60);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(smoothHeightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselHardHeightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselSoftHeightMap)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(smoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(chiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(chiselSoftLightMap)));

        out.save("bevel_emboss_4.png");

        return out;
    }

    QImage generateImage42() const
    {
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage smoothHeightMap = generateHeightMap(mask1, Technique_Smooth, 20);
        QImage chiselHardHeightMap = generateHeightMap(mask1, Technique_ChiselHard, 20);
        QImage chiselSoftHeightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);
        QImage smoothLightMap = generateLightMap(smoothHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselHardLightMap = generateLightMap(chiselHardHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselSoftLightMap = generateLightMap(chiselSoftHeightMap, 100, Direction_Up, 20, 135, 60);

        Curve curve({{0.0, 0.0}, {0.25, 0.95}, {0.75, 0.05}, {1.0, 1.0}});
        QImage mappedSmoothLightMap = ContourTransform::applyCurve(smoothLightMap, curve, false);
        QImage mappedChiselHardLightMap = ContourTransform::applyCurve(chiselHardLightMap, curve, false);
        QImage mappedChiselSoftLightMap = ContourTransform::applyCurve(chiselSoftLightMap, curve, false);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(smoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselSoftLightMap)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedSmoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedChiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedChiselSoftLightMap)));

        out.save("bevel_emboss_5.png");

        return out;
    }

    QImage generateImage43() const
    {
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage smoothHeightMap = generateHeightMap(mask1, Technique_Smooth, 20);
        QImage chiselHardHeightMap = generateHeightMap(mask1, Technique_ChiselHard, 20);
        QImage chiselSoftHeightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);
        
        QImage smoothLightMap = generateLightMap(smoothHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselHardLightMap = generateLightMap(chiselHardHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselSoftLightMap = generateLightMap(chiselSoftHeightMap, 100, Direction_Up, 20, 135, 60);

        Curve curve({{0.0, 0.0}, {0.25, 0.95}, {0.75, 0.05}, {1.0, 1.0}});
        QImage mappedSmoothLightMap = ContourTransform::applyCurve(smoothLightMap, curve, false);
        QImage mappedChiselHardLightMap = ContourTransform::applyCurve(chiselHardLightMap, curve, false);
        QImage mappedChiselSoftLightMap = ContourTransform::applyCurve(chiselSoftLightMap, curve, false);

        smoothLightMap = Filtering::gaussianBlurMask(smoothLightMap, 7);
        chiselHardLightMap = Filtering::gaussianBlurMask(chiselHardLightMap, 7);
        chiselSoftLightMap = Filtering::gaussianBlurMask(chiselSoftLightMap, 7);
        mappedSmoothLightMap = Filtering::gaussianBlurMask(mappedSmoothLightMap, 7);
        mappedChiselHardLightMap = Filtering::gaussianBlurMask(mappedChiselHardLightMap, 7);
        mappedChiselSoftLightMap = Filtering::gaussianBlurMask(mappedChiselSoftLightMap, 7);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(smoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselSoftLightMap)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedSmoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedChiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedChiselSoftLightMap)));

        out.save("bevel_emboss_6.png");

        return out;
    }

    QImage generateImage44() const
    {
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage smoothHeightMap = generateHeightMap(mask1, Technique_Smooth, 20);
        QImage chiselHardHeightMap = generateHeightMap(mask1, Technique_ChiselHard, 20);
        QImage chiselSoftHeightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);
        
        QImage smoothLightMap = generateLightMap(smoothHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselHardLightMap = generateLightMap(chiselHardHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselSoftLightMap = generateLightMap(chiselSoftHeightMap, 100, Direction_Up, 20, 135, 60);

        Curve curve({{0.0, 0.0}, {0.25, 0.95}, {0.75, 0.05}, {1.0, 1.0}});
        QImage mappedSmoothLightMap = ContourTransform::applyCurve(smoothLightMap, curve, false);
        QImage mappedChiselHardLightMap = ContourTransform::applyCurve(chiselHardLightMap, curve, false);
        QImage mappedChiselSoftLightMap = ContourTransform::applyCurve(chiselSoftLightMap, curve, false);

        applyMidGrayRemapping(smoothLightMap, 60);
        applyMidGrayRemapping(chiselHardLightMap, 60);
        applyMidGrayRemapping(chiselSoftLightMap, 60);
        applyMidGrayRemapping(mappedSmoothLightMap, 60);
        applyMidGrayRemapping(mappedChiselHardLightMap, 60);
        applyMidGrayRemapping(mappedChiselSoftLightMap, 60);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(smoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselSoftLightMap)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedSmoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedChiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedChiselSoftLightMap)));

        out.save("bevel_emboss_7.png");

        return out;
    }

    QImage generateImage45() const
    {
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage smoothHeightMap = generateHeightMap(mask1, Technique_Smooth, 20);
        QImage chiselHardHeightMap = generateHeightMap(mask1, Technique_ChiselHard, 20);
        QImage chiselSoftHeightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);
        
        
        QImage smoothLightMap = generateLightMap(smoothHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselHardLightMap = generateLightMap(chiselHardHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselSoftLightMap = generateLightMap(chiselSoftHeightMap, 100, Direction_Up, 20, 135, 60);

        Curve curve({{0.0, 0.0}, {0.25, 0.95}, {0.75, 0.05}, {1.0, 1.0}});
        QImage mappedSmoothLightMap = ContourTransform::applyCurve(smoothLightMap, curve, false);
        QImage mappedChiselHardLightMap = ContourTransform::applyCurve(chiselHardLightMap, curve, false);
        QImage mappedChiselSoftLightMap = ContourTransform::applyCurve(chiselSoftLightMap, curve, false);

        applyMidGrayRemappingAndFade(smoothHeightMap, smoothLightMap, 60);
        applyMidGrayRemappingAndFade(chiselHardHeightMap, chiselHardLightMap, 60);
        applyMidGrayRemappingAndFade(chiselSoftHeightMap, chiselSoftLightMap, 60);
        applyMidGrayRemappingAndFade(smoothHeightMap, mappedSmoothLightMap, 60);
        applyMidGrayRemappingAndFade(chiselHardHeightMap, mappedChiselHardLightMap, 60);
        applyMidGrayRemappingAndFade(chiselSoftHeightMap, mappedChiselSoftLightMap, 60);

        QImage out(250 * 3 + 10 * 4, 250 * 2 + 10 * 3, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(smoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselSoftLightMap)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedSmoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedChiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(mappedChiselSoftLightMap)));

        out.save("bevel_emboss_8.png");

        return out;
    }

    QImage generateImage46() const
    {
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage smoothHeightMap = generateHeightMap(mask1, Technique_Smooth, 20);
        QImage chiselHardHeightMap = generateHeightMap(mask1, Technique_ChiselHard, 20);
        QImage chiselSoftHeightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);
        
        QImage smoothHeightMapTex = smoothHeightMap;
        QImage chiselHardHeightMapTex = chiselHardHeightMap;
        QImage chiselSoftHeightMapTex = chiselSoftHeightMap;
        
        QImage textureHeightMap = generateTextureHeightMap(mask1, pattern, 50, {0, 0});
        applyTextureHeightMap(smoothHeightMapTex, textureHeightMap, -3, false);
        applyTextureHeightMap(chiselHardHeightMapTex, textureHeightMap, -3, false);
        applyTextureHeightMap(chiselSoftHeightMapTex, textureHeightMap, -3, false);

        QImage smoothLightMap = generateLightMap(smoothHeightMapTex, 100, Direction_Up, 20, 135, 60);
        QImage chiselHardLightMap = generateLightMap(chiselHardHeightMapTex, 100, Direction_Up, 20, 135, 60);
        QImage chiselSoftLightMap = generateLightMap(chiselSoftHeightMapTex, 100, Direction_Up, 20, 135, 60);

        QImage smoothLightMapFade = smoothLightMap;
        QImage chiselHardLightMapFade = chiselHardLightMap;
        QImage chiselSoftLightMapFade = chiselSoftLightMap;
        
        applyMidGrayRemappingAndFade(smoothHeightMap, smoothLightMapFade, 60);
        applyMidGrayRemappingAndFade(chiselHardHeightMap, chiselHardLightMapFade, 60);
        applyMidGrayRemappingAndFade(chiselSoftHeightMap, chiselSoftLightMapFade, 60);

        qint32 min = 0xFF00, max = 0;
        visitPixels(smoothHeightMapTex,
            [&min, &max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                if (*p > max) {
                    max = *p;
                }
                if (*p < min) {
                    min = *p;
                }
            }
        );
        visitPixels(smoothHeightMapTex,
            [min, max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                const quint64 v1 = (*p - min);
                const quint64 v2 = (max - min);
                *p = v1 * 0xFF00UL / v2;
            }
        );

        min = 0xFF00;
        max = 0;
        visitPixels(chiselHardHeightMapTex,
            [&min, &max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                if (*p > max) {
                    max = *p;
                }
                if (*p < min) {
                    min = *p;
                }
            }
        );
        visitPixels(chiselHardHeightMapTex,
            [min, max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                const quint64 v1 = (*p - min);
                const quint64 v2 = (max - min);
                *p = v1 * 0xFF00UL / v2;
            }
        );

        min = 0xFF00;
        max = 0;
        visitPixels(chiselSoftHeightMapTex,
            [&min, &max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                if (*p > max) {
                    max = *p;
                }
                if (*p < min) {
                    min = *p;
                }
            }
        );
        visitPixels(chiselSoftHeightMapTex,
            [min, max](quint8 *pixel)
            {
                qint32 *p = reinterpret_cast<qint32*>(pixel);
                const quint64 v1 = (*p - min);
                const quint64 v2 = (max - min);
                *p = v1 * 0xFF00UL / v2;
            }
        );

        QImage out(250 * 3 + 10 * 4, 250 * 3 + 10 * 4, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(smoothHeightMapTex)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselHardHeightMapTex)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselSoftHeightMapTex)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(smoothLightMap)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(chiselHardLightMap)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(chiselSoftLightMap)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 2 + 10 * 3, convertMaskToImage(scaledDownMask(smoothLightMapFade)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 2 + 10 * 3, convertMaskToImage(scaledDownMask(chiselHardLightMapFade)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 2 + 10 * 3, convertMaskToImage(scaledDownMask(chiselSoftLightMapFade)));

        out.save("bevel_emboss_9.png");

        return out;
    }

    QImage generateImage47() const
    {
        QImage mask1 = generateEllipseMask(250, 250, 0.8, 0.2, 0.97, 1.0);
        QImage mask2 = generateEllipseMask(250, 250, 0.2, 0.8, 0.8, 1.0);
        visitPixels(mask1, mask2,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                *pixel1 = std::max(*pixel2, *pixel1);
            }
        );

        using namespace BevelAndEmbossTransform;

        QImage smoothHeightMap = generateHeightMap(mask1, Technique_Smooth, 20);
        QImage chiselHardHeightMap = generateHeightMap(mask1, Technique_ChiselHard, 20);
        QImage chiselSoftHeightMap = generateHeightMap(mask1, Technique_ChiselSoft, 20);
        
        QImage smoothLightMap1 = generateLightMap(smoothHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselHardLightMap1 = generateLightMap(chiselHardHeightMap, 100, Direction_Up, 20, 135, 60);
        QImage chiselSoftLightMap1 = generateLightMap(chiselSoftHeightMap, 100, Direction_Up, 20, 135, 60);

        QImage smoothLightMap2 = generateLightMap(smoothHeightMap, 100, Direction_Down, 20, 135, 60);
        QImage chiselHardLightMap2 = generateLightMap(chiselHardHeightMap, 100, Direction_Down, 20, 135, 60);
        QImage chiselSoftLightMap2 = generateLightMap(chiselSoftHeightMap, 100, Direction_Down, 20, 135, 60);

        QImage smoothLightMapFade1 = smoothLightMap1;
        QImage chiselHardLightMapFade1 = chiselHardLightMap1;
        QImage chiselSoftLightMapFade1 = chiselSoftLightMap1;
        applyMidGrayRemappingAndFade(smoothHeightMap, smoothLightMapFade1, 60);
        applyMidGrayRemappingAndFade(chiselHardHeightMap, chiselHardLightMapFade1, 60);
        applyMidGrayRemappingAndFade(chiselSoftHeightMap, chiselSoftLightMapFade1, 60);
        QImage smoothLightMapFade2 = smoothLightMap2;
        QImage chiselHardLightMapFade2 = chiselHardLightMap2;
        QImage chiselSoftLightMapFade2 = chiselSoftLightMap2;
        applyMidGrayRemappingAndFade(smoothHeightMap, smoothLightMapFade2, 60);
        applyMidGrayRemappingAndFade(chiselHardHeightMap, chiselHardLightMapFade2, 60);
        applyMidGrayRemappingAndFade(chiselSoftHeightMap, chiselSoftLightMapFade2, 60);

        QImage out(250 * 3 + 10 * 4, 250 * 3 + 10 * 4, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(smoothLightMapFade1)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselHardLightMapFade1)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, convertMaskToImage(scaledDownMask(chiselSoftLightMapFade1)));
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(smoothLightMapFade2)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(chiselHardLightMapFade2)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, convertMaskToImage(scaledDownMask(chiselSoftLightMapFade2)));

        visitPixels(smoothLightMapFade1, smoothLightMapFade2, mask1,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                quint32 *p1 = reinterpret_cast<quint32*>(pixel1);
                const quint32 *p2 = reinterpret_cast<const quint32*>(pixel2);
                const quint32 t = *pixel3;
                *p1 = (*p1 * t + *p2 * (0xFF - t)) / 0xFF;
            }
        );
        visitPixels(chiselHardLightMapFade1, chiselHardLightMapFade2, mask1,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                quint32 *p1 = reinterpret_cast<quint32*>(pixel1);
                const quint32 *p2 = reinterpret_cast<const quint32*>(pixel2);
                const quint32 t = *pixel3;
                *p1 = (*p1 * t + *p2 * (0xFF - t)) / 0xFF;
            }
        );
        visitPixels(chiselSoftLightMapFade1, chiselSoftLightMapFade2, mask1,
            [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                quint32 *p1 = reinterpret_cast<quint32*>(pixel1);
                const quint32 *p2 = reinterpret_cast<const quint32*>(pixel2);
                const quint32 t = *pixel3;
                *p1 = (*p1 * t + *p2 * (0xFF - t)) / 0xFF;
            }
        );

        p.drawImage(250 * 0 + 10 * 1, 250 * 2 + 10 * 3, convertMaskToImage(scaledDownMask(smoothLightMapFade1)));
        p.drawImage(250 * 1 + 10 * 2, 250 * 2 + 10 * 3, convertMaskToImage(scaledDownMask(chiselHardLightMapFade1)));
        p.drawImage(250 * 2 + 10 * 3, 250 * 2 + 10 * 3, convertMaskToImage(scaledDownMask(chiselSoftLightMapFade1)));

        out.save("bevel_emboss_10.png");

        return out;
    }

    QImage generateImage48() const
    {
        QImage background = generateBackgroundImage(250, 250);
        QImage layer1 = generateLayer1Image(250, 250);
        QImage currentLayer = generateCurrentLayerImage(250, 250);
        QImage checkerboardPattern = generateCheckerboardPattern(250, 250);

        QImage backdrop = background;
        blend(layer1, backdrop);

        BevelAndEmboss effectOptions;
        effectOptions.size = 20;
        effectOptions.lightOpacity = 100;
        effectOptions.shadowOpacity = 100;
        BlendingOptions blendingOptions;
        Layer layer;
        layer.setImage(currentLayer);
        layer.setBlendingOptions(blendingOptions);
        layer.setBevelAndEmbossOptions(effectOptions);
        layer.setBevelAndEmbossActive(true);

        QImage img1 = checkerboardPattern;
        blend(currentLayer, img1);

        QImage out(250 * 3 + 10 * 4, 250 * 4 + 10 * 5, QImage::Format_ARGB32);
        out.fill(Qt::white);
        QPainter p(&out);
    
        QImage img2 = blend(layer, background, backdrop);
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_ChiselHard;
        layer.setBevelAndEmbossOptions(effectOptions);
        QImage img3 = blend(layer, background, backdrop);
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_ChiselSoft;
        layer.setBevelAndEmbossOptions(effectOptions);
        QImage img4 = blend(layer, background, backdrop);
        p.drawImage(250 * 0 + 10 * 1, 250 * 0 + 10 * 1, img2);
        p.drawImage(250 * 1 + 10 * 2, 250 * 0 + 10 * 1, img3);
        p.drawImage(250 * 2 + 10 * 3, 250 * 0 + 10 * 1, img4);

        effectOptions.style = LayerStyles::BevelAndEmbossStyle_InnerBevel;
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_Smooth;
        layer.setBevelAndEmbossOptions(effectOptions);
        img2 = blend(layer, background, backdrop);
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_ChiselHard;
        layer.setBevelAndEmbossOptions(effectOptions);
        img3 = blend(layer, background, backdrop);
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_ChiselSoft;
        layer.setBevelAndEmbossOptions(effectOptions);
        img4 = blend(layer, background, backdrop);
        p.drawImage(250 * 0 + 10 * 1, 250 * 1 + 10 * 2, img2);
        p.drawImage(250 * 1 + 10 * 2, 250 * 1 + 10 * 2, img3);
        p.drawImage(250 * 2 + 10 * 3, 250 * 1 + 10 * 2, img4);

        effectOptions.style = LayerStyles::BevelAndEmbossStyle_Emboss;
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_Smooth;
        layer.setBevelAndEmbossOptions(effectOptions);
        img2 = blend(layer, background, backdrop);
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_ChiselHard;
        layer.setBevelAndEmbossOptions(effectOptions);
        img3 = blend(layer, background, backdrop);
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_ChiselSoft;
        layer.setBevelAndEmbossOptions(effectOptions);
        img4 = blend(layer, background, backdrop);
        p.drawImage(250 * 0 + 10 * 1, 250 * 2 + 10 * 3, img2);
        p.drawImage(250 * 1 + 10 * 2, 250 * 2 + 10 * 3, img3);
        p.drawImage(250 * 2 + 10 * 3, 250 * 2 + 10 * 3, img4);

        effectOptions.style = LayerStyles::BevelAndEmbossStyle_PillowEmboss;
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_Smooth;
        layer.setBevelAndEmbossOptions(effectOptions);
        img2 = blend(layer, background, backdrop);
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_ChiselHard;
        layer.setBevelAndEmbossOptions(effectOptions);
        img3 = blend(layer, background, backdrop);
        effectOptions.technique = LayerStyles::BevelAndEmbossTechnique_ChiselSoft;
        layer.setBevelAndEmbossOptions(effectOptions);
        img4 = blend(layer, background, backdrop);
        p.drawImage(250 * 0 + 10 * 1, 250 * 3 + 10 * 4, img2);
        p.drawImage(250 * 1 + 10 * 2, 250 * 3 + 10 * 4, img3);
        p.drawImage(250 * 2 + 10 * 3, 250 * 3 + 10 * 4, img4);

        out.save("bevel_emboss_11.png");

        return out;
    }

    QImage generateImage() const
    {
        if (currentImageIndex == 1) {
            return generateImage1();
        } else if (currentImageIndex == 2) {
            return generateImage2();
        } else if (currentImageIndex == 3) {
            return generateImage3();
        } else if (currentImageIndex == 4) {
            return generateImage4();
        } else if (currentImageIndex == 5) {
            return generateImage5();
        } else if (currentImageIndex == 6) {
            return generateImage6();
        } else if (currentImageIndex == 7) {
            return generateImage7();
        } else if (currentImageIndex == 8) {
            return generateImage8();
        } else if (currentImageIndex == 9) {
            return generateImage9();
        } else if (currentImageIndex == 10) {
            return generateImage10();
        } else if (currentImageIndex == 11) {
            return generateImage11();
        } else if (currentImageIndex == 12) {
            return generateImage12();
        } else if (currentImageIndex == 13) {
            return generateImage14();
        } else if (currentImageIndex == 14) {
            return generateImage14();
        } else if (currentImageIndex == 15) {
            return generateImage15();
        } else if (currentImageIndex == 16) {
            return generateImage16();
        } else if (currentImageIndex == 17) {
            return generateImage17();
        } else if (currentImageIndex == 18) {
            return generateImage18();
        } else if (currentImageIndex == 19) {
            return generateImage19();
        } else if (currentImageIndex == 20) {
            return generateImage20();
        } else if (currentImageIndex == 21) {
            return generateImage21();
        } else if (currentImageIndex == 22) {
            return generateImage22();
        } else if (currentImageIndex == 23) {
            return generateImage23();
        } else if (currentImageIndex == 24) {
            return generateImage24();
        } else if (currentImageIndex == 25) {
            return generateImage25();
        } else if (currentImageIndex == 26) {
            return generateImage26();
        } else if (currentImageIndex == 27) {
            return generateImage27();
        } else if (currentImageIndex == 28) {
            return generateImage28();
        } else if (currentImageIndex == 29) {
            return generateImage29();
        } else if (currentImageIndex == 30) {
            return generateImage30();
        } else if (currentImageIndex == 31) {
            return generateImage31();
        } else if (currentImageIndex == 32) {
            return generateImage32();
        } else if (currentImageIndex == 33) {
            return generateImage33();
        } else if (currentImageIndex == 34) {
            return generateImage34();
        } else if (currentImageIndex == 35) {
            return generateImage35();
        } else if (currentImageIndex == 36) {
            return generateImage36();
        } else if (currentImageIndex == 37) {
            return generateImage37();
        } else if (currentImageIndex == 38) {
            return generateImage38();
        } else if (currentImageIndex == 39) {
            return generateImage39();
        } else if (currentImageIndex == 40) {
            return generateImage40();
        } else if (currentImageIndex == 41) {
            return generateImage41();
        } else if (currentImageIndex == 42) {
            return generateImage42();
        } else if (currentImageIndex == 43) {
            return generateImage43();
        } else if (currentImageIndex == 44) {
            return generateImage44();
        } else if (currentImageIndex == 45) {
            return generateImage45();
        } else if (currentImageIndex == 46) {
            return generateImage46();
        } else if (currentImageIndex == 47) {
            return generateImage47();
        } else if (currentImageIndex == 48) {
            return generateImage48();
        }
        return QImage();
    }
};

Window::Window()
    : m_d(new Private(this))
{
    m_d->tex01 = QImage(":/tex01").convertToFormat(QImage::Format_ARGB32);
    m_d->tex02 = QImage(":/tex02").convertToFormat(QImage::Format_ARGB32);
    m_d->tex03 = QImage(":/tex03").convertToFormat(QImage::Format_ARGB32);
    m_d->pattern = QImage(":/pattern").convertToFormat(QImage::Format_ARGB32);
    // Image 1
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Mixing masks");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 1;
                requestViewportUpdate();
            }
        );
    }
    // Image 2
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        m_d->tonalRangeSelector1 = new TonalRangeSliderWithSpinBoxes;
        m_d->tonalRangeSelector1->tonalRangeSlider()->setFixedWidth(150);
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(m_d->tonalRangeSelector1);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Tonal ranges");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 2;
                requestViewportUpdate();
            }
        );
        connect(
            m_d->tonalRangeSelector1, &TonalRangeSliderWithSpinBoxes::tonalRangeChanged,
            [this]()
            {
                m_d->currentImageIndex = 2;
                requestViewportUpdate();
            }
        );
    }
    // Image 3
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        m_d->tonalRangeSelector2 = new TonalRangeSliderWithSpinBoxes;
        m_d->tonalRangeSelector2->tonalRangeSlider()->setFixedWidth(150);
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(m_d->tonalRangeSelector2);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Tonal ranges 2");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 3;
                requestViewportUpdate();
            }
        );
        connect(
            m_d->tonalRangeSelector2, &TonalRangeSliderWithSpinBoxes::tonalRangeChanged,
            [this]()
            {
                m_d->currentImageIndex = 3;
                requestViewportUpdate();
            }
        );
    }
    // Image 4
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Color blending");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 4;
                requestViewportUpdate();
            }
        );
    }
    // Image 5
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Cross dissolve");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 5;
                requestViewportUpdate();
            }
        );
    }
    // Image 6
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Exclude color components");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 6;
                requestViewportUpdate();
            }
        );
    }
    // Image 7
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Distance transform 1");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 7;
                requestViewportUpdate();
            }
        );
    }
    // Image 8
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Distance transform 3");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 8;
                requestViewportUpdate();
            }
        );
    }
    // Image 9
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Size and spread 1");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 9;
                requestViewportUpdate();
            }
        );
    }
    // Image 10
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Size and spread 2");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 10;
                requestViewportUpdate();
            }
        );
    }
    // Image 11
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Size and spread 3");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 11;
                requestViewportUpdate();
            }
        );
    }
    // Image 12
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Size and spread 4");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 12;
                requestViewportUpdate();
            }
        );
    }
    // Image 13
    {
        m_d->slider1 = new SliderSpinBox;
        m_d->slider1->setRange(0, 100);
        m_d->slider1->setValue(100);
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(m_d->slider1);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Range");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 13;
                requestViewportUpdate();
            }
        );
        connect(
            m_d->slider1, &SliderSpinBox::valueChanged,
            [this](qint32 v)
            {
                m_d->currentImageIndex = 13;
                requestViewportUpdate();
            }
        );
    }
    // Image 14
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Noise");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 14;
                requestViewportUpdate();
            }
        );
    }
    // Image 15
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Curve");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 15;
                requestViewportUpdate();
            }
        );
    }
    // Image 16
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Curve AA");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 16;
                requestViewportUpdate();
            }
        );
    }
    // Image 17
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Gradient Map");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 17;
                requestViewportUpdate();
            }
        );
    }
    // Image 18
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Gradient Map Jitter");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 18;
                requestViewportUpdate();
            }
        );
    }
    // Image 19
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Fade 1");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 19;
                requestViewportUpdate();
            }
        );
    }
    // Image 20
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Fade 2");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 20;
                requestViewportUpdate();
            }
        );
    }
    // Image 21
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Fade 3");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 21;
                requestViewportUpdate();
            }
        );
    }
    // Image 22
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Contour transform 1");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 22;
                requestViewportUpdate();
            }
        );
    }
    // Image 23
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Contour transform 2");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 23;
                requestViewportUpdate();
            }
        );
    }
    // Image 24
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Contour transform 3");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 24;
                requestViewportUpdate();
            }
        );
    }
    // Image 25
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Knockout 1");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 25;
                requestViewportUpdate();
            }
        );
    }
    // Image 26
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Knockout 2");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 26;
                requestViewportUpdate();
            }
        );
    }
    // Image 27
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Knockout 3");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 27;
                requestViewportUpdate();
            }
        );
    }
    // Image 28
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Knockout 4");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 28;
                requestViewportUpdate();
            }
        );
    }
    // Image 29
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Drop shadow");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 29;
                requestViewportUpdate();
            }
        );
    }
    // Image 30
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Outer glow");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 30;
                requestViewportUpdate();
            }
        );
    }
    // Image 31
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Pattern overlay");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 31;
                requestViewportUpdate();
            }
        );
    }
    // Image 32
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Gradient overlay");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 32;
                requestViewportUpdate();
            }
        );
    }
    // Image 33
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Color overlay");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 33;
                requestViewportUpdate();
            }
        );
    }
    // Image 34
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Satin");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 34;
                requestViewportUpdate();
            }
        );
    }
    // Image 35
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Inner glow");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 35;
                requestViewportUpdate();
            }
        );
    }
    // Image 36
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Inner shadow");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 36;
                requestViewportUpdate();
            }
        );
    }
    // Image 37
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Stroke");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 37;
                requestViewportUpdate();
            }
        );
    }
    // Image 38
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 1");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 38;
                requestViewportUpdate();
            }
        );
    }
    // Image 39
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 2");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 39;
                requestViewportUpdate();
            }
        );
    }
    // Image 40
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 3");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 40;
                requestViewportUpdate();
            }
        );
    }
    // Image 41
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 4");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 41;
                requestViewportUpdate();
            }
        );
    }
    // Image 42
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 5");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 42;
                requestViewportUpdate();
            }
        );
    }
    // Image 43
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 6");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 43;
                requestViewportUpdate();
            }
        );
    }
    // Image 44
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 7");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 44;
                requestViewportUpdate();
            }
        );
    }
    // Image 45
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 8");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 45;
                requestViewportUpdate();
            }
        );
    }
    // Image 46
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 9");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 46;
                requestViewportUpdate();
            }
        );
    }
    // Image 47
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 10");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 47;
                requestViewportUpdate();
            }
        );
    }
    // Image 48
    {
        QPushButton *buttonGenerate = new QPushButton("Generate");
        QWidget *container = new QWidget;
        QVBoxLayout *layoutContainer = new QVBoxLayout;
        layoutContainer->setContentsMargins(10, 10, 10, 10);
        layoutContainer->setSpacing(5);
        layoutContainer->addWidget(buttonGenerate);
        layoutContainer->addStretch();
        container->setLayout(layoutContainer);
        addControlsPanel(container, "Bevel 11");
        connect(
            buttonGenerate, &QPushButton::clicked,
            [this]()
            {
                m_d->currentImageIndex = 48;
                requestViewportUpdate();
            }
        );
    }

    setCurrentControlsPanel(47);

    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );
}

Window::~Window()
{}
