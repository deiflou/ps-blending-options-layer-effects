
/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <cmath>

#include <vector>

#include "TonalRange.h"

bool TonalRange::isValid() const
{
    return blackRampStart >= 0.0 && blackRampStart <= 1.0 && blackRampStart <= blackRampEnd &&
           whiteRampStart >= 0.0 && whiteRampStart <= 1.0 && whiteRampStart <= whiteRampEnd;
}

bool TonalRange::isNoOp() const
{
    return blackRampStart == 0.0 && blackRampEnd == 0.0 && whiteRampStart == 1.0 && whiteRampEnd == 1.0;
}

bool PixelTonalRanges::isValid() const
{
    return red.isValid() && green.isValid() && blue.isValid() && gray.isValid();
}

bool PixelTonalRanges::isNoOp() const
{
    return red.isNoOp() && green.isNoOp() && blue.isNoOp() && gray.isNoOp();
}

struct Q_DECL_HIDDEN TonalRangeLUT::Private
{
    std::vector<quint8> lut;

    void makeLUT(const TonalRange &tonalRange)
    {
        lut = std::vector<quint8>(256);

        const qreal deltaB = tonalRange.blackRampEnd - tonalRange.blackRampStart;
        const qreal deltaW = tonalRange.whiteRampEnd - tonalRange.whiteRampStart;
        if (tonalRange.whiteRampEnd < tonalRange.blackRampStart) {
            for (qint32 x = 0; x < 256; ++x) {
                const qreal xf = static_cast<qreal>(x) / 255.0;
                const qreal dB = xf - tonalRange.blackRampStart;
                const qreal b = deltaB == 0.0 ? (dB < 0.0 ? 0.0 : 1.0) : dB / deltaB;
                const qreal dW = xf - tonalRange.whiteRampStart;
                const qreal w = deltaW == 0.0 ? (dW > 0.0 ? 0.0 : 1.0) : 1.0 - dW / deltaW;
                const qreal y = std::max(0.0, std::min(1.0, std::max(b, w)));
                lut[x] = static_cast<quint8>(std::round(y * 255.0));
            }
        } else {
            for (qint32 x = 0; x < 256; ++x) {
                const qreal xf = static_cast<qreal>(x) / 255.0;
                const qreal dB = xf - tonalRange.blackRampStart;
                const qreal b = deltaB == 0.0 ? (dB < 0.0 ? 0.0 : 1.0) : dB / deltaB;
                const qreal dW = xf - tonalRange.whiteRampStart;
                const qreal w = deltaW == 0.0 ? (dW > 0.0 ? 0.0 : 1.0) : 1.0 - dW / deltaW;
                const qreal y = std::max(0.0, std::min(1.0, std::min(b, w)));
                lut[x] = static_cast<quint8>(std::round(y * 255.0));
            }
        }
    }
};

TonalRangeLUT::TonalRangeLUT()
    : m_d(new Private)
{}

TonalRangeLUT::TonalRangeLUT(const TonalRange &tonalRange)
    : m_d(new Private)
{
    m_d->makeLUT(tonalRange);
}

TonalRangeLUT::TonalRangeLUT(const TonalRangeLUT &other)
    : m_d(new Private)
{
    m_d->lut = other.m_d->lut;
}

TonalRangeLUT::TonalRangeLUT(TonalRangeLUT &&other)
    : m_d(new Private)
{
    m_d->lut = std::move(other.m_d->lut);
}

TonalRangeLUT& TonalRangeLUT::operator=(const TonalRangeLUT &other)
{
    if (&other != this) {
        m_d->lut = other.m_d->lut;
    }
    return *this;
}

TonalRangeLUT& TonalRangeLUT::operator=(TonalRangeLUT &&other)
{
    if (&other != this) {
        m_d->lut = std::move(other.m_d->lut);
    }
    return *this;
}

TonalRangeLUT::~TonalRangeLUT()
{}

bool TonalRangeLUT::isValid() const
{
    return m_d->lut.size() == 256;
}

quint8 TonalRangeLUT::operator()(quint8 x) const
{
    return m_d->lut[x];
}
