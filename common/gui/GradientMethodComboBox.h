/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef GRADIENTMETHODCOMBOBOX_H
#define GRADIENTMETHODCOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>
#include <Export.h>

class COMMON_EXPORT GradientMethodComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit GradientMethodComboBox(QWidget *parent = nullptr);
    ~GradientMethodComboBox() override;

    LayerStyles::GradientMethod currentGradientMethod() const;

public Q_SLOTS:
    void setCurrentGradientMethod(LayerStyles::GradientMethod newGradientMethod);

Q_SIGNALS:
    void currentGradientMethodChanged(LayerStyles::GradientMethod newGradientMethod);
};

#endif
