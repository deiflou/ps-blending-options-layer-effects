/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QVBoxLayout>
#include <QCheckBox>

#include <SliderSpinBox.h>
#include <Viewport.h>
#include <ImgProcUtil.h>
#include <ImgProcCommon.h>
#include <DistanceTransform.h>

#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    QCheckBox *checkBoxUseThreshold {nullptr};
    QCheckBox *checkBoxUseAntialias {nullptr};
    SliderSpinBox *sliderThreshold {nullptr};
    QImage mask;

    QImage generateImage() const
    {
        QImage out = DistanceTransform::chamfer5x5_Euclidean(mask);
        const bool useThreshold = checkBoxUseThreshold->isChecked();
        const bool useAntialias = checkBoxUseAntialias->isChecked();
        const int threshold = sliderThreshold->value() << 8;
        const int thresholdPlusOne = threshold + 256;
        
        ImgProcUtil::visitPixels(
            out,
            [useThreshold, useAntialias, threshold, thresholdPlusOne](quint8 *pixel)
            {
                const quint32 distance = *reinterpret_cast<quint32*>(pixel);
                ImgProcCommon::ARGB *color = reinterpret_cast<ImgProcCommon::ARGB*>(pixel);

                if (useThreshold) {
                    if (useAntialias) {
                        color->b = color->g = color->r =
                            static_cast<quint8>(
                                distance >= thresholdPlusOne
                                ? 255
                                : distance > threshold
                                  ? distance & 0xFF
                                  : 0
                            );
                        
                    } else {
                        color->b = color->g = color->r = static_cast<quint8>((distance > threshold) * 255);
                    }
                    color->a = 255;
                } else {
                    color->b = color->g = color->r = static_cast<quint8>(qMin(255U, distance >> 8));
                    color->a = 255;
                }
            }
        );

        return out;
    }
};

Window::Window()
    : m_d(new Private)
{
    m_d->mask = QImage(300, 300, QImage::Format_Alpha8);
    m_d->mask.fill(0);
    m_d->mask.scanLine(150)[150] = 255;

    m_d->checkBoxUseThreshold = new QCheckBox;
    m_d->checkBoxUseAntialias = new QCheckBox;
    m_d->sliderThreshold = new SliderSpinBox;
    m_d->sliderThreshold->setRange(0, 250);

    QWidget *containerWidgets = new QWidget;

    QFormLayout *layoutWidgets1 = new QFormLayout;
    layoutWidgets1->setContentsMargins(10, 10, 10, 10);
    layoutWidgets1->setSpacing(5);
    layoutWidgets1->addRow("Use threshold:", m_d->checkBoxUseThreshold);
    layoutWidgets1->addRow("Use antialias:", m_d->checkBoxUseAntialias);
    layoutWidgets1->addRow("Threshold:", m_d->sliderThreshold);

    QVBoxLayout *layoutWidgets2 = new QVBoxLayout;
    layoutWidgets2->setContentsMargins(0, 0, 0, 0);
    layoutWidgets2->setSpacing(0);
    layoutWidgets2->addLayout(layoutWidgets1);
    layoutWidgets2->addStretch();
    containerWidgets->setLayout(layoutWidgets2);

    addControlsPanel(containerWidgets, "Options");

    viewport()->setImage(m_d->generateImage());

    connect(m_d->checkBoxUseThreshold, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxUseAntialias, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->sliderThreshold, SIGNAL(valueChanged(int)),
            this, SLOT(requestViewportUpdate()));
    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );
}

Window::~Window()
{}
