/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QPaintEvent>
#include <QPainter>
#include <QStyle>

#include "CurveDialog.h"
#include "CurveButton.h"

class Q_DECL_HIDDEN CurveButton::Private
{
public:
    CurveButton *q;
    Curve curve;
    QImage curveThumbnail;

    Private(CurveButton *q)
        : q(q)
        , curveThumbnail(64, 64, QImage::Format_ARGB32)
    {}

    void generateThumbnail()
    {
        curveThumbnail.fill(q->palette().color(QPalette::Dark).darker(150).rgb());


        QPainter p(&curveThumbnail);
        p.setRenderHint(QPainter::Antialiasing);
        QPolygonF path;
        const QVector<quint8> lut = curve.lut<quint8>(64, 63 * 4);

        for (qint32 i = 0; i < 64; ++i) {
            path.append(QPointF(i + 0.5, 63 - (lut[i] / 4.0 - 0.5)));
        }
        path.append(QPointF(63.0 + 10.0, path.last().y()));
        path.append(QPointF(63.0 + 10.0, 63.0 + 10.0));
        path.append(QPointF(-10.0, 63.0 + 10.0));
        path.append(QPointF(-10.0, path.first().y()));
        p.setBrush(q->palette().color(QPalette::Light));
        p.setPen(Qt::NoPen);
        p.drawPolygon(path);
    }
};

CurveButton::CurveButton(QWidget *parent)
    : QPushButton(parent)
    , m_d(new Private(this))
{
    setFixedSize(40, 40);
    m_d->generateThumbnail();

    connect(
        this,&QPushButton::clicked, [this]()
        {
            const Curve previousCurve = m_d->curve;
            CurveDialog *curveDialog = new CurveDialog(this, m_d->curve);
            connect(curveDialog, &CurveDialog::curveChanged, this, &CurveButton::setCurve);
            if (curveDialog->exec() == QDialog::Rejected) {
                setCurve(previousCurve);
            }
        }
    );
}

CurveButton::~CurveButton()
{}

const Curve& CurveButton::curve() const
{
    return m_d->curve;
}

void CurveButton::setCurve(const Curve &newCurve)
{
    if (m_d->curve == newCurve) {
        return;
    }
    m_d->curve = newCurve;
    emit curveChanged(m_d->curve);
    m_d->generateThumbnail();
    update();
}

void CurveButton::paintEvent(QPaintEvent *e)
{
    QPushButton::paintEvent(e);

    QPainter p(this);
    p.setRenderHint(QPainter::SmoothPixmapTransform);

    const int buttonMargin = style()->pixelMetric(QStyle::PM_ButtonMargin);
    QRect thumbnailRect = rect().adjusted(buttonMargin, buttonMargin, -buttonMargin, -buttonMargin);
    if (isDown()) {
        const int buttonHorizontalShift = style()->pixelMetric(QStyle::PM_ButtonShiftHorizontal);
        const int buttonVerticalShift = style()->pixelMetric(QStyle::PM_ButtonShiftVertical);
        thumbnailRect.translate(buttonHorizontalShift, buttonVerticalShift);
    }
    p.drawImage(thumbnailRect, m_d->curveThumbnail);
    p.setBrush(Qt::NoBrush);
    p.setPen(QColor(0, 0, 0, 128));
    p.drawRect(thumbnailRect.adjusted(0, 0, -1, -1));
}
