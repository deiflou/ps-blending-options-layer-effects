/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "GlowSourceComboBox.h"

GlowSourceComboBox::GlowSourceComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Center", static_cast<int>(LayerStyles::GlowSource_Center));
    addItem("Edge", static_cast<int>(LayerStyles::GlowSource_Edge));

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentGlowSourceChanged(static_cast<LayerStyles::GlowSource>(currentData().toInt()));
        }
    );
}
GlowSourceComboBox::~GlowSourceComboBox()
{}

LayerStyles::GlowSource GlowSourceComboBox::currentGlowSource() const
{
    return static_cast<LayerStyles::GlowSource>(currentData().toInt());
}

void GlowSourceComboBox::setCurrentGlowSource(LayerStyles::GlowSource newGlowSource)
{
    setCurrentIndex(findData(static_cast<int>(newGlowSource)));
}
