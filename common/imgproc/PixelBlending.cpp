/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <type_traits>

#include "ColorConversion.h"
#include "ImgProcUtil.h"

#include "PixelBlending.h"

namespace PixelBlending
{

using namespace ImgProcCommon;

struct AlphaBlendFunction_Source
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return srcA;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return srcA * srcC / outA;
    }
};

struct AlphaBlendFunction_Destination : AlphaBlendFunction_Source
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return dstA;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return dstC;
    }
};

struct AlphaBlendFunction_SourceOverDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return srcA + dstA * (maxValue - srcA) / maxValue;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        blendC = (srcC * (maxValue - dstA) + blendC * dstA) / maxValue;
        return (srcA * maxValue * blendC + dstA * (maxValue - srcA) * dstC) / (maxValue * outA);
    }
};

struct AlphaBlendFunction_SourceOverDestinationSpecial
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return srcA + dstA * (maxValue - srcA) / maxValue;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return (srcA * srcC * (maxValue - dstA) + blendC * maxValue * dstA) / (maxValue * outA);
    }
};

struct AlphaBlendFunction_DestinationOverSource : AlphaBlendFunction_SourceOverDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return dstA + srcA * (maxValue - dstA) / maxValue;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        blendC = (srcC * (maxValue - dstA) + blendC * dstA) / maxValue;
        return (srcA * (maxValue - dstA) * blendC + dstA * maxValue * dstC) / (maxValue * outA);
    }
};

struct AlphaBlendFunction_SourceInDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return srcA * dstA / maxValue;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return srcC * srcA * dstA / (maxValue * outA);
    }
};

struct AlphaBlendFunction_DestinationInSource : AlphaBlendFunction_SourceInDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return srcA * dstA / maxValue;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return dstC * dstA * srcA / (maxValue * outA);
    }
};

struct AlphaBlendFunction_SourceOutDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return srcA * (maxValue - dstA) / maxValue;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return srcC * srcA * (maxValue - dstA) / (maxValue * outA);
    }
};

struct AlphaBlendFunction_DestinationOutSource : AlphaBlendFunction_SourceOutDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return (maxValue - srcA) * dstA / maxValue;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return dstC * (maxValue - srcA) * dstA / (maxValue * outA);
    }
};

struct AlphaBlendFunction_SourceAtopDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return dstA;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return (srcA * dstA * blendC + dstA * (maxValue - srcA) * dstC) / (maxValue * outA);
    }
};

struct AlphaBlendFunction_SourceAtopDestinationSpecial
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return dstA;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return blendC;
    }
};

struct AlphaBlendFunction_DestinationAtopSource
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return srcA;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return (srcA * (maxValue - dstA) * blendC + dstA * srcA * dstC) / (maxValue * outA);
    }
};

struct AlphaBlendFunction_SourceClearDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return 0;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return 0;
    }
};

struct AlphaBlendFunction_SourceXorDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return (srcA * (maxValue - dstA) + dstA * (maxValue - srcA)) / maxValue;
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return (srcA * (maxValue - dstA) * srcC + dstA * (maxValue - srcA) * dstC) / (maxValue * outA);
    }
};

struct AlphaBlendFunction_SourcePlusDestination
{
    qint32 alpha(qint32 srcA, qint32 dstA) const
    {
        return std::min(maxValue, srcA + dstA);
    }

    qint32 color(qint32 srcC, qint32 srcA, qint32 dstC, qint32 dstA, qint32 blendC, qint32 outA) const
    {
        return std::min(maxValue, (srcA * blendC + dstA * dstC) / outA);
    }
};

struct ColorBlendFunction_Normal
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return srcC;
    }
};

struct ColorBlendFunction_Darken
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return std::min(srcC, dstC);
    }
};

struct ColorBlendFunction_Multiply
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return srcC * dstC / maxValue;
    }
};

struct ColorBlendFunction_ColorBurn
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return 
            dstC == maxValue
            ? maxValue
            : srcC == 0
                ? 0
                : maxValue - std::min(maxValue, (maxValue - dstC) * maxValue / srcC);
    }
};

struct ColorBlendFunction_ColorBurnSpecial : ColorBlendFunction_ColorBurn
{
    qint32 operator()(qint32 srcC, qint32 srcA, qint32 dstC) const
    {
        srcC = maxValue - (maxValue - srcC) * srcA / maxValue;
        return ColorBlendFunction_ColorBurn::operator()(srcC, dstC);
    }
};

struct ColorBlendFunction_LinearBurn
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return std::max(0, srcC + dstC - maxValue);
    }
};

struct ColorBlendFunction_LinearBurnSpecial : ColorBlendFunction_LinearBurn
{
    qint32 operator()(qint32 srcC, qint32 srcA, qint32 dstC) const
    {
        srcC = maxValue - (maxValue - srcC) * srcA / maxValue;
        return ColorBlendFunction_LinearBurn::operator()(srcC, dstC);
    }
};

struct ColorBlendFunction_DarkerColor
{
    ARGB operator()(ARGB src, ARGB dst) const
    {
        return ColorConversion::intensity(src) < ColorConversion::intensity(dst)
               ? src : dst;
    }
};

struct ColorBlendFunction_Lighten
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return std::max(srcC, dstC);
    }
};

struct ColorBlendFunction_Screen
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return srcC + dstC - srcC * dstC / maxValue;
    }
};

struct ColorBlendFunction_ColorDodge
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return 
            dstC == 0
            ? 0
            : srcC == maxValue
                ? maxValue
                : std::min(maxValue, dstC * maxValue / (maxValue - srcC));
    }
};

struct ColorBlendFunction_ColorDodgeSpecial : ColorBlendFunction_ColorDodge
{
    qint32 operator()(qint32 srcC, qint32 srcA, qint32 dstC) const
    {
        srcC = srcC * srcA / maxValue;
        return ColorBlendFunction_ColorDodge::operator()(srcC, dstC);
    }
};

struct ColorBlendFunction_LinearDodge
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return std::min(maxValue, srcC + dstC);
    }
};

struct ColorBlendFunction_LinearDodgeSpecial : ColorBlendFunction_LinearDodge
{
    qint32 operator()(qint32 srcC, qint32 srcA, qint32 dstC) const
    {
        srcC = srcC * srcA / maxValue;
        return ColorBlendFunction_LinearDodge::operator()(srcC, dstC);
    }
};

struct ColorBlendFunction_LighterColor
{
    ARGB operator()(ARGB src, ARGB dst) const
    {
        return ColorConversion::intensity(src) > ColorConversion::intensity(dst)
               ? src : dst;
    }
};

struct ColorBlendFunction_Overlay
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return 
            dstC < halfValue
            ? 2 * srcC * dstC / maxValue
            : maxValue - 2 * (maxValue - srcC) * (maxValue - dstC) / maxValue;
    }
};

struct ColorBlendFunction_SoftLight
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return 
            srcC < halfValue
            ? dstC - (maxValue - 2 * srcC) * dstC * (maxValue - dstC) / (maxValue * maxValue)
            : dstC < quarterValue
                ? dstC + (2 * srcC - maxValue) * (((16 * dstC - 12 * maxValue) * dstC + 4 * maxValue * maxValue) * dstC / (maxValue * maxValue) - dstC) / maxValue
                : dstC + (2 * srcC - maxValue) * (static_cast<qint32>(std::round(std::sqrt(static_cast<qreal>(dstC * maxValue)))) - dstC) / maxValue;
    }
};

struct ColorBlendFunction_HardLight
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return 
            srcC < halfValue
            ? 2 * srcC * dstC / maxValue
            : maxValue - 2 * (maxValue - srcC) * (maxValue - dstC) / maxValue;
    }
};

struct ColorBlendFunction_VividLight
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return 
            srcC < halfValue
            ? dstC == maxValue
                ? maxValue
                : srcC == 0
                    ? 0
                    : maxValue - std::min(maxValue, (maxValue - dstC) * maxValue / (2 * srcC))
            : dstC == 0
                ? 0
                : srcC == maxValue
                    ? maxValue
                    : std::min(maxValue, dstC * maxValue / (maxValue - 2 * (srcC - halfValue)));
    }
};

struct ColorBlendFunction_VividLightSpecial
{
    qint32 operator()(qint32 srcC, qint32 srcA, qint32 dstC) const
    {
        if (srcC < halfValue) {
            srcC = maxValue - (maxValue - 2 * srcC) * srcA / maxValue;
            return 
                dstC == maxValue
                ? maxValue
                : srcC == 0
                  ? 0
                  : maxValue - std::min(maxValue, (maxValue - dstC) * maxValue / srcC);
        } else {
            srcC = 2 * (srcC - halfValue) * srcA / maxValue;
            return 
                dstC == 0
                ? 0
                : srcC == maxValue
                  ? maxValue
                  : std::min(maxValue, dstC * maxValue / (maxValue - srcC));
        }
    }
};

struct ColorBlendFunction_LinearLight
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return 
            srcC < halfValue
            ? std::max(0, 2 * srcC + dstC - maxValue)
            : std::min(maxValue, 2 * (srcC - halfValue) + dstC);
    }
};

struct ColorBlendFunction_LinearLightSpecial
{
    qint32 operator()(qint32 srcC, qint32 srcA, qint32 dstC) const
    {
        if (srcC < halfValue) {
            srcC = maxValue - (maxValue - 2 * srcC) * srcA / maxValue;
            return std::max(0, srcC + dstC - maxValue);
        } else {
            srcC = 2 * (srcC - halfValue) * srcA / maxValue;
            return std::min(maxValue, srcC + dstC);
        }
    }
};

struct ColorBlendFunction_PinLight
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return 
            srcC < halfValue
            ? std::min(2 * srcC, dstC)
            : std::max(2 * (srcC - halfValue), dstC);
    }
};

struct ColorBlendFunction_HardMix
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        // Same as "vivid_light < halfValue ? 0 : maxValue"
        return 
            srcC < halfValue
            ? (dstC >= (maxValue - srcC)) * maxValue
            : (dstC > srcC) * maxValue;
    }
};

struct ColorBlendFunction_HardMixSpecial : ColorBlendFunction_HardMix
{
    qint32 operator()(qint32 srcC, qint32 srcA, qint32 dstC) const
    {
        if (srcA == maxValue) {
            return ColorBlendFunction_HardMix::operator()(srcC, dstC);
        }
        
        const qint32 width = maxValue - srcA * maxValue / (maxValue + 1);

        if (srcC < halfValue) {
            srcC = srcC * (srcA) / maxValue;
            return 
                std::max(0, std::min(maxValue, maxValue - ((maxValue - dstC) - srcC) * maxValue / width));
        } else {
            srcC = (maxValue - srcC) * (srcA) / maxValue;
            return 
                std::max(0, std::min(maxValue, (dstC - srcC) * maxValue / width));
        }
    }
};

struct ColorBlendFunction_Difference
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return std::abs(dstC - srcC);
    }
};

struct ColorBlendFunction_DifferenceSpecial : ColorBlendFunction_Difference
{
    qint32 operator()(qint32 srcC, qint32 srcA, qint32 dstC) const
    {
        srcC = srcC * srcA / maxValue;
        return ColorBlendFunction_Difference::operator()(srcC, dstC);
    }
};

struct ColorBlendFunction_Exclusion
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return srcC + dstC - 2 * srcC * dstC / maxValue;
    }
};

struct ColorBlendFunction_Subtract
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return std::max(0, dstC - srcC);
    }
};

struct ColorBlendFunction_Divide
{
    qint32 operator()(qint32 srcC, qint32 dstC) const
    {
        return 
            srcC == 0
            ? maxValue
            : std::min(maxValue, dstC * maxValue / srcC);
    }
};

qint32 hslLum(qint32 b, qint32 g, qint32 r)
{
    return ColorConversion::intensity(b, g, r);
}

qint32 hslLum(ARGB c)
{
    return ColorConversion::intensity(c);
}

ARGB hslClipColor(qint32 b, qint32 g, qint32 r, qint32 a)
{
    const qint32 l = hslLum(b, g, r);
    const qint32 n = std::min(b, std::min(g, r));
    const qint32 x = std::max(b, std::max(g, r));
    if (n < 0) {
        b = l + (((b - l) * l) / (l - n));
        g = l + (((g - l) * l) / (l - n));
        r = l + (((r - l) * l) / (l - n));
    }
    if (x > maxValue) {
        b = l + (((b - l) * (maxValue - l)) / (x - l));
        g = l + (((g - l) * (maxValue - l)) / (x - l));
        r = l + (((r - l) * (maxValue - l)) / (x - l));
    }
    return {static_cast<quint8>(b), static_cast<quint8>(g), static_cast<quint8>(r), static_cast<quint8>(a)};
}

ARGB hslSetLum(ARGB c, qint32 l)
{
    const qint32 d = l - hslLum(c.b, c.g, c.r);
    const qint32 b = c.b + d;
    const qint32 g = c.g + d;
    const qint32 r = c.r + d;
    return hslClipColor(b, g, r, c.a);
}

qint32 hslSat(ARGB c)
{
    return std::max(c.b, std::max(c.g, c.r)) - std::min(c.b, std::min(c.g, c.r));
}

ARGB hslSetSat(ARGB c, qint32 s)
{
    qint32 b = c.b;
    qint32 g = c.g;
    qint32 r = c.r;
    qint32 *min = &b;
    qint32 *mid = &g;
    qint32 *max = &r;
    if (*max < *mid) {
        std::swap(max, mid);
    }
    if (*mid < *min) {
        std::swap(mid, min);
    }
    if (*max < *mid) {
        std::swap(max, mid);
    }
    if (*max > *min) {
        *mid = (((*mid - *min) * s) / (*max - *min));
        *max = s;
    } else {
        *mid = *max = 0;
    }
    *min = 0;
    return {static_cast<quint8>(b), static_cast<quint8>(g), static_cast<quint8>(r), c.a};
}

struct ColorBlendFunction_Hue
{
    ARGB operator()(ARGB src, ARGB dst) const
    {
        return hslSetLum(hslSetSat(src, hslSat(dst)), hslLum(dst));
    }

    ARGB operator()(ARGB src, ARGB dst, quint8 specialModeFactor) const
    {
        return operator()(src, dst);
    }
};

struct ColorBlendFunction_Saturation
{
    ARGB operator()(ARGB src, ARGB dst) const
    {
        return hslSetLum(hslSetSat(dst, hslSat(src)), hslLum(dst));
    }

    ARGB operator()(ARGB src, ARGB dst, quint8 specialModeFactor) const
    {
        return operator()(src, dst);
    }
};

struct ColorBlendFunction_Color
{
    ARGB operator()(ARGB src, ARGB dst) const
    {
        return hslSetLum(src, hslLum(dst));
    }

    ARGB operator()(ARGB src, ARGB dst, quint8 specialModeFactor) const
    {
        return operator()(src, dst);
    }
};

struct ColorBlendFunction_Luminosity
{
    ARGB operator()(ARGB src, ARGB dst) const
    {
        return hslSetLum(dst, hslLum(src));
    }

    ARGB operator()(ARGB src, ARGB dst, quint8 specialModeFactor) const
    {
        return operator()(src, dst);
    }
};

template <AlphaBlendMethod alphaBlendMethod, bool useSpecialColorBlending>
struct AlphaBlendFunctionSelector
{
    using type = std::conditional_t<alphaBlendMethod == Source, AlphaBlendFunction_Source,
                 std::conditional_t<alphaBlendMethod == Destination, AlphaBlendFunction_Destination,
                 std::conditional_t<alphaBlendMethod == SourceOverDestination,
                    std::conditional_t<useSpecialColorBlending, AlphaBlendFunction_SourceOverDestinationSpecial, AlphaBlendFunction_SourceOverDestination>,
                 std::conditional_t<alphaBlendMethod == DestinationOverSource, AlphaBlendFunction_DestinationOverSource,
                 std::conditional_t<alphaBlendMethod == SourceInDestination, AlphaBlendFunction_SourceInDestination,
                 std::conditional_t<alphaBlendMethod == DestinationInSource, AlphaBlendFunction_DestinationInSource,
                 std::conditional_t<alphaBlendMethod == SourceOutDestination, AlphaBlendFunction_SourceOutDestination,
                 std::conditional_t<alphaBlendMethod == DestinationOutSource, AlphaBlendFunction_DestinationOutSource,
                 std::conditional_t<alphaBlendMethod == SourceAtopDestination,
                    std::conditional_t<useSpecialColorBlending, AlphaBlendFunction_SourceAtopDestinationSpecial, AlphaBlendFunction_SourceAtopDestination>,
                 std::conditional_t<alphaBlendMethod == DestinationAtopSource, AlphaBlendFunction_DestinationAtopSource,
                 std::conditional_t<alphaBlendMethod == SourceClearDestination, AlphaBlendFunction_SourceClearDestination,
                 std::conditional_t<alphaBlendMethod == SourceXorDestination, AlphaBlendFunction_SourceXorDestination,
                 AlphaBlendFunction_SourcePlusDestination>>>>>>>>>>>>;
};

template <AlphaBlendMethod alphaBlendMethod, bool useSpecialColorBlending>
using AlphaBlendFunctionSelector_t = typename AlphaBlendFunctionSelector<alphaBlendMethod, useSpecialColorBlending>::type;

template <ColorBlendMethod colorBlendMethod, bool useSpecialColorBlending>
struct ColorBlendFunctionSelector
{
    using type = std::conditional_t<colorBlendMethod == Normal, ColorBlendFunction_Normal,
                 std::conditional_t<colorBlendMethod == Darken, ColorBlendFunction_Darken,
                 std::conditional_t<colorBlendMethod == Multiply, ColorBlendFunction_Multiply,
                 std::conditional_t<colorBlendMethod == ColorBurn,
                    std::conditional_t<useSpecialColorBlending, ColorBlendFunction_ColorBurnSpecial, ColorBlendFunction_ColorBurn>,
                 std::conditional_t<colorBlendMethod == LinearBurn,
                    std::conditional_t<useSpecialColorBlending, ColorBlendFunction_LinearBurnSpecial, ColorBlendFunction_LinearBurn>,
                 std::conditional_t<colorBlendMethod == DarkerColor, ColorBlendFunction_DarkerColor,
                 std::conditional_t<colorBlendMethod == Lighten, ColorBlendFunction_Lighten,
                 std::conditional_t<colorBlendMethod == Screen, ColorBlendFunction_Screen,
                 std::conditional_t<colorBlendMethod == ColorDodge,
                    std::conditional_t<useSpecialColorBlending, ColorBlendFunction_ColorDodgeSpecial, ColorBlendFunction_ColorDodge>,
                 std::conditional_t<colorBlendMethod == LinearDodge,
                    std::conditional_t<useSpecialColorBlending, ColorBlendFunction_LinearDodgeSpecial, ColorBlendFunction_LinearDodge>,
                 std::conditional_t<colorBlendMethod == LighterColor, ColorBlendFunction_LighterColor,
                 std::conditional_t<colorBlendMethod == Overlay, ColorBlendFunction_Overlay,
                 std::conditional_t<colorBlendMethod == SoftLight, ColorBlendFunction_SoftLight,
                 std::conditional_t<colorBlendMethod == HardLight, ColorBlendFunction_HardLight,
                 std::conditional_t<colorBlendMethod == VividLight,
                    std::conditional_t<useSpecialColorBlending, ColorBlendFunction_VividLightSpecial, ColorBlendFunction_VividLight>,
                 std::conditional_t<colorBlendMethod == LinearLight,
                    std::conditional_t<useSpecialColorBlending, ColorBlendFunction_LinearLightSpecial, ColorBlendFunction_LinearLight>,
                 std::conditional_t<colorBlendMethod == PinLight, ColorBlendFunction_PinLight,
                 std::conditional_t<colorBlendMethod == HardMix,
                    std::conditional_t<useSpecialColorBlending, ColorBlendFunction_HardMixSpecial, ColorBlendFunction_HardMix>,
                 std::conditional_t<colorBlendMethod == Difference,
                    std::conditional_t<useSpecialColorBlending, ColorBlendFunction_DifferenceSpecial, ColorBlendFunction_Difference>,
                 std::conditional_t<colorBlendMethod == Exclusion, ColorBlendFunction_Exclusion,
                 std::conditional_t<colorBlendMethod == Subtract, ColorBlendFunction_Subtract,
                 std::conditional_t<colorBlendMethod == Divide, ColorBlendFunction_Divide,
                 std::conditional_t<colorBlendMethod == Hue, ColorBlendFunction_Hue,
                 std::conditional_t<colorBlendMethod == Saturation, ColorBlendFunction_Saturation,
                 std::conditional_t<colorBlendMethod == Color, ColorBlendFunction_Color,
                 ColorBlendFunction_Luminosity>>>>>>>>>>>>>>>>>>>>>>>>>;
};

template <ColorBlendMethod colorBlendMethod, bool useSpecialColorBlending>
using ColorBlendFunctionSelector_t = typename ColorBlendFunctionSelector<colorBlendMethod, useSpecialColorBlending>::type;

struct AbstractBlendOperator
{
    virtual ~AbstractBlendOperator() {};
    virtual ARGB operator()(ARGB src, ARGB dst) const = 0;
};

template <AlphaBlendMethod alphaBlendMethod, ColorBlendMethod colorBlendMethod,
          bool useSpecialColorBlending>
struct ComponentBasedBlendOperator : AbstractBlendOperator
{
    ARGB operator()(ARGB src, ARGB dst) const override
    {
        qint32 blendA = alphaBlendFunction.alpha(src.a, dst.a);
        if (blendA == 0) {
            return {0, 0, 0, 0};
        }

        qint32 blendB, blendG, blendR;

        if constexpr (useSpecialColorBlending) {
            blendB = colorBlendFunction(src.b, src.a, dst.b);
        } else {
            blendB = colorBlendFunction(src.b, dst.b);
        }
        blendB = alphaBlendFunction.color(src.b, src.a, dst.b, dst.a, blendB, blendA);
        blendB = std::max(0, std::min(maxValue, blendB));

        if constexpr (useSpecialColorBlending) {
            blendG = colorBlendFunction(src.g, src.a, dst.g);
        } else {
            blendG = colorBlendFunction(src.g, dst.g);
        }
        blendG = alphaBlendFunction.color(src.g, src.a, dst.g, dst.a, blendG, blendA);
        blendG = std::max(0, std::min(maxValue, blendG));

        if constexpr (useSpecialColorBlending) {
            blendR = colorBlendFunction(src.r, src.a, dst.r);
        } else {
            blendR = colorBlendFunction(src.r, dst.r);
        }
        blendR = alphaBlendFunction.color(src.r, src.a, dst.r, dst.a, blendR, blendA);
        blendR = std::max(0, std::min(maxValue, blendR));

        return {static_cast<quint8>(blendB), static_cast<quint8>(blendG),
                static_cast<quint8>(blendR), static_cast<quint8>(blendA)};
    }

private:
    AlphaBlendFunctionSelector_t<alphaBlendMethod, useSpecialColorBlending> alphaBlendFunction;
    ColorBlendFunctionSelector_t<colorBlendMethod, useSpecialColorBlending> colorBlendFunction;
};

template <AlphaBlendMethod alphaBlendMethod, ColorBlendMethod colorBlendMethod>
struct PixelBasedBlendOperator : AbstractBlendOperator
{
    ARGB operator()(ARGB src, ARGB dst) const override
    {
        qint32 blendA = alphaBlendFunction.alpha(src.a, dst.a);
        if (blendA == 0) {
            return {0, 0, 0, 0};
        }

        const ARGB colorBlend = colorBlendFunction(src, dst);
        qint32 blendB = colorBlend.b, blendG = colorBlend.g, blendR = colorBlend.r;

        blendB = alphaBlendFunction.color(src.b, src.a, dst.b, dst.a, blendB, blendA);
        blendB = std::max(0, std::min(maxValue, blendB));
        blendG = alphaBlendFunction.color(src.g, src.a, dst.g, dst.a, blendG, blendA);
        blendG = std::max(0, std::min(maxValue, blendG));
        blendR = alphaBlendFunction.color(src.r, src.a, dst.r, dst.a, blendR, blendA);
        blendR = std::max(0, std::min(maxValue, blendR));

        return {static_cast<quint8>(blendB), static_cast<quint8>(blendG),
                static_cast<quint8>(blendR), static_cast<quint8>(blendA)};
    }

private:
    AlphaBlendFunctionSelector_t<alphaBlendMethod, false> alphaBlendFunction;
    ColorBlendFunctionSelector_t<colorBlendMethod, false> colorBlendFunction;
};

template <AlphaBlendMethod alphaBlendMethod, ColorBlendMethod colorBlendMethod,
          bool useSpecialColorBlending = false>
struct SpecificBlendOperatorSelector
{
    using type = std::conditional_t<colorBlendMethod == DarkerColor ||
                                    colorBlendMethod == LighterColor ||
                                    colorBlendMethod == Hue ||
                                    colorBlendMethod == Saturation ||
                                    colorBlendMethod == Color ||
                                    colorBlendMethod == Luminosity,
                                    PixelBasedBlendOperator<alphaBlendMethod, colorBlendMethod>,
                                    ComponentBasedBlendOperator<alphaBlendMethod, colorBlendMethod,
                                                                useSpecialColorBlending>>;
};

template <AlphaBlendMethod alphaBlendMethod, ColorBlendMethod colorBlendMethod,
          bool useSpecialColorBlending = false>
using BlendOperator = typename SpecificBlendOperatorSelector<
                                    alphaBlendMethod, colorBlendMethod,
                                    useSpecialColorBlending
                               >::type;

template <qint32 LookUpComponents, bool useDst>
struct TonalRangeOperator
{
    static constexpr bool isNoOp()
    {
        return LookUpComponents == PixelComponent_None;
    }

    TonalRangeOperator(const PixelTonalRanges &tonalRanges)
    {
        if constexpr (LookUpComponents & PixelComponent_Gray) {
            grayLUT = TonalRangeLUT(tonalRanges.gray);
        }
        if constexpr (LookUpComponents & PixelComponent_Red) {
            redLUT = TonalRangeLUT(tonalRanges.red);
        }
        if constexpr (LookUpComponents & PixelComponent_Green) {
            greenLUT = TonalRangeLUT(tonalRanges.green);
        }
        if constexpr (LookUpComponents & PixelComponent_Blue) {
            blueLUT = TonalRangeLUT(tonalRanges.blue);
        }
    }
    
    quint8 operator()(ARGB src, ARGB dst) const
    {
        if constexpr (useDst) {
            const quint8 opacity = computeOpacity(dst);
            return static_cast<quint8>(maxValue - (maxValue - opacity) * dst.a / maxValue);
        } else {
            return computeOpacity(src);
        }
    }

private:
    TonalRangeLUT grayLUT, redLUT, greenLUT, blueLUT;

    quint8 computeOpacity(ARGB pixel) const
    {
        if constexpr (LookUpComponents == PixelComponent_None) {
            return maxValue;
        } else if constexpr (LookUpComponents == PixelComponent_Gray) {
            return grayLUT(ColorConversion::intensity(pixel));
        } else if constexpr (LookUpComponents == (PixelComponent_Gray | PixelComponent_Red)) {
            return grayLUT(ColorConversion::intensity(pixel)) * redLUT(pixel.r) / maxValue;
        } else if constexpr (LookUpComponents == (PixelComponent_Gray | PixelComponent_Green)) {
            return grayLUT(ColorConversion::intensity(pixel)) * greenLUT(pixel.g) / maxValue;
        } else if constexpr (LookUpComponents == (PixelComponent_Gray | PixelComponent_Red | PixelComponent_Green)) {
            return grayLUT(ColorConversion::intensity(pixel)) * redLUT(pixel.r) * greenLUT(pixel.g) / (maxValue * maxValue);
        } else if constexpr (LookUpComponents == (PixelComponent_Gray | PixelComponent_Blue)) {
            return grayLUT(ColorConversion::intensity(pixel)) * blueLUT(pixel.b) / maxValue;
        } else if constexpr (LookUpComponents == (PixelComponent_Gray | PixelComponent_Red | PixelComponent_Blue)) {
            return grayLUT(ColorConversion::intensity(pixel)) * redLUT(pixel.r) * blueLUT(pixel.b) / (maxValue * maxValue);
        } else if constexpr (LookUpComponents == (PixelComponent_Gray | PixelComponent_Green | PixelComponent_Blue)) {
            return grayLUT(ColorConversion::intensity(pixel)) * greenLUT(pixel.g) * blueLUT(pixel.b) / (maxValue * maxValue);
        } else if constexpr (LookUpComponents == (PixelComponent_Gray | PixelComponent_Red | PixelComponent_Green | PixelComponent_Blue)) {
            return grayLUT(ColorConversion::intensity(pixel)) * redLUT(pixel.r) * greenLUT(pixel.g) * blueLUT(pixel.b)
                   / (maxValue * maxValue * maxValue);
        } else if constexpr (LookUpComponents == PixelComponent_Red) {
            return redLUT(pixel.r);
        } else if constexpr (LookUpComponents == (PixelComponent_Red | PixelComponent_Green)) {
            return redLUT(pixel.r) * greenLUT(pixel.g) / maxValue;
        } else if constexpr (LookUpComponents == (PixelComponent_Red | PixelComponent_Blue)) {
            return redLUT(pixel.r) * blueLUT(pixel.b) / maxValue;
        } else if constexpr (LookUpComponents == (PixelComponent_Red | PixelComponent_Green | PixelComponent_Blue)) {
            return redLUT(pixel.r) * greenLUT(pixel.g) * blueLUT(pixel.b) / (maxValue * maxValue);
        } else if constexpr (LookUpComponents == PixelComponent_Green) {
            return greenLUT(pixel.g);
        } else if constexpr (LookUpComponents == (PixelComponent_Green | PixelComponent_Blue)) {
            return greenLUT(pixel.g) * blueLUT(pixel.b) / maxValue;
        } else {
            return blueLUT(pixel.b);
        }
    }
};

struct AbstractSourceOpacityOperator
{
    virtual ~AbstractSourceOpacityOperator() {};
    virtual ARGB operator()(ARGB src, ARGB dst) const = 0;
};

template <bool useAlphaChannel, bool useConstantOpacity,
          typename TonalRangeOperatorType>
struct SourceOpacityOperator : AbstractSourceOpacityOperator
{
    SourceOpacityOperator(quint8 opacity, const PixelTonalRanges &srcTonalRanges)
        : opacity(opacity)
        , tonalRangesOperator(srcTonalRanges)
    {}

    ARGB operator()(ARGB src, ARGB dst) const override
    {
        enum { useNone = 0, useAlpha = 1 << 0, useOpacity = 1 << 1, useTonalRanges = 1 << 2 };
        static constexpr qint32 options =
            (useAlphaChannel ? useAlpha : 0) |
            (useConstantOpacity ? useOpacity : 0) |
            (TonalRangeOperatorType::isNoOp() ? 0 : useTonalRanges);

        if constexpr (options == useNone) {
            src.a = maxValue;
        } else if constexpr (options == useAlpha) {
        } else if constexpr (options == useOpacity) {
            src.a = opacity;
        } else if constexpr (options == (useAlpha | useOpacity)) {
            src.a = src.a * opacity / maxValue;
        } else if constexpr (options == useTonalRanges) {
            src.a = tonalRangesOperator(src, dst);
        } else if constexpr (options == (useAlpha | useTonalRanges)) {
            src.a = src.a * tonalRangesOperator(src, dst) / maxValue;
        } else if constexpr (options == (useOpacity | useTonalRanges)) {
            src.a = opacity * tonalRangesOperator(src, dst) / maxValue;
        } else {
            src.a = src.a * opacity * tonalRangesOperator(src, dst) / (maxValue * maxValue);
        }
        return src;
    }

private:
    quint8 opacity;
    TonalRangeOperatorType tonalRangesOperator;
};

struct AbstractActiveColorComponentsOperator
{
    virtual ~AbstractActiveColorComponentsOperator() {};
    virtual ARGB operator()(ARGB src, ARGB dst) const = 0;
};

template <qint32 activeColorComponents>
struct ActiveColorComponentsOperator : AbstractActiveColorComponentsOperator
{
    ARGB operator()(ARGB src, ARGB dst) const override
    {
        if constexpr (activeColorComponents == PixelComponent_None) {
            return dst;
        }
        if constexpr (activeColorComponents == (PixelComponent_Blue |
                                                PixelComponent_Green |
                                                PixelComponent_Red)) {
            return src;
        }
        const quint8 finalAlpha = std::max(src.a, dst.a);
        if (finalAlpha == 0) {
            return {0, 0, 0, 0};
        }
        if constexpr (activeColorComponents & PixelComponent_Blue) {
            src.b = src.b * src.a / finalAlpha;
        } else {
            src.b = dst.b * dst.a / finalAlpha;
        }
        if constexpr (activeColorComponents & PixelComponent_Green) {
            src.g = src.g * src.a / finalAlpha;
        } else {
            src.g = dst.g * dst.a / finalAlpha;
        }
        if constexpr (activeColorComponents & PixelComponent_Red) {
            src.r = src.r * src.a / finalAlpha;
        } else {
            src.r = dst.r * dst.a / finalAlpha;
        }
        src.a = finalAlpha;
        return src;
    }
};

struct CrossDissolveOperator
{
    ARGB operator()(ARGB src, ARGB dst, quint8 t) const
    {
        src.a = static_cast<quint8>(src.a * t / maxValue);
        dst.a = static_cast<quint8>(dst.a * (maxValue - t) / maxValue);
        return blendOp(src, dst);
    }

private:
    ComponentBasedBlendOperator<
        SourcePlusDestination, Normal, false
    > blendOp;
};

AbstractActiveColorComponentsOperator* makeActiveColorComponentsOperator(qint32 activeColorComponents)
{
    if (activeColorComponents == PixelComponent_None) {
        return new ActiveColorComponentsOperator<PixelComponent_None>;
    } else if (activeColorComponents == PixelComponent_Red) {
        return new ActiveColorComponentsOperator<PixelComponent_Red>;
    } else if (activeColorComponents == (PixelComponent_Red | PixelComponent_Green)) {
        return new ActiveColorComponentsOperator<PixelComponent_Red | PixelComponent_Green>;
    } else if (activeColorComponents == (PixelComponent_Red | PixelComponent_Blue)) {
        return new ActiveColorComponentsOperator<PixelComponent_Red | PixelComponent_Blue>;
    } else if (activeColorComponents == (PixelComponent_Red | PixelComponent_Green | PixelComponent_Blue)) {
        return new ActiveColorComponentsOperator<PixelComponent_Red | PixelComponent_Green | PixelComponent_Blue>;
    } else if (activeColorComponents == PixelComponent_Green) {
        return new ActiveColorComponentsOperator<PixelComponent_Green>;
    } else if (activeColorComponents == (PixelComponent_Green | PixelComponent_Blue)) {
        return new ActiveColorComponentsOperator<PixelComponent_Green | PixelComponent_Blue>;
    } else {
        return new ActiveColorComponentsOperator<PixelComponent_Blue>;
    }
}

template <bool useAlphaChannel, bool useConstantOpacity, bool useDstInTonalRanges>
AbstractSourceOpacityOperator* makeSourceOpacityOperator4(quint8 opacity, const PixelTonalRanges &tonalRanges)
{
    const quint32 components = (tonalRanges.gray.isNoOp() || !tonalRanges.gray.isValid() ? 0 : PixelComponent_Gray) |
                               (tonalRanges.red.isNoOp() || !tonalRanges.red.isValid() ? 0 : PixelComponent_Red) |
                               (tonalRanges.green.isNoOp() || !tonalRanges.green.isValid() ? 0 : PixelComponent_Green) |
                               (tonalRanges.blue.isNoOp() || !tonalRanges.blue.isValid() ? 0 : PixelComponent_Blue);

    if (components == PixelComponent_None) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_None, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == PixelComponent_Gray) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Gray, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Gray | PixelComponent_Red)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Gray | PixelComponent_Red, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Gray | PixelComponent_Green)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Gray | PixelComponent_Green, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Gray | PixelComponent_Red | PixelComponent_Green)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Gray | PixelComponent_Red | PixelComponent_Green, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Gray | PixelComponent_Blue)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Gray | PixelComponent_Blue, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Gray | PixelComponent_Red | PixelComponent_Blue)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Gray | PixelComponent_Red | PixelComponent_Blue, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Gray | PixelComponent_Green | PixelComponent_Blue)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Gray | PixelComponent_Green | PixelComponent_Blue, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Gray | PixelComponent_Red | PixelComponent_Green | PixelComponent_Blue)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Gray | PixelComponent_Red | PixelComponent_Green | PixelComponent_Blue, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Red)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Red, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Red | PixelComponent_Green)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Red | PixelComponent_Green, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Red | PixelComponent_Blue)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Red | PixelComponent_Blue, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Red | PixelComponent_Green | PixelComponent_Blue)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Red | PixelComponent_Green | PixelComponent_Blue, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Green)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Green, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else if (components == (PixelComponent_Green | PixelComponent_Blue)) {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Green | PixelComponent_Blue, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    } else {
        return new SourceOpacityOperator<useAlphaChannel, useConstantOpacity,
                    TonalRangeOperator<PixelComponent_Blue, useDstInTonalRanges>>
                    (opacity, tonalRanges);
    }
}

template <bool useAlphaChannel, bool useConstantOpacity>
AbstractSourceOpacityOperator* makeSourceOpacityOperator3(quint8 opacity,
                                                          const PixelTonalRanges &tonalRanges,
                                                          bool useDstInTonalRanges)
{
    if (useDstInTonalRanges) {
        return makeSourceOpacityOperator4<useAlphaChannel, useConstantOpacity, true>(opacity, tonalRanges);
    } else {
        return makeSourceOpacityOperator4<useAlphaChannel, useConstantOpacity, false>(opacity, tonalRanges);
    }
}

template <bool useAlphaChannel>
AbstractSourceOpacityOperator* makeSourceOpacityOperator2(quint8 opacity,
                                                          const PixelTonalRanges &tonalRanges,
                                                          bool useDstInTonalRanges)
{
    if (opacity == maxValue) {
        return makeSourceOpacityOperator3<useAlphaChannel, false>(opacity, tonalRanges, useDstInTonalRanges);
    } else {
        return makeSourceOpacityOperator3<useAlphaChannel, true>(opacity, tonalRanges, useDstInTonalRanges);
    }
}

AbstractSourceOpacityOperator* makeSourceOpacityOperator(bool ignoreSourceAlpha, quint8 opacity,
                                                         const PixelTonalRanges &tonalRanges,
                                                         bool useDstInTonalRanges)
{
    if (ignoreSourceAlpha) {
        return makeSourceOpacityOperator2<false>(opacity, tonalRanges, useDstInTonalRanges);
    } else {
        return makeSourceOpacityOperator2<true>(opacity, tonalRanges, useDstInTonalRanges);
    }
}

template <AlphaBlendMethod alphaBlendMethod, bool useSpecialColorBlending>
AbstractBlendOperator* makeBlendOperator2(ColorBlendMethod colorBlendMethod)
{
    if (colorBlendMethod == Normal) {
        return new BlendOperator<alphaBlendMethod, Normal, false>;
    } else if (colorBlendMethod == Darken) {
        return new BlendOperator<alphaBlendMethod, Darken, false>;
    } else if (colorBlendMethod == Multiply) {
        return new BlendOperator<alphaBlendMethod, Multiply, false>;
    } else if (colorBlendMethod == ColorBurn) {
        return new BlendOperator<alphaBlendMethod, ColorBurn, useSpecialColorBlending>;
    } else if (colorBlendMethod == LinearBurn) {
        return new BlendOperator<alphaBlendMethod, LinearBurn, useSpecialColorBlending>;
    } else if (colorBlendMethod == DarkerColor) {
        return new BlendOperator<alphaBlendMethod, DarkerColor>;
    } else if (colorBlendMethod == Lighten) {
        return new BlendOperator<alphaBlendMethod, Lighten, false>;
    } else if (colorBlendMethod == Screen) {
        return new BlendOperator<alphaBlendMethod, Screen, false>;
    } else if (colorBlendMethod == ColorDodge) {
        return new BlendOperator<alphaBlendMethod, ColorDodge, useSpecialColorBlending>;
    } else if (colorBlendMethod == LinearDodge) {
        return new BlendOperator<alphaBlendMethod, LinearDodge, useSpecialColorBlending>;
    } else if (colorBlendMethod == LighterColor) {
        return new BlendOperator<alphaBlendMethod, LighterColor>;
    } else if (colorBlendMethod == Overlay) {
        return new BlendOperator<alphaBlendMethod, Overlay, false>;
    } else if (colorBlendMethod == SoftLight) {
        return new BlendOperator<alphaBlendMethod, SoftLight, false>;
    } else if (colorBlendMethod == HardLight) {
        return new BlendOperator<alphaBlendMethod, HardLight, false>;
    } else if (colorBlendMethod == VividLight) {
        return new BlendOperator<alphaBlendMethod, VividLight, useSpecialColorBlending>;
    } else if (colorBlendMethod == LinearLight) {
        return new BlendOperator<alphaBlendMethod, LinearLight, useSpecialColorBlending>;
    } else if (colorBlendMethod == PinLight) {
        return new BlendOperator<alphaBlendMethod, PinLight, false>;
    } else if (colorBlendMethod == HardMix) {
        return new BlendOperator<alphaBlendMethod, HardMix, useSpecialColorBlending>;
    } else if (colorBlendMethod == Difference) {
        return new BlendOperator<alphaBlendMethod, Difference, useSpecialColorBlending>;
    } else if (colorBlendMethod == Exclusion) {
        return new BlendOperator<alphaBlendMethod, Exclusion, false>;
    } else if (colorBlendMethod == Subtract) {
        return new BlendOperator<alphaBlendMethod, Subtract, false>;
    } else if (colorBlendMethod == Divide) {
        return new BlendOperator<alphaBlendMethod, Divide, false>;
    } else if (colorBlendMethod == Hue) {
        return new BlendOperator<alphaBlendMethod, Hue>;
    } else if (colorBlendMethod == Saturation) {
        return new BlendOperator<alphaBlendMethod, Saturation>;
    } else if (colorBlendMethod == Color) {
        return new BlendOperator<alphaBlendMethod, Color>;
    } else {
        return new BlendOperator<alphaBlendMethod, Luminosity>;
    }
}

AbstractBlendOperator* makeBlendOperator(AlphaBlendMethod alphaBlendMethod,
                                         ColorBlendMethod colorBlendMethod,
                                         bool useSpecialColorBlending)
{
    if (alphaBlendMethod == Source) {
        return makeBlendOperator2<Source, false>(colorBlendMethod);
    } else if (alphaBlendMethod == Destination) {
        return makeBlendOperator2<Destination, false>(colorBlendMethod);
    } else if (alphaBlendMethod == SourceOverDestination) {
        if (useSpecialColorBlending) {
            return makeBlendOperator2<SourceOverDestination, true>(colorBlendMethod);
        } else {
            return makeBlendOperator2<SourceOverDestination, false>(colorBlendMethod);
        }
    } else if (alphaBlendMethod == DestinationOverSource) {
        return makeBlendOperator2<DestinationOverSource, false>(colorBlendMethod);
    } else if (alphaBlendMethod == SourceInDestination) {
        return makeBlendOperator2<SourceInDestination, false>(colorBlendMethod);
    } else if (alphaBlendMethod == DestinationInSource) {
        return makeBlendOperator2<DestinationInSource, false>(colorBlendMethod);
    } else if (alphaBlendMethod == SourceOutDestination) {
        return makeBlendOperator2<SourceOutDestination, false>(colorBlendMethod);
    } else if (alphaBlendMethod == DestinationOutSource) {
        return makeBlendOperator2<DestinationOutSource, false>(colorBlendMethod);
    } else if (alphaBlendMethod == SourceAtopDestination) {
        if (useSpecialColorBlending) {
            return makeBlendOperator2<SourceAtopDestination, true>(colorBlendMethod);
        } else {
            return makeBlendOperator2<SourceAtopDestination, false>(colorBlendMethod);
        }
    } else if (alphaBlendMethod == DestinationAtopSource) {
        return makeBlendOperator2<DestinationAtopSource, false>(colorBlendMethod);
    } else if (alphaBlendMethod == SourceClearDestination) {
        return makeBlendOperator2<SourceClearDestination, false>(colorBlendMethod);
    } else if (alphaBlendMethod == SourceXorDestination) {
        return makeBlendOperator2<SourceXorDestination, false>(colorBlendMethod);
    } else {
        return makeBlendOperator2<SourcePlusDestination, false>(colorBlendMethod);
    }
}

bool colorBlendMethodCanBeSpecial(ColorBlendMethod colorBlendMethod)
{
    return colorBlendMethod == ColorBurn || colorBlendMethod == LinearBurn ||
           colorBlendMethod == ColorDodge || colorBlendMethod == LinearDodge ||
           colorBlendMethod == VividLight || colorBlendMethod == LinearLight ||
           colorBlendMethod == HardMix || colorBlendMethod == Difference;
}

void blend(const QImage &srcImage, QImage &dstImage,
           AlphaBlendMethod alphaBlendMethod,
           ColorBlendMethod colorBlendMethod, bool useSpecialColorBlending,
           quint8 opacity, bool ignoreSourceAlpha,
           const PixelTonalRanges &tonalRanges)
{
    AbstractSourceOpacityOperator *sourceOpacityOperator =
        makeSourceOpacityOperator(ignoreSourceAlpha, opacity, tonalRanges, false);
    AbstractBlendOperator *blendOperator =
        makeBlendOperator(alphaBlendMethod, colorBlendMethod,
                          useSpecialColorBlending && colorBlendMethodCanBeSpecial(colorBlendMethod));

    ImgProcUtil::visitPixels(
        dstImage, srcImage,
        [sourceOpacityOperator, blendOperator]
        (quint8 *pixel1, const quint8 *pixel2)
        {
            ARGB *dst = reinterpret_cast<ARGB*>(pixel1);
            ARGB src = *reinterpret_cast<const ARGB*>(pixel2);

            src = (*sourceOpacityOperator)(src, *dst);
            *dst = (*blendOperator)(src, *dst);
        }
    );

    delete sourceOpacityOperator;
    delete blendOperator;
}

void blendWithBackdrop(const QImage &srcImage, QImage &backdrop, const QImage &mask,
                       qint32 activeColorComponents, quint8 opacity,
                       const PixelTonalRanges &tonalRanges)
{
    AbstractActiveColorComponentsOperator *activeColorComponentsOperator =
        makeActiveColorComponentsOperator(activeColorComponents);
    AbstractSourceOpacityOperator *opacityOperator =
        makeSourceOpacityOperator(true, opacity, tonalRanges, true);
    CrossDissolveOperator blendOperator;

    if (mask.isNull()) {
        ImgProcUtil::visitPixels(
            backdrop, srcImage,
            [activeColorComponentsOperator, opacityOperator, blendOperator]
            (quint8 *pixel1, const quint8 *pixel2)
            {
                ARGB *dst = reinterpret_cast<ARGB*>(pixel1);
                ARGB src = *reinterpret_cast<const ARGB*>(pixel2);

                src = (*activeColorComponentsOperator)(src, *dst);
                const quint8 t = (*opacityOperator)(src, *dst).a;
                *dst = blendOperator(src, *dst, t);            
            }
        );
    } else {
        ImgProcUtil::visitPixels(
            backdrop, srcImage, mask,
            [activeColorComponentsOperator, opacityOperator, blendOperator]
            (quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                ARGB *dst = reinterpret_cast<ARGB*>(pixel1);
                ARGB src = *reinterpret_cast<const ARGB*>(pixel2);

                src = (*activeColorComponentsOperator)(src, *dst);
                const quint8 t = (*opacityOperator)(src, *dst).a * *pixel3 / maxValue;
                *dst = blendOperator(src, *dst, t);            
            }
        );
    }

    delete activeColorComponentsOperator;
    delete opacityOperator;
}

void crossDissolve(const QImage &srcImage, QImage &dstImage, quint8 t)
{
    CrossDissolveOperator blendOperator;
    ImgProcUtil::visitPixels(
        dstImage, srcImage,
        [blendOperator, t](quint8 *pixel1, const quint8 *pixel2)
        {
            ARGB *dst = reinterpret_cast<ARGB*>(pixel1);
            const ARGB src = *reinterpret_cast<const ARGB*>(pixel2);
            *dst = blendOperator(src, *dst, t);
        }
    );
}

void crossDissolve(const QImage &srcImage, QImage &dstImage,
                   const QImage &mask, bool invertMask)
{
    CrossDissolveOperator blendOperator;
    if (invertMask) {
        ImgProcUtil::visitPixels(
            dstImage, srcImage, mask,
            [blendOperator](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                ARGB *dst = reinterpret_cast<ARGB*>(pixel1);
                const ARGB src = *reinterpret_cast<const ARGB*>(pixel2);
                *dst = blendOperator(src, *dst, maxValue - *pixel3);
            }
        );
    } else {
        ImgProcUtil::visitPixels(
            dstImage, srcImage, mask,
            [blendOperator](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
            {
                ARGB *dst = reinterpret_cast<ARGB*>(pixel1);
                const ARGB src = *reinterpret_cast<const ARGB*>(pixel2);
                *dst = blendOperator(src, *dst, *pixel3);
            }
        );
    }
}

void setAlphaChannelConstant(QImage &image, quint8 value)
{
    ImgProcUtil::visitPixels(
        image,
        [value](quint8 *pixel)
        {
            pixel[3] = value;
        }
    );
}

void setAlphaChannel(QImage &image, const QImage &alphaMask)
{
    ImgProcUtil::visitPixels(
        image, alphaMask,
        [](quint8 *pixel1, const quint8 *pixel2)
        {
            pixel1[3] = *pixel2;
        }
    );
}

void multiplyAlphaChannelConstant(QImage &image, quint8 value)
{
    ImgProcUtil::visitPixels(
        image,
        [value](quint8 *pixel)
        {
            pixel[3] = static_cast<quint8>(pixel[3] * value / maxValue);
        }
    );
}

void multiplyAlphaChannel(QImage &image, const QImage &alphaMask)
{
    ImgProcUtil::visitPixels(
        image, alphaMask,
        [](quint8 *pixel1, const quint8 *pixel2)
        {
            pixel1[3] = static_cast<quint8>(pixel1[3] * *pixel2 / maxValue);
        }
    );
}

void multiplyMasks(QImage &mask1, const QImage &mask2)
{
    ImgProcUtil::visitPixels(
        mask1, mask2,
        [](quint8 *pixel1, const quint8 *pixel2)
        {
            *pixel1 = static_cast<quint8>(*pixel1 * *pixel2 / maxValue);
        }
    );
}

}
