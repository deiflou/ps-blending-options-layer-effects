/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QRandomGenerator>

#include <SliderSpinBox.h>
#include <Viewport.h>
#include <CurveButton.h>
#include <ImgProcUtil.h>
#include <ImgProcCommon.h>
#include <ContourTransform.h>

#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    SliderSpinBox *sliderRange {nullptr};
    QCheckBox *checkBoxInvert {nullptr};
    QCheckBox *checkBoxReverseRange {nullptr};
    CurveButton *buttonCurve {nullptr};
    QCheckBox *checkBoxAntiAlias {nullptr};
    SliderSpinBox *sliderNoise {nullptr};
    QCheckBox *checkBoxUseFade {nullptr};
    QImage gradMask;

    QImage generateImage() const
    {
        QRandomGenerator rng;
        const QImage transformedMask =
            ContourTransform::apply(
                ImgProcUtil::scaledUpMask(gradMask),
                checkBoxInvert->isChecked(),
                sliderRange->value(),
                checkBoxReverseRange->isChecked(),
                buttonCurve->curve(),
                checkBoxAntiAlias->isChecked(),
                sliderNoise->value(),
                rng,
                checkBoxUseFade->isChecked()
            );
        QImage out(gradMask.size(), QImage::Format_ARGB32);
        ImgProcUtil::visitPixels(
            out, transformedMask,
            [](quint8 *pixel1, const quint8 *pixel2)
            {
                ImgProcCommon::ARGB *color = reinterpret_cast<ImgProcCommon::ARGB*>(pixel1);
                const quint32 p2 = *reinterpret_cast<const quint32*>(pixel2);
                const quint8 t = static_cast<quint8>((p2 >> 8) + ((p2 & 0xFF) > 0x7F));
                *color = {t, t, t, 255};
            }
        );
        return out;
    }
};

Window::Window()
    : m_d(new Private)
{
    QImage grad01 = QImage(":/grad01").convertToFormat(QImage::Format_ARGB32);
    m_d->gradMask = QImage(grad01.size(), QImage::Format_Alpha8);
    ImgProcUtil::visitPixels(
        m_d->gradMask, grad01,
        [](quint8 *pixel1, const quint8 *pixel2)
        {
            *pixel1 = *pixel2;
        }
    );

    m_d->sliderRange = new SliderSpinBox;
    m_d->sliderRange->setRange(1, 100);
    m_d->checkBoxReverseRange = new QCheckBox;
    m_d->checkBoxInvert = new QCheckBox;
    m_d->buttonCurve = new CurveButton;
    m_d->checkBoxAntiAlias = new QCheckBox;
    m_d->sliderNoise = new SliderSpinBox;
    m_d->checkBoxUseFade = new QCheckBox;

    QWidget *containerOptions = new QWidget;

    QFormLayout *layoutOptions1 = new QFormLayout;
    layoutOptions1->setContentsMargins(10, 10, 10, 10);
    layoutOptions1->setSpacing(5);
    layoutOptions1->addRow("Range:", m_d->sliderRange);
    layoutOptions1->addRow("Reverse range:", m_d->checkBoxReverseRange);
    layoutOptions1->addRow("Invert:", m_d->checkBoxInvert);
    layoutOptions1->addRow("Contour curve:", m_d->buttonCurve);
    layoutOptions1->addRow("Anti-alias:", m_d->checkBoxAntiAlias);
    layoutOptions1->addRow("Noise:", m_d->sliderNoise);
    layoutOptions1->addRow("Use fade:", m_d->checkBoxUseFade);

    QVBoxLayout *layoutOptions2 = new QVBoxLayout;
    layoutOptions2->setContentsMargins(0, 0, 0, 0);
    layoutOptions2->setSpacing(0);
    layoutOptions2->addLayout(layoutOptions1);
    layoutOptions2->addStretch();
    containerOptions->setLayout(layoutOptions2);

    addControlsPanel(containerOptions, "Options");

    viewport()->setImage(m_d->generateImage());

    connect(m_d->sliderRange, SIGNAL(valueChanged(qint32)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxReverseRange, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxInvert, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->buttonCurve, SIGNAL(curveChanged(const Curve&)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxAntiAlias, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->sliderNoise, SIGNAL(valueChanged(qint32)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxUseFade, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );

    resize(m_d->gradMask.size() + QSize(2, 2));
}

Window::~Window()
{}
