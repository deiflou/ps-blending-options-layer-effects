/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QPalette>
#include <QColor>

#include "Application.h"

Application::Application(int &argc, char **argv)
    : QApplication(argc, argv)
{
    setStyle("fusion");

    QPalette pal = palette();
    pal.setColor(QPalette::Window, QColor(64, 64, 64));
    pal.setColor(QPalette::WindowText, Qt::white);
    pal.setColor(QPalette::Base, QColor(32, 32, 32));
    pal.setColor(QPalette::AlternateBase, QColor(48, 48, 48));
    pal.setColor(QPalette::ToolTipBase, QColor(32, 32, 32));
    pal.setColor(QPalette::ToolTipText, Qt::white);
    pal.setColor(QPalette::PlaceholderText, Qt::gray);
    pal.setColor(QPalette::Text, Qt::white);
    pal.setColor(QPalette::Button, QColor(64, 64, 64));
    pal.setColor(QPalette::ButtonText, Qt::white);
    pal.setColor(QPalette::BrightText, Qt::white);
    pal.setColor(QPalette::Light, QColor(128, 128, 128));
    pal.setColor(QPalette::Midlight, QColor(96, 96, 96));
    pal.setColor(QPalette::Dark, QColor(32, 32, 32));
    pal.setColor(QPalette::Mid, QColor(64, 64, 64));
    pal.setColor(QPalette::Shadow, Qt::black);
    pal.setColor(QPalette::Highlight, QColor(64, 192, 255));
    pal.setColor(QPalette::HighlightedText, Qt::black);
    setPalette(pal);
}

Application::~Application()
{}
