/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef LAYER_H
#define LAYER_H

#include <QScopedPointer>
#include <QImage>
#include <QVector>

#include "LayerStyles.h"

class Layer
{
public:
    Layer();
    ~Layer();

    QImage image() const;
    QImage shape() const;
    QImage rasterMask() const;
    bool rasterMaskIsActive() const;
    const LayerStyles::BlendingOptions& blendingOptions() const;
    const LayerStyles::DropShadow& dropShadowOptions() const;
    bool isDropShadowActive() const;
    QImage dropShadowImage() const;
    const LayerStyles::OuterGlow& outerGlowOptions() const;
    bool isOuterGlowActive() const;
    QImage outerGlowImage() const;
    const LayerStyles::PatternOverlay& patternOverlayOptions() const;
    bool isPatternOverlayActive() const;
    QImage patternOverlayImage() const;
    const LayerStyles::GradientOverlay& gradientOverlayOptions() const;
    bool isGradientOverlayActive() const;
    QImage gradientOverlayImage() const;
    const LayerStyles::ColorOverlay& colorOverlayOptions() const;
    bool isColorOverlayActive() const;
    QImage colorOverlayImage() const;
    const LayerStyles::Satin& satinOptions() const;
    bool isSatinActive() const;
    QImage satinImage() const;
    const LayerStyles::InnerGlow& innerGlowOptions() const;
    bool isInnerGlowActive() const;
    QImage innerGlowImage() const;
    const LayerStyles::InnerShadow& innerShadowOptions() const;
    bool isInnerShadowActive() const;
    QImage innerShadowImage() const;
    const LayerStyles::Stroke& strokeOptions() const;
    bool isStrokeActive() const;
    QImage strokeImage() const;
    QImage strokeShape() const;
    QImage strokeFill() const;
    const LayerStyles::BevelAndEmboss& bevelAndEmbossOptions() const;
    bool isBevelAndEmbossActive() const;
    QImage bevelAndEmbossLightImage() const;
    QImage bevelAndEmbossShadowImage() const;

    void setImage(const QImage &newImage);
    void setRasterMask(const QImage &newRasterMask);
    void setRasterMaskActive(bool active);
    void setBlendingOptions(const LayerStyles::BlendingOptions &newBlendingOptions);
    void setDropShadowOptions(const LayerStyles::DropShadow &newDropShadowOptions);
    void setDropShadowActive(bool active);
    void setOuterGlowOptions(const LayerStyles::OuterGlow &newOuterGlowOptions);
    void setOuterGlowActive(bool active);
    void setPatternOverlayOptions(const LayerStyles::PatternOverlay &newPatternOverlayOptions);
    void setPatternOverlayActive(bool active);
    void setGradientOverlayOptions(const LayerStyles::GradientOverlay &newGradientOverlayOptions);
    void setGradientOverlayActive(bool active);
    void setColorOverlayOptions(const LayerStyles::ColorOverlay &newColorOverlayOptions);
    void setColorOverlayActive(bool active);
    void setSatinOptions(const LayerStyles::Satin &newSatinOptions);
    void setSatinActive(bool active);
    void setInnerGlowOptions(const LayerStyles::InnerGlow &newInnerGlowOptions);
    void setInnerGlowActive(bool active);
    void setInnerShadowOptions(const LayerStyles::InnerShadow &newInnerShadowOptions);
    void setInnerShadowActive(bool active);
    void setStrokeOptions(const LayerStyles::Stroke &newStrokeOptions);
    void setStrokeActive(bool active);
    void setBevelAndEmbossOptions(const LayerStyles::BevelAndEmboss &newBevelAndEmbossOptions);
    void setBevelAndEmbossActive(bool active);

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
