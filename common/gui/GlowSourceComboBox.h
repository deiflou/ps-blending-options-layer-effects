/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef GLOWSOURCECOMBOBOX_H
#define GLOWSOURCECOMBOBOX_H

#include <QComboBox>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT GlowSourceComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit GlowSourceComboBox(QWidget *parent = nullptr);
    ~GlowSourceComboBox() override;

    LayerStyles::GlowSource currentGlowSource() const;

public Q_SLOTS:
    void setCurrentGlowSource(LayerStyles::GlowSource newGlowSource);

Q_SIGNALS:
    void currentGlowSourceChanged(LayerStyles::GlowSource newGlowSource);
};

#endif
