/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef PATTERNOVERLAYOPTIONSWIDGET_H
#define PATTERNOVERLAYOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT PatternOverlayOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PatternOverlayOptionsWidget(QWidget *parent = nullptr);
    ~PatternOverlayOptionsWidget() override;

    LayerStyles::PatternOverlay params() const;

public Q_SLOTS:
    void setPattern(const QImage &pattern);

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
