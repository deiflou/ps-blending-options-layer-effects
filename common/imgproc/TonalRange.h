/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef TONALRANGE_H
#define TONALRANGE_H

#include <QtGlobal>
#include <QScopedPointer>

#include <Export.h>

struct COMMON_EXPORT TonalRange
{
    qreal blackRampStart {0.0}, blackRampEnd {0.0}, whiteRampStart {1.0}, whiteRampEnd {1.0};

    bool isValid() const;
    bool isNoOp() const;
};

struct COMMON_EXPORT PixelTonalRanges
{
    TonalRange gray, red, green, blue;

    bool isValid() const;
    bool isNoOp() const;
};

struct COMMON_EXPORT TonalRangeLUT
{
    TonalRangeLUT();
    TonalRangeLUT(const TonalRange &tonalRange);
    TonalRangeLUT(const TonalRangeLUT &other);
    TonalRangeLUT(TonalRangeLUT &&other);
    TonalRangeLUT& operator=(const TonalRangeLUT &other);
    TonalRangeLUT& operator=(TonalRangeLUT &&other);
    ~TonalRangeLUT();

    bool isValid() const;
    quint8 operator()(quint8 x) const;

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
