/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <cmath>

#include "ImgProcUtil.h"
#include "Filtering.h"
#include "DistanceTransform.h"
#include "ContourTransform.h"
#include "ColorConversion.h"
#include "BevelAndEmbossTransform.h"

namespace BevelAndEmbossTransform
{

QImage generateSmoothHeightMap(const QImage &mask, qint32 size)
{
    QImage heightMap;
    heightMap = ImgProcUtil::scaledUpMask(mask);
    heightMap = Filtering::gaussianBlurMask(heightMap, size);
    return heightMap;
}

QImage distanceTransformToChiselHeightMap(const QImage outerDistanceMap,
                                          const QImage innerDistanceMap,
                                          qint32 size)
{
    const quint32 scaledSize = size << 8;
    const quint32 scaledSizePlusOne = scaledSize + 0x0100;
    QImage heightMap(outerDistanceMap.size(), QImage::Format_ARGB32);

    ImgProcUtil::visitPixels(
        heightMap, outerDistanceMap, innerDistanceMap,
        [scaledSize, scaledSizePlusOne]
        (quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
        {
            quint32 *p1 = reinterpret_cast<quint32*>(pixel1);
            const quint32 *p2 = reinterpret_cast<const quint32*>(pixel2);
            const quint32 *p3 = reinterpret_cast<const quint32*>(pixel3);
            const quint32 outerHeight = *p2 >= scaledSizePlusOne
                                        ? 0x0000
                                        : (scaledSizePlusOne - *p2) * 0x7F80 / scaledSizePlusOne;
            const quint32 innerHeight = *p3 > scaledSizePlusOne || scaledSize == 0
                                        ? 0xFF00
                                        : 0x7F80 + (*p3 - 0x0100) * 0x7F80 / scaledSize;
            *p1 = *p2 == 0 ? innerHeight : outerHeight;
        }
    );
    return heightMap;
}

QImage generateChiselHardHeightMap(const QImage &mask, qint32 size)
{
    QImage outerDistanceMap;
    QImage innerDistanceMap;
    outerDistanceMap = DistanceTransform::chamfer5x5_Euclidean(mask);
    innerDistanceMap = DistanceTransform::chamfer5x5_Euclidean(mask, DistanceTransform::InvertMaskInitFn());
    return distanceTransformToChiselHeightMap(outerDistanceMap, innerDistanceMap, size);
}

QImage generateChiselSoftHeightMap(const QImage &mask, qint32 size)
{
    QImage outerDistanceMap;
    QImage innerDistanceMap;
    outerDistanceMap = DistanceTransform::chamfer5x5_Euclidean(
                            mask,
                            [](const quint8 *pixel, quint32 infinity) -> quint32
                            {
                                return *pixel < 128 ? infinity : 0;
                            }
                       );
    innerDistanceMap = DistanceTransform::chamfer5x5_Euclidean(
                            mask,
                            [](const quint8 *pixel, quint32 infinity) -> quint32
                            {
                                return *pixel < 128 ? 0 : infinity;
                            }
                       );
    return distanceTransformToChiselHeightMap(outerDistanceMap, innerDistanceMap, size);
}

QImage generateHeightMap(const QImage &mask,
                         Technique technique,
                         qint32 size)
{
    if (technique == Technique_Smooth) {
        return generateSmoothHeightMap(mask, size);
    } else if (technique == Technique_ChiselHard) {
        return generateChiselHardHeightMap(mask, size);
    } else {
        return generateChiselSoftHeightMap(mask, size);
    }
}

QImage applyProfile(const QImage &heightMap,
                    qint32 profileRange,
                    bool reverseProfileRange,
                    const Curve &profileCurve,
                    bool useProfileCurveAntiAliasing)
{
    QImage profileHeightMap = ContourTransform::applyRangeAndCurve(heightMap,
                                                                   profileRange,
                                                                   reverseProfileRange,
                                                                   profileCurve,
                                                                   useProfileCurveAntiAliasing);
    if (profileRange == 100 || !reverseProfileRange || profileCurve.knot(0).x > 0.0) {
        return profileHeightMap;
    }

    // Extrapolate the slope of the bevel outwards linearly
    const qint32 scaledRange = profileRange == 0 ? 326 : profileRange * 0xFF00 / 100;
    const qint32 cutPoint = 0xFF00 - scaledRange;
    const qreal normalizedRange = profileRange == 0
                                  ? 0.005
                                  : static_cast<qreal>(profileRange) / 100.0;
    const qreal y1 = std::min(1.0, std::max(0.0, profileCurve.f(0.0)));
    const qreal y2 = std::min(1.0, std::max(0.0, profileCurve.f(0.005)));
    const qreal m = (y2 - y1) / (0.005 * normalizedRange);
    const qint32 b = static_cast<qint32>(std::round(y1 * 0xFF00));
    ImgProcUtil::visitPixels(
        profileHeightMap, heightMap,
        [m, b, cutPoint]
        (quint8 *pixel1, const quint8 *pixel2)
        {
            qint32 *p1 = reinterpret_cast<qint32*>(pixel1);
            const qint32 *p2 = reinterpret_cast<const qint32*>(pixel2);
            *p1 = *p2 < cutPoint
                  ? static_cast<qint32>(std::round(static_cast<qreal>((*p2 - cutPoint)) * m)) + b
                  : *p1;
        }
    );
    return profileHeightMap;
}

QImage generateLightMap(const QImage &heightMap,
                        qint32 depth,
                        Direction direction,
                        qint32 size,
                        qint32 lightAngle,
                        qint32 lightAltitude)
{
    struct vec3 {
        qreal x, y, z;
        qreal dot(const vec3 &other) const { return x * other.x + y * other.y + z * other.z; }
        qreal length() const { return std::sqrt(x * x + y * y + z * z); }
    };

    const qreal angle = static_cast<qreal>(lightAngle) * M_PI / 180.0;
    const qreal altitude = static_cast<qreal>(lightAltitude) * M_PI / 180.0;
    const qreal sinAltitude = std::sin(altitude);
    const vec3 lightDirection =
        direction == Direction_Up
        ? vec3{std::cos(altitude) * std::cos(angle), std::cos(altitude) * -std::sin(angle), sinAltitude}
        : vec3{std::cos(altitude) * -std::cos(angle), std::cos(altitude) * std::sin(angle), sinAltitude};
    const qreal normalizedDepth = static_cast<qreal>(depth) / 100.0;
    const qreal surfaceNormalZ = 6.0 * 0xFF00 / ((size + 1) << 8);
    QImage lightMap(heightMap.size(), QImage::Format_ARGB32);

    ImgProcUtil::visitPixelsWithEightConnectedNeighbors(
        lightMap, heightMap,
        [normalizedDepth, &lightDirection, surfaceNormalZ]
        (quint8 *outPixel, const quint8 *inPixel,
         const quint8 *inPixelNW, const quint8 *inPixelN, const quint8 *inPixelNE,
         const quint8 *inPixelW, const quint8 *inPixelE,
         const quint8 *inPixelSW, const quint8 *inPixelS, const quint8 *inPixelSE)
        {
            const qint32 inValueNW = *reinterpret_cast<const qint32*>(inPixelNW);
            const qint32 inValueN = *reinterpret_cast<const qint32*>(inPixelN);
            const qint32 inValueNE = *reinterpret_cast<const qint32*>(inPixelNE);
            const qint32 inValueW = *reinterpret_cast<const qint32*>(inPixelW);
            const qint32 inValue = *reinterpret_cast<const qint32*>(inPixel);
            const qint32 inValueE = *reinterpret_cast<const qint32*>(inPixelE);
            const qint32 inValueSW = *reinterpret_cast<const qint32*>(inPixelSW);
            const qint32 inValueS = *reinterpret_cast<const qint32*>(inPixelS);
            const qint32 inValueSE = *reinterpret_cast<const qint32*>(inPixelSE);

            // Prewitt
            const qreal dx = (static_cast<qreal>(inValueNE + inValueE + inValueSE) -
                              static_cast<qreal>(inValueNW + inValueW + inValueSW)) / 256.0;
            const qreal dy = (static_cast<qreal>(inValueSW + inValueS + inValueSE) -
                              static_cast<qreal>(inValueNW + inValueN + inValueNE)) / 256.0;

            const vec3 surfaceNormal {-dx * normalizedDepth, -dy * normalizedDepth, surfaceNormalZ};
            const qreal lightAmount = std::max(0.0, surfaceNormal.dot(lightDirection) / surfaceNormal.length());
            
            quint32 *outValue = reinterpret_cast<quint32*>(outPixel);
            *outValue = static_cast<quint32>(std::round(lightAmount * 0xFF00));
        }
    );

    return lightMap;
}

void applyMidGrayRemapping(QImage &lightMap,
                           qint32 lightAltitude)
{
    const qreal altitude = static_cast<qreal>(lightAltitude) * M_PI / 180.0;
    const qreal sinAltitude = std::sin(altitude);
    const quint32 scaledSinAltitude = static_cast<quint32>(std::round(sinAltitude * 0xFF00));
    const quint32 m1 = scaledSinAltitude == 0x0000 ? 0xFF00U * 0xFF00U : 0xFF00U * 0xFF00U / (2 * scaledSinAltitude);
    const quint32 m2 = scaledSinAltitude == 0xFF00 ? 0x0000 : 0xFF00U * 0xFF00U / (2 * (0xFF00U - scaledSinAltitude));
    ImgProcUtil::visitPixels(
        lightMap,
        [scaledSinAltitude, m1, m2]
        (quint8 *pixel)
        {
            quint32 *lightAmount = reinterpret_cast<quint32*>(pixel);
            // Mid-gray remapping
            const quint32 mappedLightAmount = *lightAmount < scaledSinAltitude
                                              ? m1 * *lightAmount / 0xFF00U
                                              : m2 * (*lightAmount - scaledSinAltitude) / 0xFF00U + 0x7F80;

            *lightAmount = mappedLightAmount;
        }
    );
}

void applyMidGrayRemappingAndFade(const QImage &heightMap,
                                  QImage &lightMap,
                                  qint32 lightAltitude)
{
    const qreal altitude = static_cast<qreal>(lightAltitude) * M_PI / 180.0;
    const qreal sinAltitude = std::sin(altitude);
    const quint32 scaledSinAltitude = static_cast<quint32>(std::round(sinAltitude * 0xFF00));
    const quint32 m1 = scaledSinAltitude == 0x0000 ? 0xFF00U * 0xFF00U : 0xFF00U * 0xFF00U / (2 * scaledSinAltitude);
    const quint32 m2 = scaledSinAltitude == 0xFF00 ? 0x0000 : 0xFF00U * 0xFF00U / (2 * (0xFF00U - scaledSinAltitude));
    ImgProcUtil::visitPixels(
        lightMap, heightMap,
        [scaledSinAltitude, m1, m2]
        (quint8 *pixel1, const quint8 *pixel2)
        {
            quint32 *lightAmount = reinterpret_cast<quint32*>(pixel1);
            // Mid-gray remapping
            const quint32 mappedLightAmount = *lightAmount < scaledSinAltitude
                                              ? m1 * *lightAmount / 0xFF00U
                                              : m2 * (*lightAmount - scaledSinAltitude) / 0xFF00U + 0x7F80;
            // Fading
            const quint32 heightValue = *reinterpret_cast<const quint32*>(pixel2);
            const quint32 fadeValue = std::min(
                                          0xFF00U,
                                          mappedLightAmount == 0x7F80U
                                          ? 0xFF00U
                                          : mappedLightAmount < 0x7F80U
                                            ? (heightValue * 0xFF00U / (0xFF00U - mappedLightAmount * 2)) << 3
                                            : (heightValue * 0xFF00U / (mappedLightAmount * 2 - 0xFF00U)) << 3
                                      );

            *lightAmount = (0x7F80U * (0xFF00U - fadeValue) + mappedLightAmount * fadeValue) / 0xFF00U;
        }
    );
}

QImage generateTextureHeightMap(const QImage &mask,
                                const QImage &texture,
                                qint32 textureScale,
                                const QPoint &textureOffset)
{
    QImage textureHeightMap(mask.size(), QImage::Format_ARGB32);
    const qreal scale = static_cast<qreal>(textureScale) / 100.0;
    QTransform transform;
    transform.scale(scale, scale);
    transform.translate(textureOffset.x(), textureOffset.y());
    const QTransform inverseTransform = transform.inverted();

    ImgProcUtil::visitPixelsWithCoords(
        textureHeightMap,
        [&inverseTransform, &texture]
        (quint8 *pixel, qint32 x, qint32 y)
        {
            const QPointF mappedPos = inverseTransform.map(QPointF(x, y));
            const qreal textureX = mappedPos.x() - std::floor(mappedPos.x() / (texture.width() - 1)) * (texture.width() - 1);
            const qreal textureY = mappedPos.y() - std::floor(mappedPos.y() / (texture.height() - 1)) * (texture.height() - 1);
            const quint32 sample = texture.pixel(static_cast<qint32>(std::round(textureX)),
                                                 static_cast<qint32>(std::round(textureY)));
            const ImgProcCommon::ARGB color = {static_cast<quint8>(sample & 0xFF),
                                               static_cast<quint8>((sample >> 8) & 0xFF),
                                               static_cast<quint8>((sample >> 16) & 0xFF),
                                               static_cast<quint8>((sample >> 24) & 0xFF)};
            quint32 *p = reinterpret_cast<quint32*>(pixel);
            const quint32 gray = 0xFF - static_cast<quint32>(ColorConversion::intensity(color));
            *p = (gray * color.a / 0xFFU) << 8;
        }
    );

    return textureHeightMap;
}

void applyTextureHeightMap(QImage &heightMap,
                           const QImage &textureHeightMap,
                           qint32 textureDepth,
                           bool invertTexture)
{
    textureDepth = invertTexture ? -textureDepth : textureDepth;
    ImgProcUtil::visitPixels(
        heightMap, textureHeightMap,
        [textureDepth](quint8 *pixel1, const quint8 *pixel2)
        {
            qint32 *p1 = reinterpret_cast<qint32*>(pixel1);
            const qint32 *p2 = reinterpret_cast<const qint32*>(pixel2);
            
            const qint32 amount = *p2 * textureDepth / 100;
            *p1 += amount;
        }
    );
}

QImage apply(const QImage &mask,
             Style style,
             Technique technique,
             qint32 depth,
             Direction direction,
             qint32 size,
             qint32 soften,
             qint32 lightAngle,
             qint32 lightAltitude,
             const Curve &profileCurve,
             bool useProfileCurveAntiAliasing,
             qint32 profileRange,
             bool reverseProfileRange,
             const QImage &texture,
             qint32 textureScale,
             const QPoint &textureOffset,
             qint32 textureDepth,
             bool invertTexture,
             const Curve &glossCurve,
             bool useGlossCurveAntiAliasing,
             bool applyFade)
{
    // Height map
    const QImage heightMap = generateHeightMap(mask, technique, size);
    // Profile contour
    QImage mappedHeightMap = applyProfile(heightMap,
                                          profileRange,
                                          reverseProfileRange,
                                          profileCurve,
                                          useProfileCurveAntiAliasing);
    // Texture
    if (!texture.isNull()) {
        QImage textureHeightMap = generateTextureHeightMap(mask,
                                                           texture,
                                                           textureScale,
                                                           textureOffset);
        applyTextureHeightMap(mappedHeightMap,
                              textureHeightMap,
                              textureDepth,
                              invertTexture);
    }
    
    QImage lightMap;
    
    if (style != Style_PillowEmboss) {
        // Normal map and lighting
        lightMap = generateLightMap(mappedHeightMap, depth, direction, size, lightAngle, lightAltitude);
        // Gloss contour
        lightMap = ContourTransform::applyCurve(lightMap, glossCurve, useGlossCurveAntiAliasing);
        // Soften
        if (soften > 0) {
            lightMap = Filtering::gaussianBlurMask(lightMap, soften);
        }
        // Mid-gray remapping and fade
        if (applyFade) {
            applyMidGrayRemappingAndFade(heightMap, lightMap, lightAltitude);
        } else{
            applyMidGrayRemapping(lightMap, lightAltitude);
        }

        return lightMap;
    }

    // Normal map and lighting
    lightMap = generateLightMap(mappedHeightMap, depth, direction, size, lightAngle, lightAltitude);
    QImage outerLightMap =
        generateLightMap(mappedHeightMap, depth,
                            direction == Direction_Up ? Direction_Down : Direction_Up,
                            size, lightAngle, lightAltitude);
    // Gloss contour
    lightMap = ContourTransform::applyCurve(lightMap, glossCurve, useGlossCurveAntiAliasing);
    outerLightMap = ContourTransform::applyCurve(outerLightMap, glossCurve, useGlossCurveAntiAliasing);
    // Soften
    if (soften > 0) {
        lightMap = Filtering::gaussianBlurMask(lightMap, soften);
        outerLightMap = Filtering::gaussianBlurMask(outerLightMap, soften);
    }
    // Mid-gray remapping and fade
    if (applyFade) {
        applyMidGrayRemappingAndFade(heightMap, lightMap, lightAltitude);
        applyMidGrayRemappingAndFade(heightMap, outerLightMap, lightAltitude);
    } else {
        applyMidGrayRemapping(lightMap, lightAltitude);
        applyMidGrayRemapping(outerLightMap, lightAltitude);
    }
    // Combine light maps
    ImgProcUtil::visitPixels(
        lightMap, outerLightMap, mask,
        [](quint8 *pixel1, const quint8 *pixel2, const quint8 *pixel3)
        {
            quint32 *p1 = reinterpret_cast<quint32*>(pixel1);
            const quint32 *p2 = reinterpret_cast<const quint32*>(pixel2);
            const quint32 t = *pixel3;
            *p1 = (*p1 * t + *p2 * (0xFF - t)) / 0xFF;
        }
    );

    return lightMap;
}

}
