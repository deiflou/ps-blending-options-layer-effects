/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QHBoxLayout>
#include <QCheckBox>

#include "ColorBlendMethodComboBox.h"
#include "KnockoutComboBox.h"
#include "SliderSpinBox.h"
#include "TonalRangesSelector.h"

#include "BlendingOptionsWidget.h"

class Q_DECL_HIDDEN BlendingOptionsWidget::Private
{
public:
    ColorBlendMethodComboBox *comboBoxBlendMode {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
    SliderSpinBox *sliderFillOpacity {nullptr};
    QCheckBox *checkBoxChannelRed {nullptr};
    QCheckBox *checkBoxChannelGreen {nullptr};
    QCheckBox *checkBoxChannelBlue {nullptr};
    KnockoutComboBox *comboBoxKnockout {nullptr};
    QCheckBox *checkBoxBlendInteriorEffectsAsGroup {nullptr};
    QCheckBox *checkBoxBlendClippedLayersAsGroup {nullptr};
    QCheckBox *checkBoxTransparencyShapesLayer {nullptr};
    QCheckBox *checkBoxLayerMaskHidesEffects {nullptr};
    QCheckBox *checkBoxVectorMaskHidesEffects {nullptr};
    QCheckBox *checkBoxLayerKnocksOutDropShadow {nullptr};
    TonalRangesSelector *widgetTonalRangesSelector {nullptr};
};

BlendingOptionsWidget::BlendingOptionsWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    LayerStyles::BlendingOptions defaultParams;

    m_d->comboBoxBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxBlendMode->setCurrentColorBlendMethod(defaultParams.blendMode);

    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(defaultParams.opacity);

    m_d->sliderFillOpacity = new SliderSpinBox;
    m_d->sliderFillOpacity->setRange(0, 100);
    m_d->sliderFillOpacity->setValue(defaultParams.fillOpacity);

    m_d->checkBoxChannelRed = new QCheckBox;
    m_d->checkBoxChannelRed->setChecked(defaultParams.channels & LayerStyles::Channel_Red);
    m_d->checkBoxChannelRed->setText("Red");

    m_d->checkBoxChannelGreen = new QCheckBox;
    m_d->checkBoxChannelGreen->setChecked(defaultParams.channels & LayerStyles::Channel_Green);
    m_d->checkBoxChannelGreen->setText("Green");

    m_d->checkBoxChannelBlue = new QCheckBox;
    m_d->checkBoxChannelBlue->setChecked(defaultParams.channels & LayerStyles::Channel_Blue);
    m_d->checkBoxChannelBlue->setText("Blue");

    QHBoxLayout *layoutChannels = new QHBoxLayout;
    layoutChannels->setContentsMargins(0, 0, 0, 0);
    layoutChannels->setSpacing(5);
    layoutChannels->addWidget(m_d->checkBoxChannelRed);
    layoutChannels->addWidget(m_d->checkBoxChannelGreen);
    layoutChannels->addWidget(m_d->checkBoxChannelBlue);
    layoutChannels->addStretch();

    m_d->comboBoxKnockout = new KnockoutComboBox;
    m_d->comboBoxKnockout->setCurrentKnockout(defaultParams.knockout);

    m_d->checkBoxBlendInteriorEffectsAsGroup = new QCheckBox;
    m_d->checkBoxBlendInteriorEffectsAsGroup->setChecked(defaultParams.blendInteriorEffectsAsGroup);
    m_d->checkBoxBlendInteriorEffectsAsGroup->setText("Blend interior effects as group");

    m_d->checkBoxBlendClippedLayersAsGroup = new QCheckBox;
    m_d->checkBoxBlendClippedLayersAsGroup->setChecked(defaultParams.blendClippedLayersAsGroup);
    m_d->checkBoxBlendClippedLayersAsGroup->setText("Blend clipped layers as group");

    m_d->checkBoxTransparencyShapesLayer = new QCheckBox;
    m_d->checkBoxTransparencyShapesLayer->setChecked(defaultParams.transparencyShapesLayer);
    m_d->checkBoxTransparencyShapesLayer->setText("Transparency shapes layer");

    m_d->checkBoxLayerMaskHidesEffects = new QCheckBox;
    m_d->checkBoxLayerMaskHidesEffects->setChecked(defaultParams.layerMaskHidesEffects);
    m_d->checkBoxLayerMaskHidesEffects->setText("Layer mask hides effects");

    m_d->checkBoxVectorMaskHidesEffects = new QCheckBox;
    m_d->checkBoxVectorMaskHidesEffects->setChecked(defaultParams.vectorMaskHidesEffects);
    m_d->checkBoxVectorMaskHidesEffects->setText("Vector mask hides effects");

    m_d->checkBoxLayerKnocksOutDropShadow = new QCheckBox;
    m_d->checkBoxLayerKnocksOutDropShadow->setChecked(defaultParams.layerKnocksOutDropShadow);
    m_d->checkBoxLayerKnocksOutDropShadow->setText("Layer knocks out drop shadow");

    m_d->widgetTonalRangesSelector = new TonalRangesSelector;

    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addRow("Blend mode:", m_d->comboBoxBlendMode);
    mainLayout->addRow("Opacity:", m_d->sliderOpacity);
    mainLayout->addRow("Fill opacity:", m_d->sliderFillOpacity);
    mainLayout->addRow("Channels:", layoutChannels);
    mainLayout->addRow("Knockout:", m_d->comboBoxKnockout);
    mainLayout->addRow("", m_d->checkBoxBlendInteriorEffectsAsGroup);
    mainLayout->addRow("", m_d->checkBoxBlendClippedLayersAsGroup);
    mainLayout->addRow("", m_d->checkBoxTransparencyShapesLayer);
    mainLayout->addRow("", m_d->checkBoxLayerMaskHidesEffects);
    mainLayout->addRow("", m_d->checkBoxVectorMaskHidesEffects);
    mainLayout->addRow("", m_d->checkBoxLayerKnocksOutDropShadow);
    mainLayout->addRow("Blend if:", m_d->widgetTonalRangesSelector);

    setLayout(mainLayout);

    connect(m_d->comboBoxBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderFillOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxChannelRed, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxChannelGreen, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxChannelBlue, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxKnockout, SIGNAL(currentKnockoutChanged(LayerStyles::Knockout)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxBlendInteriorEffectsAsGroup, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxBlendClippedLayersAsGroup, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxTransparencyShapesLayer, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxLayerMaskHidesEffects, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxVectorMaskHidesEffects, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxLayerKnocksOutDropShadow, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->widgetTonalRangesSelector, SIGNAL(tonalRangesChanged(const PixelTonalRanges&, const PixelTonalRanges&)),
                                            SIGNAL(paramsChanged()));
}

BlendingOptionsWidget::~BlendingOptionsWidget()
{}

LayerStyles::BlendingOptions BlendingOptionsWidget::params() const
{
    LayerStyles::BlendingOptions params;

    params.blendMode = static_cast<LayerStyles::BlendMode>(m_d->comboBoxBlendMode->currentColorBlendMethod());
    params.opacity = m_d->sliderOpacity->value();
    params.fillOpacity = m_d->sliderFillOpacity->value();
    params.knockout = m_d->comboBoxKnockout->currentKnockout();
    params.channels = (m_d->checkBoxChannelRed->isChecked() ? LayerStyles::Channel_Red : 0) |
                      (m_d->checkBoxChannelGreen->isChecked() ? LayerStyles::Channel_Green : 0) |
                      (m_d->checkBoxChannelBlue->isChecked() ? LayerStyles::Channel_Blue : 0);
    params.blendInteriorEffectsAsGroup = m_d->checkBoxBlendInteriorEffectsAsGroup->isChecked();
    params.blendClippedLayersAsGroup = m_d->checkBoxBlendClippedLayersAsGroup->isChecked();
    params.transparencyShapesLayer = m_d->checkBoxTransparencyShapesLayer->isChecked();
    params.layerMaskHidesEffects = m_d->checkBoxLayerMaskHidesEffects->isChecked();
    params.vectorMaskHidesEffects = m_d->checkBoxVectorMaskHidesEffects->isChecked();
    params.layerKnocksOutDropShadow = m_d->checkBoxLayerKnocksOutDropShadow->isChecked();
    params.sourceTonalRanges = m_d->widgetTonalRangesSelector->sourceTonalRanges();
    params.destinationTonalRanges = m_d->widgetTonalRangesSelector->destinationTonalRanges();
    
    return params;
}
