/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef SATINOPTIONSWIDGET_H
#define SATINOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT SatinOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SatinOptionsWidget(QWidget *parent = nullptr);
    ~SatinOptionsWidget() override;

    LayerStyles::Satin params() const;

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
