/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef GRADIENTOVERLAYOPTIONSWIDGET_H
#define GRADIENTOVERLAYOPTIONSWIDGET_H

#include <QWidget>
#include <QGradient>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT GradientOverlayOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GradientOverlayOptionsWidget(QWidget *parent = nullptr);
    ~GradientOverlayOptionsWidget() override;

    LayerStyles::GradientOverlay params() const;

public Q_SLOTS:
    void setGradient(const QGradientStops &gradient);

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
