/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

 #include <cmath>

#include <QVector>
#include <QtConcurrent>

#include "ImgProcCommon.h"
#include "ImgProcUtil.h"
#include "Filtering.h"

namespace Filtering
{

struct ARGBAccumulator
{
    using pixelType = ImgProcCommon::ARGB;

    qint32 sumOfWeightedBlue {0};
    qint32 sumOfWeightedGreen {0};
    qint32 sumOfWeightedRed {0};
    qint32 sumOfWeightedAlpha {0};
    const qint32 sumOfWeights;

    ARGBAccumulator(qint32 radius)
        : sumOfWeights(2 * radius + 1)
    {}

    void add(const quint8 *pixel)
    {
        const ImgProcCommon::ARGB *color = reinterpret_cast<const ImgProcCommon::ARGB*>(pixel);
        sumOfWeightedBlue += color->b;
        sumOfWeightedGreen += color->g;
        sumOfWeightedRed += color->r;
        sumOfWeightedAlpha += color->a;
    }

    void subtract(const quint8 *pixel)
    {
        const ImgProcCommon::ARGB *color = reinterpret_cast<const ImgProcCommon::ARGB*>(pixel);
        sumOfWeightedBlue -= color->b;
        sumOfWeightedGreen -= color->g;
        sumOfWeightedRed -= color->r;
        sumOfWeightedAlpha -= color->a;
    }

    void setAveragedValue(quint8 *pixel)
    {
        ImgProcCommon::ARGB *color = reinterpret_cast<ImgProcCommon::ARGB*>(pixel);
        color->b = sumOfWeightedBlue / sumOfWeights;
        color->g = sumOfWeightedGreen / sumOfWeights;
        color->r = sumOfWeightedRed / sumOfWeights;
        color->a = sumOfWeightedAlpha / sumOfWeights;
    }
};

template <typename PixelType>
struct MaskAccumulator
{
    using pixelType = PixelType;

    quint32 sumOfWeightedValues {0};
    const quint32 sumOfWeights;

    MaskAccumulator(qint32 radius)
        : sumOfWeights(2 * radius + 1)
    {}

    void add(const quint8 *pixel)
    {
        sumOfWeightedValues += *reinterpret_cast<const PixelType*>(pixel);
    }

    void subtract(const quint8 *pixel)
    {
        sumOfWeightedValues -= *reinterpret_cast<const PixelType*>(pixel);
    }

    void setAveragedValue(quint8 *pixel)
    {
        *reinterpret_cast<PixelType*>(pixel) = sumOfWeightedValues / sumOfWeights;
    }
};

template <typename Accumulator>
void boxBlurScanLine(const quint8 *inScanLine, quint8 *outScanLine,
                     qint32 pixelSize, qint32 scanLineSize, qint32 radius)
{
    using PixelType = typename Accumulator::pixelType;
    const PixelType leftMostValue = *reinterpret_cast<const PixelType*>(inScanLine);
    const quint8 *leftMostValuePtr = reinterpret_cast<const quint8*>(&leftMostValue);
    const PixelType rightMostValue = *reinterpret_cast<const PixelType*>(inScanLine + scanLineSize * pixelSize - pixelSize);
    const quint8 *rightMostValuePtr = reinterpret_cast<const quint8*>(&rightMostValue);
    Accumulator accumulator(radius);
    {
        const quint8 *inPixel = inScanLine;
        quint8 *outPixel = outScanLine;
        for (qint32 x = 0; x < radius; ++x) {
            accumulator.add(leftMostValuePtr);
        }
        for (qint32 x = 0; x <= radius && x < scanLineSize; ++x, inPixel += pixelSize) {
            accumulator.add(inPixel);
        }
        accumulator.setAveragedValue(outPixel);
    }
    {
        const qint32 leftPixelDistance = radius + 1;
        const qint32 leftPixelDistanceInBytes = leftPixelDistance * pixelSize;
        const qint32 rightPixelDistance = radius;
        const qint32 rightPixelDistanceInBytes = rightPixelDistance * pixelSize;
        const quint8 *inPixel = inScanLine + pixelSize;
        quint8 *outPixel = outScanLine + pixelSize;

        for (qint32 x = 1; x < scanLineSize; ++x, inPixel += pixelSize, outPixel += pixelSize) {
            accumulator.subtract(x - leftPixelDistance >= 0
                                 ? inPixel - leftPixelDistanceInBytes
                                 : leftMostValuePtr);
            accumulator.add(x + rightPixelDistance < scanLineSize
                            ? inPixel + rightPixelDistanceInBytes
                            : rightMostValuePtr);
            accumulator.setAveragedValue(outPixel);
        }
    }
}

template <typename Accumulator>
QImage boxBlurGeneric(const QImage &image, qint32 radius)
{
    if (radius < 1) {
        return image;
    }

    const quint8 pixelSize = image.pixelFormat().bitsPerPixel() / 8;
    QImage outImage(image.size(), image.format());

    for (qint32 y = 0; y < image.height(); ++y) {
        boxBlurScanLine<Accumulator>(image.scanLine(y), outImage.scanLine(y),
                                     pixelSize, image.width(), radius);
    }

    QVector<quint8> buffer1(pixelSize * qMax(outImage.width(), outImage.height()));
    QVector<quint8> buffer2(pixelSize * qMax(outImage.width(), outImage.height()));
    quint8 *buffer1Ptr = buffer1.data();
    quint8 *buffer2Ptr = buffer2.data();

    const quint8 *outImageEnd = outImage.bits() + outImage.bytesPerLine() * outImage.height();
    for (qint32 x = 0; x < image.width(); ++x) {
        // Read column
        {
            const quint8 *pixelIn = outImage.scanLine(0) + x * pixelSize;
            quint8 *pixelOut = buffer1Ptr;
            while (pixelIn < outImageEnd) {
                memcpy(pixelOut, pixelIn, pixelSize);
                pixelIn += outImage.bytesPerLine();
                pixelOut += pixelSize;
            }
        }
        // Blur
        boxBlurScanLine<Accumulator>(buffer1Ptr, buffer2Ptr,
                                     pixelSize, image.height(), radius);
        // Write column
        {
            const quint8 *pixelIn = buffer2Ptr;
            quint8 *pixelOut = outImage.scanLine(0) + x * pixelSize;
            while (pixelOut < outImageEnd) {
                memcpy(pixelOut, pixelIn, pixelSize);
                pixelIn += pixelSize;
                pixelOut += outImage.bytesPerLine();
            }
        }
    }

    return outImage;
}

QImage boxBlurARGB(const QImage &image, qint32 radius)
{
    return boxBlurGeneric<ARGBAccumulator>(image, radius);
}

QImage boxBlurMask(const QImage &mask, qint32 radius)
{
    const qint32 bpp = mask.pixelFormat().bitsPerPixel();
    if (bpp == 8) {
        return boxBlurGeneric<MaskAccumulator<quint8>>(mask, radius);
    } else if (bpp == 16) {
        return boxBlurGeneric<MaskAccumulator<quint16>>(mask, radius);
    } else if (bpp == 32) {
        return boxBlurGeneric<MaskAccumulator<quint32>>(mask, radius);
    }
    return mask;
}

void boxBlur1D(const quint8 *inData, quint8 *outData,
               qint32 entrySize, qint32 numberOfEntries, qint32 radius)
{
    if (entrySize == 1) {
        boxBlurScanLine<MaskAccumulator<quint8>>(inData, outData, entrySize, numberOfEntries, radius);
    } else if (entrySize == 2) {
        boxBlurScanLine<MaskAccumulator<quint16>>(inData, outData, entrySize, numberOfEntries, radius);
    } else if (entrySize == 4) {
        boxBlurScanLine<MaskAccumulator<quint32>>(inData, outData, entrySize, numberOfEntries, radius);
    }
}

QVector<qint32> boxBlurRadiiFromGaussianBlurRadius(qint32 radius, qint32 n)
{
    const qreal sigma = static_cast<qreal>(radius) / 5.0;
    const qreal wIdeal = std::sqrt((12.0 * sigma * sigma / n) + 1.0);
    const qint32 flooredWIdeal = static_cast<qint32>(std::floor(wIdeal));
    const qint32 wl = flooredWIdeal - (flooredWIdeal % 2 == 0);
    const qint32 wu = wl + 2;
				
    const qreal mIdeal = (12.0 * sigma * sigma - n * wl * wl - 4.0 * n * wl - 3.0 * n) / (-4.0 * wl - 4.0);
    const qint32 m = static_cast<qint32>(std::round(mIdeal));
				
    QVector<qint32> sizes;
    for (qint32 i = 0; i < n; ++i) {
        sizes.append(i < m ? wl : wu);
    }
    return sizes;
}

template <typename Accumulator>
QImage gaussianBlurGeneric(const QImage &image, qint32 radius)
{
    if (radius < 1) {
        return image;
    }

    const quint8 pixelSize = image.pixelFormat().bitsPerPixel() / 8;
    QImage outImage(image.size(), image.format());
    constexpr qint32 numberOfBoxBlurPasses {7};
    const QVector<qint32> boxBlurRadii = boxBlurRadiiFromGaussianBlurRadius(radius, numberOfBoxBlurPasses);

    QVector<QRect> rects = ImgProcUtil::processingRects(image.rect());
    image.scanLine(0);
    QtConcurrent::blockingMap(
        rects,
        [pixelSize, numberOfBoxBlurPasses, &boxBlurRadii, &image, &outImage](const QRect &r)
        {
            QVector<quint8> buffer1(pixelSize * qMax(outImage.width(), outImage.height()));
            QVector<quint8> buffer2(pixelSize * qMax(outImage.width(), outImage.height()));
            quint8 *buffer1Ptr = buffer1.data();
            quint8 *buffer2Ptr = buffer2.data();

            for (qint32 y = r.top(); y <= r.bottom(); ++y) {
                // Read row
                memcpy(buffer1Ptr, image.constScanLine(y), image.width() * pixelSize);
                // Blur
                for (qint32 i = 0; i < numberOfBoxBlurPasses; ++i) {
                    boxBlurScanLine<Accumulator>(buffer1Ptr, buffer2Ptr,
                                                pixelSize, image.width(), boxBlurRadii[i]);
                    std::swap(buffer1Ptr, buffer2Ptr);
                }
                // Write row
                memcpy(outImage.scanLine(y), buffer1Ptr, image.width() * pixelSize);
            }
            return r;
        }
    );

    const quint8 *outImageEnd = outImage.bits() + outImage.bytesPerLine() * outImage.height();
    QtConcurrent::blockingMap(
        rects,
        [pixelSize, numberOfBoxBlurPasses, outImageEnd, &boxBlurRadii, &image, &outImage](const QRect &r)
        {
            QVector<quint8> buffer1(pixelSize * qMax(outImage.width(), outImage.height()));
            QVector<quint8> buffer2(pixelSize * qMax(outImage.width(), outImage.height()));
            quint8 *buffer1Ptr = buffer1.data();
            quint8 *buffer2Ptr = buffer2.data();
            
            for (qint32 x = r.top(); x <= r.bottom(); ++x) {
                // Read column
                {
                    const quint8 *pixelIn = outImage.constScanLine(0) + x * pixelSize;
                    quint8 *pixelOut = buffer1Ptr;
                    while (pixelIn < outImageEnd) {
                        memcpy(pixelOut, pixelIn, pixelSize);
                        pixelIn += outImage.bytesPerLine();
                        pixelOut += pixelSize;
                    }
                }
                // Blur
                for (qint32 i = 0; i < numberOfBoxBlurPasses; ++i) {
                    boxBlurScanLine<Accumulator>(buffer1Ptr, buffer2Ptr,
                                                 pixelSize, image.height(), boxBlurRadii[i]);
                    std::swap(buffer1Ptr, buffer2Ptr);
                }
                // Write column
                {
                    const quint8 *pixelIn = buffer1Ptr;
                    quint8 *pixelOut = outImage.scanLine(0) + x * pixelSize;
                    while (pixelOut < outImageEnd) {
                        memcpy(pixelOut, pixelIn, pixelSize);
                        pixelIn += pixelSize;
                        pixelOut += outImage.bytesPerLine();
                    }
                }
            }
            return r;
        }
    );

    return outImage;
}

QImage gaussianBlurARGB(const QImage &image, qint32 radius)
{
    return gaussianBlurGeneric<ARGBAccumulator>(image, radius);
}

QImage gaussianBlurMask(const QImage &mask, qint32 radius)
{
    const qint32 bpp = mask.pixelFormat().bitsPerPixel();
    if (bpp == 8) {
        return gaussianBlurGeneric<MaskAccumulator<quint8>>(mask, radius);
    } else if (bpp == 16) {
        return gaussianBlurGeneric<MaskAccumulator<quint16>>(mask, radius);
    } else if (bpp == 32) {
        return gaussianBlurGeneric<MaskAccumulator<quint32>>(mask, radius);
    }
    return mask;
}

}
