/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "PixelBlending.h"
#include "LayerStyles.h"
#include "ImgProcUtil.h"

#include "LayerBlending.h"

namespace LayerBlending
{

QImage blend(const Layer &layer, const QImage &background, const QImage &backdrop)
{
    using namespace PixelBlending;
    using namespace LayerStyles;
    using namespace ImgProcUtil;

    // The helper images that contain the composited result and the
    // knocked out region respectively
    QImage copyOfCurrentLayer = layer.image();
    QImage projection = backdrop;
    QImage composition = projection;
    QImage knockoutComposition;
    QImage strokeComposition;
    if (layer.blendingOptions().knockout == Knockout_None) {
        // Use the overall projection as backdrop for the knockout
        // of the implicit effect layers
        knockoutComposition = projection;
    } else {
        // Use the background layer or a transparent image as backdrop
        // for the knockout
        if (!background.isNull()) {
            knockoutComposition = background;
        } else {
            knockoutComposition = QImage(copyOfCurrentLayer.size(), QImage::Format_ARGB32);
            knockoutComposition.fill(0);
        }
    }
    const quint8 currentLayerOpacity = percentageOpacityToByte(layer.blendingOptions().opacity);
    const quint8 currentLayerFillOpacity = percentageOpacityToByte(layer.blendingOptions().fillOpacity);
    const quint8 dropShadowOpacity = percentageOpacityToByte(layer.dropShadowOptions().opacity);
    const quint8 outerGlowOpacity = percentageOpacityToByte(layer.outerGlowOptions().opacity);
    const quint8 patternOverlayOpacity = percentageOpacityToByte(layer.patternOverlayOptions().opacity);
    const quint8 gradientOverlayOpacity = percentageOpacityToByte(layer.gradientOverlayOptions().opacity);
    const quint8 colorOverlayOpacity = percentageOpacityToByte(layer.colorOverlayOptions().opacity);
    const quint8 satinOpacity = percentageOpacityToByte(layer.satinOptions().opacity);
    const quint8 innerGlowOpacity = percentageOpacityToByte(layer.innerGlowOptions().opacity);
    const quint8 innerShadowOpacity = percentageOpacityToByte(layer.innerShadowOptions().opacity);
    const quint8 strokeOpacity = percentageOpacityToByte(layer.strokeOptions().opacity);
    // If the "overprint" option is set on the stroke effect, its
    // opacity is multiplied by the layer opacity
    const quint8 finalStrokeOpacity = layer.strokeOptions().overprint
                                      ? strokeOpacity * currentLayerOpacity / 255
                                      : strokeOpacity;
    const quint8 bevelAndEmbossLightOpacity = percentageOpacityToByte(layer.bevelAndEmbossOptions().lightOpacity);
    const quint8 bevelAndEmbossShadowOpacity = percentageOpacityToByte(layer.bevelAndEmbossOptions().shadowOpacity);
    // "Outer" effects
    if (layer.isDropShadowActive()) {
        blend(layer.dropShadowImage(), composition, SourceOverDestination,
              layer.dropShadowOptions().blendMode, true, dropShadowOpacity);
        if (!layer.blendingOptions().layerKnocksOutDropShadow) {
            blend(layer.dropShadowImage(), knockoutComposition, SourceOverDestination,
                  layer.dropShadowOptions().blendMode, true, dropShadowOpacity);
        }
    }
    // Save the stroke knockout composition layer for later
    if (layer.isStrokeActive() && !layer.strokeOptions().overprint) {
        strokeComposition = layer.isDropShadowActive() &&
                            !layer.blendingOptions().layerKnocksOutDropShadow
                            ? composition
                            : projection;
    }
    if (layer.isOuterGlowActive()) {
        blend(layer.outerGlowImage(), composition, SourceOverDestination,
              layer.outerGlowOptions().blendMode, true, outerGlowOpacity);
    }
    if (layer.isStrokeActive() &&
        layer.strokeOptions().position == LayerStyles::StrokePosition_Outside) {
        if (layer.isBevelAndEmbossActive() &&
            layer.bevelAndEmbossOptions().style == LayerStyles::BevelAndEmbossStyle_StrokeEmboss) {
            if (layer.strokeOptions().overprint) {
                strokeComposition = composition;
            }
            blend(layer.strokeFill(), strokeComposition, SourceOverDestination,
                  layer.strokeOptions().blendMode, true, finalStrokeOpacity);
            blend(layer.bevelAndEmbossLightImage(), strokeComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().lightBlendMode, true, bevelAndEmbossLightOpacity);
            blend(layer.bevelAndEmbossShadowImage(), strokeComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().shadowBlendMode, true, bevelAndEmbossShadowOpacity);
            crossDissolve(strokeComposition, composition, layer.strokeShape());
        } else {
            if (layer.strokeOptions().overprint) {
                blend(layer.strokeImage(), composition, SourceOverDestination,
                      layer.strokeOptions().blendMode, true, finalStrokeOpacity);
            } else {
                blend(layer.strokeFill(), strokeComposition, SourceOverDestination,
                      layer.strokeOptions().blendMode, true, finalStrokeOpacity);
                crossDissolve(strokeComposition, composition, layer.strokeShape());
            }
        }
    }
    if (layer.isBevelAndEmbossActive() &&
        layer.bevelAndEmbossOptions().style == LayerStyles::BevelAndEmbossStyle_OuterBevel &&
        (!layer.isStrokeActive() ||
         layer.strokeOptions().position != LayerStyles::StrokePosition_Center)) {
        blend(layer.bevelAndEmbossLightImage(), composition, SourceOverDestination,
              layer.bevelAndEmbossOptions().lightBlendMode, true, bevelAndEmbossLightOpacity);
        blend(layer.bevelAndEmbossShadowImage(), composition, SourceOverDestination,
              layer.bevelAndEmbossOptions().shadowBlendMode, true, bevelAndEmbossShadowOpacity);
    }
    // Paint the current layer itself
    if (layer.blendingOptions().blendInteriorEffectsAsGroup) {
        if (layer.isPatternOverlayActive()) {
            blend(layer.patternOverlayImage(), copyOfCurrentLayer, SourceAtopDestination,
                  layer.patternOverlayOptions().blendMode, true, patternOverlayOpacity);
        }
        if (layer.isGradientOverlayActive()) {
            blend(layer.gradientOverlayImage(), copyOfCurrentLayer, SourceAtopDestination,
                  layer.gradientOverlayOptions().blendMode, true, gradientOverlayOpacity);
        }
        if (layer.isColorOverlayActive()) {
            blend(layer.colorOverlayImage(), copyOfCurrentLayer, SourceAtopDestination,
                  layer.colorOverlayOptions().blendMode, true, colorOverlayOpacity);
        }
        if (layer.isSatinActive()) {
            blend(layer.satinImage(), copyOfCurrentLayer, SourceAtopDestination,
                  layer.satinOptions().blendMode, true, satinOpacity);
        }
        if (layer.isInnerGlowActive()) {
            blend(layer.innerGlowImage(), copyOfCurrentLayer, SourceAtopDestination,
                  layer.innerGlowOptions().blendMode, true, innerGlowOpacity);
        }
    }
    blend(copyOfCurrentLayer, knockoutComposition,
          SourceOverDestination, layer.blendingOptions().blendMode, true,
          currentLayerFillOpacity, layer.blendingOptions().transparencyShapesLayer,
          layer.blendingOptions().sourceTonalRanges);
    // "Inner" effects
    if (!layer.blendingOptions().blendInteriorEffectsAsGroup) {
        if (layer.isPatternOverlayActive()) {
            blend(layer.patternOverlayImage(), knockoutComposition, SourceOverDestination,
                  layer.patternOverlayOptions().blendMode, true, patternOverlayOpacity);
        }
        if (layer.isGradientOverlayActive()) {
            blend(layer.gradientOverlayImage(), knockoutComposition, SourceOverDestination,
                  layer.gradientOverlayOptions().blendMode, true, gradientOverlayOpacity);
        }
        if (layer.isColorOverlayActive()) {
            blend(layer.colorOverlayImage(), knockoutComposition, SourceOverDestination,
                  layer.colorOverlayOptions().blendMode, true, colorOverlayOpacity);
        }
        if (layer.isSatinActive()) {
            blend(layer.satinImage(), knockoutComposition, SourceOverDestination,
                  layer.satinOptions().blendMode, true, satinOpacity);
        }
        if (layer.isInnerGlowActive()) {
            blend(layer.innerGlowImage(), knockoutComposition, SourceOverDestination,
                  layer.innerGlowOptions().blendMode, true, innerGlowOpacity);
        }
    }
    if (layer.isInnerShadowActive()) {
        blend(layer.innerShadowImage(), knockoutComposition, SourceOverDestination,
              layer.innerShadowOptions().blendMode, true, innerShadowOpacity);
    }
    if (layer.isStrokeActive() &&
        layer.strokeOptions().position == LayerStyles::StrokePosition_Inside) {
        if (layer.isBevelAndEmbossActive() &&
            layer.bevelAndEmbossOptions().style == LayerStyles::BevelAndEmbossStyle_StrokeEmboss) {
            if (layer.strokeOptions().overprint) {
                strokeComposition = knockoutComposition;
            }
            blend(layer.strokeFill(), strokeComposition, SourceOverDestination,
                  layer.strokeOptions().blendMode, true, finalStrokeOpacity);
            blend(layer.bevelAndEmbossLightImage(), strokeComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().lightBlendMode, true, bevelAndEmbossLightOpacity);
            blend(layer.bevelAndEmbossShadowImage(), strokeComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().shadowBlendMode, true, bevelAndEmbossShadowOpacity);
            crossDissolve(strokeComposition, knockoutComposition, layer.strokeShape());
        } else {
            if (layer.strokeOptions().overprint) {
                blend(layer.strokeImage(), knockoutComposition, SourceOverDestination,
                      layer.strokeOptions().blendMode, true, finalStrokeOpacity);
            } else {
                blend(layer.strokeFill(), strokeComposition, SourceOverDestination,
                      layer.strokeOptions().blendMode, true, finalStrokeOpacity);
                crossDissolve(strokeComposition, knockoutComposition, layer.strokeShape());
            }
        }
    }
    if (layer.isBevelAndEmbossActive() &&
        layer.bevelAndEmbossOptions().style == LayerStyles::BevelAndEmbossStyle_InnerBevel &&
        (!layer.isStrokeActive() ||
         layer.strokeOptions().position != LayerStyles::StrokePosition_Center)) {
        blend(layer.bevelAndEmbossLightImage(), knockoutComposition, SourceOverDestination,
              layer.bevelAndEmbossOptions().lightBlendMode, true, bevelAndEmbossLightOpacity);
        blend(layer.bevelAndEmbossShadowImage(), knockoutComposition, SourceOverDestination,
              layer.bevelAndEmbossOptions().shadowBlendMode, true, bevelAndEmbossShadowOpacity);
    }
    // Mix the composition with the knockout interior.
    // This is basically the same as using the following Porter-Duff operators:
    // (composition OUT currentLayer) PLUS (layerComposition IN currentLayer)
    crossDissolve(knockoutComposition, composition, layer.shape());
    // "Over" effects
    if (layer.isStrokeActive() &&
        layer.strokeOptions().position == LayerStyles::StrokePosition_Center) {
        if (layer.isBevelAndEmbossActive() &&
            layer.bevelAndEmbossOptions().style == LayerStyles::BevelAndEmbossStyle_StrokeEmboss) {
            if (layer.strokeOptions().overprint) {
                strokeComposition = composition;
            }
            blend(layer.strokeFill(), strokeComposition, SourceOverDestination,
                  layer.strokeOptions().blendMode, true, finalStrokeOpacity);
            blend(layer.bevelAndEmbossLightImage(), strokeComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().lightBlendMode, true, bevelAndEmbossLightOpacity);
            blend(layer.bevelAndEmbossShadowImage(), strokeComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().shadowBlendMode, true, bevelAndEmbossShadowOpacity);
            crossDissolve(strokeComposition, composition, layer.strokeShape());
        } else {
            if (layer.strokeOptions().overprint) {
                blend(layer.strokeImage(), composition, SourceOverDestination,
                      layer.strokeOptions().blendMode, true, finalStrokeOpacity);
            } else {
                blend(layer.strokeFill(), strokeComposition, SourceOverDestination,
                      layer.strokeOptions().blendMode, true, finalStrokeOpacity);
                crossDissolve(strokeComposition, composition, layer.strokeShape());
            }
        }
    }
    if (layer.isBevelAndEmbossActive()) {
        if (layer.bevelAndEmbossOptions().style == LayerStyles::BevelAndEmbossStyle_OuterBevel &&
            layer.isStrokeActive() &&
            layer.strokeOptions().position == LayerStyles::StrokePosition_Center) {
            QImage bevelAndEmbossComposition = composition;
            blend(layer.bevelAndEmbossLightImage(), bevelAndEmbossComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().lightBlendMode, true, bevelAndEmbossLightOpacity);
            blend(layer.bevelAndEmbossShadowImage(), bevelAndEmbossComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().shadowBlendMode, true, bevelAndEmbossShadowOpacity);
            crossDissolve(bevelAndEmbossComposition, composition, layer.shape(), true);
        
        } else if (layer.bevelAndEmbossOptions().style == LayerStyles::BevelAndEmbossStyle_InnerBevel &&
                   layer.isStrokeActive() &&
                   layer.strokeOptions().position == LayerStyles::StrokePosition_Center) {
            QImage bevelAndEmbossComposition = composition;
            blend(layer.bevelAndEmbossLightImage(), bevelAndEmbossComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().lightBlendMode, true, bevelAndEmbossLightOpacity);
            blend(layer.bevelAndEmbossShadowImage(), bevelAndEmbossComposition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().shadowBlendMode, true, bevelAndEmbossShadowOpacity);
            crossDissolve(bevelAndEmbossComposition, composition, layer.shape());
        
        } else if (layer.bevelAndEmbossOptions().style == LayerStyles::BevelAndEmbossStyle_Emboss ||
                   layer.bevelAndEmbossOptions().style == LayerStyles::BevelAndEmbossStyle_PillowEmboss) {
            blend(layer.bevelAndEmbossLightImage(), composition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().lightBlendMode, true, bevelAndEmbossLightOpacity);
            blend(layer.bevelAndEmbossShadowImage(), composition, SourceOverDestination,
                  layer.bevelAndEmbossOptions().shadowBlendMode, true, bevelAndEmbossShadowOpacity);
        }
    }
    // Blend the composition with the overall projection
    blendWithBackdrop(composition, projection,
                      layer.blendingOptions().layerMaskHidesEffects && layer.rasterMaskIsActive()
                      ? layer.rasterMask()
                      : QImage(),
                      layer.blendingOptions().channels, currentLayerOpacity,
                      layer.blendingOptions().destinationTonalRanges);

    return projection;
}

}
