/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QHBoxLayout>
#include <QCheckBox>

#include "ColorBlendMethodComboBox.h"
#include "ColorButton.h"
#include "SliderSpinBox.h"
#include "CurveButton.h"

#include "InnerShadowOptionsWidget.h"

class Q_DECL_HIDDEN InnerShadowOptionsWidget::Private
{
public:
    ColorBlendMethodComboBox *comboBoxBlendMode {nullptr};
    ColorButton *buttonColor {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
    SliderSpinBox *sliderAngle {nullptr};
    SliderSpinBox *sliderDistance {nullptr};
    SliderSpinBox *sliderSpread {nullptr};
    SliderSpinBox *sliderSize {nullptr};
    CurveButton *buttonCurve {nullptr};
    QCheckBox *checkBoxAntiAlias {nullptr};
    SliderSpinBox *sliderNoise {nullptr};
};

InnerShadowOptionsWidget::InnerShadowOptionsWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    LayerStyles::InnerShadow defaultParams;

    m_d->comboBoxBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxBlendMode->setCurrentColorBlendMethod(defaultParams.blendMode);

    m_d->buttonColor = new ColorButton;
    m_d->buttonColor->setColor(defaultParams.color);

    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(defaultParams.opacity);

    m_d->sliderAngle = new SliderSpinBox;
    m_d->sliderAngle->setRange(0, 359);
    m_d->sliderAngle->setValue(defaultParams.angle);

    m_d->sliderDistance = new SliderSpinBox;
    m_d->sliderDistance->setRange(0, 1000);
    m_d->sliderDistance->setValue(defaultParams.distance);

    m_d->sliderSpread = new SliderSpinBox;
    m_d->sliderSpread->setRange(0, 100);
    m_d->sliderSpread->setValue(defaultParams.spread);

    m_d->sliderSize = new SliderSpinBox;
    m_d->sliderSize->setRange(0, 250);
    m_d->sliderSize->setValue(defaultParams.size);

    m_d->buttonCurve = new CurveButton;
    m_d->buttonCurve->setCurve(defaultParams.curve);

    m_d->checkBoxAntiAlias = new QCheckBox("Anti-alias");
    m_d->checkBoxAntiAlias->setChecked(defaultParams.antiAliasedCurve);

    QWidget *containerContour = new QWidget;
    QHBoxLayout *layoutContainerContour = new QHBoxLayout;
    layoutContainerContour->setContentsMargins(0, 0, 0, 0);
    layoutContainerContour->setSpacing(5);
    layoutContainerContour->addWidget(m_d->buttonCurve);
    layoutContainerContour->addWidget(m_d->checkBoxAntiAlias);
    containerContour->setLayout(layoutContainerContour);

    m_d->sliderNoise = new SliderSpinBox;
    m_d->sliderNoise->setRange(0, 100);
    m_d->sliderNoise->setValue(defaultParams.noise);

    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addRow("Blend mode:", m_d->comboBoxBlendMode);
    mainLayout->addRow("Color:", m_d->buttonColor);
    mainLayout->addRow("Opacity:", m_d->sliderOpacity);
    mainLayout->addRow("Angle:", m_d->sliderAngle);
    mainLayout->addRow("Distance:", m_d->sliderDistance);
    mainLayout->addRow("Spread:", m_d->sliderSpread);
    mainLayout->addRow("Size:", m_d->sliderSize);
    mainLayout->addRow("Contour:", containerContour);
    mainLayout->addRow("Noise:", m_d->sliderNoise);

    setLayout(mainLayout);

    connect(m_d->comboBoxBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->buttonColor, SIGNAL(colorChanged(const QColor&)), SIGNAL(paramsChanged()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderAngle, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderDistance, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderSpread, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderSize, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->buttonCurve, SIGNAL(curveChanged(const Curve&)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxAntiAlias, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->sliderNoise, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
}

InnerShadowOptionsWidget::~InnerShadowOptionsWidget()
{}

LayerStyles::InnerShadow InnerShadowOptionsWidget::params() const
{
    LayerStyles::InnerShadow params;

    params.blendMode = m_d->comboBoxBlendMode->currentColorBlendMethod();
    params.color = m_d->buttonColor->color();
    params.opacity = m_d->sliderOpacity->value();
    params.angle = m_d->sliderAngle->value();
    params.distance = m_d->sliderDistance->value();
    params.spread = m_d->sliderSpread->value();
    params.size = m_d->sliderSize->value();
    params.curve = m_d->buttonCurve->curve();
    params.antiAliasedCurve = m_d->checkBoxAntiAlias->isChecked();
    params.noise = m_d->sliderNoise->value();
    
    return params;
}
