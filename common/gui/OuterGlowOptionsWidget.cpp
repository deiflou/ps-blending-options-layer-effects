/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QHBoxLayout>
#include <QCheckBox>

#include "ColorBlendMethodComboBox.h"
#include "GlowTechniqueComboBox.h"
#include "GradientMethodComboBox.h"
#include "ColorButton.h"
#include "SliderSpinBox.h"
#include "CurveButton.h"

#include "OuterGlowOptionsWidget.h"

class Q_DECL_HIDDEN OuterGlowOptionsWidget::Private
{
public:
    ColorBlendMethodComboBox *comboBoxBlendMode {nullptr};
    ColorButton *buttonColor {nullptr};
    QGradientStops gradient;
    SliderSpinBox *sliderJitter {nullptr};
    QCheckBox *checkBoxUseGradient {nullptr};
    GradientMethodComboBox *comboBoxGradientMethod {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
    GlowTechniqueComboBox *comboBoxTechnique {nullptr};
    SliderSpinBox *sliderSpread {nullptr};
    SliderSpinBox *sliderSize {nullptr};
    CurveButton *buttonCurve {nullptr};
    QCheckBox *checkBoxAntiAlias {nullptr};
    SliderSpinBox *sliderRange {nullptr};
    SliderSpinBox *sliderNoise {nullptr};
};

OuterGlowOptionsWidget::OuterGlowOptionsWidget(QWidget *parent)
    : QWidget(parent)
    , m_d(new Private)
{
    LayerStyles::OuterGlow defaultParams;

    m_d->comboBoxBlendMode = new ColorBlendMethodComboBox;
    m_d->comboBoxBlendMode->setCurrentColorBlendMethod(defaultParams.blendMode);

    m_d->buttonColor = new ColorButton;
    m_d->buttonColor->setColor(defaultParams.color);

    m_d->gradient = defaultParams.gradient;

    m_d->checkBoxUseGradient = new QCheckBox;
    m_d->checkBoxUseGradient->setChecked(defaultParams.useGradient);

    m_d->comboBoxGradientMethod = new GradientMethodComboBox;
    m_d->comboBoxGradientMethod->setCurrentGradientMethod(defaultParams.gradientMethod);

    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(defaultParams.opacity);

    m_d->sliderNoise = new SliderSpinBox;
    m_d->sliderNoise->setRange(0, 100);
    m_d->sliderNoise->setValue(defaultParams.noise);

    m_d->comboBoxTechnique = new GlowTechniqueComboBox;
    m_d->comboBoxTechnique->setCurrentGlowTechnique(defaultParams.technique);

    m_d->sliderSpread = new SliderSpinBox;
    m_d->sliderSpread->setRange(0, 100);
    m_d->sliderSpread->setValue(defaultParams.spread);

    m_d->sliderSize = new SliderSpinBox;
    m_d->sliderSize->setRange(0, 250);
    m_d->sliderSize->setValue(defaultParams.size);

    m_d->buttonCurve = new CurveButton;
    m_d->buttonCurve->setCurve(defaultParams.curve);

    m_d->checkBoxAntiAlias = new QCheckBox("Anti-alias");
    m_d->checkBoxAntiAlias->setChecked(defaultParams.antiAliasedCurve);

    QWidget *containerContour = new QWidget;
    QHBoxLayout *layoutContainerContour = new QHBoxLayout;
    layoutContainerContour->setContentsMargins(0, 0, 0, 0);
    layoutContainerContour->setSpacing(5);
    layoutContainerContour->addWidget(m_d->buttonCurve);
    layoutContainerContour->addWidget(m_d->checkBoxAntiAlias);
    containerContour->setLayout(layoutContainerContour);

    m_d->sliderRange = new SliderSpinBox;
    m_d->sliderRange->setRange(1, 100);
    m_d->sliderRange->setValue(defaultParams.range);

    m_d->sliderJitter = new SliderSpinBox;
    m_d->sliderJitter->setRange(0, 100);
    m_d->sliderJitter->setValue(defaultParams.gradientJitter);

    QFormLayout *mainLayout = new QFormLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(5);
    mainLayout->addRow("Blend mode:", m_d->comboBoxBlendMode);
    mainLayout->addRow("Color:", m_d->buttonColor);
    mainLayout->addRow("Use gradient:", m_d->checkBoxUseGradient);
    mainLayout->addRow("Method:", m_d->comboBoxGradientMethod);
    mainLayout->addRow("Opacity:", m_d->sliderOpacity);
    mainLayout->addRow("Noise:", m_d->sliderNoise);
    mainLayout->addRow("Technique:", m_d->comboBoxTechnique);
    mainLayout->addRow("Spread:", m_d->sliderSpread);
    mainLayout->addRow("Size:", m_d->sliderSize);
    mainLayout->addRow("Contour:", containerContour);
    mainLayout->addRow("Range:", m_d->sliderRange);
    mainLayout->addRow("Jitter:", m_d->sliderJitter);

    setLayout(mainLayout);

    connect(m_d->comboBoxBlendMode, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)), SIGNAL(paramsChanged()));
    connect(m_d->buttonColor, SIGNAL(colorChanged(const QColor&)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxUseGradient, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxGradientMethod, SIGNAL(currentGradientMethodChanged(LayerStyles::GradientMethod)), SIGNAL(paramsChanged()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderNoise, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->comboBoxTechnique, SIGNAL(currentGlowTechniqueChanged(LayerStyles::GlowTechnique)), SIGNAL(paramsChanged()));
    connect(m_d->sliderSpread, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderSize, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->buttonCurve, SIGNAL(curveChanged(const Curve&)), SIGNAL(paramsChanged()));
    connect(m_d->checkBoxAntiAlias, SIGNAL(toggled(bool)), SIGNAL(paramsChanged()));
    connect(m_d->sliderRange, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
    connect(m_d->sliderJitter, SIGNAL(valueChanged(int)), SIGNAL(paramsChanged()));
}

OuterGlowOptionsWidget::~OuterGlowOptionsWidget()
{}

LayerStyles::OuterGlow OuterGlowOptionsWidget::params() const
{
    LayerStyles::OuterGlow params;

    params.blendMode = m_d->comboBoxBlendMode->currentColorBlendMethod();
    params.color = m_d->buttonColor->color();
    params.gradient = m_d->gradient;
    params.useGradient = m_d->checkBoxUseGradient->isChecked();
    params.gradientMethod = m_d->comboBoxGradientMethod->currentGradientMethod();
    params.opacity = m_d->sliderOpacity->value();
    params.noise = m_d->sliderNoise->value();
    params.technique = m_d->comboBoxTechnique->currentGlowTechnique();
    params.spread = m_d->sliderSpread->value();
    params.size = m_d->sliderSize->value();
    params.curve = m_d->buttonCurve->curve();
    params.antiAliasedCurve = m_d->checkBoxAntiAlias->isChecked();
    params.range = m_d->sliderRange->value();
    params.gradientJitter = m_d->sliderJitter->value();
    
    return params;
}

void OuterGlowOptionsWidget::setGradient(const QGradientStops &gradient)
{
    m_d->gradient = gradient;
    emit paramsChanged();
}
