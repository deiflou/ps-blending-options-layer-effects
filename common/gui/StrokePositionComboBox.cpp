/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "StrokePositionComboBox.h"

StrokePositionComboBox::StrokePositionComboBox(QWidget *parent)
    : QComboBox(parent)
{
    addItem("Outside", static_cast<int>(LayerStyles::StrokePosition_Outside));
    addItem("Inside", static_cast<int>(LayerStyles::StrokePosition_Inside));
    addItem("Center", static_cast<int>(LayerStyles::StrokePosition_Center));

    connect(
        this,
        QOverload<int>::of(&QComboBox::currentIndexChanged),
        [this](int)
        {
            emit currentStrokePositionChanged(static_cast<LayerStyles::StrokePosition>(currentData().toInt()));
        }
    );
}
StrokePositionComboBox::~StrokePositionComboBox()
{}

LayerStyles::StrokePosition StrokePositionComboBox::currentStrokePosition() const
{
    return static_cast<LayerStyles::StrokePosition>(currentData().toInt());
}

void StrokePositionComboBox::setCurrentStrokePosition(LayerStyles::StrokePosition newStrokePosition)
{
    setCurrentIndex(findData(static_cast<int>(newStrokePosition)));
}
