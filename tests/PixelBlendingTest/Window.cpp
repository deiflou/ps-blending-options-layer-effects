/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <QFormLayout>
#include <QVBoxLayout>
#include <QComboBox>
#include <QCheckBox>

#include <SliderSpinBox.h>
#include <Viewport.h>
#include <AlphaBlendMethodComboBox.h>
#include <ColorBlendMethodComboBox.h>
#include <ColorButton.h>
#include <PixelBlending.h>
#include <ImgProcUtil.h>
#include <ImgProcCommon.h>

#include "Window.h"

class Q_DECL_HIDDEN Window::Private
{
public:
    AlphaBlendMethodComboBox *comboBoxAlphaBlendMethod {nullptr};
    ColorBlendMethodComboBox *comboBoxColorBlendMethod {nullptr};
    QCheckBox *checkBoxUseSpecialBlendModes {nullptr};
    SliderSpinBox *sliderOpacity {nullptr};
    ColorButton *colorButton {nullptr};
    QComboBox *comboBoxTestNumber {nullptr};
    QImage test1_bg2;
    QImage test1_bg3;
    QImage test2_bgImage;
    QImage test2_topImage;
    QImage test2_bottomImage;
    QImage test3_grad01;
    QImage test3_grad02;
    QImage test3_grad03;

    QImage generateImage() const
    {
        const PixelBlending::AlphaBlendMethod alphaBlendMethod = comboBoxAlphaBlendMethod->currentAlphaBlendMethod();
        const PixelBlending::ColorBlendMethod colorBlendMethod = comboBoxColorBlendMethod->currentColorBlendMethod();
        const quint8 opacity = static_cast<quint8>(sliderOpacity->value() * 255 / 100);

        if (comboBoxTestNumber->currentIndex() == 0) {
            QImage out(test1_bg2);
            PixelBlending::blend(test1_bg3, out, alphaBlendMethod, colorBlendMethod,
                                 checkBoxUseSpecialBlendModes->isChecked(), opacity);
            return out;
        
        } else if (comboBoxTestNumber->currentIndex() == 1) {
            QImage out(test2_bgImage);
            PixelBlending::blend(test2_topImage, out, alphaBlendMethod, colorBlendMethod, false, opacity);
            PixelBlending::blend(test2_bottomImage, out, alphaBlendMethod, colorBlendMethod, true, opacity);
            return out;

        } else if (comboBoxTestNumber->currentIndex() == 2) {
            QImage out(test3_grad01);
            PixelBlending::blend(test3_grad02, out, alphaBlendMethod, colorBlendMethod,
                                 checkBoxUseSpecialBlendModes->isChecked(), opacity);
            return out;

        } else if (comboBoxTestNumber->currentIndex() == 3) {
            QImage out(test3_grad01);
            QImage in = test3_grad03;
            ImgProcCommon::ARGB color {
                static_cast<quint8>(colorButton->color().red()),
                static_cast<quint8>(colorButton->color().green()),
                static_cast<quint8>(colorButton->color().blue()),
                0xFF,
            };
            ImgProcUtil::visitPixels(
                in,
                [color](quint8 *pixel)
                {
                    ImgProcCommon::ARGB *gradColor = reinterpret_cast<ImgProcCommon::ARGB*>(pixel);
                    *gradColor = ImgProcCommon::ARGB{color.b, color.g, color.r, gradColor->a};
                }
            );
            PixelBlending::blend(in, out, alphaBlendMethod, colorBlendMethod,
                                 checkBoxUseSpecialBlendModes->isChecked(), opacity);
            return out;

        } else if (comboBoxTestNumber->currentIndex() == 4) {
            QImage out(test3_grad01);
            QImage in(test3_grad01.size(), QImage::Format_ARGB32);
            ImgProcCommon::ARGB color {
                static_cast<quint8>(colorButton->color().red()),
                static_cast<quint8>(colorButton->color().green()),
                static_cast<quint8>(colorButton->color().blue()),
                0xFF,
            };
            ImgProcUtil::visitPixels(
                in,
                [color](quint8 *pixel)
                {
                    ImgProcCommon::ARGB *gradColor = reinterpret_cast<ImgProcCommon::ARGB*>(pixel);
                    *gradColor = ImgProcCommon::ARGB{color.b, color.g, color.r, 0xFF};
                }
            );
            PixelBlending::blend(in, out, alphaBlendMethod, colorBlendMethod,
                                 checkBoxUseSpecialBlendModes->isChecked(), opacity);
            return out;
            
        }
        return QImage();
    }
};

Window::Window()
    : m_d(new Private)
{
    m_d->test1_bg2 = QImage(":/bg_2").convertToFormat(QImage::Format_ARGB32);
    m_d->test1_bg3 = QImage(":/bg_3").convertToFormat(QImage::Format_ARGB32);
    m_d->test2_bgImage = QImage(":/ls_bm_bg").convertToFormat(QImage::Format_ARGB32);
    m_d->test2_topImage = QImage(":/ls_bm_top").convertToFormat(QImage::Format_ARGB32);
    m_d->test2_bottomImage = QImage(":/ls_bm_bottom").convertToFormat(QImage::Format_ARGB32);
    m_d->test3_grad01 = QImage(":/grad01").convertToFormat(QImage::Format_ARGB32);
    m_d->test3_grad02 = QImage(":/grad02").convertToFormat(QImage::Format_ARGB32);
    m_d->test3_grad03 = QImage(":/grad03").convertToFormat(QImage::Format_ARGB32);

    m_d->comboBoxAlphaBlendMethod = new AlphaBlendMethodComboBox;
    m_d->comboBoxColorBlendMethod = new ColorBlendMethodComboBox;
    m_d->checkBoxUseSpecialBlendModes = new QCheckBox;
    m_d->sliderOpacity = new SliderSpinBox;
    m_d->sliderOpacity->setRange(0, 100);
    m_d->sliderOpacity->setValue(100);
    m_d->colorButton = new ColorButton;
    m_d->comboBoxTestNumber = new QComboBox;
    m_d->comboBoxTestNumber->addItems({"Test 1", "Test 2", "Test 3", "Test 4", "Test 5"});

    QWidget *containerBlendingOptions = new QWidget;

    QFormLayout *layoutBlendingOptions1 = new QFormLayout;
    layoutBlendingOptions1->setContentsMargins(10, 10, 10, 10);
    layoutBlendingOptions1->setSpacing(5);
    layoutBlendingOptions1->addRow("Alpha operator:", m_d->comboBoxAlphaBlendMethod);
    layoutBlendingOptions1->addRow("Color operator:", m_d->comboBoxColorBlendMethod);
    layoutBlendingOptions1->addRow("Use special blend modes:", m_d->checkBoxUseSpecialBlendModes);
    layoutBlendingOptions1->addRow("Opacity:", m_d->sliderOpacity);
    layoutBlendingOptions1->addRow("Color:", m_d->colorButton);
    layoutBlendingOptions1->addRow("Active test:", m_d->comboBoxTestNumber);

    QVBoxLayout *layoutBlendingOptions2 = new QVBoxLayout;
    layoutBlendingOptions2->setContentsMargins(0, 0, 0, 0);
    layoutBlendingOptions2->setSpacing(0);
    layoutBlendingOptions2->addLayout(layoutBlendingOptions1);
    layoutBlendingOptions2->addStretch();
    containerBlendingOptions->setLayout(layoutBlendingOptions2);

    addControlsPanel(containerBlendingOptions, "Options");

    viewport()->setImage(m_d->generateImage());

    connect(m_d->comboBoxAlphaBlendMethod, SIGNAL(currentAlphaBlendMethodChanged(PixelBlending::AlphaBlendMethod)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->comboBoxColorBlendMethod, SIGNAL(currentColorBlendMethodChanged(PixelBlending::ColorBlendMethod)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->checkBoxUseSpecialBlendModes, SIGNAL(toggled(bool)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->sliderOpacity, SIGNAL(valueChanged(int)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->colorButton, SIGNAL(colorChanged(const QColor&)),
            this, SLOT(requestViewportUpdate()));
    connect(m_d->comboBoxTestNumber, SIGNAL(currentIndexChanged(int)),
            this, SLOT(requestViewportUpdate()));
    connect(
        this, &GenericWindow::viewportReadyForUpdate, 
        [this]()
        {
            viewport()->setImage(m_d->generateImage());
        }
    );
}

Window::~Window()
{}
