/*
 * SPDX-FileCopyrightText: 2022 Deif Lou <ginoba@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef BLENDINGOPTIONSWIDGET_H
#define BLENDINGOPTIONSWIDGET_H

#include <QWidget>
#include <QScopedPointer>

#include <LayerStyles.h>

#include <Export.h>

class COMMON_EXPORT BlendingOptionsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit BlendingOptionsWidget(QWidget *parent = nullptr);
    ~BlendingOptionsWidget() override;

    LayerStyles::BlendingOptions params() const;

Q_SIGNALS:
    void paramsChanged();

private:
    class Private;
    QScopedPointer<Private> m_d;
};

#endif
